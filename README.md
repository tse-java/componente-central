## Requerimientos

- Wildfly 19
- Mongodb (db "viruscontrol")
- Postgresql
- Redis

##### JMS queues:

- Persistir Chat:
  - java:jboss/exported/jms/queue/queue_new_chat_message
  - queue/queue_new_chat_message

---

- Persistir Reporte de ubicacion:
  - java:jboss/exported/jms/queue/queue_report_location
  - queue/queue_report_location
- Reporte de contagios:
  - java:jboss/exported/jms/queue/queue_report_contagion
  - queue/queue_report_contagion
- Nueva consulta medica:
  - java:jboss/exported/jms/queue/queue_new_medical_appointment
  - queue/queue_new_medical_appointment

---

- Notificar contagios:

  - java:jboss/exported/jms/queue/queue_notify_contagion
  - queue/queue_notify_contagion

- Notificar examenes:

  - java:jboss/exported/jms/queue/queue_notify_exam_status
  - queue/queue_notify_exam_status

- Notificar stock de recurso:

  - java:jboss/exported/jms/queue/queue_notify_resource_availability
  - queue/queue_notify_resource_availability

- Notificar asignacion de medico:

  - java:jboss/exported/jms/queue/queue_notify_medic_assignment
  - queue/queue_notify_medic_assignment

---

- Asignar Lab:

  - java:jboss/exported/jms/queue/queue_assign_lab_provider
  - queue/queue_assign_lab_provider

---

##### Configuracion de propiedades de Wilfdly

Agregar en standalone-full.xml luego de `</extensions>`

```
<system-properties>

    <!-- API url de desarrollo local-->
    <property name="api-url" value="http://localhost:8080/api"/>

    <!-- API url de server dev-->
    <!-- <property name="api-url" value="https://api.viruscontrol.fail"/> -->

    <!-- API url de server prod-->
    <!-- <property name="api-url" value="https://api.viruscontrol.tech"/> -->

</system-properties>
```
