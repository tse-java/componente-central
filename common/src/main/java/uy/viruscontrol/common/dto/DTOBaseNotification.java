package uy.viruscontrol.common.dto;

import lombok.Data;

@Data
public abstract class DTOBaseNotification {
  private String externalId;
  private Boolean read;
}
