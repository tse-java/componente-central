package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Data
public class DTOResourceAvailability implements Serializable {
  @Schema
  @JsonProperty("resource")
  private DTOResource resource;

  @Schema
  @JsonProperty("provider")
  private DTOResourceProvider provider;

  @Schema
  @JsonProperty("quantity")
  private Long quantity;
}
