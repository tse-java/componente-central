package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Data
public class DTOUpdateFrontProfile implements Serializable {
  @Schema(description = "id del usuario")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "cedula de identidad del usuario")
  @JsonProperty("documentNumber")
  private String documentNumber;

  @Schema(description = "numero de telefono")
  @JsonProperty("phoneNumber")
  private String phoneNumber;

  @Schema(description = "determina si el usuario tiene perfil de medico")
  @JsonProperty("isDoctor")
  private Boolean isDoctor;

  @Schema
  @JsonProperty("address")
  private DTOLocation address;
}
