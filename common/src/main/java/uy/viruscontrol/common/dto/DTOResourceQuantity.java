package uy.viruscontrol.common.dto;

import lombok.Data;

@Data
public class DTOResourceQuantity {
  private Long resourceId;

  private Long quantity;
}
