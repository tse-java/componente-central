package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumInformationSourceStatus;

@Data
public abstract class DTOInformationSource implements Serializable {
  private Long id;

  private String name;

  private String description;

  private EnumInformationSourceStatus status;

  private DTOLastRequestStatus lastRequestStatus;
}
