package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Data
@Schema(description = "sala de chat")
public class DTOChat implements Serializable {
  @Schema(description = "identificador de la sala")
  @JsonProperty("id")
  private String id;

  @Schema(description = "nombre de la sala")
  @JsonProperty("name")
  private String name;
}
