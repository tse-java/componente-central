package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import twitter4j.MediaEntity;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "noticia de Twitter")
@Data
public class DTOTwitterNews extends DTONews {
  @Schema(description = "nombre")
  @JsonProperty("userName")
  private String userName;

  @Schema(description = "nickName")
  @JsonProperty("nickname")
  private String nickName;

  @Schema(description = "text")
  @JsonProperty("text")
  private String text;

  @Schema(description = "link")
  @JsonProperty("link")
  private String link;

  @Schema(description = "profilePhoto")
  @JsonProperty("profilePhoto")
  private String profilePhoto;

  @Schema(description = "favoriteCount")
  @JsonProperty("favoriteCount")
  private int favoriteCount;

  @Schema(description = "retweetCount")
  @JsonProperty("retweetCount")
  private int retweetCount;

  @Schema(description = "imagenes y videos")
  @JsonProperty("mediaEntities")
  private List<MediaEntity> mediaEntities;
}
