package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@Data
public class DTOMedicalCase implements Serializable {
  private Long id;
  private EnumCaseStatus status;
  private UruguayanDepartment department;
  private DTODisease disease;
  private DTOFrontProfile patient;
}
