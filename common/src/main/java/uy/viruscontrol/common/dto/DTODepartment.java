package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class DTODepartment implements Serializable {
  @Schema(description = "identificador de departamento")
  @JsonProperty("id")
  private Integer id;

  @Schema(description = "nombre de departamento")
  @JsonProperty("name")
  private String name;

  @Schema(description = "casos del departamento")
  @JsonProperty("cases")
  private List<DTOCasesQuantity> cases = new ArrayList<>();
}
