package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@Data
public class DTOLocation implements Serializable {
  @Schema(description = "coordenadas de la unicacion")
  @JsonProperty("coordinates")
  private String coordinates;

  @Schema(description = "Direccion")
  @JsonProperty("street")
  private String street;

  @Schema(description = "barrio")
  @JsonProperty("town")
  private String town;

  @Schema(description = "ciudad")
  @JsonProperty("city")
  private String city;

  @Schema
  @JsonProperty("department")
  private UruguayanDepartment department;
}
