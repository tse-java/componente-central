package uy.viruscontrol.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Schema(description = "refresh token")
@Data
public class DTOToken implements Serializable {
  @NotNull
  @NotEmpty
  private String token;
}
