package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class DTOFrontProfile implements Serializable {
  @Schema(description = "id del usuario")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "identificador de usuario en la plataforma externa")
  @JsonProperty("externalId")
  private String externalId;

  @Schema(description = "correo del usuario")
  private String email;

  @Schema(description = "numero de documento de identidad")
  @JsonProperty("documentNumber")
  private String documentNumber;

  @Schema(description = "nombre del usuario")
  @JsonProperty("firstname")
  private String firstname;

  @Schema(description = "apellido del usuario")
  @JsonProperty("lastname")
  private String lastname;

  @Schema(description = "fecha de nacimiento")
  @JsonProperty("birthday")
  private Date birthday;

  @Schema(description = "nacionalidad")
  @JsonProperty("nationality")
  private String nationality;

  @Schema(description = "numero de telefono")
  @JsonProperty("phoneNumber")
  private String phoneNumber;

  @Schema(description = "determina si el usuario tiene perfil de medico")
  @JsonProperty("isDoctor")
  private Boolean isDoctor;

  @Schema(description = "determina si el usuario confirmo los datos de su perfil")
  @JsonProperty("isFinished")
  private Boolean isFinished;

  @Schema
  @JsonProperty("address")
  private DTOLocation address;

  private boolean pushNotify = true;
  private boolean mailNotify = true;
}
