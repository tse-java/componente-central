package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class DTOLastRequestStatus implements Serializable {
  public static final int STATUS_OK = 1;
  public static final int STATUS_ERROR = 2;

  private Date date;

  private int status;

  private String description;
}
