package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Schema(description = "location")
@Data
public class DTOCoordinates implements Serializable {
  @NotNull
  @NotEmpty
  @Schema
  @JsonProperty("y")
  private String y;

  @NotNull
  @NotEmpty
  @Schema
  @JsonProperty("x")
  private String x;

  @NotNull
  @NotEmpty
  @Schema
  @JsonProperty("date")
  private Date date;
}
