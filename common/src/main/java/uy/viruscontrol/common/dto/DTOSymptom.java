package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Schema(description = "sintoma")
@Data
public class DTOSymptom implements Serializable {
  @Schema(description = "identificador")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "nombre")
  @JsonProperty("name")
  private String name;

  @Schema(description = "descripcion")
  @JsonProperty("description")
  private String description;
}
