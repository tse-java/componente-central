package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;

@Data
public class DTONewBackProfile implements Serializable {
  private Long id;

  private String nickname;

  private String fullName;

  private EnumBackofficeUserRole role;

  private String password;
}
