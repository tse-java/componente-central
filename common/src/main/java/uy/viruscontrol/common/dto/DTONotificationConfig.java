package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTONotificationConfig implements Serializable {
  private boolean positiveMedic = false;
  private boolean negativeMedic = false;
  private boolean recoveredMedic = false;

  private boolean positiveSupervisor = false;
  private boolean negativeSupervisor = false;
  private boolean suspiciousSupervisor = false;
  private boolean recoveredSupervisor = false;

  private boolean positivePatient = false;
  private boolean negativePatient = false;
  private boolean suspiciousPatient = false;
  private boolean recoveredPatient = false;
}
