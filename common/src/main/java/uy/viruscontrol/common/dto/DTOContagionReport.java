package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
public class DTOContagionReport implements Serializable {
  private List<DTOLocationReport> locationReport;
  private DTODisease disease;
}
