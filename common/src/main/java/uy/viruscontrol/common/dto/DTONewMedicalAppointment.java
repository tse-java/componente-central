package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Schema(description = "nueva consulta medica, utilizado para su creacion")
@Data
public class DTONewMedicalAppointment implements Serializable {
  @Schema(description = "listado de sintomas")
  @JsonProperty("symptomIds")
  private List<Long> symptomIds;

  @Schema(description = "ubicacion de la consulta")
  @JsonProperty("location")
  private DTOLocation location;
}
