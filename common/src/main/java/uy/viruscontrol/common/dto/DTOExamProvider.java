package uy.viruscontrol.common.dto;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DTOExamProvider implements Serializable {
  @NotNull
  private Long id;

  private String name;
  private String url;
  private String token;

  private Boolean isActive;
  private String message;
  private Boolean ok;
}
