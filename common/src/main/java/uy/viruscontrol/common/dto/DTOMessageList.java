package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import lombok.Data;

@Schema(description = "listado de mensajes de chat")
@Data
public class DTOMessageList implements Serializable {
  @Schema(description = "cantidad restante de mensajes")
  @JsonProperty("remainingMessageQuantity")
  private Long remainingMessageQuantity;

  @Schema(description = "mensajes")
  @JsonProperty("items")
  private java.util.List<DTOMessage> items = new ArrayList<>();
}
