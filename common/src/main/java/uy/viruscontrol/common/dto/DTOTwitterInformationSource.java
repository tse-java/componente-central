package uy.viruscontrol.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTOTwitterInformationSource extends DTOInformationSource {
  private String username;
}
