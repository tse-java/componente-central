package uy.viruscontrol.common.dto;

import lombok.Data;

@Data
public class DTODistance {
  private Double distance;
}
