package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class DTOLocationReport implements Serializable {
  private Date date;
  private Double latitude;
  private Double longitude;
  private String department;
  private Long userId;
}
