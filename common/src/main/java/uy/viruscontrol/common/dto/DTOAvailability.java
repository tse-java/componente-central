package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class DTOAvailability implements Serializable {
  private DTOResourceProvider resourceProvider;
  private BigDecimal quantity;
}
