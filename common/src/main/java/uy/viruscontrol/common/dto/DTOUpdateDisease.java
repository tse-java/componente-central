package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumInfectiousAgentType;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Data
public class DTOUpdateDisease implements Serializable {
  private String name;

  private String agentName;

  private EnumInfectiousAgentType agentType;

  private EnumRequestStatus requestStatus;

  private Long infectionTime;

  private List<Long> recommendedResources;

  private List<Long> symptoms;
}
