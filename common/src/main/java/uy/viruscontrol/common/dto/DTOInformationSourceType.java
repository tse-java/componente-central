package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTOInformationSourceType implements Serializable {
  private Long id;

  private String name;

  private String description;

  private Boolean active;
}
