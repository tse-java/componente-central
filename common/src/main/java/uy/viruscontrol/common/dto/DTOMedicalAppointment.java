package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Schema(description = "consulta medica")
@Data
public class DTOMedicalAppointment implements Serializable {
  @Schema(description = "identificador")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "identificador externo")
  @JsonProperty("externaId")
  private Long externaId;

  @Schema
  @JsonProperty("location")
  private DTOLocation location;

  @Schema
  @JsonProperty("status")
  private EnumMedicalAppointmentStatus status;

  @Schema(description = "fecha y hora de la consulta")
  @JsonProperty("dateTime")
  private String dateTime;

  @Schema
  @JsonProperty("requestStatus")
  private EnumRequestStatus requestStatus;

  @Schema(description = "nombre de proveedor de salud")
  @JsonProperty("provider")
  private String provider;

  @Schema(description = "sintomas")
  @JsonProperty("symptons")
  private java.util.List<DTOSymptom> symptons = new ArrayList<>();

  @Schema
  @JsonProperty("doctor")
  private DTOFrontProfile doctor;

  @Schema
  @JsonProperty("patient")
  private DTOFrontProfile patient;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DTOMedicalAppointment that = (DTOMedicalAppointment) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
