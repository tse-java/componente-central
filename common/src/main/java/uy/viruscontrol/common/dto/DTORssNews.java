package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "noticia de RSS Feed")
@Data
public class DTORssNews extends DTONews {
  @Schema(description = "titulo")
  @JsonProperty("title")
  private String title;

  @Schema(description = "cuerpo de la noticia")
  @JsonProperty("description")
  private String description;

  @Schema(description = "enlace de acceso a la noticia")
  @JsonProperty("link")
  private String link;

  @Schema(description = "fuente de informacion")
  @JsonProperty("source")
  private String source;
}
