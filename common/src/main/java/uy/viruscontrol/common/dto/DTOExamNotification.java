package uy.viruscontrol.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTOExamNotification extends DTOBaseNotification {
  private String status;
  private String diseaseName;
  private Boolean isPositive;
}
