package uy.viruscontrol.common.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTORSSInformationSource extends DTOInformationSource {
  private String url;

  private List<String> keyWords = new ArrayList<>();
}
