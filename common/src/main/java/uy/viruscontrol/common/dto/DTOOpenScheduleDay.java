package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.Day;

@Data
public class DTOOpenScheduleDay implements Serializable {
  @Schema
  @JsonProperty("dayOfWeek")
  private Day dayOfWeek;

  @Schema(description = "hora de apertura")
  @JsonProperty("openingHour")
  private String openingHour;

  @Schema(description = "hora de cierre")
  @JsonProperty("closingHour")
  private String closingHour;
}
