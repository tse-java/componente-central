package uy.viruscontrol.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumResourceType;

@Schema(description = "update resource")
@Data
public class DTOUpdateResource implements Serializable {
  @NotNull
  @NotEmpty
  private String name;

  private String description;

  private EnumResourceType resourceType;
}
