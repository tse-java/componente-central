package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Data
public class DTOLogin implements Serializable {
  @Schema(description = "token interno de virusControl")
  @JsonProperty("accessToken")
  private String accessToken;

  @Schema(description = "token de larga duracion")
  @JsonProperty("refreshToken")
  private String refreshToken;

  @Schema(description = "indica si el usuario ya tiene el perfil termiando")
  @JsonProperty("finishedUserProfile")
  private Boolean finishedUserProfile;
}
