package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumInfectiousAgentType;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Data
public class DTODisease implements Serializable {
  private Long id;

  private String name;

  private String agentName;

  private EnumInfectiousAgentType agentType;

  private EnumRequestStatus requestStatus;

  private Long infectionTime;

  private List<DTOResource> recommendedResources;

  private List<DTOSymptom> symptoms;
}
