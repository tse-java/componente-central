package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Schema(description = "noticia")
@Data
public abstract class DTONews implements Serializable {
  @Schema(description = "fecha")
  @JsonProperty("date")
  private Date date;

  @Schema(description = "tipo de fuente")
  @JsonProperty("type")
  private String type;
}
