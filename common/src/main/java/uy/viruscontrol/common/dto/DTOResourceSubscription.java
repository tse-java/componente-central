package uy.viruscontrol.common.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
public class DTOResourceSubscription implements Serializable {
  private boolean subscribed;
  private DTOResource dtoResource;
  private List<DTOAvailability> availability;
}
