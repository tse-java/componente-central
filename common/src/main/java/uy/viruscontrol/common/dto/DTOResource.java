package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumResourceType;

@Data
public class DTOResource implements Serializable {
  @Schema(description = "identificador del recurso")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "nombre del recurso")
  @JsonProperty("name")
  private String name;

  private String description;

  @Schema
  @JsonProperty("resourceType")
  private EnumResourceType resourceType;
}
