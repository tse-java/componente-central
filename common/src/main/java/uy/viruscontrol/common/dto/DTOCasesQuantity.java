package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

@Schema(name = "DTOCasesQuantity", description = "todo")
@Data
public class DTOCasesQuantity implements Serializable {
  @JsonProperty("type")
  private EnumCaseStatus type;

  @Schema(description = "Cantidad de casos")
  @JsonProperty("quantity")
  private Long quantity;
}
