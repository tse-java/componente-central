package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

@Schema(description = "update exam")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DTOUpdateExam implements Serializable {
  private long id;
  private boolean positive;

  public boolean getPositive() {
    return positive;
  }

  public void setPositive(boolean positive) {
    this.positive = positive;
  }
}
