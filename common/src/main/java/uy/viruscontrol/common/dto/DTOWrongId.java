package uy.viruscontrol.common.dto;

import lombok.Data;

@Data
public class DTOWrongId {
  private Long wrongId;
}
