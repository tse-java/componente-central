package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTOBasicResource implements Serializable {
  private Long id;

  private String name;
}
