package uy.viruscontrol.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTOContagionNotification extends DTOBaseNotification {
  private String diseaseName;
}
