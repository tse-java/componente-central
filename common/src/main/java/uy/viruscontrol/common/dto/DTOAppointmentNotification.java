package uy.viruscontrol.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTOAppointmentNotification extends DTOBaseNotification {
  private String doctorName;
  private String date;
}
