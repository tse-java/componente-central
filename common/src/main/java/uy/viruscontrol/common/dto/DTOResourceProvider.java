package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import lombok.Data;

@Data
public class DTOResourceProvider implements Serializable {
  /**
   * identificador de provedor de recursos
   **/

  @Schema(description = "identificador de provedor de recursos")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "nombre del proveedor")
  @JsonProperty("name")
  private String name;

  @Schema
  @JsonProperty("address")
  private DTOLocation address;

  @Schema(description = "horario del local")
  @JsonProperty("openSchedule")
  private java.util.List<DTOOpenScheduleDay> openSchedule = new ArrayList<>();

  @Schema(description = "nombre de local")
  @JsonProperty("commercialName")
  private String commercialName;

  @Schema(description = "url de acceso al nodo")
  @JsonProperty("url")
  private String url;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DTOResourceProvider that = (DTOResourceProvider) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
