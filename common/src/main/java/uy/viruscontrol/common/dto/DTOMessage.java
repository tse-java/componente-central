package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Schema(description = "mensaje de chat")
@Data
public class DTOMessage implements Serializable {
  @Schema(description = "identificador del mensaje")
  @JsonProperty("id")
  private String id;

  @Schema(description = "mensaje de chat")
  @JsonProperty("text")
  private String text;

  @Schema(description = "idetificador del emisor del mensaje")
  @JsonProperty("userId")
  private String userId;

  @Schema(description = "nombre completo del emisor del mensaje")
  @JsonProperty("userName")
  private String userName;

  @Schema(description = "fecha y hora del mensaje")
  @JsonProperty("creationDate")
  private Date creationDate;

  @Schema(description = "identificador de la sala")
  @JsonProperty("chatId")
  private String chatId;
}
