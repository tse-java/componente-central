package uy.viruscontrol.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumExamStatus;

@Data
public class DTOExam implements Serializable {
  @Schema(description = "identificador de examen")
  @JsonProperty("id")
  private Long id;

  @Schema(description = "fecha de solicitud")
  @JsonProperty("date")
  private Date date;

  @Schema
  @JsonProperty("status")
  private EnumExamStatus status;

  @Schema(description = "determina si el resultado fue positivo o negativo")
  @JsonProperty("positve")
  private Boolean positive;

  @Schema(description = "nombre del medico que lo solicitó")
  @JsonProperty("requesterDoctorName")
  private String requesterDoctorName;

  private DTOMedicalCase medicalCase;

  @Schema(description = "datos del proveedor de examenes")
  @JsonProperty("examProvider")
  private DTOExamProvider examProvider;

  @Schema(description = "datos de enfermedad asociada")
  @JsonProperty("disease")
  private DTODisease disease;

  public Boolean getPositive() {
    return positive;
  }

  public void setPositive(Boolean positive) {
    this.positive = positive;
  }
}
