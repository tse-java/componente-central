package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DTOResourceNotification extends DTOBaseNotification implements Serializable {
  private DTOResource resource;
  private DTOFrontProfile frontProfile;
}
