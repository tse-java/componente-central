package uy.viruscontrol.common.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTONotifyStyle implements Serializable {
  private boolean pushNotify;
  private boolean mailNotify;
}
