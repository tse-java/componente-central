package uy.viruscontrol.common.exceptions;

import java.io.Serializable;

public class VirusControlException extends Exception implements Serializable {
  public static final int INVALID_TOKEN = 1;
  public static final int USER_ALREADY_EXIST = 2;
  public static final int USER_NOT_FOUND = 3;
  public static final int INVALID_PASSWORD = 4;
  public static final int FIELD_CANT_BE_UPDATED = 5;
  public static final int USER_IS_NOT_DOCTOR = 6;
  public static final int SYMPTOM_NOT_FOUND = 7;
  public static final int UNCOMPLETED_FIELDS = 8;
  public static final int DISEASE_NOT_FOUND = 9;
  public static final int EXAM_NOT_FOUND = 10;
  public static final int RESOURCE_NOT_FOUND = 11;
  public static final int SUBSCRIPTION_ALREADY_EXISTS = 12;
  public static final int ENTITY_NOT_FOUND = 13;
  public static final int CANT_CONNECT = 14;
  public static final int SYNC_ERROR = 15;
  public static final int INACTIVE_TYPE = 16;
  public static final int ENTITY_ALREDY_EXISTS = 17;
  public static final int HAS_EXAMS = 18;
  public static final int INVALID_USER = 19;
  public static final int ALREADY_DONE = 20;

  private final Integer code;

  public VirusControlException(int code) {
    this.code = code;
  }

  public VirusControlException(String s) {
    super(s);
    code = null;
  }

  public VirusControlException(String s, int code) {
    super(s);
    this.code = code;
  }

  public VirusControlException(String s, Throwable throwable, int code) {
    super(s, throwable);
    this.code = code;
  }

  public VirusControlException(Throwable throwable, int code) {
    super(throwable);
    this.code = code;
  }

  public VirusControlException(String s, Throwable throwable, boolean b, boolean b1, int code) {
    super(s, throwable, b, b1);
    this.code = code;
  }

  public Integer getCode() {
    return code;
  }
}
