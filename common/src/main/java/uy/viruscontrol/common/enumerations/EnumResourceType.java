package uy.viruscontrol.common.enumerations;

public enum EnumResourceType {
  PREVENTION("Prevención"),
  TREATMENT("Tratamiento");

  private final String label;

  EnumResourceType(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
