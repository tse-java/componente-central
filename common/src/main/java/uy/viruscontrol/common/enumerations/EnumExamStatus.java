package uy.viruscontrol.common.enumerations;

public enum EnumExamStatus {
  REQUESTED(1), // Solicitado por el medico, aun no solicitado al laboratorio.
  PENDING(2), // Solicitado al laboratorio, aun no se tiene resultado.
  DELIVERED(3); // Con resultado final.

  private final int code;

  EnumExamStatus(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
