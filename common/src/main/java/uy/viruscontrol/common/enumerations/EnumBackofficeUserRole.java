package uy.viruscontrol.common.enumerations;

public enum EnumBackofficeUserRole {
  ADMINISTRATOR("Administrador"),
  SUPERVISOR("Gerente");

  private final String label;

  EnumBackofficeUserRole(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
