package uy.viruscontrol.common.enumerations;

public enum EnumCaseStatus {
  SUSPECT("Sospechoso"),
  CONFIRMED("Confirmado"),
  DISCARDED("Negativo"),
  CURED("Recuperado"),
  OTHER("otro");

  private final String label;

  EnumCaseStatus(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
