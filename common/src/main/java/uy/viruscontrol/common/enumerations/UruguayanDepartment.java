package uy.viruscontrol.common.enumerations;

public enum UruguayanDepartment {
  ARTIGAS(1),
  CANELONES(2),
  CERRO_LARGO(3),
  COLONIA(4),
  DURAZNO(5),
  FLORES(6),
  FLORIDA(7),
  LAVALLEJA(8),
  MALDONADO(9),
  MONTEVIDEO(10),
  PAYSANDU(11),
  RIO_NEGRO(12),
  RIVERA(13),
  ROCHA(14),
  SALTO(15),
  SAN_JOSE(16),
  SORIANO(17),
  TACUAREMBO(18),
  TREINTA_Y_TRES(19);

  private final int code;

  UruguayanDepartment(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
