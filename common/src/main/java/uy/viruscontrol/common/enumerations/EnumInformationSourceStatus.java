package uy.viruscontrol.common.enumerations;

public enum EnumInformationSourceStatus {
  PENDING(1, "Pendiente"),
  REJECTED(2, "Rechazada"),
  ACTIVE(3, "Activa"),
  INACTIVE(4, "Inactiva");

  private final int code;

  private final String label;

  EnumInformationSourceStatus(int code, String label) {
    this.code = code;
    this.label = label;
  }

  public int getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }
}
