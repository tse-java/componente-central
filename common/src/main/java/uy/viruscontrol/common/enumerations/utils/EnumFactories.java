package uy.viruscontrol.common.enumerations.utils;

import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

public class EnumFactories {

  private EnumFactories() {
    throw new IllegalStateException("Utility class");
  }

  public static DTODepartment getDtoDepartment(UruguayanDepartment department) {
    DTODepartment result = new DTODepartment();
    result.setId(department.getCode());
    result.setName(department.toString());
    return result;
  }
}
