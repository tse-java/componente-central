package uy.viruscontrol.common.enumerations;

public enum EnumRequestStatus {
  PENDING(1),
  APPROVED(2),
  REJECTED(3);

  private final int code;

  EnumRequestStatus(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
