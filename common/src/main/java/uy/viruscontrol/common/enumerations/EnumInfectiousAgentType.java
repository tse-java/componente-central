package uy.viruscontrol.common.enumerations;

public enum EnumInfectiousAgentType {
  VIRUS("Virus"),
  BACTERIA("Bacteria"),
  FUNGUS("Fungus"),
  ALGAE("Alga"),
  PROTOZOA("Protozoa"),
  PRION("Prion"),
  VIROID("Viroid");

  private final String label;

  EnumInfectiousAgentType(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
