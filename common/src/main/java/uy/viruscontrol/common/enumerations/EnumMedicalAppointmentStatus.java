package uy.viruscontrol.common.enumerations;

public enum EnumMedicalAppointmentStatus {
  PENDING(1),
  DONE(2);

  private final int code;

  EnumMedicalAppointmentStatus(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
