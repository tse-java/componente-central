package uy.viruscontrol.common.enumerations.utils;

import java.io.Serializable;

public class EnumerationException extends Exception implements Serializable {

  public EnumerationException() {}

  public EnumerationException(String s) {
    super(s);
  }

  public EnumerationException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public EnumerationException(Throwable throwable) {
    super(throwable);
  }

  public EnumerationException(String s, Throwable throwable, boolean b, boolean b1) {
    super(s, throwable, b, b1);
  }
}
