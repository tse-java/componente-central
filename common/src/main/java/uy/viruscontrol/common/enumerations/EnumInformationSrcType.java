package uy.viruscontrol.common.enumerations;

public enum EnumInformationSrcType {
  RSS("RSS"),
  TWITTER("Twitter");

  private final String label;

  EnumInformationSrcType(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
