package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.common.dto.DTODisease;

@Path("/diseases")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class DiseaseApi {
  @Inject
  ApiDiseaseService service;

  @GET
  @Operation(
    summary = "List diseases.",
    description = "List diseases",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = { @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTODisease.class))) }
      )
    },
    tags = { "disease" }
  )
  public Response list() {
    return service.list();
  }
}
