package uy.viruscontrol.api.rest.impl;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.LocationReportsApiService;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.cache.ContagionDistanceCache;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.common.dto.DTODistance;
import uy.viruscontrol.common.dto.DTOLocationReport;

@RequestScoped
public class LocationReportsApiServiceImpl implements LocationReportsApiService {
  @EJB
  ContagionDistanceCache contagionDistanceCache;

  @EJB
  MessageProducer messageProducer;

  public Response newLocationReport(DTOLocationReport dtoLocationReport, TokenData tokenData) {
    dtoLocationReport.setUserId(tokenData.getUserId());
    messageProducer.reportLocation(dtoLocationReport);
    return Response.ok().build();
  }

  @Override
  public Response getConfigDistance() {
    DTODistance dtoDistance = new DTODistance();
    dtoDistance.setDistance(contagionDistanceCache.get());
    return Response.ok().entity(dtoDistance).build();
  }
}
