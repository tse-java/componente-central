package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import uy.viruscontrol.common.dto.DTOSymptom;

@Path("/symptoms")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class SymptomsApi {
  @Inject
  SymptomsApiService service;

  @GET
  @Operation(
    summary = "Fetch Symptoms.",
    description = "Fetch Symptoms.",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "search results matching criteria",
        content = { @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOSymptom.class))) }
      )
    },
    tags = { "symptoms" }
  )
  public Response getSymptoms() {
    return service.getSymptoms();
  }
}
