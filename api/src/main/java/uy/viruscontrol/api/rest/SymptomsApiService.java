package uy.viruscontrol.api.rest;

import javax.ws.rs.core.Response;

public interface SymptomsApiService {
  Response getSymptoms();
}
