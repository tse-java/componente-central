package uy.viruscontrol.api.rest;

import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;

public interface ExamsApiService {
  Response getExams(Long userId, TokenData tokenData);
}
