package uy.viruscontrol.api.rest;

public class NotFoundException extends ApiException {

  public NotFoundException(String msg) {
    super(msg);
  }
}
