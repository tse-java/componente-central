package uy.viruscontrol.api.rest.impl;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.ExamsApiService;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.service.ExamService;
import uy.viruscontrol.common.dto.DTOExam;

@RequestScoped
public class ExamsApiServiceImpl implements ExamsApiService {
  @EJB
  ExamService examService;

  public Response getExams(Long userId, TokenData tokenData) {
    if (!tokenData.isDoctor() && !userId.equals(tokenData.getUserId())) {
      return Response.status(Response.Status.FORBIDDEN).build();
    }
    List<DTOExam> examList = examService.getUserExams(userId);
    return Response.ok().entity(examList).build();
  }
}
