package uy.viruscontrol.api.rest;

import java.util.List;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOResourceQuantity;
import uy.viruscontrol.common.enumerations.EnumResourceType;

public interface ResourcesApiService {
  Response getResourcesAsSub(TokenData tokenData);
  Response getResources();
  Response getResourcesAvailability(
    Long diseaseId,
    EnumResourceType resourceType,
    String locationFilter,
    String resourceNameFilter,
    TokenData tokenData
  );
  Response subscribeResource(Long resourceId, TokenData tokenData);
  Response unsubscribeResource(Long resourceId, TokenData tokenData);
  Response setResourcesAvailability(List<DTOResourceQuantity> resourceQuantityList, TokenData tokenData);
  Response getResourcesListBasic();
}
