package uy.viruscontrol.api.rest;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOToken;

public interface FirebaseService {
  Response subscribeToTopic(@Valid DTOToken token, TokenData tokenData);
}
