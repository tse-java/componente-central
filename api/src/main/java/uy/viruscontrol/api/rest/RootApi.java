package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class RootApi {

  @GET
  @Operation(
    summary = "Connection check.",
    description = "Connection check.",
    responses = { @ApiResponse(responseCode = "200", description = "ok") }
  )
  public Response checkConnection() {
    return Response.ok().build();
  }
}
