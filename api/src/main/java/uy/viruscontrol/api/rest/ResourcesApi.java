package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOResourceAvailability;
import uy.viruscontrol.common.dto.DTOResourceQuantity;
import uy.viruscontrol.common.dto.DTOResourceSubscription;
import uy.viruscontrol.common.dto.DTOWrongId;
import uy.viruscontrol.common.enumerations.EnumResourceType;

@Path("/resources")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class ResourcesApi {
  @Inject
  ResourcesApiService service;

  @Inject
  TokenData tokenData;

  @GET
  @Path("/subscriptions")
  @Operation(
    summary = "Fetch resources with subscriptions",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = {
          @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOResourceSubscription.class)))
        }
      )
    },
    tags = { "resources" }
  )
  public Response getResourcesAsSub() {
    return service.getResourcesAsSub(tokenData);
  }

  @GET
  @Operation(
    summary = "Fetch resources",
    description = "Fetch resources without availability information.",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = {
          @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOResourceSubscription.class)))
        }
      )
    },
    tags = { "resources" }
  )
  public Response getResources() {
    return service.getResources();
  }

  @GET
  @Path("/availability")
  @Operation(
    summary = "Fetch provider availability",
    description = "Fetch resources availability information.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = {
          @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOResourceAvailability.class)))
        }
      )
    },
    tags = { "resources" }
  )
  public Response getResourcesAvalability(
    @NotNull @QueryParam("deseaseId") Long deseaseId,
    @NotNull @QueryParam("resourceType") EnumResourceType resourceType,
    @QueryParam("locationFilter") String locationFilter,
    @QueryParam("resourceNameFilter") String resourceNameFilter
  ) {
    return service.getResourcesAvailability(deseaseId, resourceType, locationFilter, resourceNameFilter, tokenData);
  }

  @POST
  @Path("/availability")
  @Operation(
    summary = "Set provider availability",
    description = "Set resources provider availability information. If some of the ids don't exists, these are returned on the response body",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = { @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOWrongId.class))) }
      )
    },
    tags = { "resources" }
  )
  public Response setResourcesAvailability(List<DTOResourceQuantity> resourceQuantityList) {
    return service.setResourcesAvailability(resourceQuantityList, tokenData);
  }

  @POST
  @Path("/{resourceId}/subscription")
  @Operation(
    summary = "Subscribe to resource availability",
    description = "Subscribes a user to a resource availability",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(responseCode = "200", description = "ok", content = { @Content(mediaType = "application/json") })
    },
    tags = { "resources" }
  )
  public Response subscribeResource(@NotNull @PathParam("resourceId") Long resourceId) {
    return service.subscribeResource(resourceId, tokenData);
  }

  @DELETE
  @Path("/{resourceId}/subscription")
  @Operation(
    summary = "Remove subscription to resource availability",
    description = "Remove subscription to resource availability",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(responseCode = "200", description = "ok", content = { @Content(mediaType = "application/json") })
    },
    tags = { "resources" }
  )
  public Response unsubscribeResource(@NotNull @PathParam("resourceId") Long resourceId) {
    return service.unsubscribeResource(resourceId, tokenData);
  }

  @GET
  @Path("/basic")
  @Operation(
    summary = "Return a list with basic resource information",
    description = "created for resource provider",
    responses = { @ApiResponse(responseCode = "200", description = "ok", content = { @Content(mediaType = "application/json") }) },
    tags = { "resources" }
  )
  public Response getResourcesListBasic() {
    return service.getResourcesListBasic();
  }
}
