package uy.viruscontrol.api.rest.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.lang.JoseException;
import uy.viruscontrol.api.rest.ApiResponseMessage;
import uy.viruscontrol.api.rest.UsersApiService;
import uy.viruscontrol.api.utils.Jwt;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.service.ExamService;
import uy.viruscontrol.business.service.FrontProfileService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RequestScoped
@Slf4j
public class UsersApiServiceImpl implements UsersApiService {
  @EJB
  private ExamService examService;

  @EJB
  private FrontProfileService frontProfileService;

  private final String INTERNAL_ERROR = "Error interno {}";

  public Response getUserProfile(TokenData tokenData) {
    try {
      Long myId = tokenData.getUserId();
      return Response.ok().entity(frontProfileService.getProfileById(myId)).build();
    } catch (VirusControlException e) {
      log.error(INTERNAL_ERROR, e.getMessage());
      return Response.status(Response.Status.UNAUTHORIZED).build();
    }
  }

  @SuppressWarnings("rawtypes")
  public Response login(DTOToken dtOToken) {
    String token = dtOToken.getToken();
    try {
      String googleJson = checkGoogleToken(token);
      ObjectMapper mapper = new ObjectMapper();
      Map map = mapper.readValue(googleJson, Map.class);
      String externalId = (String) map.get("id");
      DTOFrontProfile userProfile;
      userProfile = frontProfileService.getProfileByExternalId(externalId);
      if (userProfile != null) {
        doLogin(userProfile);
        DTOLogin login = doLogin(userProfile);
        return Response.ok().entity(login).build();
      } else {
        userProfile = new DTOFrontProfile();
        userProfile.setEmail((String) map.get("email"));
        userProfile.setFirstname((String) map.get("given_name"));
        userProfile.setLastname((String) map.get("family_name"));
        userProfile.setExternalId((String) map.get("id"));
        userProfile.setIsDoctor(false);
        userProfile.setIsFinished(false);
        userProfile = frontProfileService.newProfile(userProfile);
        DTOLogin login = doLogin(userProfile);
        return Response.ok().entity(login).build();
      }
    } catch (VirusControlException e) {
      if (e.getCode().equals(VirusControlException.INVALID_TOKEN)) {
        log.warn("Intento de acceso con token invalido: {}", e.getMessage());
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
      log.error(INTERNAL_ERROR, e.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    } catch (IOException | JoseException e) {
      log.error(INTERNAL_ERROR, e.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  public Response logout(TokenData tokenData) {
    try {
      frontProfileService.invalidateRefreshToken(tokenData.getUserId());
    } catch (VirusControlException e) {
      log.error(INTERNAL_ERROR, e.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "se cerro la sesion")).build();
  }

  public Response newExam(Long patientId, DTODisease dtoDisease, TokenData tokenData) {
    if (!tokenData.isDoctor()) {
      return Response.status(Response.Status.FORBIDDEN).build();
    }
    try {
      examService.newExam(tokenData.getUserId(), patientId, dtoDisease);
      return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Peticion recibida.")).build();
    } catch (VirusControlException e) {
      return Response
        .status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, e.getMessage()))
        .build();
    }
  }

  public Response newUser(DTOUpdateFrontProfile dtOUpdateFrontProfile, TokenData tokenData) {
    try {
      dtOUpdateFrontProfile.setId(tokenData.getUserId());
      DTOFrontProfile userProfile = frontProfileService.updateProfile(dtOUpdateFrontProfile);
      String accessToken = generateToken(userProfile);
      DTOLogin dtoLogin = new DTOLogin();
      dtoLogin.setAccessToken(accessToken);
      dtoLogin.setFinishedUserProfile(userProfile.getIsFinished());
      return Response.ok().entity(dtoLogin).build();
    } catch (VirusControlException e) {
      log.error("Conficto: {}", e.getMessage());
      return Response.status(Response.Status.CONFLICT).entity(new ApiResponseMessage(ApiResponseMessage.ERROR, e.getMessage())).build();
    } catch (JoseException e) {
      log.error("Error: {}", e.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  private String generateToken(DTOFrontProfile userProfile) throws JoseException {
    Map<String, Object> claims = new HashMap<>();
    claims.put("userId", userProfile.getId());
    claims.put("isDoctor", userProfile.getIsDoctor());
    claims.put("externalId", userProfile.getExternalId());
    claims.put("isFinished", userProfile.getIsFinished());
    return Jwt.createJWT(userProfile.getExternalId(), claims);
  }

  public Response patchUser(DTOLocation dtOLocation, TokenData tokenData) {
    try {
      DTOUpdateFrontProfile dtoUpdateFrontProfile = new DTOUpdateFrontProfile();
      dtoUpdateFrontProfile.setId(tokenData.getUserId());
      dtoUpdateFrontProfile.setAddress(dtOLocation);
      DTOFrontProfile dtoFrontProfile;
      frontProfileService.patchAddress(dtoUpdateFrontProfile);
      dtoFrontProfile = frontProfileService.getProfileById(dtoUpdateFrontProfile.getId());
      return Response.ok().entity(dtoFrontProfile).build();
    } catch (VirusControlException e) {
      log.error(INTERNAL_ERROR, e.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  private String checkGoogleToken(String googleToken) throws VirusControlException {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target("https://www.googleapis.com/userinfo/v2/me");
    Invocation.Builder invocationBuilder = target.request();
    invocationBuilder.header("Authorization", "Bearer " + googleToken);
    Response googleResponse = invocationBuilder.get();
    if (googleResponse.getStatus() != 200) {
      throw new VirusControlException("Token invalido", VirusControlException.INVALID_TOKEN);
    }
    return googleResponse.readEntity(String.class);
  }

  public Response refreshToken(DTOToken refreshToken, TokenData httpHeaders) {
    try {
      DTOFrontProfile userProfile = frontProfileService.getProfileByRefreshToken(refreshToken.getToken());

      Map<String, Object> claims = new HashMap<>();
      claims.put("userId", userProfile.getId());
      claims.put("isFinished", userProfile.getIsFinished());
      claims.put("isDoctor", userProfile.getIsDoctor());
      claims.put("externalId", userProfile.getExternalId());
      String accessToken = Jwt.createJWT(userProfile.getExternalId(), claims);

      DTOLogin dtoLogin = new DTOLogin();
      dtoLogin.setAccessToken(accessToken);
      dtoLogin.setRefreshToken(refreshToken.getToken());
      dtoLogin.setFinishedUserProfile(userProfile.getIsFinished());
      return Response.ok().entity(dtoLogin).build();
    } catch (VirusControlException | JoseException e) {
      log.warn("Intento de acceso con token invalido: {}", e.getMessage());
      return Response.status(Response.Status.UNAUTHORIZED).entity(new ApiResponseMessage(ApiResponseMessage.ERROR, e.getMessage())).build();
    }
  }

  @Override
  public Response patchNotifications(DTONotifyStyle dtoNotifyStyle, TokenData tokenData) {
    try {
      return Response.ok().entity(frontProfileService.patchNotifications(tokenData.getUserId(), dtoNotifyStyle)).build();
    } catch (VirusControlException virusControlException) {
      log.error(INTERNAL_ERROR, virusControlException.getMessage());
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  private DTOLogin doLogin(DTOFrontProfile userProfile) throws VirusControlException, JoseException {
    String accessToken = generateToken(userProfile);
    String refreshToken = UUID.randomUUID().toString();
    refreshToken = frontProfileService.saveRefreshToken(userProfile.getId(), refreshToken);
    DTOLogin dtoLogin = new DTOLogin();
    dtoLogin.setAccessToken(accessToken);
    dtoLogin.setRefreshToken(refreshToken);
    dtoLogin.setFinishedUserProfile(userProfile.getIsFinished());
    return dtoLogin;
  }
}
