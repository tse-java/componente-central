package uy.viruscontrol.api.rest.impl;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.NewsApiService;
import uy.viruscontrol.business.service.NewsService;

@RequestScoped
public class NewsApiServiceImpl implements NewsApiService {
  @EJB
  NewsService newsService;

  public Response getNews() {
    return Response.ok().entity(newsService.getLatestNews()).build();
  }
}
