package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOToken;

@Path("/firebaseSubscriptions")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class FirebaseApi {
  @Inject
  FirebaseService service;

  @Inject
  TokenData tokenData;

  @POST
  @Path("/")
  @Operation(
    summary = "Subscribe to Firebase topic.",
    description = "Subscribes the provided token to the provided topic.",
    responses = {
      @ApiResponse(responseCode = "502", description = "Somthing went wrong with Firebase communcation."),
      @ApiResponse(responseCode = "400", description = "Bad input"),
      @ApiResponse(responseCode = "200", description = "ok")
    },
    tags = { "firebase" }
  )
  public Response subscribeToTopic(@Valid DTOToken token) {
    return service.subscribeToTopic(token, tokenData);
  }
}
