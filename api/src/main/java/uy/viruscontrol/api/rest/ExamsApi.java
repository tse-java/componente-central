package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOExam;

@Path("/exams")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class ExamsApi {
  @Inject
  ExamsApiService service;

  @Inject
  TokenData tokenData;

  @GET
  @Path("/{userId}")
  @Operation(
    summary = "Fetch user's exams.",
    description = "Fetch user's exams.",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = { @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOExam.class))) }
      )
    },
    tags = { "exams" }
  )
  public Response getExams(@PathParam(value = "userId") Long userId) {
    return service.getExams(userId, tokenData);
  }
}
