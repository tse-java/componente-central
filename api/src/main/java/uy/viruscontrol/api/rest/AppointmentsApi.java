package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.dto.DTONewMedicalAppointment;

@Path("/appointments")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class AppointmentsApi {
  @Inject
  AppointmentsApiService service;

  @Inject
  TokenData tokenData;

  @GET
  @Operation(
    summary = "Fetch doctor appointments.",
    description = "If the user loged in is a doctor, it fetches the appointments that he has assigned.\nIf normal user, list his appointments.",
    responses = {
      @ApiResponse(responseCode = "404", description = "Not found"),
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = {
          @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTOMedicalAppointment.class)))
        }
      )
    },
    tags = { "appointments" }
  )
  public Response getMedicalAppointments(@QueryParam("requestAs") String requestAs) {
    return service.getMedicalAppointments(requestAs, tokenData);
  }

  @POST
  @Operation(
    summary = "Issue a new medical appointment.",
    description = "Issues a new medical appointment.",
    responses = { @ApiResponse(responseCode = "400", description = "Bad input."), @ApiResponse(responseCode = "200", description = "ok") },
    tags = { "appointments" }
  )
  public Response newMedicalAppointment(DTONewMedicalAppointment newMedicalAppointment) {
    return service.newMedicalAppointment(newMedicalAppointment, tokenData);
  }

  @PATCH
  @Path("/{appointmentId}")
  @Operation(
    summary = "Cierra una consulta medica",
    description = "Cierra una consulta medica",
    responses = { @ApiResponse(responseCode = "400", description = "Bad input."), @ApiResponse(responseCode = "200", description = "ok") },
    tags = { "appointments" }
  )
  public Response closeMedicalAppointment(@PathParam("appointmentId") Long appointmentId) {
    return service.closeMedicalAppointment(appointmentId, tokenData);
  }
}
