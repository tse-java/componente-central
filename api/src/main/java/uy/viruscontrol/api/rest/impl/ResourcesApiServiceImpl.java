package uy.viruscontrol.api.rest.impl;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.api.rest.ApiResponseMessage;
import uy.viruscontrol.api.rest.ResourcesApiService;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.business.service.ResourceService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumResourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RequestScoped
@Slf4j
public class ResourcesApiServiceImpl implements ResourcesApiService {
  @EJB
  private ResourceService resourceService;

  @EJB
  private ResourceProviderService providerService;

  static final String ERROR_INTERNO = "Error interno: {}";
  static final String ERROR_PATH = "Error de path: {}";

  @Override
  public Response getResourcesAsSub(TokenData tokenData) {
    return Response.ok().entity(resourceService.listResourcesSubscriptions(tokenData.getUserId())).build();
  }

  @Override
  public Response getResources() {
    return Response.ok().entity(resourceService.listResourcesSubscriptions(null)).build();
  }

  public Response getResourcesAvailability(
    Long diseaseId,
    EnumResourceType resourceType,
    String locationFilter,
    String resourceNameFilter,
    TokenData tokenData
  ) {
    try {
      List<DTOResourceAvailability> resourceAvailabilityList = resourceService.getAvailability(
        diseaseId,
        resourceType,
        locationFilter,
        resourceNameFilter
      );
      return Response.ok().entity(resourceAvailabilityList).build();
    } catch (VirusControlException e) {
      log.error(ERROR_PATH, e.getMessage());
      return Response.status(Response.Status.BAD_REQUEST).build();
    }
  }

  public Response subscribeResource(Long resourceId, TokenData tokenData) {
    try {
      resourceService.subscribeResource(tokenData.getUserId(), resourceId);
      return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Se agrego una suscripcion al recurso")).build();
    } catch (VirusControlException e) {
      if (e.getCode().equals(VirusControlException.RESOURCE_NOT_FOUND)) {
        log.error(ERROR_PATH, e.getMessage());
        return Response.status(Response.Status.NOT_FOUND).build();
      } else {
        log.error(ERROR_INTERNO, e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
      }
    }
  }

  public Response unsubscribeResource(Long resourceId, TokenData tokenData) {
    try {
      resourceService.unsubscribeResource(tokenData.getUserId(), resourceId);
      return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Se elimino la suscripcion.")).build();
    } catch (VirusControlException e) {
      if (e.getCode().equals(VirusControlException.RESOURCE_NOT_FOUND)) {
        log.error(ERROR_PATH, e.getMessage());
        return Response.status(Response.Status.NOT_FOUND).build();
      } else {
        log.error(ERROR_INTERNO, e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
      }
    }
  }

  @Override
  public Response setResourcesAvailability(List<DTOResourceQuantity> resourceQuantityList, TokenData tokenData) {
    List<DTOWrongId> wrongIds = providerService.setResourceProviderAvailability(
      tokenData.getDtoResourceProvider().getId(),
      resourceQuantityList
    );
    return Response.ok().entity(wrongIds).build();
  }

  @Override
  public Response getResourcesListBasic() {
    return Response.ok().entity(resourceService.getResourcesListBasic()).build();
  }
}
