package uy.viruscontrol.api.rest;

import java.util.List;
import javax.ws.rs.core.Response;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

public interface DepartmentsApiService {
  Response getDepartmentById(Long diseaseId, List<EnumCaseStatus> caseStatusList);
  Response getDepartments();
}
