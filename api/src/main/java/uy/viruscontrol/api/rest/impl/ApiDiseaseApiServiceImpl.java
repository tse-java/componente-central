package uy.viruscontrol.api.rest.impl;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.ApiDiseaseService;
import uy.viruscontrol.business.service.DiseaseService;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@RequestScoped
public class ApiDiseaseApiServiceImpl implements ApiDiseaseService {
  @EJB
  DiseaseService diseaseService;

  @Override
  public Response list() {
    return Response.ok().entity(diseaseService.getDiseasesByStatus(EnumRequestStatus.APPROVED)).build();
  }
}
