package uy.viruscontrol.api.rest;

import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.*;

public interface UsersApiService {
  Response getUserProfile(TokenData tokenData);
  Response login(DTOToken dtOToken);
  Response logout(TokenData tokenData);
  Response newExam(Long userId, DTODisease dtoDisease, TokenData tokenData);
  Response newUser(DTOUpdateFrontProfile dtOUpdateFrontProfile, TokenData tokenData);
  Response patchUser(DTOLocation dtOLocation, TokenData tokenData);
  Response refreshToken(DTOToken dtOToken, TokenData tokenData);
  Response patchNotifications(DTONotifyStyle dtoNotifyStyle, TokenData tokenData);
}
