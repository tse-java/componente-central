package uy.viruscontrol.api.rest.impl;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.AppointmentsApiService;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.business.service.MedicalAppointmentService;
import uy.viruscontrol.common.dto.DTONewMedicalAppointment;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RequestScoped
public class AppointmentsApiServiceImpl implements AppointmentsApiService {
  @EJB
  private MedicalAppointmentService medicalAppointmentService;

  public Response getMedicalAppointments(String requestAs, TokenData tokenData) {
    if (requestAs != null && requestAs.equalsIgnoreCase("MEDIC")) {
      if (tokenData.isDoctor()) {
        return Response.ok().entity(medicalAppointmentService.getDoctorAppointments(tokenData.getUserId())).build();
      } else {
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    }
    return Response.ok().entity(medicalAppointmentService.getPatientAppointments(tokenData.getUserId())).build();
  }

  public Response newMedicalAppointment(DTONewMedicalAppointment dtoNewMedicalAppointment, TokenData tokenData) {
    DTORequestMedicalAppointment dtoRequestMedicalAppointment = new DTORequestMedicalAppointment();
    dtoRequestMedicalAppointment.setPatientId(tokenData.getUserId());
    dtoRequestMedicalAppointment.setLocation(dtoNewMedicalAppointment.getLocation());
    dtoRequestMedicalAppointment.setSymptomIds(dtoNewMedicalAppointment.getSymptomIds());
    medicalAppointmentService.newAppointment(dtoRequestMedicalAppointment);
    return Response.ok().build();
  }

  @Override
  public Response closeMedicalAppointment(Long appointmentId, TokenData tokenData) {
    if (!tokenData.isDoctor()) {
      return Response.status(Response.Status.UNAUTHORIZED).build();
    }
    try {
      medicalAppointmentService.closeAppointment(tokenData.getUserId(), appointmentId);
    } catch (VirusControlException e) {
      if (e.getCode().equals(VirusControlException.ENTITY_NOT_FOUND)) {
        return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
      } else if (e.getCode().equals(VirusControlException.INVALID_USER)) {
        return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
      } else if (e.getCode().equals(VirusControlException.ALREADY_DONE)) {
        return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
      } else {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
      }
    }
    return Response.ok().build();
  }
}
