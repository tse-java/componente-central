package uy.viruscontrol.api.rest;

public class ApiException extends Exception {

  public ApiException(String msg) {
    super(msg);
  }
}
