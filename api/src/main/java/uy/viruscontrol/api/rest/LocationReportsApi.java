package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOLocationReport;

@Path("/location")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class LocationReportsApi {
  @Inject
  LocationReportsApiService service;

  @Inject
  TokenData tokenData;

  @GET
  @Operation(
    summary = "Get Distance Config.",
    responses = { @ApiResponse(responseCode = "400", description = "Bad input"), @ApiResponse(responseCode = "200", description = "ok") },
    tags = { "location" }
  )
  public Response getConfigDistance() {
    return service.getConfigDistance();
  }

  @POST
  @Operation(
    summary = "Report location.",
    description = "Report location.",
    responses = { @ApiResponse(responseCode = "400", description = "Bad input"), @ApiResponse(responseCode = "200", description = "ok") },
    tags = { "location" }
  )
  public Response newLocationReport(DTOLocationReport dtoLocationReport) {
    return service.newLocationReport(dtoLocationReport, tokenData);
  }
}
