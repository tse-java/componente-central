package uy.viruscontrol.api.rest;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.integration.DNICIntegrationService;
import uy.viruscontrol.business.integration.RSSIntegrationService;
import uy.viruscontrol.business.integration.TwitterIntegrationService;
import uy.viruscontrol.business.model.RSSInformationSource;
import uy.viruscontrol.business.model.TwitterInformationSource;
import uy.viruscontrol.business.service.InformationSourceService;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.common.exceptions.VirusControlException;
import uy.viruscontrol.pi.service.DtoCitizen;

@Path("/testing")
@Slf4j
public class TestApi {
  @EJB
  private DNICIntegrationService dnicIntegration;

  @EJB
  private ResourceProviderService providerService;

  @EJB
  private TwitterIntegrationService twitterService;

  @EJB
  private RSSIntegrationService rssService;

  @EJB
  private InformationSourceService sourceService;

  @PersistenceContext
  private EntityManager entityManager;

  @GET
  @Path("/tw")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testingTwitter() {
    TwitterInformationSource source = entityManager.find(TwitterInformationSource.class, 2L);

    try {
      return Response.ok().entity(twitterService.getNews(source)).build();
    } catch (VirusControlException e) {
      return Response.ok().entity(e.getStackTrace()).build();
    }
  }

  @GET
  @Path("/rss")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testingRSS() {
    String feedSource = "https://www.elpais.com.uy/rss/";
    //String feedSource = "https://www.el.com.uy/rss/";
    //String feedSource = "https://ladiaria.com.uy/feeds/articulos/";
    //feedSource = "https://www.elobservador.com.uy/rss/elobservador/salud.xml";

    RSSInformationSource infoSource = new RSSInformationSource();
    infoSource.setId(1L);
    infoSource.setUrl(feedSource);
    infoSource.setName("El Pais");
    infoSource.setDescription("Diario El Pais");
    infoSource.getKeyWords().add("COVID");
    infoSource.getKeyWords().add("coronavirus");
    infoSource.getKeyWords().add("pandemia");

    try {
      return Response.ok().entity(rssService.getNews(infoSource)).build();
    } catch (VirusControlException e) {
      return javax.ws.rs.core.Response.ok().entity(e.getMessage()).build();
    }
  }

  @GET
  @Path("/dnic")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testDnic() {
    DtoCitizen doc = dnicIntegration.getCitizenInformation("1234567");
    return javax.ws.rs.core.Response.ok().entity(doc).build();
  }

  @GET
  @Path("/provider")
  @Produces(MediaType.APPLICATION_JSON)
  public Response setProvider() throws VirusControlException {
    providerService.newResourceProvider("Prueba", "http://localhost:8081/api");
    return javax.ws.rs.core.Response.ok().build();
  }
}
