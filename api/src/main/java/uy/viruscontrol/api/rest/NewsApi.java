package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import uy.viruscontrol.common.dto.DTONews;

@Path("/news")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class NewsApi {
  @Inject
  NewsApiService service;

  @GET
  @Operation(
    summary = "Fetch news",
    description = "Fetches latest news, ordered by date.",
    responses = {
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = { @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = DTONews.class))) }
      )
    },
    tags = { "news" }
  )
  public Response getNews() {
    return service.getNews();
  }
}
