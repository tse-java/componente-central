package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.*;

@Path("/users")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class UsersApi {
  @Inject
  UsersApiService service;

  @Inject
  TokenData tokenData;

  @GET
  @Path("/profile")
  @Operation(
    summary = "User's profile.",
    description = "Fetchs the current user's profile.",
    responses = {
      @ApiResponse(responseCode = "400", description = "bad input parameter"),
      @ApiResponse(
        responseCode = "200",
        description = "search results matching criteria",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOFrontProfile.class)) }
      )
    },
    tags = { "users" }
  )
  public Response getUserProfile() {
    return service.getUserProfile(tokenData);
  }

  @POST
  @Path("/login")
  @Operation(
    summary = "Issues a new token pair.",
    description = "Returns a new JWT access token and a refresh token.",
    responses = {
      @ApiResponse(responseCode = "400", description = "bad input parameter"),
      @ApiResponse(
        responseCode = "200",
        description = "Session created",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOLogin.class)) }
      )
    },
    tags = { "users" }
  )
  public Response login(@Valid DTOToken dtOToken) {
    return service.login(dtOToken);
  }

  @POST
  @Path("/logout")
  @Operation(
    summary = "Kill session",
    description = "User's refresh token is destroyed",
    responses = {
      @ApiResponse(responseCode = "403", description = "Users is not logged in"),
      @ApiResponse(responseCode = "200", description = "Session destroyed")
    },
    tags = { "users" }
  )
  public Response logout() {
    return service.logout(tokenData);
  }

  @POST
  @Path("/{userId}/exams")
  @Operation(
    summary = "Request a new lab exam ",
    description = "Issues a new lab exam for the user.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."), @ApiResponse(responseCode = "200", description = "Issued")
    },
    tags = { "users" }
  )
  public Response newExam(@PathParam("userId") Long userId, @Valid DTODisease dtoDisease) {
    return service.newExam(userId, dtoDisease, tokenData);
  }

  @PUT
  @Operation(
    summary = "Finish new user.",
    description = "Process data to finish the new users.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOLogin.class)) }
      )
    },
    tags = { "users" }
  )
  public Response newUser(@Valid DTOUpdateFrontProfile dtOUpdateFrontProfile) {
    return service.newUser(dtOUpdateFrontProfile, tokenData);
  }

  @PATCH
  @Path("/notification")
  @Operation(
    summary = "Update user notification settings.",
    description = "Update user notification settings.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOFrontProfile.class)) }
      )
    },
    tags = { "users" }
  )
  public Response patchNotification(@Valid DTONotifyStyle notifyStyle) {
    return service.patchNotifications(notifyStyle, tokenData);
  }

  @PATCH
  @Path("/address")
  @Operation(
    summary = "Update user address.",
    description = "Patch the current user's address.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOFrontProfile.class)) }
      )
    },
    tags = { "users" }
  )
  public Response patchUser(@Valid DTOLocation dtOLocation) {
    return service.patchUser(dtOLocation, tokenData);
  }

  @POST
  @Path("/refreshToken")
  @Operation(
    summary = "Issue new access token.",
    description = "Issues a new access token from a refresh token.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTOLogin.class)) }
      )
    },
    tags = { "users" }
  )
  public Response refreshToken(@Valid DTOToken dtOToken) {
    return service.refreshToken(dtOToken, tokenData);
  }
}
