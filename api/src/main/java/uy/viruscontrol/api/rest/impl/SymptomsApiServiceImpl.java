package uy.viruscontrol.api.rest.impl;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.SymptomsApiService;
import uy.viruscontrol.business.service.SymptomService;

@RequestScoped
public class SymptomsApiServiceImpl implements SymptomsApiService {
  @EJB
  SymptomService service;

  public Response getSymptoms() {
    return Response.ok().entity(service.getSymptoms()).build();
  }
}
