package uy.viruscontrol.api.rest.security;

import java.util.Map;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.api.utils.InvalidTokenException;
import uy.viruscontrol.api.utils.Jwt;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.common.dto.DTOResourceProvider;

@Provider
@Priority(Priorities.AUTHENTICATION)
@Slf4j
public class AuthenticationTokenInterceptor implements ContainerRequestFilter {
  static final String AUTHENTICATION_SCHEME_NAME = "Bearer";
  static final String URI_SECURE_PROTOCOL = "https";

  @Inject
  TokenData tokenData;

  @EJB
  ResourceProviderService providerService;

  @Override
  public void filter(ContainerRequestContext request) {
    if (
      !(request.getMethod().equals("OPTIONS")) &&
      !request.getUriInfo().getPath().equals("/users/login") &&
      !request.getUriInfo().getPath().equals("/") &&
      !request.getUriInfo().getPath().equals("/resources") &&
      !request.getUriInfo().getPath().equals("/resources/basic") &&
      !request.getUriInfo().getPath().equals("/diseases") &&
      !request.getUriInfo().getPath().startsWith("/departments") &&
      !request.getUriInfo().getPath().startsWith("/testing") &&
      !request.getUriInfo().getPath().startsWith("/news") &&
      !request.getUriInfo().getPath().equals("/environmentconfiguration") &&
      !request.getUriInfo().getPath().equals("/users/refreshToken") &&
      !request.getUriInfo().getPath().equals("/openapi.json")
    ) {
      String authHeader = request.getHeaderString(HttpHeaders.AUTHORIZATION);

      if (authHeader == null || !authHeader.startsWith(AUTHENTICATION_SCHEME_NAME + " ")) {
        log.error("Valid Authorization header must be provided. REQUEST: {}", request);
        throw new NotAuthorizedException("Valid Authorization header must be provided");
      }

      String token = getToken(request);

      if (request.getUriInfo().getPath().equals("/resources/availability") && request.getMethod().equals("POST")) {
        //Acceso de proveedores de recursos
        DTOResourceProvider resourceProvider = providerService.getResourceProviderByToken(token);
        tokenData.setDtoResourceProvider(resourceProvider);
        if (resourceProvider == null) {
          log.error("Proveedor no autorizado. Token de sesion invalido.");
          request.abortWith(
            Response.status(Response.Status.UNAUTHORIZED).entity("Proveedor no autorizado. Token de sesion invalido").build()
          );
        }
      } else {
        //Acceso de usuarios
        try {
          Map<String, Object> claims = Jwt.getJwtClaims(token);
          tokenData.setDoctor((Boolean) claims.get("isDoctor"));
          tokenData.setExternalId((String) claims.get("externalId"));
          tokenData.setFinished((Boolean) claims.get("isFinished"));
          tokenData.setUserId((Long) claims.get("userId"));
          if (!request.getUriInfo().getPath().equals("/users") && !request.getMethod().equals("POST") && !tokenData.isFinished()) {
            log.error("Usuario sin registro finalizado intento acceder a otras funcionalidades");
            request.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("Usuario no termino el proceso de registro").build());
          }
        } catch (InvalidTokenException e) {
          log.error("Usuario no autorizado. Token de sesion invalido. Message: {}", e.getMessage());
          request.abortWith(
            Response.status(Response.Status.UNAUTHORIZED).entity("Usuario no autorizado. Token de sesion invalido").build()
          );
        }
      }
    }
  }

  private String getToken(ContainerRequestContext request) throws NotAuthorizedException {
    String authHeader = request.getHeaderString(HttpHeaders.AUTHORIZATION);
    if (authHeader == null || !authHeader.startsWith(AUTHENTICATION_SCHEME_NAME + " ")) {
      log.error("Valid Authorization header must be provided. REQUEST: {}", request);
      throw new NotAuthorizedException("Valid Authorization header must be provided");
    }
    return authHeader.substring(AUTHENTICATION_SCHEME_NAME.length()).trim();
  }
}
