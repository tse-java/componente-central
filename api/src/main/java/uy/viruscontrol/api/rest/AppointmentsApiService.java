package uy.viruscontrol.api.rest;

import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTONewMedicalAppointment;

public interface AppointmentsApiService {
  Response getMedicalAppointments(String requestAs, TokenData tokenData);
  Response newMedicalAppointment(DTONewMedicalAppointment newMedicalAppointment, TokenData tokenData);
  Response closeMedicalAppointment(Long appointmentId, TokenData tokenData);
}
