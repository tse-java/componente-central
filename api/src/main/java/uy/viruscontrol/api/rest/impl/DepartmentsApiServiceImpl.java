package uy.viruscontrol.api.rest.impl;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.rest.DepartmentsApiService;
import uy.viruscontrol.business.service.CaseService;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@RequestScoped
public class DepartmentsApiServiceImpl implements DepartmentsApiService {
  @EJB
  CaseService service;

  public Response getDepartmentById(Long diseaseId, List<EnumCaseStatus> caseStatusList) {
    List<DTODepartment> dtoDepartments = service.getCasesGroupByDepartment(diseaseId, caseStatusList);
    return Response.ok().entity(dtoDepartments).build();
  }

  public Response getDepartments() {
    return Response.ok().entity(UruguayanDepartment.values()).build();
  }
}
