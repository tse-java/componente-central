package uy.viruscontrol.api.rest.impl;

import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import uy.viruscontrol.api.AppProperties;
import uy.viruscontrol.api.rest.FirebaseService;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOToken;

@RequestScoped
public class FirebaseApiServiceImpl implements FirebaseService {
  final String firebaseKey = AppProperties.INSTANCE.getFirebaseKey();

  @Override
  public Response subscribeToTopic(@Valid DTOToken token, TokenData tokenData) {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target("https://iid.googleapis.com/iid/v1/" + token.getToken() + "/rel/topics/" + tokenData.getExternalId());
    Response response = target.request().header("Authorization", firebaseKey).post(null);
    if (response.getStatus() > 200) {
      return Response.status(502).build();
    }
    return Response.ok().build();
  }
}
