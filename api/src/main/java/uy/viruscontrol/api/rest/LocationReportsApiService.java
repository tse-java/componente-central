package uy.viruscontrol.api.rest;

import javax.ws.rs.core.Response;
import uy.viruscontrol.api.utils.TokenData;
import uy.viruscontrol.common.dto.DTOLocationReport;

public interface LocationReportsApiService {
  Response newLocationReport(DTOLocationReport dtoLocationReport, TokenData tokenData);

  Response getConfigDistance();
}
