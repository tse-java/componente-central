package uy.viruscontrol.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@Path("/departments")
@Produces({ "application/json" })
@Consumes({ "application/json" })
public class DepartmentsApi {
  @Inject
  DepartmentsApiService service;

  @GET
  @Path("/cases")
  @Operation(
    summary = "Fetch all departments cases",
    description = "Fetches all departments cases",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DTODepartment.class)) }
      )
    },
    tags = { "cases" }
  )
  public Response getDepartmentById(
    @NotNull @QueryParam("diseaseId") Long diseaseId,
    @NotNull @QueryParam("caseStatusList") List<EnumCaseStatus> caseStatusList
  ) {
    return service.getDepartmentById(diseaseId, caseStatusList);
  }

  @GET
  @Operation(
    summary = "Fetch departments.",
    description = "Fetches departments.",
    responses = {
      @ApiResponse(responseCode = "400", description = "Bad input."),
      @ApiResponse(
        responseCode = "200",
        description = "ok",
        content = {
          @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = UruguayanDepartment.class)))
        }
      )
    },
    tags = { "cases" }
  )
  public Response getDepartments() {
    return service.getDepartments();
  }
}
