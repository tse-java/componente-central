package uy.viruscontrol.api.utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOResourceProvider;

@RequestScoped
@Named
@Data
public class TokenData {
  Long userId;
  String externalId;
  boolean doctor;
  boolean finished;
  DTOResourceProvider dtoResourceProvider;
}
