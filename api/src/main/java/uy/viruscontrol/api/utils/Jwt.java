package uy.viruscontrol.api.utils;

import java.security.Key;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.JoseException;
import uy.viruscontrol.api.AppProperties;

@Slf4j
public class Jwt {
  private static final String JWT_SECRET = AppProperties.INSTANCE.getJwtSecret();
  private static final String JWT_ISSUER = AppProperties.INSTANCE.getJwtIssuer();

  //duracion del access token en minutos
  private static final Integer ACCESSTOKEN_EXPIRATION = 5;

  private Jwt() {}

  public static String createJWT(String subject, Map<String, Object> claimMap) throws JoseException {
    Key key = new AesKey(JWT_SECRET.getBytes());
    JwtClaims claims = new JwtClaims();
    claims.setExpirationTimeMinutesInTheFuture(ACCESSTOKEN_EXPIRATION);
    claims.setSubject(subject);
    claims.setIssuer(JWT_ISSUER);
    claimMap.forEach(claims::setClaim);
    JsonWebEncryption jwe = new JsonWebEncryption();
    jwe.setPayload(claims.toJson());
    jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
    jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
    jwe.setKey(key);
    return jwe.getCompactSerialization();
  }

  public static JwtClaims getClaims(String jwt) throws InvalidJwtException {
    Key key = new AesKey(JWT_SECRET.getBytes());
    JsonWebEncryption jwe = new JsonWebEncryption();
    jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
    jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
    jwe.setKey(key);
    JwtConsumerBuilder jwtConsumerBuilder = new JwtConsumerBuilder();
    jwtConsumerBuilder.setAllowedClockSkewInSeconds(0);
    jwtConsumerBuilder.setRequireSubject();
    jwtConsumerBuilder.setExpectedIssuer(JWT_ISSUER);
    jwtConsumerBuilder.setDecryptionKey(key);
    jwtConsumerBuilder.setDisableRequireSignature();
    JwtConsumer jwtConsumer = jwtConsumerBuilder.build();
    return jwtConsumer.processToClaims(jwt);
  }

  public static boolean userIsMedic(String jwt) throws InvalidTokenException {
    try {
      JwtClaims claims = getClaims(jwt);
      Map<String, Object> claimsMap = claims.getClaimsMap();
      return (boolean) claimsMap.get("isDoctor");
    } catch (InvalidJwtException e) {
      log.error("TOKEN ERROR: {}", e.getMessage());
      throw new InvalidTokenException("Token invalido");
    }
  }

  public static Map<String, Object> getJwtClaims(String jwt) throws InvalidTokenException {
    try {
      JwtClaims jwtClaims = getClaims(jwt);
      return jwtClaims.getClaimsMap();
    } catch (InvalidJwtException e) {
      log.error("TOKEN ERROR: {}", e.getMessage());
      throw new InvalidTokenException("Token invalido");
    }
  }
}
