package uy.viruscontrol.api.utils;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

@Provider
@Priority(0)
@Slf4j
public class RESTLogger implements ContainerResponseFilter, ContainerRequestFilter {
  @Context
  private HttpServletRequest servletRequest;

  @Override
  public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) {
    try {
      String stTime = MDC.get("start-time");
      if (null == stTime || stTime.length() == 0) {
        return;
      }
      long startTime = Long.parseLong(stTime);
      long executionTime = System.currentTimeMillis() - startTime;
      MDC.clear();
      log.info(
        "ip={}, uri={} {}, status={}, duration={}ms,",
        servletRequest.getRemoteAddr(),
        containerRequestContext.getMethod(),
        servletRequest.getRequestURI(),
        containerResponseContext.getStatus(),
        executionTime
      );
    } catch (RuntimeException e) {
      log.warn("Error while logging request data.", e);
    }
  }

  @Override
  public void filter(ContainerRequestContext containerRequestContext) {
    MDC.put("start-time", String.valueOf(System.currentTimeMillis()));
  }
}
