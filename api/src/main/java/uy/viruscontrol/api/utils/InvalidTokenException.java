package uy.viruscontrol.api.utils;

public class InvalidTokenException extends Exception {

  public InvalidTokenException(String s) {
    super(s);
  }
}
