package uy.viruscontrol.api;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum AppProperties {
  INSTANCE;

  private final Properties properties;

  AppProperties() {
    properties = new Properties();
    try {
      properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
    } catch (IOException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
    }
  }

  public String getFirebaseKey() {
    return properties.getProperty("firebase.key");
  }

  public String getJwtSecret() {
    return properties.getProperty("jwt.secret");
  }

  public String getJwtIssuer() {
    return properties.getProperty("jwt.issuer");
  }
}
