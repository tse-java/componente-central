function toggleSideBar() {
  if (document.getElementById("mySidebar").style.width === "300px") {
    document.getElementById("mySidebar").style.width = "0px";
    document.getElementById("main").style.marginLeft = "0px";
    document.getElementById("main").style.width =
      "calc(100% - 300px) !important";
  } else {
    document.getElementById("mySidebar").style.width = "300px";
    document.getElementById("main").style.marginLeft = "300px";
    document.getElementById("main").style.width = "100% !important";
  }
}
