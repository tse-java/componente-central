$(document).ready(
  fetchCases(1, "https://api.viruscontrol.tech/departments/cases")
);
function fetchCases(
  diseaseId,
  url = "https://api.viruscontrol.tech/departments/cases"
) {
  console.log(diseaseId, " ----- ");
  $.ajax({
    url: url,
    dataType: "json",
    data: {
      diseaseId: diseaseId,
    },
    success: function (data) {
      console.log(data);
      renderMap(data);
    },
    error: function (jqXHR, textStatus) {
      console.log(jqXHR);
      console.log(textStatus);
    },
  });
}

function renderMap(data) {
  anychart.onDocumentReady(function () {
    const mapData = [
      { id: "UY.AR", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.CA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.CL", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.CO", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.DU", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.FS", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.FD", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.LA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.MA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.MO", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.PA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.RN", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.RV", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.RO", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.SA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.SJ", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.SO", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.TA", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
      { id: "UY.TT", CURED: 0, SUSPECT: 0, CONFIRMED: 0 },
    ];
    data.forEach((data) => {
      mapData[data.id - 1].cases = 0;
      data.cases.forEach((caseData) => {
        mapData[data.id - 1][caseData.type] = caseData.quantity;
        if (caseData.type != "DISCARDED") {
          mapData[data.id - 1].cases += caseData.quantity;
        }
      });
    });
    console.table(mapData.filter((value) => value.cases > 0));
    const dataSet = anychart.data.set(
      mapData.filter((value) => value.cases > 0)
    );
    const data1 = dataSet.mapAs({ id: "id", size: "cases" });
    const map = anychart.map();

    // allow zoom
    map.interactivity().zoomOnMouseWheel(true);
    map.interactivity().keyboardZoomAndMove(true);
    map.interactivity().zoomOnDoubleClick(true);
    map.minBubbleSize("0.5%").maxBubbleSize("6%");

    map.geoData(anychart.maps.uruguay);

    // set the series
    const series = map.bubble(data1);
    series.labels(false);
    series.tooltip().titleFormat("Cases");
    series
      .tooltip()
      .format(
        "Sospechosos: {%SUSPECT}\nCurados: {%CURED}\nActivos: {%CONFIRMED}"
      );

    // set the container
    $("#container").remove();
    $("<div>", {
      id: "container",
      css: { width: "700px", height: "600px" },
    }).appendTo("#parent-container");
    map.container("container");
    map.draw();
  });
}
