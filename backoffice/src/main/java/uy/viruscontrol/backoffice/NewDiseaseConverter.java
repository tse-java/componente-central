package uy.viruscontrol.backoffice;

import java.util.List;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uy.viruscontrol.common.dto.DTOSymptom;

@FacesConverter("NewDiseaseConverter")
public class NewDiseaseConverter implements Converter {

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
    if (value == null) {
      return null;
    }
    //getting managed bean
    Application application = context.getApplication();
    RequestNewDiseaseBean requestNewDiseaseBean = application.evaluateExpressionGet(
      context,
      "#{requestNewDiseaseBean}",
      RequestNewDiseaseBean.class
    );

    List<DTOSymptom> listFromBean = requestNewDiseaseBean.getSymptoms();
    //returning a null or not null DTOSyntom
    return listFromBean.stream().parallel().filter(dtoSymptom -> dtoSymptom.getName().equals(value)).findFirst().orElse(null);
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value != null) {
      DTOSymptom dtoValue = (DTOSymptom) value;
      return dtoValue.getName();
    } else {
      return null;
    }
  }
}
