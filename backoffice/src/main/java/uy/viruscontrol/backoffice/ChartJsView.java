package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;
import uy.viruscontrol.backoffice.dto.DTOCasesQuantities;
import uy.viruscontrol.business.service.CaseService;
import uy.viruscontrol.business.service.DiseaseService;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Named
@ViewScoped
@Getter
@Setter
@Slf4j
public class ChartJsView implements Serializable {
  @EJB
  private CaseService caseService;

  @Inject
  @Push(channel = "donut")
  private PushContext push;

  @EJB
  private DiseaseService diseaseService;

  private DonutChartModel donutModel;
  private List<EnumCaseStatus> caseStatusList = new ArrayList<>();
  private List<DTODepartment> departmentsData = new ArrayList<>();
  private Long selectedDisease;
  private List<DTODisease> diseases = new ArrayList<>();
  private DTOCasesQuantities casesTotals = new DTOCasesQuantities();

  @PostConstruct
  public void init() {
    caseStatusList.add(EnumCaseStatus.CURED);
    caseStatusList.add(EnumCaseStatus.SUSPECT);
    caseStatusList.add(EnumCaseStatus.CONFIRMED);
    diseases = diseaseService.getDiseasesByStatus(EnumRequestStatus.APPROVED);
    if (selectedDisease == null) selectedDisease = !diseases.isEmpty() ? diseases.get(0).getId() : null;
    handleCasesData();
  }

  public void handleCasesData() {
    this.casesTotals = new DTOCasesQuantities();
    caseService
      .getCasesGroupByDepartment(selectedDisease, null)
      .forEach(
        dtoDepartment -> {
          dtoDepartment
            .getCases()
            .forEach(
              dtoCasesQuantity -> {
                switch (dtoCasesQuantity.getType()) {
                  case CONFIRMED:
                    {
                      this.casesTotals.setConfirmedTotal(this.casesTotals.getConfirmedTotal() + dtoCasesQuantity.getQuantity());
                      break;
                    }
                  case SUSPECT:
                    {
                      this.casesTotals.setSuspectTotal(this.casesTotals.getSuspectTotal() + dtoCasesQuantity.getQuantity());
                      break;
                    }
                  case CURED:
                    {
                      this.casesTotals.setCuredTotal(this.casesTotals.getCuredTotal() + dtoCasesQuantity.getQuantity());
                      break;
                    }
                }
              }
            );
        }
      );
    createDonutModel();
  }

  public List<Number> setValuesToDonut() {
    List<Number> values = new ArrayList<>();
    if (this.casesTotals.getConfirmedTotal() == 0 && this.casesTotals.getCuredTotal() == 0 && this.casesTotals.getSuspectTotal() == 0) {
      values.add(1L);
    } else {
      values.add(this.casesTotals.getSuspectTotal());
      values.add(this.casesTotals.getCuredTotal());
      values.add(this.casesTotals.getConfirmedTotal());
    }
    return values;
  }

  public List<String> setStylesToDonut() {
    List<String> styles = new ArrayList<>();
    if (this.casesTotals.getConfirmedTotal() == 0 && this.casesTotals.getCuredTotal() == 0 && this.casesTotals.getSuspectTotal() == 0) {
      styles.add("rgb(193, 198, 193)");
    } else {
      styles.add("rgb(232, 49, 49)");
      styles.add("rgb(45, 149, 40)");
      styles.add("rgb(255, 205, 86)");
    }
    return styles;
  }

  public List<String> setLabelsToDonut() {
    List<String> labels = new ArrayList<>();
    if (this.casesTotals.getConfirmedTotal() == 0 && this.casesTotals.getCuredTotal() == 0 && this.casesTotals.getSuspectTotal() == 0) {
      labels.add("No hay casos");
    } else {
      labels.add("Sospechosos");
      labels.add("Curados");
      labels.add("Confirmados");
    }
    return labels;
  }

  public void createDonutModel() {
    donutModel = new DonutChartModel();
    ChartData data = new ChartData();

    DonutChartDataSet dataSet = new DonutChartDataSet();
    List<Number> values = setValuesToDonut();
    dataSet.setData(values);

    List<String> bgColors = setStylesToDonut();
    dataSet.setBackgroundColor(bgColors);

    data.addChartDataSet(dataSet);
    List<String> labels = setLabelsToDonut();
    data.setLabels(labels);

    donutModel.setData(data);
    push.send(selectedDisease);
  }

  public DonutChartModel getDonutModel() {
    return donutModel;
  }

  public void setDonutModel(DonutChartModel donutModel) {
    this.donutModel = donutModel;
  }
}
