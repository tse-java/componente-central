package uy.viruscontrol.backoffice;

import java.util.List;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uy.viruscontrol.common.dto.DTOResource;

@FacesConverter("NewResourceConverter")
public class NewResourceConverter implements Converter {

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
    if (value == null) {
      return null;
    }

    //getting managed bean
    Application application = context.getApplication();
    RequestNewDiseaseBean requestNewDiseaseBean = application.evaluateExpressionGet(
      context,
      "#{requestNewDiseaseBean}",
      RequestNewDiseaseBean.class
    );

    List<DTOResource> listFromBean = requestNewDiseaseBean.getResources();

    //returning a null or not null DTOSymptom
    return listFromBean.stream().parallel().filter(dtoSymptom -> dtoSymptom.getName().equals(value)).findFirst().orElse(null);
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value != null) {
      DTOResource dtoValue = (DTOResource) value;
      return dtoValue.getName();
    } else {
      return null;
    }
  }
}
