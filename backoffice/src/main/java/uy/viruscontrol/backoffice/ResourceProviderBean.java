package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.common.dto.DTOResourceProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
public class ResourceProviderBean implements Serializable {
  @EJB
  private ResourceProviderService resourceProviderService;

  private DTOResourceProvider dtoCreateResourceProvider = new DTOResourceProvider();
  private DTOResourceProvider dtoSelectedResourceProviderView = new DTOResourceProvider();
  private DTOResourceProvider dtoSelectedResourceProviderUpdate = new DTOResourceProvider();
  private DTOResourceProvider dtoSelectedResourceProviderDelete = new DTOResourceProvider();
  private List<DTOResourceProvider> resourceProviders;
  private static final String HIDE_MODAL = "PF('updateResourceModal').hide();";

  @PostConstruct
  public void init() {
    resourceProviders = resourceProviderService.getResourceProviders(null);
  }

  public void createResource() {
    try {
      DTOResourceProvider returnDTOCreate = resourceProviderService.newResourceProvider(
        dtoCreateResourceProvider.getName(),
        dtoCreateResourceProvider.getUrl()
      );
      dtoCreateResourceProvider.setName("");
      dtoCreateResourceProvider.setUrl("");
      resourceProviders = resourceProviderService.getResourceProviders(null);
      if (returnDTOCreate.getCommercialName() != null && !returnDTOCreate.getCommercialName().equals("")) {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('createResourceModal').hide();");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Proveedor creado", "El proveedor ha sido creado con exito"));
      } else {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('createResourceModal').hide();");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Proveedor creado", "El proveedor ha sido creado pero no esta en estado funcional"));
      }
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Faltan completar campos", null));
    } catch (EJBTransactionRolledbackException ejb) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la URL", "Por favor, ingrese una valida"));
    }
  }

  public void updateResource() {
    try {
      DTOResourceProvider returnDTOUpdate = resourceProviderService.updateResourceProvider(
        dtoSelectedResourceProviderUpdate.getId(),
        dtoSelectedResourceProviderUpdate.getName(),
        dtoSelectedResourceProviderUpdate.getUrl()
      );
      resourceProviders = resourceProviderService.getResourceProviders(null);
      if (returnDTOUpdate.getCommercialName() != null && !returnDTOUpdate.getCommercialName().equals("")) {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript(HIDE_MODAL);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Proveedor modificado", "El proveedor ha sido modificado con exito"));
      } else {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript(HIDE_MODAL);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(
          null,
          new FacesMessage("Proveedor modificado", "El proveedor ha sido modificado pero no esta en estado funcional")
        );
      }
    } catch (VirusControlException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", "Error de conexion, por favor intentelo nuevamente")
        );
      PrimeFaces current = PrimeFaces.current();
      current.executeScript(HIDE_MODAL);
    } catch (EJBTransactionRolledbackException ejb) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error en la URL", "Por favor, ingrese una valida"));
    }
  }

  public void deleteResource() {
    try {
      resourceProviderService.deleteResourceProvider(dtoSelectedResourceProviderDelete.getId());
      PrimeFaces current = PrimeFaces.current();
      resourceProviders = resourceProviderService.getResourceProviders(null);
      current.executeScript("PF('deleteResourceProviderModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Proveedor eliminado", "El proveedor ha sido eliminado"));
    } catch (VirusControlException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ha ocurrido un error", "Error de conexion, por favor intentelo nuevamente")
        );
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('deleteResourceProviderModal').hide();");
    }
  }
}
