package uy.viruscontrol.backoffice.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uy.viruscontrol.backoffice.dto.DTORegisterRequest;
import uy.viruscontrol.backoffice.dto.DTORegisterResponse;

public interface HealthService {
  @POST("register")
  Call<DTORegisterResponse> register(@Body DTORegisterRequest dtoRegisterRequest);
}
