package uy.viruscontrol.backoffice.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import uy.viruscontrol.backoffice.dto.DTORegisterRequest;
import uy.viruscontrol.backoffice.dto.DTORegisterResponse;

public interface ExamService {
  @POST("register")
  Call<DTORegisterResponse> register(@Body DTORegisterRequest dtoRegisterRequest);

  @GET("register")
  Call<Void> verifyToken(@Header(value = "X-Authorization") String token);
}
