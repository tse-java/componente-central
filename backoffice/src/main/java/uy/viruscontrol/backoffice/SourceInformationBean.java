package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.InformationSourceService;
import uy.viruscontrol.common.dto.DTOInformationSource;
import uy.viruscontrol.common.dto.DTORSSInformationSource;
import uy.viruscontrol.common.dto.DTOTwitterInformationSource;
import uy.viruscontrol.common.enumerations.EnumInformationSrcType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
@Slf4j
public class SourceInformationBean implements Serializable {
  @EJB
  private InformationSourceService informationSourceService;

  private List<DTOInformationSource> sources;
  private EnumInformationSrcType srcTypes;

  private DTOTwitterInformationSource createTwitterSource;
  private DTORSSInformationSource createRssSource;

  private DTOInformationSource updateSource;
  private DTOTwitterInformationSource updateTwitterSource;
  private DTORSSInformationSource updateRssSource;

  private DTOInformationSource viewSource;
  private DTOTwitterInformationSource viewTwitterSource;
  private DTORSSInformationSource viewRssSource;

  private DTOInformationSource deleteSource;
  private static final String ERROR_PROCESSING_DATA = "Error while processing data: {}";

  @PostConstruct
  public void init() {
    sources = informationSourceService.getInformationSourceList();
    createTwitterSource = new DTOTwitterInformationSource();
    createRssSource = new DTORSSInformationSource();
  }

  public void handleViewSource(DTOInformationSource viewSource) {
    if (viewSource instanceof DTOTwitterInformationSource) {
      this.viewTwitterSource = (DTOTwitterInformationSource) viewSource;
    } else {
      this.viewRssSource = (DTORSSInformationSource) viewSource;
    }
  }

  public void handleUpdateSource(DTOInformationSource updateSource) {
    if (updateSource instanceof DTOTwitterInformationSource) {
      this.updateTwitterSource = (DTOTwitterInformationSource) updateSource;
    } else {
      this.updateRssSource = (DTORSSInformationSource) updateSource;
    }
  }

  public String handleSourceTypeShow(DTOInformationSource dtoSource) {
    String returnValue = "";
    try {
      DTOInformationSource returnDto = sources
        .stream()
        .parallel()
        .filter(dtoInformationSource -> dtoInformationSource.getId().equals(dtoSource.getId()))
        .findFirst()
        .orElse(null);
      if (returnDto instanceof DTOTwitterInformationSource) {
        returnValue = "Twitter";
      } else {
        returnValue = "RSS";
      }
    } catch (NullPointerException e) {
      log.error(ERROR_PROCESSING_DATA, e.getMessage());
    }
    return returnValue;
  }

  public String handleViewModalShow(DTOInformationSource dtoSource) {
    String returnValue = "";
    try {
      DTOInformationSource returnDto = sources
        .stream()
        .parallel()
        .filter(dtoInformationSource -> dtoInformationSource.getId().equals(dtoSource.getId()))
        .findFirst()
        .orElse(null);
      if (returnDto instanceof DTOTwitterInformationSource) {
        returnValue = "viewTwitterModal";
      } else {
        returnValue = "viewRssModal";
      }
    } catch (NullPointerException e) {
      log.error(ERROR_PROCESSING_DATA, e.getMessage());
    }
    return returnValue;
  }

  public String handleUpdateModalShow(DTOInformationSource dtoSource) {
    String returnValue = "";
    try {
      DTOInformationSource returnDto = sources
        .stream()
        .parallel()
        .filter(dtoInformationSource -> dtoInformationSource.getId().equals(dtoSource.getId()))
        .findFirst()
        .orElse(null);
      if (returnDto instanceof DTOTwitterInformationSource) {
        returnValue = "updateTwitterModal";
      } else {
        returnValue = "updateRssModal";
      }
    } catch (NullPointerException e) {
      log.error(ERROR_PROCESSING_DATA, e.getMessage());
    }
    return returnValue;
  }

  public String handleViewFormShow(DTOInformationSource dtoSource) {
    String returnValue = "";
    try {
      DTOInformationSource returnDto = sources
        .stream()
        .parallel()
        .filter(dtoInformationSource -> dtoInformationSource.getId().equals(dtoSource.getId()))
        .findFirst()
        .orElse(null);
      if (returnDto instanceof DTOTwitterInformationSource) {
        returnValue = "viewTwitterForm";
      } else {
        returnValue = "viewRssForm";
      }
    } catch (NullPointerException e) {
      log.error(ERROR_PROCESSING_DATA, e.getMessage());
    }
    return returnValue;
  }

  public String handleUpdateFormShow(DTOInformationSource dtoSource) {
    String returnValue = "";
    try {
      DTOInformationSource returnDto = sources
        .stream()
        .parallel()
        .filter(dtoInformationSource -> dtoInformationSource.getId().equals(dtoSource.getId()))
        .findFirst()
        .orElse(null);
      if (returnDto instanceof DTOTwitterInformationSource) {
        returnValue = "updateTwitterForm";
      } else {
        returnValue = "updateRssForm";
      }
    } catch (NullPointerException e) {
      log.error(ERROR_PROCESSING_DATA, e.getMessage());
    }
    return returnValue;
  }

  public void createTwitterSource() {
    try {
      informationSourceService.newTwitterSource(
        createTwitterSource.getName(),
        createTwitterSource.getDescription(),
        createTwitterSource.getUsername()
      );
      createTwitterSource.setName("");
      createTwitterSource.setDescription("");
      createTwitterSource.setUsername("");
      sources = informationSourceService.getInformationSourceList();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('addTwitterModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Fuente agregada", "La fuente ha sido agregada correctamente"));
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
    }
  }

  public void createRssSource() {
    try {
      informationSourceService.newRSSFeed(
        createRssSource.getName(),
        createRssSource.getDescription(),
        createRssSource.getUrl(),
        createRssSource.getKeyWords()
      );
      createRssSource.setName("");
      createRssSource.setDescription("");
      createRssSource.setUrl("");
      createRssSource.getKeyWords().clear();
      sources = informationSourceService.getInformationSourceList();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('addRssModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Fuente agregada", "La fuente ha sido agregada correctamente"));
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
    }
  }

  public void updateTwitterSource() {
    try {
      informationSourceService.editTwitterSource(updateTwitterSource);
      updateTwitterSource.setName("");
      updateTwitterSource.setDescription("");
      updateTwitterSource.setUsername("");
      sources = informationSourceService.getInformationSourceList();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('updateTwitterModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Fuente Modificada", "La fuente ha sido modificada correctamente"));
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
    }
  }

  public void updateRssSource() {
    try {
      informationSourceService.editRSSFeed(updateRssSource);
      updateRssSource.setName("");
      updateRssSource.setDescription("");
      updateRssSource.setUrl("");
      updateRssSource.getKeyWords().clear();
      sources = informationSourceService.getInformationSourceList();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('updateRssModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Fuente Modificada", "La fuente ha sido modificada correctamente"));
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
    }
  }

  public void deleteResource() {
    try {
      informationSourceService.deleteInformationSource(deleteSource.getId());
      sources = informationSourceService.getInformationSourceList();
      deleteSource.setName("");
      deleteSource.setDescription("");
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('deleteSourceModal').hide();");
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
    }
  }
}
