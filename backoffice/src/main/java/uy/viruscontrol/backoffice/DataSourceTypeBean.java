package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import uy.viruscontrol.business.service.InformationSourceTypeService;
import uy.viruscontrol.common.dto.DTOInformationSourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
public class DataSourceTypeBean implements Serializable {
  @EJB
  private InformationSourceTypeService dataSourceTypeService;

  private DTOInformationSourceType dtoDataSourceType = new DTOInformationSourceType();
  private DTOInformationSourceType selectedDataSourceTypeUpdate = new DTOInformationSourceType();
  private DTOInformationSourceType selectedDataSourceTypeView = new DTOInformationSourceType();
  private List<DTOInformationSourceType> dataSourceTypes = new ArrayList<>();
  private String status = "";

  @PostConstruct
  public void init() {
    dataSourceTypes = dataSourceTypeService.getInformationSourceTypes(null);
  }

  public void handleBooleanChange() {
    FacesContext
      .getCurrentInstance()
      .addMessage(
        null,
        new FacesMessage(
          "Operacion relizada correctamente",
          selectedDataSourceTypeUpdate.getActive()
            ? "El tipo de fuente ha sido actualizado a activo"
            : "El tipo de fuente ha sido actualizado a inactivo"
        )
      );
  }

  public String activeTextBadge(Boolean isActive) {
    return isActive ? "Activa" : "Inactiva";
  }

  public String activeTextButton(Boolean isActive) {
    return isActive ? "Desactivar" : "Activar";
  }

  public String handleButtonStyleChange(Boolean isActive) {
    return isActive ? "blue-grey" : "blue-grey lighten-3";
  }

  public void setInformationSourceTypeStatus() {
    try {
      dataSourceTypeService.setInformationSourceTypeStatus(selectedDataSourceTypeUpdate.getId(), !selectedDataSourceTypeUpdate.getActive());
      handleBooleanChange();
      dataSourceTypes
        .stream()
        .filter(dtoInformationSourceType -> dtoInformationSourceType.getId().equals(selectedDataSourceTypeUpdate.getId()))
        .findFirst()
        .ifPresent(item -> item.setActive(!selectedDataSourceTypeUpdate.getActive()));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error.", null));
    }
  }
}
