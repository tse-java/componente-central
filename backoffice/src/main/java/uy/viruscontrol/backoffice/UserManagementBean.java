package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.BackProfileService;
import uy.viruscontrol.common.dto.DTOBackProfile;
import uy.viruscontrol.common.dto.DTONewBackProfile;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
public class UserManagementBean implements Serializable {
  @EJB
  BackProfileService backProfileService;

  private String searchText = "";
  private List<String> userRoles;
  private DTONewBackProfile dtoCreateProfile = new DTONewBackProfile();
  private DTONewBackProfile dtoUpdateProfile = new DTONewBackProfile();
  private DTOBackProfile selectedProfileEdit = new DTOBackProfile();
  private DTOBackProfile selectedProfileDelete = new DTOBackProfile();
  private List<DTOBackProfile> users;
  private List<DTOBackProfile> usersCopy;
  private static final String SOMETHING_HAPPENED = "Ocurrio un error.";

  @PostConstruct
  public void init() {
    userRoles =
      Arrays
        .stream(EnumBackofficeUserRole.values())
        .map(enumBackofficeUserRole -> capitalize(enumBackofficeUserRole.name()))
        .collect(Collectors.toList());
    users = backProfileService.listUsers();
    usersCopy = users;
  }

  public EnumBackofficeUserRole[] getEnumUserRoles() {
    return EnumBackofficeUserRole.values();
  }

  public String capitalize(String string) {
    return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
  }

  public void search() {
    if (searchText.isEmpty()) {
      usersCopy = users;
    } else {
      usersCopy =
        users
          .stream()
          .filter(dtoBackProfile -> dtoBackProfile.getNickname().toLowerCase().contains(searchText.toLowerCase()))
          .collect(Collectors.toList());
    }
  }

  public void createProfile() {
    try {
      backProfileService.newProfile(this.dtoCreateProfile);
      dtoCreateProfile.setNickname("");
      dtoCreateProfile.setFullName("");
      dtoCreateProfile.setPassword("");
      dtoCreateProfile.setRole(null);
      users = backProfileService.listUsers();
      usersCopy = users;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('addUserModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Usuario creado", "El usuario ha sido creado exitosamente"));
    } catch (VirusControlException virusControlException) {
      if (virusControlException.getCode().equals(2)) {
        FacesContext
          .getCurrentInstance()
          .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nickname duplicado", "Por favor ingrese otro nickname"));
      } else {
        FacesContext
          .getCurrentInstance()
          .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, SOMETHING_HAPPENED, virusControlException.getMessage()));
      }
    }
  }

  public void updateProfile() {
    try {
      dtoUpdateProfile.setId(selectedProfileEdit.getId());
      dtoUpdateProfile.setNickname(selectedProfileEdit.getNickname());
      dtoUpdateProfile.setFullName(selectedProfileEdit.getFullName());
      dtoUpdateProfile.setRole(selectedProfileEdit.getRole());

      if (dtoUpdateProfile.getNickname() == null || dtoUpdateProfile.getNickname().equals("")) {
        dtoUpdateProfile.setNickname(selectedProfileEdit.getNickname());
      }

      if (dtoUpdateProfile.getFullName() == null || dtoUpdateProfile.getFullName().equals("")) {
        dtoUpdateProfile.setFullName(selectedProfileEdit.getFullName());
      }

      if (dtoUpdateProfile.getRole() == null) {
        dtoUpdateProfile.setRole(selectedProfileEdit.getRole());
      }

      backProfileService.updateUser(this.dtoUpdateProfile);

      dtoUpdateProfile.setFullName("");
      dtoUpdateProfile.setNickname("");
      dtoUpdateProfile.setPassword("");
      dtoUpdateProfile.setRole(null);
      users = backProfileService.listUsers();
      usersCopy = users;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('modifyUserModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Usuario modificado", "El usuario ha sido modificado exitosamente"));
    } catch (VirusControlException virusControlException) {
      if (virusControlException.getCode().equals(2)) {
        FacesContext
          .getCurrentInstance()
          .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nickname duplicado", "Por favor ingrese otro nickname"));
      } else {
        FacesContext
          .getCurrentInstance()
          .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, SOMETHING_HAPPENED, virusControlException.getMessage()));
      }
    }
  }

  public void deleteProfile() {
    try {
      backProfileService.delete(selectedProfileDelete.getId());
      users = backProfileService.listUsers();
      usersCopy = users;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('deleteUserModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Usuario eliminado", "El usuario ha sido eliminado exitosamente"));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, SOMETHING_HAPPENED, null));
    }
  }
}
