package uy.viruscontrol.backoffice;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Data;

@Named
@SessionScoped
@Data
public class NavigationBean implements Serializable {
  private String page = "dashboard.xhtml";
  private Boolean logged = false;
}
