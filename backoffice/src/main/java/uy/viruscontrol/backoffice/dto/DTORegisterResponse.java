package uy.viruscontrol.backoffice.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTORegisterResponse implements Serializable {
  private String token;
}
