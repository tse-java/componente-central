package uy.viruscontrol.backoffice.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTORegisterRequest implements Serializable {
  private String description;
}
