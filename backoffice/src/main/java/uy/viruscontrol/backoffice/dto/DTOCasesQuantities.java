package uy.viruscontrol.backoffice.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class DTOCasesQuantities implements Serializable {
  private Long suspectTotal = 0L;
  private Long curedTotal = 0L;
  private Long confirmedTotal = 0L;
}
