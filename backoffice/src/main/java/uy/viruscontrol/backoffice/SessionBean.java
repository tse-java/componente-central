package uy.viruscontrol.backoffice;

import com.google.cloud.firestore.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.cloud.FirestoreClient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.business.service.BackProfileService;
import uy.viruscontrol.common.dto.DTOBackProfile;
import uy.viruscontrol.common.dto.DTONewBackProfile;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@SessionScoped
@Data
@Slf4j
public class SessionBean implements Serializable {
  @EJB
  private BackProfileService backProfileService;

  @Inject
  private NavigationBean navigationBean;

  @Inject
  @Push(channel = "push")
  private PushContext push;

  private String nickname;
  private String password;
  private DTOBackProfile dtoBackProfile;
  private Integer notis = 0;
  private Set<DTONotifyMedicalCaseStatus> notificationData = new HashSet<>();
  private DTONotifyMedicalCaseStatus markNotificationAsRead;
  private String oldPassword;
  private String newPassword;

  public SessionBean() {}

  @PostConstruct
  public void createUser() {
    try {
      DTONewBackProfile backProfileAdmin = new DTONewBackProfile();
      DTONewBackProfile backProfileSuper = new DTONewBackProfile();

      backProfileAdmin.setNickname("admin");
      backProfileAdmin.setFullName("Juan Perez");
      backProfileAdmin.setPassword("admin");
      backProfileAdmin.setRole(EnumBackofficeUserRole.ADMINISTRATOR);

      backProfileSuper.setNickname("super");
      backProfileSuper.setFullName("Sergio Denis");
      backProfileSuper.setPassword("super");
      backProfileSuper.setRole(EnumBackofficeUserRole.SUPERVISOR);

      backProfileService.newProfile(backProfileAdmin);
      backProfileService.newProfile(backProfileSuper);
    } catch (EJBException | VirusControlException e) {
      log.error("Session error: {}", e.getMessage());
    }
  }

  public void changeUserPassword() {
    try {
      backProfileService.changePassword(this.dtoBackProfile.getNickname(), this.oldPassword, this.newPassword);
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('userDataModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Contraseña editada", null));
    } catch (VirusControlException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Oops, ocurrio un error", null));
    }
  }

  public void doLogin() {
    try {
      dtoBackProfile = backProfileService.doLogin(nickname, password);
      navigationBean.setLogged(true);
      aVoid();
    } catch (EJBException | VirusControlException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario o contraseña incorrectos", null));
    }
  }

  public void doLogout() {
    navigationBean.setLogged(false);
    FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
  }

  public void markAsRead() {
    Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
    try {
      db.collection("statusNotifications").document(markNotificationAsRead.getId()).update("read", true).get();
      notificationData.remove(markNotificationAsRead);
      notis--;
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  public void aVoid() {
    Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
    db
      .collection("statusNotifications")
      .whereEqualTo("externalId", this.nickname)
      .whereEqualTo("read", false)
      .addSnapshotListener(
        (snapshot, e) -> {
          if (e != null) {
            log.error("Listen failed: {}", e.getMessage());
            return;
          }
          if (snapshot != null) {
            snapshot
              .getDocumentChanges()
              .forEach(
                documentChange -> {
                  DTONotifyMedicalCaseStatus dto = documentChange.getDocument().toObject(DTONotifyMedicalCaseStatus.class);
                  if (documentChange.getType().equals(DocumentChange.Type.ADDED)) {
                    this.notis++;
                    dto.setId(documentChange.getDocument().getId());
                    notificationData.add(dto);
                  }
                }
              );
            //notifica a jsf
            push.send("updateNotifications");
          }
        }
      );
  }
}
