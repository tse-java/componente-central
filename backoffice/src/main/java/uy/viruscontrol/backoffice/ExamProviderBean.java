package uy.viruscontrol.backoffice;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.PrimeFaces;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import uy.viruscontrol.backoffice.dto.DTORegisterRequest;
import uy.viruscontrol.backoffice.dto.DTORegisterResponse;
import uy.viruscontrol.backoffice.service.ExamService;
import uy.viruscontrol.business.service.ExamProviderService;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Slf4j
@Named
@ViewScoped
@Getter
@Setter
public class ExamProviderBean implements Serializable {
  @EJB
  private ExamProviderService examProviderService;

  private String description; //Description que se setea en el dto correspondiente si tod0 sale bien
  private String token = "";
  private DTOExamProvider dtoCreateExamProvider = new DTOExamProvider();
  private DTOExamProvider selectedExamProviderView = new DTOExamProvider();
  private DTOExamProvider selectedExamProviderUpdate = new DTOExamProvider();
  private DTOExamProvider selectedExamProviderDelete = new DTOExamProvider();
  private List<DTOExamProvider> examProviders;
  private static final String INVALID_URL = "URL incorrecta";

  @PostConstruct
  public void init() {
    examProviders = examProviderService.listLabs();
  }

  private String registerExamProviderCreate() {
    String returnTokenCreate = "";
    try {
      Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(dtoCreateExamProvider.getUrl()) //paso la url de mi dtCreate o de mi dtUpdate
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
      ExamService examService = retrofit.create(ExamService.class);

      DTORegisterRequest dtoRegisterRequest = new DTORegisterRequest();
      dtoRegisterRequest.setDescription(description);
      Response<DTORegisterResponse> response = examService.register(dtoRegisterRequest).execute();
      if (response.isSuccessful()) {
        returnTokenCreate = response.body().getToken();
        log.info("Token: {}", returnTokenCreate);
      } else {
        FacesContext
          .getCurrentInstance()
          .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, INVALID_URL, "Ingrese una url valida"));
      }
    } catch (IOException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error inesperado", null));
    } catch (IllegalArgumentException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, INVALID_URL, "Ingrese una url valida"));
    }
    return returnTokenCreate;
  }

  private String registerExamProviderUpdate() {
    String registerToken = selectedExamProviderUpdate.getToken();
    try {
      Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(selectedExamProviderUpdate.getUrl())
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
      ExamService examService = retrofit.create(ExamService.class);
      if (!examService.verifyToken(registerToken).execute().isSuccessful()) {
        DTORegisterRequest dtoRegisterRequest = new DTORegisterRequest();
        dtoRegisterRequest.setDescription(description);
        Response<DTORegisterResponse> response = examService.register(dtoRegisterRequest).execute();
        if (response.isSuccessful()) {
          registerToken = response.body().getToken();
        } else {
          FacesContext
            .getCurrentInstance()
            .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, INVALID_URL, "Ingrese una url valida"));
        }
      }
    } catch (IOException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error inesperado", null));
    } catch (IllegalArgumentException e) {
      FacesContext
        .getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, INVALID_URL, "Ingrese una url valida"));
    }
    return registerToken;
  }

  public void createExamProvider() {
    this.token = registerExamProviderCreate();
    if (this.token != null && !this.token.isEmpty()) {
      dtoCreateExamProvider.setToken(this.token);
      examProviderService.addLab(dtoCreateExamProvider);
      examProviders = examProviderService.listLabs();
      this.description = "";
      this.token = "";
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('createExamModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Proveedor creado", "El proveedor ha sido creado exitosamente"));
    } else {
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Url invalida", "La url ingresada no es correcta, por favor ingrese otra")
        );
    }
  }

  public void updateExamProvider() {
    this.token = registerExamProviderUpdate();
    if (this.token != null && !this.token.isEmpty()) {
      selectedExamProviderUpdate.setToken(this.token);
      try {
        examProviderService.updateLab(selectedExamProviderUpdate);
        examProviders = examProviderService.listLabs();
        this.description = "";
        this.token = "";
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('updateExamModal').hide();");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Proveedor modificado", "El proveedor ha sido modificado exitosamente"));
      } catch (VirusControlException e) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Laboratorio no existe", null));
      }
    } else {
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Url invalida", "La url ingresada no es correcta, por favor ingrese otra")
        );
    }
  }
}
