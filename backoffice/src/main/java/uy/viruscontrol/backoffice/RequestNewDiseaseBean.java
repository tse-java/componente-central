package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.service.DiseaseService;
import uy.viruscontrol.business.service.ResourceService;
import uy.viruscontrol.business.service.SymptomService;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOResource;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.enumerations.EnumInfectiousAgentType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@RequestScoped
@Getter
@Setter
@Slf4j
public class RequestNewDiseaseBean implements Serializable {
  @EJB
  private DiseaseService diseaseService;

  @EJB
  private SymptomService symptomService;

  @EJB
  private ResourceService resourcesService;

  private String diseaseName;
  private Long infectionTime;
  private String symptomName;
  private String symptomDescription;
  private String agentName;
  private EnumInfectiousAgentType agentType;
  private List<String> agentTypes = new ArrayList<>();
  private List<DTOSymptom> symptoms = new ArrayList<>();
  private List<DTOSymptom> selectedSymptoms = new ArrayList<>();
  private List<DTOResource> resources;
  private List<DTOResource> selectedResources = new ArrayList<>();

  @PostConstruct
  public void init() {
    loadSymptoms();
    listAgentTypes();
    loadResources();
  }

  public void listAgentTypes() {
    for (EnumInfectiousAgentType item : EnumInfectiousAgentType.values()) {
      agentTypes.add(capitalize(item.name()));
    }
  }

  public EnumInfectiousAgentType[] getEnumInfectiousAgentType() {
    return EnumInfectiousAgentType.values();
  }

  public void loadSymptoms() {
    symptoms = symptomService.getSymptoms();
  }

  public void loadResources() {
    resources = resourcesService.listResources();
  }

  public void createSymptom() {
    DTOSymptom dtoSymtom = new DTOSymptom();
    dtoSymtom.setName(symptomName);
    dtoSymtom.setDescription(symptomDescription);
    try {
      symptomService.newSymptom(dtoSymtom);
      symptoms.add(dtoSymtom);
      log.info("Se ha creado el sintoma.");
    } catch (EJBException | VirusControlException e) {
      log.error("Error: {}", e.getMessage());
    }
  }

  public void createDiseaseRequest() {
    DTODisease dtoDisease = new DTODisease();
    dtoDisease.setName(diseaseName);
    dtoDisease.setAgentName(agentName);
    dtoDisease.setAgentType(agentType);
    dtoDisease.setInfectionTime(infectionTime);
    dtoDisease.setRecommendedResources(selectedResources);
    dtoDisease.setSymptoms(selectedSymptoms);
    FacesContext context = FacesContext.getCurrentInstance();
    context.addMessage(null, new FacesMessage("Enfermedad creada", "La enfermedad ha sido creada exitosamente"));
    try {
      diseaseService.requestNewDisease(dtoDisease);
      log.info("Se ha creado el sintoma.");
    } catch (EJBException | VirusControlException e) {
      log.error("Error: {}", e.getMessage());
    }
  }

  public String capitalize(String string) {
    return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
  }
}
