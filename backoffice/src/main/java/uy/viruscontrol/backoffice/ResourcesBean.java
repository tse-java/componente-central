package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.ResourceService;
import uy.viruscontrol.common.dto.DTOResource;
import uy.viruscontrol.common.dto.DTOUpdateResource;
import uy.viruscontrol.common.enumerations.EnumResourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
public class ResourcesBean implements Serializable {
  @EJB
  private ResourceService resourceService;

  private String searchText = "";
  private DTOResource dtoResource = new DTOResource();
  private DTOUpdateResource dtoUpdateResource = new DTOUpdateResource();
  private DTOResource selectedResourceView = new DTOResource();
  private DTOResource selectedResourceUpdate = new DTOResource();
  private DTOResource selectedResourceDelete = new DTOResource();
  private List<String> resourceTypes;
  private List<DTOResource> resources;
  private List<DTOResource> resourcesCopy;

  @PostConstruct
  public void init() {
    resourceTypes =
      Arrays.stream(EnumResourceType.values()).map(enumResourceType -> capitalize(enumResourceType.name())).collect(Collectors.toList());
    resources = resourceService.listResources();
    resourcesCopy = resources;
  }

  public String capitalize(String string) {
    return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
  }

  public void updateResource() {
    try {
      dtoUpdateResource.setName(selectedResourceUpdate.getName());
      dtoUpdateResource.setDescription(selectedResourceUpdate.getDescription());
      dtoUpdateResource.setResourceType(selectedResourceUpdate.getResourceType());
      resourceService.updateResource(selectedResourceUpdate.getId(), dtoUpdateResource);
      dtoUpdateResource = new DTOUpdateResource();
      dtoUpdateResource.setName("");
      dtoUpdateResource.setDescription("");
      resources = resourceService.listResources();
      resourcesCopy = resources;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('updateResourceModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Recurso editado", "El recurso ha sido editado correctamente"));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error.", null));
    }
  }

  public EnumResourceType[] getEnumResourceTypes() {
    return EnumResourceType.values();
  }

  public void newResource() {
    resourceService.newResource(this.dtoResource);
    dtoResource.setName("");
    dtoResource.setDescription("");
    resources = resourceService.listResources();
    resourcesCopy = resources;
    PrimeFaces current = PrimeFaces.current();
    current.executeScript("PF('addResourceModal').hide();");
    FacesContext context = FacesContext.getCurrentInstance();
    context.addMessage(null, new FacesMessage("Recurso creado", "El recurso ha sido creado exitosamente"));
  }

  public void deleteResource() {
    try {
      resourceService.deleteResource(selectedResourceDelete.getId());
      dtoResource.setName("");
      dtoResource.setDescription("");
      resources = resourceService.listResources();
      resourcesCopy = resources;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('deleteResourceModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Recurso eliminado", "El recurso ha sido eliminado exitosamente"));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error.", null));
    }
  }

  public void search() {
    if (searchText.isEmpty()) {
      resourcesCopy = resources;
    } else {
      resourcesCopy =
        resources.stream().filter(item -> item.getName().toLowerCase().contains(searchText.toLowerCase())).collect(Collectors.toList());
    }
  }
}
