package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.xml.ws.WebServiceException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.HealthCareProviderService;
import uy.viruscontrol.common.dto.DTOHealthProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;
import uy.viruscontrol.cps.Service;
import uy.viruscontrol.cps.service.HealthProviderService;

@Slf4j
@Named
@ViewScoped
@Getter
@Setter
public class HealthCareProviderBean implements Serializable {
  @EJB
  private HealthCareProviderService healthCareProviderService;

  private String updateUrl;
  private DTOHealthProvider dtoCreateHealthCareProvider = new DTOHealthProvider();
  private DTOHealthProvider dtoSelectedHealthCareProviderView = new DTOHealthProvider();
  private DTOHealthProvider dtoSelectedHealthCareProviderUpdate = new DTOHealthProvider();
  private DTOHealthProvider dtoSelectedHealthCareProviderDelete = new DTOHealthProvider();
  private List<DTOHealthProvider> healthCareProviders;

  @PostConstruct
  public void init() {
    healthCareProviders = healthCareProviderService.getHealthProviders();
  }

  public void checkConnection(String path) throws MalformedURLException {
    URL url = new URL(path);
    Service service = new Service(url);
    HealthProviderService port = service.getPort();
    port.ping();
  }

  public void createHealthCareProvider() {
    try {
      checkConnection(dtoCreateHealthCareProvider.getUrl());
      healthCareProviderService.newHealthCareProvider(dtoCreateHealthCareProvider);
      dtoCreateHealthCareProvider.setName("");
      dtoCreateHealthCareProvider.setUrl("");
      healthCareProviders = healthCareProviderService.getHealthProviders();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('createHealthCareModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Proveedor creado", "El proveedor ha sido creado exitosamente"));
    } catch (MalformedURLException | WebServiceException e) {
      log.error("Error: {}", e.getMessage());
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Url invalida", "La url ingresada no es correcta, por favor ingrese otra")
        );
    }
  }

  public void updateHealthCareProvider() {
    try {
      checkConnection(dtoSelectedHealthCareProviderUpdate.getUrl());
      healthCareProviderService.editHealthProvider(
        dtoSelectedHealthCareProviderUpdate.getId(),
        dtoSelectedHealthCareProviderUpdate.getUrl(),
        dtoSelectedHealthCareProviderUpdate.getName()
      );
      dtoSelectedHealthCareProviderUpdate.setName("");
      dtoSelectedHealthCareProviderUpdate.setUrl("");
      healthCareProviders = healthCareProviderService.getHealthProviders();
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('editHealthProviderModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Proveedor modificado", "El proveedor ha sido modificado exitosamente"));
    } catch (MalformedURLException | WebServiceException | VirusControlException e) {
      log.error("Error: {}", e.getMessage());
      FacesContext
        .getCurrentInstance()
        .addMessage(
          null,
          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Url invalida", "La url ingresada no es correcta, por favor ingrese otra")
        );
    }
  }
}
