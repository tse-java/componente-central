package uy.viruscontrol.backoffice;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import uy.viruscontrol.business.service.SettingsService;
import uy.viruscontrol.common.dto.DTONotificationConfig;

@Named
@ViewScoped
@Getter
@Setter
public class SettingsBean implements Serializable {
  @EJB
  private SettingsService settingsService;

  private DTONotificationConfig viewNotifications;
  private DTONotificationConfig changeNotification;

  private Double contagionDistance;
  private Double configureContagionDistance;

  @PostConstruct
  public void init() {
    viewNotifications = settingsService.getNotificationConfig();
    contagionDistance = settingsService.getContagionDistance();
  }

  public void handleChangeNotifications() {
    try {
      changeNotification = viewNotifications;
      settingsService.configureNotifications(changeNotification);
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Cambios realizados con exito", null));
    } catch (EJBException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Oops, ha ocurrido un error", null));
    }
  }

  public void handleContagionDistanceChange() {
    try {
      configureContagionDistance = contagionDistance;
      settingsService.configureContagionDistance(configureContagionDistance);
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Cambios realizados con exito", null));
    } catch (EJBException e) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Oops, ha ocurrido un error", null));
    }
  }
}
