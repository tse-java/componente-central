package uy.viruscontrol.backoffice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import uy.viruscontrol.business.service.DiseaseService;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Named
@ViewScoped
@Getter
@Setter
public class ApproveDisease implements Serializable {
  @EJB
  private DiseaseService diseaseService;

  private String searchText = "";
  private DTODisease selectedDiseaseAccept = new DTODisease();
  private DTODisease selectedDiseaseView = new DTODisease();
  private DTODisease selectedDiseaseReject = new DTODisease();
  private List<DTODisease> pendingStatusDiseases = new ArrayList<>();
  private List<DTODisease> pendingStatusDiseasesCopy = new ArrayList<>();

  @PostConstruct
  public void init() {
    pendingStatusDiseases = diseaseService.getDiseasesByStatus(EnumRequestStatus.PENDING);
    pendingStatusDiseasesCopy = pendingStatusDiseases;
  }

  public void search() {
    if (searchText.isEmpty()) {
      pendingStatusDiseasesCopy = pendingStatusDiseases;
    } else {
      pendingStatusDiseasesCopy =
        pendingStatusDiseases
          .stream()
          .filter(dtoResource -> dtoResource.getName().toLowerCase().contains(searchText.toLowerCase()))
          .collect(Collectors.toList());
    }
  }

  public void approveNewDisease() {
    try {
      diseaseService.approveNewDisease(selectedDiseaseAccept.getId());
      pendingStatusDiseases = diseaseService.getDiseasesByStatus(EnumRequestStatus.PENDING);
      pendingStatusDiseasesCopy = pendingStatusDiseases;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('approveDiseaseModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Enfermedad aprobada", "La enfermedad ha sido aprobada exitosamente"));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error.", null));
    }
  }

  public void rejectNewDisease() {
    try {
      diseaseService.rejectNewDisease(selectedDiseaseReject.getId());
      pendingStatusDiseases = diseaseService.getDiseasesByStatus(EnumRequestStatus.PENDING);
      pendingStatusDiseasesCopy = pendingStatusDiseases;
      PrimeFaces current = PrimeFaces.current();
      current.executeScript("PF('rejectDiseaseModal').hide();");
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("Enfermedad rechazada", "La enfermedad ha sido rechazada exitosamente"));
    } catch (VirusControlException virusControlException) {
      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrio un error.", null));
    }
  }
}
