package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.integration.impl.DNICIntegration;
import uy.viruscontrol.pi.service.DtoCitizen;

@RunWith(MockitoJUnitRunner.class)
public class DnicTest {
  @InjectMocks
  private DNICIntegration dnicIntegration;

  @Test
  public void getCitizenInformationTest() {
    String documentNumber = "1234567-8";
    DtoCitizen citizen = dnicIntegration.getCitizenInformation(documentNumber);
    assertEquals(citizen.getDocumentNumber(), documentNumber);
    DtoCitizen citizen2 = dnicIntegration.getCitizenInformation(documentNumber);
    assertEquals(citizen.getDocumentNumber(), citizen2.getDocumentNumber());
    assertEquals(citizen.getBirthDay(), citizen2.getBirthDay());
  }
}
