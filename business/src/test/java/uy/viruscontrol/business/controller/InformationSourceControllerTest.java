package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.init.InformationSourceTypeInitializer;
import uy.viruscontrol.business.model.InformationSource;
import uy.viruscontrol.business.model.InformationSourceType;
import uy.viruscontrol.business.model.RSSInformationSource;
import uy.viruscontrol.business.model.TwitterInformationSource;
import uy.viruscontrol.common.dto.DTOInformationSource;
import uy.viruscontrol.common.dto.DTORSSInformationSource;
import uy.viruscontrol.common.dto.DTOTwitterInformationSource;
import uy.viruscontrol.common.enumerations.EnumInformationSrcType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class InformationSourceControllerTest {
  @InjectMocks
  private InformationSourceController sourceController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void getInformationSourceListTest() {
    List<InformationSource> sourceList = new ArrayList<>();
    Stream<RSSInformationSource> rss = generator.objects(RSSInformationSource.class, 5);
    Stream<TwitterInformationSource> twitter = generator.objects(TwitterInformationSource.class, 5);
    sourceList.addAll(rss.collect(Collectors.toList()));
    sourceList.addAll(twitter.collect(Collectors.toList()));
    when(entityManager.createNamedQuery("InformationSource.getAll", InformationSource.class).getResultList()).thenReturn(sourceList);
    List<DTOInformationSource> result = sourceController.getInformationSourceList();
    assertEquals(sourceList.size(), result.size());
  }

  @Test
  public void newTwitterTestOk() throws VirusControlException {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    when(
        entityManager
          .createNamedQuery("TwitterInformationSource.findByNickname", TwitterInformationSource.class)
          .setParameter("username", url)
          .getResultList()
      )
      .thenReturn(new ArrayList<>());
    DTOTwitterInformationSource result = sourceController.newTwitterSource("nombre", "desc", url);
    assertEquals(result.getUsername(), url);
  }

  @Test
  public void newTwitterTestInactive() {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(false);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    assertThrows(VirusControlException.class, () -> sourceController.newTwitterSource("nombre", "desc", url));
  }

  @Test
  public void newTwitterTestAlreadyExists() {
    TwitterInformationSource source = new TwitterInformationSource();
    source.setUsername("url");
    List<TwitterInformationSource> sourceList = new ArrayList<>();
    sourceList.add(source);

    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    when(
        entityManager
          .createNamedQuery("TwitterInformationSource.findByNickname", TwitterInformationSource.class)
          .setParameter("username", url)
          .getResultList()
      )
      .thenReturn(sourceList);
    assertThrows(VirusControlException.class, () -> sourceController.newTwitterSource("nombre", "desc", url));
  }

  @Test
  public void editTwitterTestOk() throws VirusControlException {
    TwitterInformationSource informationSource = new TwitterInformationSource();
    informationSource.setUsername("url");
    informationSource.setId(1L);
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    when(entityManager.find(TwitterInformationSource.class, informationSource.getId())).thenReturn(informationSource);
    when(
        entityManager
          .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
          .setParameter("url", informationSource.getUsername())
          .getResultList()
      )
      .thenReturn(new ArrayList<>());
    DTOTwitterInformationSource result = sourceController.editTwitterSource(
      modelMapper.map(informationSource, DTOTwitterInformationSource.class)
    );
    assertEquals(result.getUsername(), informationSource.getUsername());
  }

  @Test
  public void editTwitterTestInactive() {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(false);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    assertThrows(VirusControlException.class, () -> sourceController.editTwitterSource(new DTOTwitterInformationSource()));
  }

  @Test
  public void editTwitterTestNotFound() {
    TwitterInformationSource informationSource2 = new TwitterInformationSource();
    informationSource2.setUsername("url");
    informationSource2.setId(2L);

    InformationSourceType type = new InformationSourceType();
    type.setActive(true);

    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    assertThrows(
      VirusControlException.class,
      () -> sourceController.editTwitterSource(modelMapper.map(informationSource2, DTOTwitterInformationSource.class))
    );
  }

  @Test
  public void editTwitterTestAlreadyExists() {
    TwitterInformationSource informationSource = new TwitterInformationSource();
    informationSource.setUsername("url");
    informationSource.setId(1L);
    List<TwitterInformationSource> sourceList = new ArrayList<>();
    sourceList.add(informationSource);

    TwitterInformationSource informationSource2 = new TwitterInformationSource();
    informationSource2.setUsername("url");
    informationSource2.setId(2L);

    InformationSourceType type = new InformationSourceType();
    type.setActive(true);

    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);
    when(entityManager.find(TwitterInformationSource.class, informationSource2.getId())).thenReturn(informationSource2);
    when(
        entityManager
          .createNamedQuery("TwitterInformationSource.findByNickname", TwitterInformationSource.class)
          .setParameter("username", informationSource.getUsername())
          .getResultList()
      )
      .thenReturn(sourceList);
    assertThrows(
      VirusControlException.class,
      () -> sourceController.editTwitterSource(modelMapper.map(informationSource2, DTOTwitterInformationSource.class))
    );
  }

  @Test
  public void deleteSourceTestNotFound() {
    assertThrows(VirusControlException.class, () -> sourceController.deleteInformationSource(1L));
  }

  @Test
  public void deleteSourceTestOk() throws VirusControlException {
    RSSInformationSource informationSource2 = new RSSInformationSource();
    informationSource2.setUrl("url");
    informationSource2.setId(2L);
    when(entityManager.find(InformationSource.class, 2L)).thenReturn(informationSource2);
    sourceController.deleteInformationSource(2L);
  }

  @Test
  public void newRSSFeedTestOk() throws VirusControlException {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(
        entityManager
          .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
          .setParameter("url", url)
          .getResultList()
      )
      .thenReturn(new ArrayList<>());
    DTORSSInformationSource result = sourceController.newRSSFeed("nombre", "desc", url, null);
    assertEquals(result.getUrl(), url);
  }

  @Test
  public void newRSSFeedTestInactive() {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(false);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    assertThrows(VirusControlException.class, () -> sourceController.newRSSFeed("nombre", "desc", url, null));
  }

  @Test
  public void newRSSFeedTestAlreadyExists() {
    RSSInformationSource source = new RSSInformationSource();
    source.setUrl("url");
    List<RSSInformationSource> sourceList = new ArrayList<>();
    sourceList.add(source);

    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(
        entityManager
          .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
          .setParameter("url", url)
          .getResultList()
      )
      .thenReturn(sourceList);
    assertThrows(VirusControlException.class, () -> sourceController.newRSSFeed("nombre", "desc", url, null));
  }

  @Test
  public void editRSSFeedTestOk() throws VirusControlException {
    RSSInformationSource informationSource = new RSSInformationSource();
    informationSource.setUrl("url");
    informationSource.setId(1L);
    InformationSourceType type = new InformationSourceType();
    type.setActive(true);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(entityManager.find(RSSInformationSource.class, informationSource.getId())).thenReturn(informationSource);
    when(
        entityManager
          .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
          .setParameter("url", informationSource.getUrl())
          .getResultList()
      )
      .thenReturn(new ArrayList<>());
    DTORSSInformationSource result = sourceController.editRSSFeed(modelMapper.map(informationSource, DTORSSInformationSource.class));
    assertEquals(result.getUrl(), informationSource.getUrl());
  }

  @Test
  public void editRSSFeedTestInactive() {
    String url = "url";
    InformationSourceType type = new InformationSourceType();
    type.setActive(false);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    assertThrows(VirusControlException.class, () -> sourceController.editRSSFeed(new DTORSSInformationSource()));
  }

  @Test
  public void editRSSFeedTestNotFound() {
    RSSInformationSource informationSource2 = new RSSInformationSource();
    informationSource2.setUrl("url");
    informationSource2.setId(2L);

    InformationSourceType type = new InformationSourceType();
    type.setActive(true);

    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(entityManager.find(RSSInformationSource.class, informationSource2.getId())).thenReturn(null);
    assertThrows(
      VirusControlException.class,
      () -> sourceController.editRSSFeed(modelMapper.map(informationSource2, DTORSSInformationSource.class))
    );
  }

  @Test
  public void editRSSFeedTestAlreadyExists() {
    RSSInformationSource informationSource = new RSSInformationSource();
    informationSource.setUrl("url");
    informationSource.setId(1L);
    List<RSSInformationSource> sourceList = new ArrayList<>();
    sourceList.add(informationSource);

    RSSInformationSource informationSource2 = new RSSInformationSource();
    informationSource2.setUrl("url");
    informationSource2.setId(2L);

    InformationSourceType type = new InformationSourceType();
    type.setActive(true);

    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(entityManager.find(RSSInformationSource.class, informationSource2.getId())).thenReturn(informationSource2);
    when(
        entityManager
          .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
          .setParameter("url", informationSource.getUrl())
          .getResultList()
      )
      .thenReturn(sourceList);
    assertThrows(
      VirusControlException.class,
      () -> sourceController.editRSSFeed(modelMapper.map(informationSource2, DTORSSInformationSource.class))
    );
  }

  @Test
  public void getInformationSourceListByClassTest() {
    List<InformationSource> sourceList = new ArrayList<>();
    Stream<RSSInformationSource> rss = generator.objects(RSSInformationSource.class, 5);
    Stream<TwitterInformationSource> twitter = generator.objects(TwitterInformationSource.class, 5);
    sourceList.addAll(rss.collect(Collectors.toList()));
    sourceList.addAll(twitter.collect(Collectors.toList()));
    when(
        entityManager
          .createNamedQuery("InformationSource.getByClass", InformationSource.class)
          .setParameter("class", TwitterInformationSource.class)
          .getResultList()
      )
      .thenReturn(sourceList);
    List<DTOInformationSource> result = sourceController.getInformationSourceList(EnumInformationSrcType.TWITTER);
    assertEquals(sourceList.size(), result.size());
  }

  @Test
  public void getInformationSourceListByClass2Test() {
    List<InformationSource> sourceList = new ArrayList<>();
    Stream<RSSInformationSource> rss = generator.objects(RSSInformationSource.class, 5);
    Stream<TwitterInformationSource> twitter = generator.objects(TwitterInformationSource.class, 5);
    sourceList.addAll(rss.collect(Collectors.toList()));
    sourceList.addAll(twitter.collect(Collectors.toList()));
    when(
        entityManager
          .createNamedQuery("InformationSource.getByClass", InformationSource.class)
          .setParameter("class", RSSInformationSource.class)
          .getResultList()
      )
      .thenReturn(sourceList);
    List<DTOInformationSource> result = sourceController.getInformationSourceList(EnumInformationSrcType.RSS);
    assertEquals(sourceList.size(), result.size());
  }
}
