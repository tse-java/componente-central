package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.CaseCache;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.MedicalCase;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@RunWith(MockitoJUnitRunner.class)
public class CaseControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private CaseController caseController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CaseCache caseCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Test
  public void getCasesGroupBycountGroupByDepartmentAndStatusFilterByStatus() {
    EasyRandom generator = new EasyRandom();
    List<Object[]> resultList = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      Object[] pair = new Object[3];
      pair[0] = generator.nextObject(UruguayanDepartment.class);
      pair[1] = generator.nextObject(EnumCaseStatus.class);
      pair[2] = generator.nextObject(Long.class);
      resultList.add(pair);
    }
    List<EnumCaseStatus> list = new ArrayList<>();
    list.add(EnumCaseStatus.CONFIRMED);
    list.add(EnumCaseStatus.CURED);
    when(
        entityManagerMock
          .createNamedQuery("Case.countGroupByDepartmentAndStatusFilterByStatus", Object[].class)
          .setParameter("status", list)
          .setParameter("diseaseId", 1L)
          .getResultStream()
      )
      .thenReturn(resultList.stream());
    List<DTODepartment> dtoResulList = caseController.getCasesGroupByDepartment(1L, list);
    Assert.assertFalse(dtoResulList.isEmpty());
  }

  @Test
  public void countGroupByDepartmentAndStatus() {
    EasyRandom generator = new EasyRandom();
    List<Object[]> resultList = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      Object[] pair = new Object[3];
      pair[0] = generator.nextObject(UruguayanDepartment.class);
      pair[1] = generator.nextObject(EnumCaseStatus.class);
      pair[2] = generator.nextObject(Long.class);
      resultList.add(pair);
    }
    when(caseCache.getCases(anyLong())).thenReturn(null);
    when(
        entityManagerMock
          .createNamedQuery("Case.countGroupByDepartmentAndStatus", Object[].class)
          .setParameter("diseaseId", 1L)
          .getResultStream()
      )
      .thenReturn(resultList.stream());
    List<DTODepartment> dtoResulList = caseController.getCasesGroupByDepartment(1L, null);
    Assert.assertFalse(dtoResulList.isEmpty());
  }

  @Test
  public void newCase() {
    EasyRandom generator = new EasyRandom();
    Disease disease = generator.nextObject(Disease.class);
    FrontProfile profile = generator.nextObject(FrontProfile.class);
    EnumCaseStatus caseStatus = generator.nextObject(EnumCaseStatus.class);
    MedicalCase medicalCase = generator.nextObject(MedicalCase.class);
    medicalCase.setPatient(profile);
    medicalCase.setDisease(disease);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    when(entityManagerMock.find(FrontProfile.class, profile.getId())).thenReturn(profile);
    when(entityManagerMock.merge(any(MedicalCase.class))).thenReturn(medicalCase);

    DTOMedicalCase dtoMedicalCase = caseController.newCase(profile.getId(), disease.getId(), caseStatus);
    assertEquals(dtoMedicalCase.getDisease().getName(), disease.getName());
    assertEquals(dtoMedicalCase.getPatient().getFirstname(), profile.getFirstname());
  }
}
