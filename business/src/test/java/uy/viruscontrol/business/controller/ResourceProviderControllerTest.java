package uy.viruscontrol.business.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.integration.impl.ResourceProviderIntegration;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.ResourceAvailability;
import uy.viruscontrol.business.model.ResourceProvider;
import uy.viruscontrol.common.dto.DTOResourceProvider;
import uy.viruscontrol.common.dto.DTOResourceQuantity;
import uy.viruscontrol.common.dto.DTOWrongId;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class ResourceProviderControllerTest {
  @Mock
  private MessageProducer messageProducer;

  @Mock
  private ResourceProviderIntegration providerIntegration;

  @InjectMocks
  private ResourceProviderController resourceProviderController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void newResourceStatusOKProvider() throws VirusControlException {
    String name = generator.nextObject(String.class);
    String url = generator.nextObject(String.class);
    when(providerIntegration.sync(url)).thenReturn(generator.nextObject(ResourceProvider.class));
    DTOResourceProvider result = resourceProviderController.newResourceProvider(name, url);
    Assert.assertEquals(name, result.getName());
  }

  @Test
  public void newResourceStatusOKFailProvider() throws VirusControlException {
    String name = generator.nextObject(String.class);
    String url = generator.nextObject(String.class);
    ResourceProvider r = generator.nextObject(ResourceProvider.class);
    when(providerIntegration.sync(url)).thenReturn(r);
    doThrow(VirusControlException.class).when(providerIntegration).register(anyString(), anyString());
    DTOResourceProvider result = resourceProviderController.newResourceProvider(name, url);
    Assert.assertEquals(name, result.getName());
  }

  @Test
  public void newResourceProvider() throws VirusControlException {
    String name = generator.nextObject(String.class);
    String url = generator.nextObject(String.class);
    when(providerIntegration.sync(url)).thenReturn(null);
    DTOResourceProvider result = resourceProviderController.newResourceProvider(name, url);
    Assert.assertEquals(name, result.getName());
  }

  @Test
  public void newResourceProviderMissingData() {
    String url = generator.nextObject(String.class);
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.newResourceProvider(null, url));
  }

  @Test
  public void syncResourceProvider() throws VirusControlException {
    ResourceProvider resourceProvider = generator.nextObject(ResourceProvider.class);
    ResourceProvider newData = generator.nextObject(ResourceProvider.class);
    newData.setId(resourceProvider.getId());
    when(entityManagerMock.find(ResourceProvider.class, resourceProvider.getId())).thenReturn(resourceProvider);
    when(providerIntegration.sync(resourceProvider.getUrl())).thenReturn(newData);
    Assert.assertNotEquals(resourceProvider.getCommercialName(), newData.getCommercialName());
    DTOResourceProvider result = resourceProviderController.syncResourceProvider(resourceProvider.getId());
    Assert.assertEquals(newData.getCommercialName(), result.getCommercialName());
  }

  @Test
  public void syncResourceProviderCanNotSync() {
    ResourceProvider resourceProvider = generator.nextObject(ResourceProvider.class);
    ResourceProvider newData = generator.nextObject(ResourceProvider.class);
    newData.setId(resourceProvider.getId());
    when(entityManagerMock.find(ResourceProvider.class, resourceProvider.getId())).thenReturn(resourceProvider);
    when(providerIntegration.sync(resourceProvider.getUrl())).thenReturn(null);
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.syncResourceProvider(resourceProvider.getId()));
  }

  @Test
  public void syncResourceProviderMisisngId() {
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.syncResourceProvider(null));
  }

  @Test
  public void syncResourceProviderNotFound() {
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.syncResourceProvider(1L));
  }

  @Test
  public void updateResourceProvider() throws VirusControlException {
    ResourceProvider resourceProvider = generator.nextObject(ResourceProvider.class);
    String name = generator.nextObject(String.class);
    String url = generator.nextObject(String.class);
    when(entityManagerMock.find(ResourceProvider.class, resourceProvider.getId())).thenReturn(resourceProvider);
    resourceProviderController.updateResourceProvider(resourceProvider.getId(), name, url);
  }

  @Test
  public void updateResourceNullProvider() throws VirusControlException {
    ResourceProvider resourceProvider = generator.nextObject(ResourceProvider.class);
    String name = generator.nextObject(String.class);
    String url = generator.nextObject(String.class);
    when(providerIntegration.sync(anyString())).thenReturn(generator.nextObject(ResourceProvider.class));
    when(entityManagerMock.find(ResourceProvider.class, resourceProvider.getId())).thenReturn(resourceProvider);
    resourceProviderController.updateResourceProvider(resourceProvider.getId(), name, url);
  }

  @Test
  public void deleteResourceProvider() throws VirusControlException {
    ResourceProvider resourceProvider = generator.nextObject(ResourceProvider.class);
    resourceProvider.setResources(generator.objects(ResourceAvailability.class, 4).collect(Collectors.toSet()));
    when(entityManagerMock.find(ResourceProvider.class, resourceProvider.getId())).thenReturn(resourceProvider);
    resourceProviderController.deleteResourceProvider(resourceProvider.getId());
  }

  @Test
  public void deleteResourceProviderNullId() {
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.deleteResourceProvider(null));
  }

  @Test
  public void deleteResourceProviderNotFound() {
    Assert.assertThrows(VirusControlException.class, () -> resourceProviderController.deleteResourceProvider(generator.nextLong()));
  }

  @Test
  public void getResourceProviders() {
    List<ResourceProvider> providerList = new ArrayList<>();
    providerList.add(generator.nextObject(ResourceProvider.class));
    providerList.add(generator.nextObject(ResourceProvider.class));
    providerList.add(generator.nextObject(ResourceProvider.class));

    when(entityManagerMock.createNamedQuery("ResourceProvider.findAll", ResourceProvider.class).getResultList()).thenReturn(providerList);
    List<DTOResourceProvider> result = resourceProviderController.getResourceProviders(null);
    Assert.assertEquals(result.size(), providerList.size());
    Assert.assertEquals(result.get(0).getName(), providerList.get(0).getName());
  }

  @Test
  public void getResourceProvidersByName() {
    List<ResourceProvider> providerList = new ArrayList<>();
    providerList.add(generator.nextObject(ResourceProvider.class));
    providerList.add(generator.nextObject(ResourceProvider.class));
    providerList.add(generator.nextObject(ResourceProvider.class));
    String name = generator.nextObject(String.class);
    when(
        entityManagerMock
          .createNamedQuery("ResourceProvider.findByName", ResourceProvider.class)
          .setParameter("nameFilter", "%" + name + "%")
          .getResultList()
      )
      .thenReturn(providerList);
    List<DTOResourceProvider> result = resourceProviderController.getResourceProviders(name);
    Assert.assertEquals(result.size(), providerList.size());
    Assert.assertEquals(result.get(0).getName(), providerList.get(0).getName());
  }

  @Test
  public void setResourceProviderAvailabilityTest() {
    ResourceProvider provider = generator.nextObject(ResourceProvider.class);
    when(entityManagerMock.find(ResourceProvider.class, 1L)).thenReturn(provider);

    Resource resource = generator.nextObject(Resource.class);
    resource.setSubscribers(generator.objects(FrontProfile.class, 10).collect(Collectors.toSet()));
    when(entityManagerMock.find(Resource.class, 1L)).thenReturn(resource);

    List<DTOResourceQuantity> dtoResourceQuantityList = new ArrayList<>();
    DTOResourceQuantity dtoResourceQuantity = new DTOResourceQuantity();
    dtoResourceQuantity.setQuantity(1L);
    dtoResourceQuantity.setResourceId(1L);
    dtoResourceQuantityList.add(dtoResourceQuantity);

    List<DTOWrongId> wrongIds = resourceProviderController.setResourceProviderAvailability(1L, dtoResourceQuantityList);

    Assert.assertEquals(wrongIds.size(), 0);
  }

  @Test
  public void setResourceProviderAvailabilityTest2() {
    ResourceProvider provider = generator.nextObject(ResourceProvider.class);
    provider.setResources(generator.objects(ResourceAvailability.class, 5).collect(Collectors.toSet()));
    ResourceAvailability resourceAvailability = provider.getResources().stream().findFirst().orElse(null);
    assert resourceAvailability != null;
    resourceAvailability.setQuantity(new BigDecimal(0));
    resourceAvailability.getResource().setId(1L);
    resourceAvailability.getResource().setSubscribers(generator.objects(FrontProfile.class, 10).collect(Collectors.toSet()));
    when(entityManagerMock.find(ResourceProvider.class, 1L)).thenReturn(provider);

    Resource resource = generator.nextObject(Resource.class);
    resource.setSubscribers(generator.objects(FrontProfile.class, 10).collect(Collectors.toSet()));
    List<DTOResourceQuantity> dtoResourceQuantityList = new ArrayList<>();
    DTOResourceQuantity dtoResourceQuantity = new DTOResourceQuantity();
    dtoResourceQuantity.setQuantity(1L);
    dtoResourceQuantity.setResourceId(1L);
    dtoResourceQuantityList.add(dtoResourceQuantity);

    List<DTOWrongId> wrongIds = resourceProviderController.setResourceProviderAvailability(1L, dtoResourceQuantityList);

    Assert.assertEquals(wrongIds.size(), 0);
  }

  @Test
  public void setResourceProviderAvailabilityTestResourceNotFound() {
    ResourceProvider provider = generator.nextObject(ResourceProvider.class);
    when(entityManagerMock.find(ResourceProvider.class, 1L)).thenReturn(provider);

    List<DTOResourceQuantity> dtoResourceQuantityList = new ArrayList<>();
    DTOResourceQuantity dtoResourceQuantity = new DTOResourceQuantity();
    dtoResourceQuantity.setQuantity(1L);
    dtoResourceQuantity.setResourceId(1L);
    dtoResourceQuantityList.add(dtoResourceQuantity);

    List<DTOWrongId> wrongIds = resourceProviderController.setResourceProviderAvailability(1L, dtoResourceQuantityList);

    Assert.assertEquals(wrongIds.size(), 1);
  }

  @Test
  public void getResourceProviderByTokenTest() {
    ResourceProvider provider = generator.nextObject(ResourceProvider.class);
    when(
        entityManagerMock
          .createNamedQuery("ResourceProvider.getByToken", ResourceProvider.class)
          .setParameter("token", "token")
          .getResultStream()
          .findFirst()
      )
      .thenReturn(java.util.Optional.ofNullable(provider));

    DTOResourceProvider dtoResourceProvider = resourceProviderController.getResourceProviderByToken("token");
    Assert.assertEquals(dtoResourceProvider.getId(), provider.getId());
  }

  @Test
  public void registerTest() throws VirusControlException {
    ResourceProvider provider = generator.nextObject(ResourceProvider.class);
    when(entityManagerMock.find(ResourceProvider.class, 1L)).thenReturn(provider);

    resourceProviderController.register(1L);
  }
}
