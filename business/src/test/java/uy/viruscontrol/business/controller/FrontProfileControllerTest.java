package uy.viruscontrol.business.controller;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.time.DateUtils;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.integration.impl.DNICIntegration;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.Location;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTOLocation;
import uy.viruscontrol.common.dto.DTONotifyStyle;
import uy.viruscontrol.common.dto.DTOUpdateFrontProfile;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;
import uy.viruscontrol.common.exceptions.VirusControlException;
import uy.viruscontrol.pi.service.DtoCitizen;

@RunWith(MockitoJUnitRunner.class)
public class FrontProfileControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private FrontProfileController frontProfileController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Mock
  private DNICIntegration dnicIntegration;

  @Test
  public void invalidateRefreshToken() throws VirusControlException {
    FrontProfile userProfile = new FrontProfile();
    userProfile.setRefreshToken("faketoken");
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(userProfile);
    frontProfileController.invalidateRefreshToken(1L);
    assertNull(userProfile.getRefreshToken());
  }

  @Test
  public void invalidateRefreshTokenInvalidUser() {
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> frontProfileController.invalidateRefreshToken(1L));
  }

  @Test
  public void getProfileByDoc() {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setExternalId("4545454");
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByExternalId", FrontProfile.class)
          .setParameter("externalId", frontProfile.getExternalId())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(frontProfile));
    DTOFrontProfile dtoProfile = frontProfileController.getProfileByExternalId("4545454");
    assertEquals(dtoProfile.getDocumentNumber(), frontProfile.getDocumentNumber());
  }

  @Test
  public void getFrontofficeUserByDocumentInvalid() {
    assertNull(frontProfileController.getProfileByExternalId("6.666.666-6"));
  }

  @Test
  public void newProfile() throws VirusControlException {
    EasyRandom easyRandom = new EasyRandom();
    FrontProfile frontProfile = easyRandom.nextObject(FrontProfile.class);
    DTOFrontProfile dtoFrontProfile = modelMapper.map(frontProfile, DTOFrontProfile.class);
    when(entityManagerMock.merge(any(FrontProfile.class))).thenReturn(frontProfile);
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByExternalId", FrontProfile.class)
          .setParameter("externalId", frontProfile.getExternalId())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.empty());
    frontProfileController.newProfile(dtoFrontProfile);
  }

  @Test
  public void newProfileDuplicated() {
    FrontProfile profile = new FrontProfile();
    profile.setId(1L);
    profile.setExternalId("121");
    profile.setDocumentNumber("6.666.666-6");
    profile.setFirstname("Juan");
    profile.setLastname("Perez");
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByExternalId", FrontProfile.class)
          .setParameter("externalId", profile.getExternalId())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(profile));
    assertThrows(VirusControlException.class, () -> frontProfileController.newProfile(modelMapper.map(profile, DTOFrontProfile.class)));
  }

  @Test
  public void getProfileById() throws VirusControlException {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);

    frontProfile.setAddress(location);
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    DTOFrontProfile dtoProfile = frontProfileController.getProfileById(1L);
    assertEquals(dtoProfile.getDocumentNumber(), frontProfile.getDocumentNumber());
  }

  @Test
  public void getProfileByIdNonExistent() {
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> frontProfileController.getProfileById(1L));
  }

  @Test
  public void saveRefreshToken() throws VirusControlException {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    frontProfileController.saveRefreshToken(1L, "faketoken");
  }

  @Test
  public void saveRefreshTokenAlreadyExistent() throws VirusControlException {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    frontProfile.setRefreshToken("existent");
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    String result = frontProfileController.saveRefreshToken(1L, "faketoken");
    assertEquals(result, frontProfile.getRefreshToken());
  }

  @Test
  public void saveRefreshTokenFail() {
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> frontProfileController.saveRefreshToken(1L, "faketoken"));
  }

  @Test
  public void getProfileByInvalidRefreshToken() {
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByRefreshToken", FrontProfile.class)
          .setParameter("refreshToken", "randomtoken")
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.empty());
    assertThrows(VirusControlException.class, () -> frontProfileController.getProfileByRefreshToken("randomtoken"));
  }

  @Test
  public void getProfileByExpiredRefreshToken() {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), -1));
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByRefreshToken", FrontProfile.class)
          .setParameter("refreshToken", frontProfile.getRefreshToken())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(frontProfile));
    assertThrows(VirusControlException.class, () -> frontProfileController.getProfileByRefreshToken(frontProfile.getRefreshToken()));
  }

  @Test
  public void getProfileByRefreshToken() throws VirusControlException {
    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByRefreshToken", FrontProfile.class)
          .setParameter("refreshToken", frontProfile.getRefreshToken())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(frontProfile));
    DTOFrontProfile result = frontProfileController.getProfileByRefreshToken(frontProfile.getRefreshToken());
    assertEquals(result.getDocumentNumber(), frontProfile.getDocumentNumber());
  }

  @Test
  public void updateUser() throws VirusControlException {
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);

    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setAddress(location);
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));

    DTOUpdateFrontProfile dtoUpdateFrontProfile = modelMapper.map(frontProfile, DTOUpdateFrontProfile.class);
    dtoUpdateFrontProfile.getAddress().setDepartment(UruguayanDepartment.MONTEVIDEO);
    dtoUpdateFrontProfile.getAddress().setStreet("Av. Siempre Viva 742");

    DtoCitizen citizen = new DtoCitizen();
    citizen.setDocumentNumber("6.666.666-6");
    citizen.setBirthDay(new GregorianCalendar(1993, Calendar.OCTOBER, 4));
    citizen.setFirstName("Jose");
    citizen.setLastName("Rodriguez");
    citizen.setNationality("Uruguay");

    when(dnicIntegration.getCitizenInformation("6.666.666-6")).thenReturn(citizen);
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    assertNotEquals(dtoUpdateFrontProfile.getAddress(), frontProfile.getAddress());
    frontProfileController.updateProfile(dtoUpdateFrontProfile);
    assertEquals(dtoUpdateFrontProfile.getAddress().getStreet(), frontProfile.getAddress().getStreet());
  }

  @Test
  public void updateNonExistentUser() {
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);

    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setAddress(location);
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));

    DTOUpdateFrontProfile dtoUpdateFrontProfile = modelMapper.map(frontProfile, DTOUpdateFrontProfile.class);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> frontProfileController.updateProfile(dtoUpdateFrontProfile));
  }

  @Test
  public void updateUserMedic() throws VirusControlException {
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);

    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setAddress(location);
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));

    DTOUpdateFrontProfile dtoUpdateFrontProfile = modelMapper.map(frontProfile, DTOUpdateFrontProfile.class);
    dtoUpdateFrontProfile.setId(1L);
    dtoUpdateFrontProfile.setDocumentNumber("6.666.666-6");
    dtoUpdateFrontProfile.getAddress().setDepartment(UruguayanDepartment.MONTEVIDEO);
    dtoUpdateFrontProfile.getAddress().setStreet("Av. Siempre Viva 742");

    DtoCitizen citizen = new DtoCitizen();
    citizen.setDocumentNumber("6.666.666-6");
    citizen.setBirthDay(new GregorianCalendar(1993, Calendar.OCTOBER, 4));
    citizen.setFirstName("Jose");
    citizen.setLastName("Rodriguez");
    citizen.setNationality("Uruguay");

    when(dnicIntegration.getCitizenInformation("6.666.666-6")).thenReturn(citizen);
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.findByDocumentNumber", FrontProfile.class)
          .setParameter("documentNumber", "6.666.666-6")
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(frontProfile));
    assertNotEquals(dtoUpdateFrontProfile.getAddress(), frontProfile.getAddress());
    frontProfileController.updateProfile(dtoUpdateFrontProfile);
    assertEquals(dtoUpdateFrontProfile.getAddress().getStreet(), frontProfile.getAddress().getStreet());
  }

  @Test
  public void patchAddress() throws VirusControlException {
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);

    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setAddress(location);
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));

    DTOUpdateFrontProfile dtoUpdateFrontProfile = modelMapper.map(frontProfile, DTOUpdateFrontProfile.class);

    DTOLocation dtoLocation = new DTOLocation();
    dtoLocation.setStreet("Av. Siempre Viva 742");

    dtoLocation.setDepartment(UruguayanDepartment.CANELONES);
    dtoUpdateFrontProfile.setAddress(dtoLocation);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(frontProfile);
    assertNotEquals(dtoUpdateFrontProfile.getAddress(), frontProfile.getAddress());
    frontProfileController.patchAddress(dtoUpdateFrontProfile);
    assertEquals(dtoUpdateFrontProfile.getAddress().getStreet(), frontProfile.getAddress().getStreet());
  }

  @Test
  public void patchAddressNonExistentUser() {
    Location location = new Location();
    location.setStreet("Elm Street");

    FrontProfile frontProfile = new FrontProfile();
    frontProfile.setId(1L);
    frontProfile.setDocumentNumber("6.666.666-6");
    frontProfile.setFirstname("Juan");
    frontProfile.setLastname("Perez");
    frontProfile.setRefreshToken("randomtoken");
    frontProfile.setAddress(location);
    frontProfile.setRefreshTokenExpire(DateUtils.addDays(new Date(), 1));
    DTOUpdateFrontProfile dtoUpdateFrontProfile = modelMapper.map(frontProfile, DTOUpdateFrontProfile.class);
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> frontProfileController.patchAddress(dtoUpdateFrontProfile));
  }

  @Test
  public void hasActiveCase() {
    EasyRandom generator = new EasyRandom();
    FrontProfile frontProfile = generator.nextObject(FrontProfile.class);
    Disease disease = generator.nextObject(Disease.class);
    when(
        entityManagerMock
          .createNamedQuery("FrontProfile.hasActiveCaseForDisease", FrontProfile.class)
          .setParameter("userId", frontProfile.getId())
          .setParameter("diseaseId", disease.getId())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(frontProfile));
    boolean result = frontProfileController.hasAnActiveCase(frontProfile.getId(), disease.getId());
    assertTrue(result);
  }

  @Test
  public void patchNotify() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    FrontProfile frontProfile = generator.nextObject(FrontProfile.class);
    DTONotifyStyle dtoNotifyStyle = generator.nextObject(DTONotifyStyle.class);
    when(entityManagerMock.find(FrontProfile.class, frontProfile.getId())).thenReturn(frontProfile);
    when(entityManagerMock.merge(any(FrontProfile.class))).thenReturn(frontProfile);
    frontProfileController.patchNotifications(frontProfile.getId(), dtoNotifyStyle);
  }

  @Test
  public void patchNotifyNull() {
    assertThrows(VirusControlException.class, () -> frontProfileController.patchNotifications(1L, null));
  }
}
