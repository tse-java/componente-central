package uy.viruscontrol.business.controller;

import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.model.settings.ContagionDistance;
import uy.viruscontrol.business.model.settings.NotificationConfig;
import uy.viruscontrol.common.dto.DTONotificationConfig;

@RunWith(MockitoJUnitRunner.class)
public class SettingsControllerTest {
  @InjectMocks
  private SettingsController notificationController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Test
  public void getContagionConfig() {
    EasyRandom generator = new EasyRandom();
    ContagionDistance contagionDistance = generator.nextObject(ContagionDistance.class);
    when(entityManagerMock.find(ContagionDistance.class, 1L)).thenReturn(contagionDistance);
    notificationController.getContagionDistance();
  }

  @Test
  public void getContagionConfigNull() {
    notificationController.getContagionDistance();
  }

  @Test
  public void updateContagionConfigNull() {
    notificationController.configureContagionDistance(2D);
  }

  @Test
  public void updateContagionConfig() {
    EasyRandom generator = new EasyRandom();
    ContagionDistance contagionDistance = generator.nextObject(ContagionDistance.class);
    when(entityManagerMock.find(ContagionDistance.class, 1L)).thenReturn(contagionDistance);
    notificationController.configureContagionDistance(2D);
  }

  @Test
  public void getNotiConfig() {
    when(entityManagerMock.find(NotificationConfig.class, 1L)).thenReturn(new NotificationConfig());
    notificationController.getNotificationConfig();
  }

  @Test
  public void getNotiConfigNull() {
    notificationController.getNotificationConfig();
  }

  @Test
  public void updateNotiConfigNull() {
    notificationController.configureNotifications(new DTONotificationConfig());
  }

  @Test
  public void updateNotiConfig() {
    EasyRandom generator = new EasyRandom();
    DTONotificationConfig dtoNotificationConfig = generator.nextObject(DTONotificationConfig.class);
    NotificationConfig notificationConfig = new NotificationConfig();
    when(entityManagerMock.find(NotificationConfig.class, 1L)).thenReturn(notificationConfig);
    notificationController.configureNotifications(dtoNotificationConfig);
  }
}
