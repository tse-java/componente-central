package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.model.Symptom;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class SymptomControllerTest {
  @InjectMocks
  private SymptomController symptomController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Test
  public void newSymptom() throws VirusControlException {
    DTOSymptom dtoSymptom = new DTOSymptom();
    dtoSymptom.setName("Fiebre");
    dtoSymptom.setDescription("temperatura superior a 37 grados");
    DTOSymptom resultSymptom = symptomController.newSymptom(dtoSymptom);
    Assert.assertEquals(dtoSymptom.getName(), resultSymptom.getName());
  }

  @Test
  public void newSymtomBadRequest() {
    DTOSymptom dtoSymptom = new DTOSymptom();
    Assert.assertThrows(VirusControlException.class, () -> symptomController.newSymptom(dtoSymptom));
  }

  @Test
  public void getSymptoms() {
    List<Symptom> list = new ArrayList<>();
    Symptom symptom = new Symptom();
    symptom.setDescription("Dolor de tronco");
    symptom.setName("Cefalea");
    symptom.setId(1L);
    list.add(symptom);
    Mockito.when(entityManagerMock.createNamedQuery("Symptom.findAll", Symptom.class).getResultList()).thenReturn(list);
    List<DTOSymptom> result = symptomController.getSymptoms();
    Assert.assertEquals(result.get(0).getName(), symptom.getName());
    Assert.assertEquals(result.get(0).getDescription(), symptom.getDescription());
    Assert.assertEquals(result.get(0).getId(), symptom.getId());
  }
}
