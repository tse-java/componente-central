package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.HealthProviderCache;
import uy.viruscontrol.business.integration.SaludUyIntegrationService;
import uy.viruscontrol.business.model.HealthCareProvider;
import uy.viruscontrol.common.dto.DTOHealthProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class HealthProviderControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private HealthCareProviderController providerController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private HealthProviderCache healthProviderCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Mock
  SaludUyIntegrationService saludUyService;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void newHealthProvider() {
    HealthCareProvider provider = new HealthCareProvider();
    provider.setId(1L);
    provider.setName("casmu");
    provider.setUrl("casmu.com?wsdl");
    DTOHealthProvider dto = modelMapper.map(provider, DTOHealthProvider.class);
    DTOHealthProvider dto2 = providerController.newHealthCareProvider(dto);
    assertEquals(dto.getName(), dto2.getName());
  }

  @Test
  public void editHealthProvider() throws VirusControlException {
    HealthCareProvider provider = new HealthCareProvider();
    provider.setId(1L);
    provider.setName("casmu");
    provider.setUrl("casmu.com?wsdl");
    when(entityManagerMock.find(HealthCareProvider.class, 1L)).thenReturn(provider);
    DTOHealthProvider dto = providerController.editHealthProvider(1L, "otraUrl", "name");
    assertEquals("name", dto.getName());
    assertEquals("otraUrl", dto.getUrl());
  }

  @Test
  public void editHealthProviderNotFound() {
    when(entityManagerMock.find(HealthCareProvider.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> providerController.editHealthProvider(1L, "otraUrl", "name"));
  }

  @Test
  public void getHealthProvider() throws VirusControlException {
    HealthCareProvider provider = new HealthCareProvider();
    provider.setId(1L);
    provider.setName("casmu");
    provider.setUrl("casmu.com?wsdl");
    when(entityManagerMock.find(HealthCareProvider.class, 1L)).thenReturn(provider);
    DTOHealthProvider dto = providerController.getHealthProvider(1L);
    assertEquals("casmu", dto.getName());
  }

  @Test
  public void getHealthProviderNotFound() {
    when(entityManagerMock.find(HealthCareProvider.class, 1L)).thenReturn(null);
    assertThrows(VirusControlException.class, () -> providerController.getHealthProvider(1L));
  }

  @Test
  public void getHealthProviderList() {
    HealthCareProvider provider = new HealthCareProvider();
    provider.setId(1L);
    provider.setName("casmu");
    provider.setUrl("casmu.com?wsdl");

    HealthCareProvider provider2 = new HealthCareProvider();
    provider2.setId(1L);
    provider2.setName("hb");
    provider2.setUrl("casmu.com?wsdl");

    List<HealthCareProvider> providerList = new ArrayList<>();
    providerList.add(provider);
    providerList.add(provider2);

    when(entityManagerMock.createNamedQuery("HealthCareProvider.findAll", HealthCareProvider.class).getResultList())
      .thenReturn(providerList);
    List<DTOHealthProvider> dto = providerController.getHealthProviders();

    assertEquals("casmu", dto.get(0).getName());
    assertEquals("hb", dto.get(1).getName());
  }
}
