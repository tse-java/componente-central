package uy.viruscontrol.business.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.LabCache;
import uy.viruscontrol.business.model.ExamProvider;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class ExamProviderControllerTest {
  @InjectMocks
  private ExamProviderController examProviderController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private LabCache labCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  private final EasyRandom generator = new EasyRandom();
  private final ModelMapper modelMapper = new ModelMapper();

  @Test
  public void addLab() {
    DTOExamProvider dtoExamProvider = generator.nextObject(DTOExamProvider.class);
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    when(entityManagerMock.merge(any(ExamProvider.class))).thenReturn(examProvider);
    Assert.assertNotNull(examProviderController.addLab(dtoExamProvider));
  }

  @Test
  public void listLabs() {
    List<ExamProvider> examProviders = generator.objects(ExamProvider.class, 10).collect(Collectors.toList());
    when(entityManagerMock.createNamedQuery("ExamProvider.list", ExamProvider.class).getResultList()).thenReturn(examProviders);
    Assert.assertNotNull(examProviderController.listLabs());
  }

  @Test
  public void updateLab() throws VirusControlException {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    DTOExamProvider dtoExamProvider = modelMapper.map(examProvider, DTOExamProvider.class);
    when(entityManagerMock.find(ExamProvider.class, examProvider.getId())).thenReturn(examProvider);
    when(entityManagerMock.merge(any(ExamProvider.class))).thenReturn(examProvider);
    Assert.assertNotNull(examProviderController.updateLab(dtoExamProvider));
  }

  @Test
  public void updateLabNonExistent() {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    DTOExamProvider dtoExamProvider = modelMapper.map(examProvider, DTOExamProvider.class);
    Assert.assertThrows(VirusControlException.class, () -> examProviderController.updateLab(dtoExamProvider));
  }

  @Test
  public void deleteLab() throws VirusControlException {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    when(entityManagerMock.find(ExamProvider.class, examProvider.getId())).thenReturn(examProvider);
    when(
        entityManagerMock
          .createNamedQuery("ExamProvider.getExamsQuantity", Integer.class)
          .setParameter("id", examProvider.getId())
          .getSingleResult()
      )
      .thenReturn(0);
    examProviderController.deleteLab(examProvider.getId());
  }

  @Test
  public void deleteLabHasExams() {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    when(entityManagerMock.find(ExamProvider.class, examProvider.getId())).thenReturn(examProvider);
    when(
        entityManagerMock
          .createNamedQuery("ExamProvider.getExamsQuantity", Integer.class)
          .setParameter("id", examProvider.getId())
          .getSingleResult()
      )
      .thenReturn(1);
    Assert.assertThrows(VirusControlException.class, () -> examProviderController.deleteLab(examProvider.getId()));
  }

  @Test
  public void deleteLabNonExistent() {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    Assert.assertThrows(VirusControlException.class, () -> examProviderController.deleteLab(examProvider.getId()));
  }

  @Test
  public void updateLabStatus() throws VirusControlException {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    when(entityManagerMock.find(ExamProvider.class, examProvider.getId())).thenReturn(examProvider);
    examProviderController.updateLabStatus(examProvider.getId(), true);
  }

  @Test
  public void updateLabStatusNonExistent() {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    Assert.assertThrows(VirusControlException.class, () -> examProviderController.updateLabStatus(examProvider.getId(), true));
  }

  @Test
  public void setStatus() throws VirusControlException {
    ExamProvider examProvider = generator.nextObject(ExamProvider.class);
    when(entityManagerMock.find(ExamProvider.class, examProvider.getId())).thenReturn(examProvider);
    DTOExamProvider result = examProviderController.setStatus(examProvider.getId(), "fail", false);
    Assert.assertEquals("fail", result.getMessage());
    Assert.assertFalse(result.getOk());
  }

  @Test
  public void setStatusNonExistent() {
    Assert.assertThrows(VirusControlException.class, () -> examProviderController.setStatus(1L, "", false));
  }
}
