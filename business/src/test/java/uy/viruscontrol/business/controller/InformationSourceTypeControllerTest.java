package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.model.InformationSourceType;
import uy.viruscontrol.common.dto.DTOInformationSourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class InformationSourceTypeControllerTest {
  @InjectMocks
  private InformationSourceTypeController informationSourceTypeController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Test
  public void getInformationSourceTypesTest() {
    InformationSourceType type1 = new InformationSourceType();
    type1.setActive(true);
    type1.setDescription("des");
    type1.setId(1L);
    type1.setName("Facebook");

    InformationSourceType type2 = new InformationSourceType();
    type2.setActive(false);
    type2.setDescription("des");
    type2.setId(2L);
    type2.setName("Twitter");

    List<InformationSourceType> typeList = new ArrayList<>();
    typeList.add(type1);
    typeList.add(type2);

    Mockito
      .when(entityManagerMock.createNamedQuery("InformationSourceType.findAll", InformationSourceType.class).getResultList())
      .thenReturn(typeList);

    List<DTOInformationSourceType> dtoList = informationSourceTypeController.getInformationSourceTypes(null);
    assertEquals("Facebook", dtoList.get(0).getName());
  }

  @Test
  public void getInformationSourceTypesFilterTest() {
    InformationSourceType type2 = new InformationSourceType();
    type2.setActive(false);
    type2.setDescription("des");
    type2.setId(2L);
    type2.setName("Twitter");

    List<InformationSourceType> typeList = new ArrayList<>();
    typeList.add(type2);

    Mockito
      .when(
        entityManagerMock
          .createNamedQuery("InformationSourceType.findByStatus", InformationSourceType.class)
          .setParameter("active", false)
          .getResultList()
      )
      .thenReturn(typeList);

    List<DTOInformationSourceType> dtoList = informationSourceTypeController.getInformationSourceTypes(false);
    assertEquals("Twitter", dtoList.get(0).getName());
  }

  @Test
  public void getInformationSourceTypeByIdTest() throws VirusControlException {
    InformationSourceType type1 = new InformationSourceType();
    type1.setActive(true);
    type1.setDescription("des");
    type1.setId(1L);
    type1.setName("Facebook");

    Mockito.when(entityManagerMock.find(InformationSourceType.class, 1L)).thenReturn(type1);

    DTOInformationSourceType dto = informationSourceTypeController.getInformationSourceType(1L);
    assertEquals("Facebook", dto.getName());
  }

  @Test
  public void getInformationSourceTypeByIdTestNotFound() {
    Mockito.when(entityManagerMock.find(InformationSourceType.class, 1L)).thenReturn(null);

    assertThrows(VirusControlException.class, () -> informationSourceTypeController.getInformationSourceType(1L));
  }

  @Test
  public void setInformationSourceTypeStatusTest() throws VirusControlException {
    InformationSourceType type1 = new InformationSourceType();
    type1.setActive(true);
    type1.setDescription("des");
    type1.setId(1L);
    type1.setName("Facebook");
    Mockito.when(entityManagerMock.find(InformationSourceType.class, 1L)).thenReturn(type1);

    informationSourceTypeController.setInformationSourceTypeStatus(1L, false);
    assertEquals(false, type1.getActive());
  }

  @Test
  public void setInformationSourceTypeStatusTestNotFound() {
    Mockito.when(entityManagerMock.find(InformationSourceType.class, 1L)).thenReturn(null);

    assertThrows(VirusControlException.class, () -> informationSourceTypeController.setInformationSourceTypeStatus(1L, true));
  }
}
