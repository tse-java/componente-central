package uy.viruscontrol.business.controller;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.Symptom;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOResource;
import uy.viruscontrol.common.dto.DTOUpdateDisease;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class DiseaseControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private DiseaseController diseaseController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void requestNewDisease() throws VirusControlException {
    Disease disease = generator.nextObject(Disease.class);
    disease.setSymptoms(new HashSet<>());
    Symptom symptom = generator.nextObject(Symptom.class);
    disease.getSymptoms().add(symptom);
    DTODisease dtoDisease = modelMapper.map(disease, DTODisease.class);
    dtoDisease.setRecommendedResources(generator.objects(DTOResource.class, 10).collect(Collectors.toList()));
    when(entityManagerMock.find(Symptom.class, symptom.getId())).thenReturn(symptom);
    when(entityManagerMock.find(eq(Resource.class), anyLong())).thenReturn(new Resource());
    diseaseController.requestNewDisease(dtoDisease);
  }

  @Test
  public void requestNewDiseaseResourceNotFound() {
    Disease disease = generator.nextObject(Disease.class);
    disease.setSymptoms(new HashSet<>());
    Symptom symptom = generator.nextObject(Symptom.class);
    disease.getSymptoms().add(symptom);
    DTODisease dtoDisease = modelMapper.map(disease, DTODisease.class);
    dtoDisease.setRecommendedResources(generator.objects(DTOResource.class, 10).collect(Collectors.toList()));
    when(entityManagerMock.find(Symptom.class, symptom.getId())).thenReturn(symptom);
    when(entityManagerMock.find(eq(Resource.class), anyLong())).thenReturn(null);
    assertThrows(VirusControlException.class, () -> diseaseController.requestNewDisease(dtoDisease));
  }

  @Test
  public void requestNewDiseaseMissingData() {
    Disease disease = generator.nextObject(Disease.class);
    DTODisease dtoDisease = modelMapper.map(disease, DTODisease.class);
    assertThrows(VirusControlException.class, () -> diseaseController.requestNewDisease(dtoDisease));
  }

  @Test
  public void requestNewDiseaseSymptomNotFound() {
    Disease disease = generator.nextObject(Disease.class);
    disease.getSymptoms().add(generator.nextObject(Symptom.class));
    DTODisease dtoDisease = modelMapper.map(disease, DTODisease.class);
    assertThrows(VirusControlException.class, () -> diseaseController.requestNewDisease(dtoDisease));
  }

  @Test
  public void approveNewDisease() throws VirusControlException {
    Disease disease = generator.nextObject(Disease.class);
    disease.setRequestStatus(EnumRequestStatus.PENDING);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    diseaseController.approveNewDisease(disease.getId());
    assertEquals(EnumRequestStatus.APPROVED, disease.getRequestStatus());
  }

  @Test
  public void approveNewDiseaseNotFound() {
    assertThrows(VirusControlException.class, () -> diseaseController.approveNewDisease(1L));
  }

  @Test
  public void rejectNewDisease() throws VirusControlException {
    Disease disease = generator.nextObject(Disease.class);
    disease.setRequestStatus(EnumRequestStatus.PENDING);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    diseaseController.rejectNewDisease(disease.getId());
    assertEquals(EnumRequestStatus.REJECTED, disease.getRequestStatus());
  }

  @Test
  public void rejectNewDiseaseNotFound() {
    assertThrows(VirusControlException.class, () -> diseaseController.rejectNewDisease(1L));
  }

  @Test
  public void getDiseaseById() throws VirusControlException {
    Disease disease = generator.nextObject(Disease.class);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    DTODisease result = diseaseController.getDiseasesById(disease.getId());
    assertNotNull(result);
    assertEquals(result.getName(), disease.getName());
  }

  @Test
  public void getDiseaseByIdNotFound() {
    assertThrows(VirusControlException.class, () -> diseaseController.getDiseasesById(1L));
  }

  @Test
  public void getDiseasesByStatus() {
    List<Disease> diseases = new ArrayList<>();
    diseases.add(generator.nextObject(Disease.class));
    diseases.add(generator.nextObject(Disease.class));
    diseases.add(generator.nextObject(Disease.class));

    when(entityManagerMock.createNamedQuery("Disease.findAll", Disease.class).getResultList()).thenReturn(diseases);

    List<DTODisease> result = diseaseController.getDiseasesByStatus(null);
    assertEquals(3, result.size());
  }

  @Test
  public void getDiseasesByStatusOfStatus() {
    List<Disease> diseases = new ArrayList<>();
    diseases.add(generator.nextObject(Disease.class));
    diseases.add(generator.nextObject(Disease.class));
    diseases.add(generator.nextObject(Disease.class));

    when(
        entityManagerMock
          .createNamedQuery("Disease.findByRequestStatus", Disease.class)
          .setParameter("status", EnumRequestStatus.PENDING)
          .getResultList()
      )
      .thenReturn(diseases);

    List<DTODisease> result = diseaseController.getDiseasesByStatus(EnumRequestStatus.PENDING);
    assertEquals(3, result.size());
  }

  @Test
  public void updateDiseaseNotFound() {
    Disease disease = generator.nextObject(Disease.class);
    DTOUpdateDisease dtoDisease = modelMapper.map(disease, DTOUpdateDisease.class);

    assertThrows(VirusControlException.class, () -> diseaseController.updateDisease(disease.getId(), dtoDisease));
  }

  @Test
  public void updateDiseaseResurceNotFound() {
    Disease disease = generator.nextObject(Disease.class);
    DTOUpdateDisease dtoDisease = generator.nextObject(DTOUpdateDisease.class);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    assertThrows(VirusControlException.class, () -> diseaseController.updateDisease(disease.getId(), dtoDisease));
  }

  @Test
  public void updateDiseaseSymptomNotFound() {
    Disease disease = generator.nextObject(Disease.class);
    DTOUpdateDisease dtoDisease = generator.nextObject(DTOUpdateDisease.class);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    dtoDisease.getRecommendedResources().forEach(aLong -> when(entityManagerMock.find(Resource.class, aLong)).thenReturn(new Resource()));
    assertThrows(VirusControlException.class, () -> diseaseController.updateDisease(disease.getId(), dtoDisease));
  }

  @Test
  public void updateDisease() throws VirusControlException {
    Disease disease = generator.nextObject(Disease.class);
    DTOUpdateDisease dtoDisease = generator.nextObject(DTOUpdateDisease.class);
    when(entityManagerMock.find(Disease.class, disease.getId())).thenReturn(disease);
    dtoDisease
      .getRecommendedResources()
      .forEach(aLong -> when(entityManagerMock.find(Resource.class, aLong)).thenReturn(generator.nextObject(Resource.class)));
    dtoDisease
      .getSymptoms()
      .forEach(aLong -> when(entityManagerMock.find(Symptom.class, aLong)).thenReturn(generator.nextObject(Symptom.class)));
    when(entityManagerMock.merge(any(Disease.class))).thenReturn(generator.nextObject(Disease.class));
    diseaseController.updateDisease(disease.getId(), dtoDisease);
  }
}
