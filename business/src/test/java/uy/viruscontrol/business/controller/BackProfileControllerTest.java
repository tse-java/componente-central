package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.BackProfile;
import uy.viruscontrol.common.dto.DTOBackProfile;
import uy.viruscontrol.common.dto.DTONewBackProfile;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class BackProfileControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private BackProfileController backProfileController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void deleteUser() throws VirusControlException {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    when(entityManagerMock.find(BackProfile.class, backProfile.getId())).thenReturn(backProfile);
    backProfileController.delete(backProfile.getId());
  }

  @Test
  public void deleteNonExistentUser() {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    assertThrows(VirusControlException.class, () -> backProfileController.delete(backProfile.getId()));
  }

  @Test
  public void list() {
    Stream<BackProfile> backProfiles = generator.objects(BackProfile.class, 10);
    when(entityManagerMock.createNamedQuery("BackProfile.list", BackProfile.class).getResultStream()).thenReturn(backProfiles);
    List<DTOBackProfile> result = backProfileController.listUsers();
    assertEquals(10, result.size());
  }

  @Test
  public void updateBackprofile() throws VirusControlException {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    DTONewBackProfile dtoNewBackProfile = modelMapper.map(backProfile, DTONewBackProfile.class);
    when(entityManagerMock.find(BackProfile.class, dtoNewBackProfile.getId())).thenReturn(backProfile);
    backProfileController.updateUser(dtoNewBackProfile);
  }

  @Test
  public void updateBackprofileDuplicated() {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    BackProfile backProfile2 = generator.nextObject(BackProfile.class);
    DTONewBackProfile dtoNewBackProfile = modelMapper.map(backProfile, DTONewBackProfile.class);
    when(entityManagerMock.find(BackProfile.class, dtoNewBackProfile.getId())).thenReturn(backProfile);
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", dtoNewBackProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.ofNullable(backProfile2));
    assertThrows(VirusControlException.class, () -> backProfileController.updateUser(dtoNewBackProfile));
  }

  @Test
  public void updateBackprofileIgnorePasswd() throws VirusControlException {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    DTONewBackProfile dtoNewBackProfile = modelMapper.map(backProfile, DTONewBackProfile.class);
    dtoNewBackProfile.setPassword("");
    when(entityManagerMock.find(BackProfile.class, dtoNewBackProfile.getId())).thenReturn(backProfile);
    backProfileController.updateUser(dtoNewBackProfile);
  }

  @Test
  public void updateBackprofileNonExistent() {
    BackProfile backProfile = generator.nextObject(BackProfile.class);
    DTONewBackProfile dtoNewBackProfile = modelMapper.map(backProfile, DTONewBackProfile.class);
    assertThrows(VirusControlException.class, () -> backProfileController.updateUser(dtoNewBackProfile));
  }

  @Test
  public void doLogin() throws VirusControlException {
    BackProfile userProfile = new BackProfile();
    userProfile.setNickname("jperez");
    userProfile.setPassword("secure");
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", userProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(userProfile));
    DTOBackProfile result = backProfileController.doLogin("jperez", "secure");
    assertEquals(result.getNickname(), userProfile.getNickname());
  }

  @Test
  public void doLoginBadUser() {
    BackProfile userProfile = new BackProfile();
    userProfile.setNickname("jperez");
    userProfile.setPassword("secure");
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", userProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.empty());

    assertThrows(VirusControlException.class, () -> backProfileController.doLogin("jperez", "badpassword"));
  }

  @Test
  public void doLoginBadPassword() {
    BackProfile userProfile = new BackProfile();
    userProfile.setNickname("jperez");
    userProfile.setPassword("secure");
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", userProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(userProfile));
    assertThrows(VirusControlException.class, () -> backProfileController.doLogin("jperez", "badpassword"));
  }

  @Test
  public void newProfile() throws VirusControlException {
    DTONewBackProfile dtoProfile = new DTONewBackProfile();
    dtoProfile.setFullName("Juan Perez");
    dtoProfile.setNickname("jperez");
    dtoProfile.setRole(EnumBackofficeUserRole.ADMINISTRATOR);
    dtoProfile.setPassword("secure");
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", dtoProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.empty());

    DTOBackProfile result = backProfileController.newProfile(dtoProfile);
    assertEquals(dtoProfile.getId(), result.getId());
  }

  @Test
  public void newDuplicatedProfile() {
    DTONewBackProfile dtoProfile = new DTONewBackProfile();
    dtoProfile.setFullName("Juan Perez");
    dtoProfile.setNickname("jperez");
    dtoProfile.setRole(EnumBackofficeUserRole.ADMINISTRATOR);
    dtoProfile.setPassword("secure");
    BackProfile backProfile = modelMapper.map(dtoProfile, BackProfile.class);

    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", dtoProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.ofNullable(backProfile));

    assertThrows(VirusControlException.class, () -> backProfileController.newProfile(dtoProfile));
  }

  @Test
  public void changePassword() throws VirusControlException {
    BackProfile userProfile = new BackProfile();
    userProfile.setNickname("jperez");
    userProfile.setPassword("secure");

    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", userProfile.getNickname())
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.of(userProfile));

    backProfileController.changePassword("jperez", "secure", "super-secure");
    assertThrows(VirusControlException.class, () -> backProfileController.changePassword("jperez", "secure", "super-secure"));
  }

  @Test
  public void changeWrongPassword() {
    when(
        entityManagerMock
          .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
          .setParameter("nickname", "jperez")
          .getResultStream()
          .findFirst()
      )
      .thenReturn(Optional.empty());
    assertThrows(VirusControlException.class, () -> backProfileController.changePassword("jperez", "secure", "super-secure"));
  }
}
