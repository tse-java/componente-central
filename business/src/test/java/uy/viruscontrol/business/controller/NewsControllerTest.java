package uy.viruscontrol.business.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.cache.NewsCache;
import uy.viruscontrol.business.init.InformationSourceTypeInitializer;
import uy.viruscontrol.business.integration.RSSIntegrationService;
import uy.viruscontrol.business.integration.TwitterIntegrationService;
import uy.viruscontrol.business.model.*;
import uy.viruscontrol.common.enumerations.EnumInformationSourceStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class NewsControllerTest {
  @InjectMocks
  private NewsController newsController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManager;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private NewsCache newsCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private RSSIntegrationService rssIntegrationService;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private TwitterIntegrationService twitterIntegrationService;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void refreshNewsTest() throws VirusControlException {
    InformationSourceType type = generator.nextObject(InformationSourceType.class);
    type.setActive(true);

    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID)).thenReturn(type);
    when(entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID)).thenReturn(type);

    RSSInformationSource rssSource = new RSSInformationSource();
    rssSource.setStatus(EnumInformationSourceStatus.ACTIVE);
    rssSource.setUrl("url");
    rssSource.setName("zczx");
    rssSource.setId(1L);

    List<RSSInformationSource> rssList = new ArrayList<>();
    rssList.add(rssSource);
    when(entityManager.createNamedQuery("RSSInformationSource.findAll", RSSInformationSource.class).getResultList()).thenReturn(rssList);

    TwitterInformationSource twitterInformationSource = generator.nextObject(TwitterInformationSource.class);
    List<TwitterInformationSource> twitterInformationSources = new ArrayList<>();
    twitterInformationSources.add(twitterInformationSource);

    when(entityManager.createNamedQuery("TwitterInformationSource.findAll", TwitterInformationSource.class).getResultList())
      .thenReturn(twitterInformationSources);

    when(rssIntegrationService.getNews(rssSource)).thenReturn(new ArrayList<>());
    when(twitterIntegrationService.getNews(twitterInformationSource)).thenReturn(new ArrayList<>());

    newsController.refreshRSS();
    newsController.refreshTwitter();
  }

  @Test
  public void getLatestNewsTest() {
    List<News> news = new ArrayList<>();
    news.add(generator.nextObject(RSSNews.class));
    news.add(new TwitterNews());

    when(newsCache.getLatestNews()).thenReturn(news);

    newsController.getLatestNews();
  }
}
