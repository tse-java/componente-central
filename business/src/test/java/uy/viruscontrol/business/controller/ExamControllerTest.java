package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;
import javax.enterprise.event.Event;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.CaseCache;
import uy.viruscontrol.business.cache.UserDiseaseCache;
import uy.viruscontrol.business.dto.DTOContagionAlert;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.Exam;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.MedicalCase;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOUpdateExam;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class ExamControllerTest {
  @InjectMocks
  private ExamController examController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private Event<Exam> notifyContagion;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private Event<DTOContagionAlert> triggerContagionAlert;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private UserDiseaseCache userDiaseaseCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private MessageProducer messageProducer;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private CaseCache caseCache;

  @Test
  public void updateExamNonExistent() {
    assertThrows(VirusControlException.class, () -> examController.updateExam(new DTOUpdateExam()));
  }

  private final ModelMapper modelMapper = new ModelMapper();

  @Test
  public void fetchExams() {
    EasyRandom generator = new EasyRandom();
    Stream<Exam> exams = generator.objects(Exam.class, 10);
    when(entityManagerMock.createNamedQuery("Exam.getUserExams", Exam.class).setParameter("userId", 1L).getResultStream())
      .thenReturn(exams);
    List<DTOExam> result = examController.getUserExams(1L);
    assertEquals(10, result.size());
  }

  @Test
  public void updateExam() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);

    DTOUpdateExam dtoUpdateExam = new DTOUpdateExam();
    dtoUpdateExam.setPositive(true);
    when(entityManagerMock.find(Exam.class, dtoUpdateExam.getId())).thenReturn(exam);
    when(entityManagerMock.merge(any(Exam.class))).thenReturn(exam);
    examController.updateExam(dtoUpdateExam);
  }

  @Test
  public void updateExamPositive() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    exam.setMedicalCase(generator.nextObject(MedicalCase.class));
    exam.getMedicalCase().setStatus(EnumCaseStatus.SUSPECT);
    DTOUpdateExam dtoUpdateExam = new DTOUpdateExam();
    dtoUpdateExam.setPositive(true);
    when(entityManagerMock.find(Exam.class, dtoUpdateExam.getId())).thenReturn(exam);
    when(entityManagerMock.merge(any(Exam.class))).thenReturn(exam);
    examController.updateExam(dtoUpdateExam);
  }

  @Test
  public void updateExamNegative() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    exam.setMedicalCase(generator.nextObject(MedicalCase.class));
    exam.getMedicalCase().setStatus(EnumCaseStatus.SUSPECT);
    DTOUpdateExam dtoUpdateExam = new DTOUpdateExam();
    dtoUpdateExam.setPositive(false);
    when(entityManagerMock.find(Exam.class, dtoUpdateExam.getId())).thenReturn(exam);
    when(entityManagerMock.merge(any(Exam.class))).thenReturn(exam);
    examController.updateExam(dtoUpdateExam);
  }

  @Test
  public void updateExamCured() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    exam.setMedicalCase(generator.nextObject(MedicalCase.class));
    exam.getMedicalCase().setStatus(EnumCaseStatus.CONFIRMED);
    DTOUpdateExam dtoUpdateExam = new DTOUpdateExam();
    dtoUpdateExam.setPositive(false);
    when(entityManagerMock.find(Exam.class, dtoUpdateExam.getId())).thenReturn(exam);
    when(entityManagerMock.merge(any(Exam.class))).thenReturn(exam);
    examController.updateExam(dtoUpdateExam);
  }

  @Test
  public void newExam() throws VirusControlException {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    DTOExam dtoExam = modelMapper.map(exam, DTOExam.class);
    DTODisease dtoDisease = modelMapper.map(exam.getMedicalCase().getDisease(), DTODisease.class);
    dtoExam.setDisease(dtoDisease);
    exam.getRequestingDoctor().setIsDoctor(true);
    when(entityManagerMock.find(FrontProfile.class, exam.getRequestingDoctor().getId())).thenReturn(exam.getRequestingDoctor());
    when(entityManagerMock.find(FrontProfile.class, exam.getPatient().getId())).thenReturn(exam.getPatient());
    when(entityManagerMock.find(Disease.class, exam.getMedicalCase().getDisease().getId())).thenReturn(exam.getMedicalCase().getDisease());
    examController.newExam(exam.getRequestingDoctor().getId(), exam.getPatient().getId(), dtoDisease);
  }

  @Test
  public void newExamNotADoctor() {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    DTOExam dtoExam = modelMapper.map(exam, DTOExam.class);
    DTODisease dtoDisease = modelMapper.map(exam.getMedicalCase().getDisease(), DTODisease.class);
    dtoExam.setDisease(dtoDisease);
    exam.getRequestingDoctor().setIsDoctor(false);
    when(entityManagerMock.find(FrontProfile.class, exam.getRequestingDoctor().getId())).thenReturn(exam.getRequestingDoctor());
    when(entityManagerMock.find(FrontProfile.class, exam.getPatient().getId())).thenReturn(exam.getPatient());
    assertThrows(
      VirusControlException.class,
      () -> examController.newExam(exam.getRequestingDoctor().getId(), exam.getPatient().getId(), dtoDisease)
    );
  }

  @Test
  public void newExamUserNotFound() {
    EasyRandom generator = new EasyRandom();
    Exam exam = generator.nextObject(Exam.class);
    DTOExam dtoExam = modelMapper.map(exam, DTOExam.class);
    DTODisease dtoDisease = modelMapper.map(exam.getMedicalCase().getDisease(), DTODisease.class);
    dtoExam.setDisease(dtoDisease);
    exam.getRequestingDoctor().setIsDoctor(false);
    assertThrows(
      VirusControlException.class,
      () -> examController.newExam(exam.getRequestingDoctor().getId(), exam.getPatient().getId(), dtoDisease)
    );
  }
}
