package uy.viruscontrol.business.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.ResourceAvailability;
import uy.viruscontrol.common.dto.DTOResource;
import uy.viruscontrol.common.dto.DTOResourceAvailability;
import uy.viruscontrol.common.dto.DTOUpdateResource;
import uy.viruscontrol.common.enumerations.EnumResourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@RunWith(MockitoJUnitRunner.class)
public class ResourceControllerTest {
  private final ModelMapper modelMapper = new ModelMapper();

  @InjectMocks
  private ResourceController resourceController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void listResourcesSubscriptionsTest() {
    FrontProfile frontProfile = generator.nextObject(FrontProfile.class);
    List<Resource> resourceList1 = generator.objects(Resource.class, 10).collect(Collectors.toList());
    List<Resource> resourceList2 = generator.objects(Resource.class, 10).collect(Collectors.toList());
    when(
        entityManagerMock
          .createNamedQuery("Resource.subscribed", Resource.class)
          .setParameter("userId", frontProfile.getId())
          .getResultList()
      )
      .thenReturn(resourceList1);
    when(
        entityManagerMock
          .createNamedQuery("Resource.notSubscribed", Resource.class)
          .setParameter("userId", frontProfile.getId())
          .getResultList()
      )
      .thenReturn(resourceList2);
    resourceController.listResourcesSubscriptions(frontProfile.getId());
  }

  @Test
  public void listResourcesSubscriptionsNullUserTest() {
    List<Resource> resourceList1 = generator.objects(Resource.class, 10).collect(Collectors.toList());
    when(entityManagerMock.createNamedQuery("Resource.list", Resource.class).getResultList()).thenReturn(resourceList1);
    resourceController.listResourcesSubscriptions(null);
  }

  @Test
  public void newResource() {
    DTOResource dtoResource = new DTOResource();
    dtoResource.setName("Perifar");
    dtoResource.setDescription("Via Oral");
    dtoResource.setResourceType(EnumResourceType.PREVENTION);
    resourceController.newResource(dtoResource);
  }

  @Test
  public void listResources() {
    Resource resource = new Resource();
    resource.setDescription("100% al palo");
    resource.setName("Viagra");
    resource.setType(EnumResourceType.PREVENTION);

    List<Resource> resources = new ArrayList<>();
    resources.add(resource);
    when(entityManagerMock.createNamedQuery("Resource.list", Resource.class).getResultList()).thenReturn(resources);
    List<DTOResource> result = resourceController.listResources();
    assertEquals(1, result.size());
    assertEquals(EnumResourceType.PREVENTION, resources.get(0).getType());
  }

  @Test
  public void subscribeResource() throws VirusControlException {
    Resource resource = new Resource();
    resource.setId(1L);
    resource.setDescription("100% al palo");
    resource.setName("Viagra");
    resource.setType(EnumResourceType.PREVENTION);

    FrontProfile user = new FrontProfile();
    user.setId(1L);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(user);
    when(entityManagerMock.find(Resource.class, 1L)).thenReturn(resource);

    resourceController.subscribeResource(1L, 1L);
    assertEquals(1, resource.getSubscribers().size());
  }

  @Test
  public void subscribeResourceAlreadyExists() {
    Resource resource = new Resource();
    resource.setId(1L);
    resource.setDescription("100% al palo");
    resource.setName("Viagra");
    resource.setType(EnumResourceType.PREVENTION);

    FrontProfile user = new FrontProfile();
    user.setId(1L);
    user.getSubscriptions().add(resource);
    resource.getSubscribers().add(user);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(user);
    when(entityManagerMock.find(Resource.class, 1L)).thenReturn(resource);

    assertThrows(VirusControlException.class, () -> resourceController.subscribeResource(1L, 1L));
  }

  @Test
  public void subscribeResourceNotExistsResource() {
    FrontProfile user = new FrontProfile();
    user.setId(1L);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(user);
    when(entityManagerMock.find(Resource.class, 1L)).thenReturn(null);

    assertThrows(VirusControlException.class, () -> resourceController.subscribeResource(1L, 1L));
  }

  @Test
  public void subscribeResourceNotExistsUser() {
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(null);

    assertThrows(VirusControlException.class, () -> resourceController.subscribeResource(1L, 1L));
  }

  @Test
  public void unsubscribeResource() throws VirusControlException {
    Resource resource = new Resource();
    resource.setId(1L);
    resource.setDescription("100% al palo");
    resource.setName("Viagra");
    resource.setType(EnumResourceType.PREVENTION);

    FrontProfile user = new FrontProfile();
    user.setId(1L);
    resource.getSubscribers().add(user);

    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(user);
    when(entityManagerMock.find(Resource.class, 1L)).thenReturn(resource);
    resourceController.unsubscribeResource(1L, 1L);
    assertEquals(0, resource.getSubscribers().size());
  }

  @Test
  public void unsubscribeResourceNotExistsResource() {
    Resource resource = new Resource();
    resource.setId(1L);
    resource.setDescription("100% al palo");
    resource.setName("Viagra");
    resource.setType(EnumResourceType.PREVENTION);

    FrontProfile user = new FrontProfile();
    user.setId(1L);
    user.getSubscriptions().add(resource);
    when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(user);

    assertThrows(VirusControlException.class, () -> resourceController.unsubscribeResource(1L, 2L));
  }

  @Test
  public void unsubscribeResourceNonExistentUser() {
    assertThrows(VirusControlException.class, () -> resourceController.unsubscribeResource(1L, 1L));
  }

  @Test
  public void getAvailability() throws VirusControlException {
    Disease disease = new Disease();
    disease.setId(1L);

    ResourceAvailability resourceAvailability = new ResourceAvailability();
    resourceAvailability.setId(1L);
    resourceAvailability.setQuantity(BigDecimal.TEN);
    List<ResourceAvailability> resourceAvailabilityList = new ArrayList<>();
    resourceAvailabilityList.add(resourceAvailability);
    when(entityManagerMock.find(Disease.class, 1L)).thenReturn(disease);
    when(
        entityManagerMock
          .createNamedQuery("ResourceAvailibility.Filter", ResourceAvailability.class)
          .setParameter("disease", disease)
          .setParameter("resource_type", EnumResourceType.PREVENTION)
          .setParameter("provider_name", "%" + "hola" + "%")
          .setParameter("location_filter", "%" + "hola" + "%")
          .getResultList()
      )
      .thenReturn(resourceAvailabilityList);
    List<DTOResourceAvailability> dtoResourceAvailabilityList = resourceController.getAvailability(
      1L,
      EnumResourceType.PREVENTION,
      "hola",
      "hola"
    );
    assertEquals(1, dtoResourceAvailabilityList.size());
  }

  @Test
  public void getAvailabilityNotFound() {
    when(entityManagerMock.find(Disease.class, 1L)).thenReturn(null);

    assertThrows(VirusControlException.class, () -> resourceController.getAvailability(1L, EnumResourceType.PREVENTION, "hola", "hola"));
  }

  @Test
  public void deleteResourceNotFound() {
    final EasyRandom generator = new EasyRandom();
    Resource resource = generator.nextObject(Resource.class);
    assertThrows(VirusControlException.class, () -> resourceController.deleteResource(resource.getId()));
  }

  @Test
  public void deleteResource() throws VirusControlException {
    final EasyRandom generator = new EasyRandom();
    Resource resource = generator.nextObject(Resource.class);
    when(entityManagerMock.find(Resource.class, resource.getId())).thenReturn(resource);
    resourceController.deleteResource(resource.getId());
  }

  @Test
  public void updateResource() throws VirusControlException {
    final EasyRandom generator = new EasyRandom();
    Resource resource = generator.nextObject(Resource.class);
    DTOUpdateResource dtoUpdateResource = new DTOUpdateResource();

    dtoUpdateResource.setName("Papelpalculo");
    dtoUpdateResource.setDescription("superdesc");

    when(entityManagerMock.find(Resource.class, resource.getId())).thenReturn(resource);
    DTOResource result = resourceController.updateResource(resource.getId(), dtoUpdateResource);
    assertEquals(dtoUpdateResource.getName(), result.getName());
    assertEquals(dtoUpdateResource.getDescription(), result.getDescription());
  }

  @Test
  public void updateResourceNotFound() {
    final EasyRandom generator = new EasyRandom();
    Resource resource = generator.nextObject(Resource.class);
    DTOUpdateResource dtoUpdateResource = new DTOUpdateResource();
    assertThrows(VirusControlException.class, () -> resourceController.updateResource(resource.getId(), dtoUpdateResource));
  }
}
