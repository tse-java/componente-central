package uy.viruscontrol.business.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.cache.HealthProviderCache;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.business.integration.DNICIntegrationService;
import uy.viruscontrol.business.integration.HealthProviderIntegrationService;
import uy.viruscontrol.business.integration.SaludUyIntegrationService;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.*;
import uy.viruscontrol.common.dto.DTOHealthProvider;
import uy.viruscontrol.common.dto.DTOLocation;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;
import uy.viruscontrol.cps.service.DtoAppointment;
import uy.viruscontrol.cps.service.DtoMedic;
import uy.viruscontrol.pi.service.DtoCitizen;

@RunWith(MockitoJUnitRunner.class)
public class MedicalAppointmentControllerTest {
  @InjectMocks
  MedicalAppointmentController appointmentService;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private EntityManager entityManagerMock;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  SaludUyIntegrationService saludUyService;

  @Mock
  HealthProviderCache healthProviderCache;

  @Mock
  HealthProviderIntegrationService healthProviderService;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  MessageProducer messageProducer;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  DNICIntegrationService dnicIntegrationService;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void newAppointment() {
    FrontProfile patient = new FrontProfile();
    patient.setId(1L);
    patient.setDocumentNumber("6.666.666-6");
    patient.setFirstname("Juan");
    patient.setLastname("Perez");
    Location location = new Location();
    location.setStreet("Elm Street");
    location.setDepartment(UruguayanDepartment.CANELONES);
    Mockito.when(entityManagerMock.find(FrontProfile.class, 1L)).thenReturn(patient);

    Symptom symptom = new Symptom();
    symptom.setId(1L);
    symptom.setDescription("dsfsd");
    symptom.setName("tu vieja");
    Mockito.when(entityManagerMock.find(Symptom.class, 1L)).thenReturn(symptom);

    HealthCareProvider healthCareProvider = new HealthCareProvider();
    healthCareProvider.setId(1L);
    healthCareProvider.setUrl("url");
    healthCareProvider.setName("name");

    HealthCareProvider healthCareProvider2 = new HealthCareProvider();
    healthCareProvider2.setId(1L);
    healthCareProvider2.setUrl("url2");
    healthCareProvider2.setName("name2");
    List<HealthCareProvider> providerList = new ArrayList<>();
    providerList.add(healthCareProvider);
    providerList.add(healthCareProvider2);
    Mockito
      .when(entityManagerMock.createNamedQuery("HealthCareProvider.findAll", HealthCareProvider.class).getResultList())
      .thenReturn(providerList);

    DtoAppointment dtoAppointment = new DtoAppointment();
    dtoAppointment.setDate(new GregorianCalendar());
    DtoMedic dtoMedic = new DtoMedic();
    dtoMedic.setDocumentNumber(1234567);
    dtoMedic.setName("dsa");
    dtoAppointment.setMedic(dtoMedic);

    DtoCitizen citizen = new DtoCitizen();
    citizen.setNationality("Uruguay");
    citizen.setLastName("Gomez");
    citizen.setFirstName("Jose");
    citizen.setBirthDay(new GregorianCalendar());
    citizen.setDocumentNumber("1234567");

    DTORequestMedicalAppointment dtoRequestMedicalAppointment = new DTORequestMedicalAppointment();
    dtoRequestMedicalAppointment.setSymptomIds(new ArrayList<>());
    dtoRequestMedicalAppointment.getSymptomIds().add(1L);
    DTOLocation dtolocation = new DTOLocation();
    dtolocation.setStreet("Elm Street");
    dtolocation.setDepartment(UruguayanDepartment.CANELONES);
    dtoRequestMedicalAppointment.setLocation(dtolocation);
    dtoRequestMedicalAppointment.setPatientId(1L);

    MedicalAppointment appointment = new MedicalAppointment();
    appointment.setPatient(patient);
    appointment.setLocation(location);
    appointment.setSymptoms(new HashSet<>());
    appointment.getSymptoms().add(symptom);
    appointment.setRequestStatus(EnumRequestStatus.PENDING);
    appointment.setStatus(EnumMedicalAppointmentStatus.PENDING);
    appointment.setHealthCareProvider(healthCareProvider);
    appointment.setId(1L);
    when(entityManagerMock.merge(any())).thenReturn(appointment);

    appointmentService.newAppointment(dtoRequestMedicalAppointment);
  }

  @Test
  public void assignAppointmentUseUserProvider() {
    final EasyRandom generator = new EasyRandom();
    DTORequestMedicalAppointment requestMedicalAppointment = generator.nextObject(DTORequestMedicalAppointment.class);
    requestMedicalAppointment.setUseUserHealthProvider(true);
    MedicalAppointment medicalAppointment = generator.nextObject(MedicalAppointment.class);
    DtoAppointment dtoAppointment = generator.nextObject(DtoAppointment.class);

    when(saludUyService.getPatientHealthProvider(anyString())).thenReturn(1L);
    when(healthProviderService.requestAppointment(anyString(), anyString())).thenReturn(generator.nextObject(DtoAppointment.class));
    when(dnicIntegrationService.getCitizenInformation(dtoAppointment.getMedic().getDocumentNumber().toString()))
      .thenReturn(generator.nextObject(DtoCitizen.class));

    when(entityManagerMock.find(MedicalAppointment.class, requestMedicalAppointment.getId())).thenReturn(medicalAppointment);
    when(entityManagerMock.find(HealthCareProvider.class, 1L)).thenReturn(generator.nextObject(HealthCareProvider.class));

    appointmentService.findAppointment(requestMedicalAppointment);
  }

  @Test
  public void assignAppointment() {
    final EasyRandom generator = new EasyRandom();
    DTORequestMedicalAppointment requestMedicalAppointment = generator.nextObject(DTORequestMedicalAppointment.class);
    requestMedicalAppointment.setUseUserHealthProvider(false);
    MedicalAppointment medicalAppointment = generator.nextObject(MedicalAppointment.class);
    DtoAppointment dtoAppointment = generator.nextObject(DtoAppointment.class);
    when(healthProviderCache.getRandomProvider()).thenReturn(generator.nextObject(DTOHealthProvider.class));
    when(healthProviderService.requestAppointment(anyString(), anyString())).thenReturn(generator.nextObject(DtoAppointment.class));
    when(dnicIntegrationService.getCitizenInformation(dtoAppointment.getMedic().getDocumentNumber().toString()))
      .thenReturn(generator.nextObject(DtoCitizen.class));

    when(entityManagerMock.find(MedicalAppointment.class, requestMedicalAppointment.getId())).thenReturn(medicalAppointment);
    appointmentService.findAppointment(requestMedicalAppointment);
  }

  @Test
  public void getAppointmentsAsDoctor() {
    Stream<MedicalAppointment> medicalAppointmentStream = generator.objects(MedicalAppointment.class, 10);
    when(
        entityManagerMock
          .createNamedQuery("MedicalAppointment.listAsDoctor", MedicalAppointment.class)
          .setParameter("doctorId", 1L)
          .getResultStream()
      )
      .thenReturn(medicalAppointmentStream);
    List<DTOMedicalAppointment> result = appointmentService.getDoctorAppointments(1L);
    Assert.assertEquals(10, result.size());
  }

  @Test
  public void getAppointmentsAsPatient() {
    Stream<MedicalAppointment> medicalAppointmentStream = generator.objects(MedicalAppointment.class, 10);
    when(
        entityManagerMock
          .createNamedQuery("MedicalAppointment.listAsPatient", MedicalAppointment.class)
          .setParameter("patientId", 1L)
          .getResultStream()
      )
      .thenReturn(medicalAppointmentStream);
    List<DTOMedicalAppointment> result = appointmentService.getPatientAppointments(1L);
    Assert.assertEquals(10, result.size());
  }
}
