package uy.viruscontrol.business.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import dev.morphia.geo.Point;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uy.viruscontrol.business.cache.ContagionDistanceCache;
import uy.viruscontrol.business.dao.LocationDao;
import uy.viruscontrol.business.model.LocationReport;
import uy.viruscontrol.common.dto.DTOContagionReport;
import uy.viruscontrol.common.dto.DTOLocationReport;

@RunWith(MockitoJUnitRunner.class)
public class ReportControllerTest {
  @InjectMocks
  private LocationController locationController;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ContagionDistanceCache contagionDistanceCache;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private LocationDao locationDao;

  private final EasyRandom generator = new EasyRandom();

  @Test
  public void newReport() {
    DTOLocationReport dtoLocationReport = generator.nextObject(DTOLocationReport.class);
    locationController.newReport(dtoLocationReport);
  }

  @Test
  public void findContagionByLocation() {
    DTOContagionReport dtoContagionReport = generator.nextObject(DTOContagionReport.class);
    List<LocationReport> locationReports = generator.objects(LocationReport.class, 5).collect(Collectors.toList());
    when(locationDao.findPointsNearLocationAtTime(anyList(), any(Date.class), any(Date.class), any(Point.class), any(Double.class)))
      .thenReturn(locationReports);
    locationController.findContagionByLocation(dtoContagionReport);
  }

  @Test
  public void getLocationsUntilDate() {
    List<LocationReport> locationReports = generator.objects(LocationReport.class, 5).collect(Collectors.toList());
    when(locationDao.getLocationsUntilDate(anyLong(), any(Date.class))).thenReturn(locationReports);
    locationController.getLocationsUntilDate(1L, new Date());
  }
}
