package uy.viruscontrol.business.cache;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.dao.MongoDatastore;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.common.dto.DTODepartment;

@Singleton
@Startup
public class CaseCache {
  private RMap<Long, List<DTODepartment>> casesCache;

  @PostConstruct
  public void init() {
    MongoDatastore.getInstance();
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.casesCache = redisson.getMap("casesCache");
    this.casesCache.clear();
  }

  public List<DTODepartment> getCases(Long diseaseId) {
    return casesCache.get(diseaseId);
  }

  public void addCases(Long diseaseId, List<DTODepartment> dtoDepartments) {
    casesCache.fastPut(diseaseId, dtoDepartments);
  }

  public void clear(Long diseaseId) {
    casesCache.fastRemove(diseaseId);
  }
}
