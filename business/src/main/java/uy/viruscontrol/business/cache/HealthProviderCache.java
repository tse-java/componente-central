package uy.viruscontrol.business.cache;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.*;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.business.service.HealthCareProviderService;
import uy.viruscontrol.common.dto.DTOHealthProvider;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup
public class HealthProviderCache {
  @EJB
  private HealthCareProviderService healthCareProviderService;

  private RMap<Long, DTOHealthProvider> providerCache;
  private final Random randomGenerator = new SecureRandom();

  @PostConstruct
  public void init() {
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.providerCache = redisson.getMap("healthCache");
    this.providerCache.clear();
    healthCareProviderService.getHealthProviders().forEach(healthProvider -> providerCache.fastPut(healthProvider.getId(), healthProvider));
  }

  public synchronized void addProvider(DTOHealthProvider healthCareProvider) {
    providerCache.fastPut(healthCareProvider.getId(), healthCareProvider);
  }

  public synchronized void removeLab(Long healthProviderId) {
    providerCache.fastRemove(healthProviderId);
  }

  public synchronized DTOHealthProvider getRandomProvider() {
    List<Long> keys = new ArrayList<>();
    providerCache.forEach((key, dtoExamProvider) -> keys.add(key));
    int index = randomGenerator.nextInt(keys.size());
    Long key = keys.get(index);
    return providerCache.get(key);
  }

  public List<DTOHealthProvider> getExamProviders() {
    return providerCache.readAllEntrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
  }
}
