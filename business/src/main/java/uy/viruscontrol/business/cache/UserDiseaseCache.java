package uy.viruscontrol.business.cache;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import lombok.Getter;
import lombok.Setter;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.init.AppProperties;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup
@Getter
@Setter
public class UserDiseaseCache {
  private RMap<Long, Set<Long>> usersCases;

  @PostConstruct
  public void init() {
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.usersCases = redisson.getMap("userCases");
    this.usersCases.clear();
  }

  public synchronized void storeDisease(Long userId, Long diseaseId) {
    Set<Long> diseases = usersCases.get(userId);
    if (null == diseases) {
      diseases = new HashSet<>();
      diseases.add(diseaseId);
      usersCases.put(userId, diseases);
    } else {
      diseases.add(diseaseId);
    }
  }

  public synchronized void removeDisease(Long userId, Long diseaseId) {
    Set<Long> diseases = usersCases.get(userId);
    if (diseases != null) {
      diseases.remove(diseaseId);
    }
  }

  public synchronized boolean userHasCase(Long userId, Long disseaseId) {
    Set<Long> diseases = usersCases.get(userId);
    if (null == diseases) {
      return false;
    } else {
      return diseases.contains(disseaseId);
    }
  }
}
