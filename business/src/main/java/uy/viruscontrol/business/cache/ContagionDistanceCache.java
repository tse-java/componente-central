package uy.viruscontrol.business.cache;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.business.service.SettingsService;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Singleton
@Startup
@Slf4j
public class ContagionDistanceCache {
  private static final long SETTING_ID = 1L;

  @EJB
  private SettingsService settingsService;

  private RMap<Long, Double> contagionDistance;

  @PostConstruct
  public void init() {
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.contagionDistance = redisson.getMap("contagionConfigCache");
    this.contagionDistance.clear();
    Double distance = settingsService.getContagionDistance();
    contagionDistance.fastPut(SETTING_ID, distance);
  }

  public void update(Double distance) {
    contagionDistance.fastPut(SETTING_ID, distance);
  }

  public Double get() {
    return contagionDistance.get(SETTING_ID);
  }
}
