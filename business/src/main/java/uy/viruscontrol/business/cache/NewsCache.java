package uy.viruscontrol.business.cache;

import java.util.*;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.redisson.Redisson;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.business.model.News;
import uy.viruscontrol.business.model.RSSNews;
import uy.viruscontrol.business.model.TwitterNews;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup
public class NewsCache {
  private static final int MAX_NEWS_CACHE = 50;

  private RList<TwitterNews> twitterNews;
  private RList<RSSNews> rssNews;

  @PostConstruct
  public void init() {
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.rssNews = redisson.getList("rssNews");
    this.twitterNews = redisson.getList("twitterNews");
  }

  public List<News> getLatestNews() {
    List<News> news = new ArrayList<>();
    news.addAll(this.twitterNews);
    news.addAll(this.rssNews);
    Collections.shuffle(news);
    return news;
  }

  public void addRSSNews(List<RSSNews> newsToAdd) {
    if (newsToAdd != null && !newsToAdd.isEmpty()) {
      RSSNews firstNewsToAdd = newsToAdd.get(0);
      RSSNews lastNewsAdded = rssNews
        .stream()
        .filter(news -> news.getSource().getId().equals(firstNewsToAdd.getSource().getId()))
        .findFirst()
        .orElse(null);
      List<RSSNews> notExistentNews = new ArrayList<>();
      for (RSSNews newsItem : newsToAdd) {
        if (lastNewsAdded != null && newsItem.getTitle().equals(lastNewsAdded.getTitle())) {
          break;
        } else {
          notExistentNews.add(newsItem);
        }
      }
      this.rssNews.addAll(notExistentNews);
      this.rssNews.sort(Collections.reverseOrder());
      int limit = MAX_NEWS_CACHE / 2;
      if (this.rssNews.size() < limit) {
        limit = this.rssNews.size();
      }
      List<RSSNews> list = this.rssNews.readAll().subList(0, limit);
      this.rssNews.clear();
      this.rssNews.addAll(list);
    }
  }

  public void addTwitterNews(List<TwitterNews> newsToAdd) {
    if (newsToAdd != null && !newsToAdd.isEmpty()) {
      TwitterNews firstNewsToAdd = newsToAdd.get(0);
      TwitterNews lastNewsAdded = twitterNews
        .stream()
        .filter(news -> news.getNickName().equals(firstNewsToAdd.getNickName()))
        .findFirst()
        .orElse(null);
      List<TwitterNews> notExistentNews = new ArrayList<>();
      for (TwitterNews newsItem : newsToAdd) {
        if (lastNewsAdded != null && newsItem.getNickName().equals(lastNewsAdded.getNickName())) {
          break;
        } else {
          notExistentNews.add(newsItem);
        }
      }
      this.twitterNews.addAll(notExistentNews);
      this.twitterNews.sort(Collections.reverseOrder());
      int limit = MAX_NEWS_CACHE / 2;
      if (this.twitterNews.size() < limit) {
        limit = this.twitterNews.size();
      }
      List<TwitterNews> list = this.twitterNews.readAll().subList(0, limit);
      this.twitterNews.clear();
      this.twitterNews.addAll(list);
    }
  }
}
