package uy.viruscontrol.business.cache;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.business.service.SettingsService;
import uy.viruscontrol.common.dto.DTONotificationConfig;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Singleton
@Startup
@Slf4j
public class NotificationConfigCache {
  @EJB
  private SettingsService settingsService;

  private RMap<Long, DTONotificationConfig> notificationCache;

  @PostConstruct
  public void init() {
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.notificationCache = redisson.getMap("notificationCache");
    this.notificationCache.clear();
    DTONotificationConfig dtoNotificationConfig;
    dtoNotificationConfig = settingsService.getNotificationConfig();
    notificationCache.put(1L, dtoNotificationConfig);
  }

  public void update(DTONotificationConfig config) {
    notificationCache.put(1L, config);
  }

  public DTONotificationConfig get() {
    return notificationCache.get(1L);
  }
}
