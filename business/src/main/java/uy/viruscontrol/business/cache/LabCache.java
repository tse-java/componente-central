package uy.viruscontrol.business.cache;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import uy.viruscontrol.business.dao.MongoDatastore;
import uy.viruscontrol.business.init.AppProperties;
import uy.viruscontrol.business.service.ExamProviderService;
import uy.viruscontrol.common.dto.DTOExamProvider;

@Singleton
@Startup
public class LabCache {
  @EJB
  private ExamProviderService labsController;

  private final Random randomGenerator = new SecureRandom();
  private RMap<Long, DTOExamProvider> labsCache;

  @PostConstruct
  public void init() {
    MongoDatastore.getInstance();
    Config config = new Config();
    config.useSingleServer().setAddress(AppProperties.INSTANCE.getRedisUri());
    RedissonClient redisson = Redisson.create(config);
    this.labsCache = redisson.getMap("labsCache");
    this.labsCache.clear();
    for (DTOExamProvider dtoExamProvider : labsController.listLabs()) {
      labsCache.fastPut(dtoExamProvider.getId(), dtoExamProvider);
    }
  }

  public void addLab(DTOExamProvider examProvider) {
    labsCache.fastPut(examProvider.getId(), examProvider);
  }

  public void removeLab(Long examProviderId) {
    labsCache.fastRemove(examProviderId);
  }

  public DTOExamProvider getRandomLab() {
    List<Long> keys = new ArrayList<>();
    labsCache.forEach(
      (key, dtoExamProvider) -> {
        if (dtoExamProvider.getIsActive() && dtoExamProvider.getOk()) {
          keys.add(key);
        }
      }
    );
    if (keys.size() > 0) {
      int index = randomGenerator.nextInt(keys.size());
      Long key = keys.get(index);
      return labsCache.get(key);
    }
    return null;
  }

  public List<DTOExamProvider> getExamProviders() {
    return labsCache.readAllEntrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
  }
}
