package uy.viruscontrol.business.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.integration.ResourceProviderIntegrationService;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.LastRequestStatus;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.ResourceAvailability;
import uy.viruscontrol.business.model.ResourceProvider;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class ResourceProviderController implements ResourceProviderService {
  @Inject
  private MessageProducer messageProducer;

  @EJB
  private ResourceProviderIntegrationService integrationService;

  @PersistenceContext
  private EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();
  private static final String FAIL_SYNC = "No se pudo sincronizar";
  private static final String MISSING_ID = "Falta completar la id";

  @Override
  public DTOResourceProvider newResourceProvider(String name, String url) throws VirusControlException {
    if (name == null || name.equals("") || url == null || url.equals("")) {
      throw new VirusControlException("Faltan completar campos", VirusControlException.UNCOMPLETED_FIELDS);
    }
    ResourceProvider resourceProvider = integrationService.sync(url);
    if (resourceProvider == null) {
      resourceProvider = new ResourceProvider();
      resourceProvider.setLastRequestStatus(LastRequestStatus.error(FAIL_SYNC));
    } else {
      resourceProvider.setLastRequestStatus(LastRequestStatus.ok());
    }
    resourceProvider.setName(name);
    resourceProvider.setUrl(url);
    resourceProvider.setToken(UUID.randomUUID().toString());
    entityManager.merge(resourceProvider);
    if (resourceProvider.getLastRequestStatus().getStatus() == LastRequestStatus.STATUS_OK) {
      try {
        integrationService.register(url, resourceProvider.getToken());
      } catch (VirusControlException e) {
        resourceProvider.setLastRequestStatus(LastRequestStatus.error("No se pudo registrar"));
      }
    }
    return modelMapper.map(resourceProvider, DTOResourceProvider.class);
  }

  @Override
  public DTOResourceProvider syncResourceProvider(Long id) throws VirusControlException {
    if (id == null) {
      throw new VirusControlException(MISSING_ID, VirusControlException.UNCOMPLETED_FIELDS);
    }
    ResourceProvider resourceProvider = entityManager.find(ResourceProvider.class, id);
    if (resourceProvider == null) {
      throw new VirusControlException(
        "No se encontro un proveedor de recursos con id = [" + id + "]",
        VirusControlException.ENTITY_NOT_FOUND
      );
    }
    ResourceProvider externalResourceProvider = integrationService.sync(resourceProvider.getUrl());
    if (externalResourceProvider == null) {
      resourceProvider.setLastRequestStatus(LastRequestStatus.error(FAIL_SYNC));
      entityManager.persist(resourceProvider);
      throw new VirusControlException("No se pudo conectar con el nodo periferico", VirusControlException.CANT_CONNECT);
    }
    resourceProvider.setAddress(externalResourceProvider.getAddress());
    resourceProvider.setCommercialName(externalResourceProvider.getCommercialName());
    resourceProvider.setOpenSchedule(externalResourceProvider.getOpenSchedule());
    resourceProvider.setLastRequestStatus(LastRequestStatus.ok());
    entityManager.persist(resourceProvider);
    return modelMapper.map(resourceProvider, DTOResourceProvider.class);
  }

  @Override
  public DTOResourceProvider updateResourceProvider(Long id, String name, String url) throws VirusControlException {
    if (id == null) {
      throw new VirusControlException(MISSING_ID, VirusControlException.UNCOMPLETED_FIELDS);
    }
    ResourceProvider resourceProvider = entityManager.find(ResourceProvider.class, id);
    if (resourceProvider == null) {
      throw new VirusControlException(
        "No se encontro un proveedor de recursos con id = [" + id + "]",
        VirusControlException.ENTITY_NOT_FOUND
      );
    }
    if (name != null && !name.equals("")) {
      resourceProvider.setName(name);
    }
    if (url != null && !url.equals("")) {
      resourceProvider.setUrl(url);
    }
    ResourceProvider externalResourceProvider = integrationService.sync(resourceProvider.getUrl());
    if (externalResourceProvider != null) {
      resourceProvider.setAddress(externalResourceProvider.getAddress());
      resourceProvider.setCommercialName(externalResourceProvider.getCommercialName());
      resourceProvider.setOpenSchedule(externalResourceProvider.getOpenSchedule());
      resourceProvider.setLastRequestStatus(LastRequestStatus.ok());
    } else {
      resourceProvider.setLastRequestStatus(LastRequestStatus.error(FAIL_SYNC));
    }
    entityManager.persist(resourceProvider);
    return modelMapper.map(resourceProvider, DTOResourceProvider.class);
  }

  @Override
  public void deleteResourceProvider(Long id) throws VirusControlException {
    if (id == null) {
      throw new VirusControlException(MISSING_ID, VirusControlException.UNCOMPLETED_FIELDS);
    }
    ResourceProvider resourceProvider = entityManager.find(ResourceProvider.class, id);
    if (resourceProvider == null) {
      throw new VirusControlException(
        "No se encontro un proveedor de recursos con id = [" + id + "]",
        VirusControlException.ENTITY_NOT_FOUND
      );
    }
    resourceProvider
      .getResources()
      .forEach(
        availability -> {
          //remover la disponibilidad desde el resource (owner)
          availability.getResource().getLines().remove(availability);
          //remover el resource
          availability.setResource(null);
          availability.setProvider(null);
          entityManager.remove(availability);
        }
      );
    resourceProvider.setAddress(null);
    entityManager.remove(resourceProvider);
  }

  @Override
  public List<DTOResourceProvider> getResourceProviders(String nameFilter) {
    List<ResourceProvider> resourceProviderList;
    if (nameFilter == null || nameFilter.equals("")) {
      resourceProviderList = entityManager.createNamedQuery("ResourceProvider.findAll", ResourceProvider.class).getResultList();
    } else {
      resourceProviderList =
        entityManager
          .createNamedQuery("ResourceProvider.findByName", ResourceProvider.class)
          .setParameter("nameFilter", "%" + nameFilter + "%")
          .getResultList();
    }
    List<DTOResourceProvider> dtoResourceProviders = new ArrayList<>();
    for (ResourceProvider resourceProvider : resourceProviderList) {
      dtoResourceProviders.add(modelMapper.map(resourceProvider, DTOResourceProvider.class));
    }
    return dtoResourceProviders;
  }

  @Override
  public DTOResourceProvider getResourceProviderByToken(String token) {
    ResourceProvider resourceProvider = entityManager
      .createNamedQuery("ResourceProvider.getByToken", ResourceProvider.class)
      .setParameter("token", token)
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (resourceProvider != null) {
      return modelMapper.map(resourceProvider, DTOResourceProvider.class);
    } else {
      return null;
    }
  }

  @Override
  public List<DTOWrongId> setResourceProviderAvailability(Long providerId, List<DTOResourceQuantity> resourceQuantityList) {
    List<DTOWrongId> wrongIds = new ArrayList<>();
    ResourceProvider provider = entityManager.find(ResourceProvider.class, providerId);
    if (provider != null) {
      for (DTOResourceQuantity dtoResourceQuantity : resourceQuantityList) {
        if (dtoResourceQuantity.getQuantity() != null && dtoResourceQuantity.getQuantity().compareTo(0L) >= 0) {
          ResourceAvailability availability = provider
            .getResources()
            .stream()
            .filter(resourceAvailability -> resourceAvailability.getResource().getId().equals(dtoResourceQuantity.getResourceId()))
            .findFirst()
            .orElse(null);
          if (availability == null) {
            Resource resource = entityManager.find(Resource.class, dtoResourceQuantity.getResourceId());
            if (resource == null) {
              DTOWrongId wrongId = new DTOWrongId();
              wrongId.setWrongId(dtoResourceQuantity.getResourceId());
              wrongIds.add(wrongId);
            } else if (dtoResourceQuantity.getQuantity() > 0) {
              availability = new ResourceAvailability();
              availability.setProvider(provider);
              availability.setResource(resource);
              resource.getLines().add(availability);
              entityManager.persist(resource);
              provider.getResources().add(availability);
              entityManager.persist(provider);
              availability.setQuantity(BigDecimal.valueOf(dtoResourceQuantity.getQuantity()));
              entityManager.persist(availability);
              resource
                .getSubscribers()
                .forEach(
                  frontProfile -> {
                    DTOResourceNotification dtoResourceNotification = new DTOResourceNotification();
                    dtoResourceNotification.setFrontProfile(modelMapper.map(frontProfile, DTOFrontProfile.class));
                    dtoResourceNotification.setResource(modelMapper.map(resource, DTOResource.class));
                    messageProducer.notifyResourceAvailability(dtoResourceNotification);
                  }
                );
            }
          } else {
            if (dtoResourceQuantity.getQuantity() > 0) {
              if (availability.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
                Resource resource = availability.getResource();
                resource
                  .getSubscribers()
                  .forEach(
                    frontProfile -> {
                      DTOResourceNotification dtoResourceNotification = new DTOResourceNotification();
                      dtoResourceNotification.setFrontProfile(modelMapper.map(frontProfile, DTOFrontProfile.class));
                      dtoResourceNotification.setResource(modelMapper.map(resource, DTOResource.class));
                      messageProducer.notifyResourceAvailability(dtoResourceNotification);
                    }
                  );
              }
              availability.setQuantity(BigDecimal.valueOf(dtoResourceQuantity.getQuantity()));
              entityManager.persist(availability);
            } else {
              Resource resource = availability.getResource();
              resource.getLines().remove(availability);
              entityManager.persist(resource);
              ResourceProvider resourceProvider = availability.getProvider();
              resourceProvider.getResources().remove(availability);
              entityManager.persist(resourceProvider);
              entityManager.remove(availability);
            }
          }
        }
      }
    }

    return wrongIds;
  }

  @Override
  public void register(Long providerId) throws VirusControlException {
    ResourceProvider provider = entityManager.find(ResourceProvider.class, providerId);
    if (provider == null) {
      throw new VirusControlException("No se encontro el proveedor", VirusControlException.ENTITY_NOT_FOUND);
    }
    integrationService.register(provider.getUrl(), provider.getToken());
  }
}
