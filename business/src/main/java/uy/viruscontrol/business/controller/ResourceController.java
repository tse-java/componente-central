package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.ResourceAvailability;
import uy.viruscontrol.business.service.ResourceService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumResourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class ResourceController implements ResourceService {
  @PersistenceContext
  private EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public void newResource(DTOResource dtoResource) {
    Resource resource = modelMapper.map(dtoResource, Resource.class);
    entityManager.persist(resource);
  }

  @Override
  public List<DTOResource> listResources() {
    List<Resource> resources = entityManager.createNamedQuery("Resource.list", Resource.class).getResultList();
    return resources.stream().map(resource -> modelMapper.map(resource, DTOResource.class)).distinct().collect(Collectors.toList());
  }

  @Override
  public List<DTOResourceSubscription> listResourcesSubscriptions(Long userId) {
    List<DTOResourceSubscription> result = new ArrayList<>();
    if (userId == null) {
      List<Resource> resourceStream = entityManager.createNamedQuery("Resource.list", Resource.class).getResultList();
      Set<DTOResourceSubscription> dtoResourceProviders = resourceStream
        .stream()
        .map(
          resource -> {
            DTOResourceSubscription dtoResourceSubscription = createDTOResourceSubscription(resource);
            dtoResourceSubscription.setSubscribed(false);
            return dtoResourceSubscription;
          }
        )
        .collect(Collectors.toSet());
      result.addAll(dtoResourceProviders);
      return result;
    }
    List<Resource> subscribedList = entityManager
      .createNamedQuery("Resource.subscribed", Resource.class)
      .setParameter("userId", userId)
      .getResultList();
    List<Resource> notSubscribedList = entityManager
      .createNamedQuery("Resource.notSubscribed", Resource.class)
      .setParameter("userId", userId)
      .getResultList();
    Set<DTOResourceSubscription> subscriptionSet = subscribedList
      .stream()
      .map(
        resource -> {
          DTOResourceSubscription dtoResourceSubscription = createDTOResourceSubscription(resource);
          dtoResourceSubscription.setSubscribed(true);
          return dtoResourceSubscription;
        }
      )
      .collect(Collectors.toSet());
    Set<DTOResourceSubscription> nonSubscriptionSet = notSubscribedList
      .stream()
      .map(
        resource -> {
          DTOResourceSubscription dtoResourceSubscription = createDTOResourceSubscription(resource);
          dtoResourceSubscription.setSubscribed(false);
          return dtoResourceSubscription;
        }
      )
      .collect(Collectors.toSet());

    result.addAll(subscriptionSet);
    result.addAll(nonSubscriptionSet);
    return result;
  }

  private DTOResourceSubscription createDTOResourceSubscription(Resource resource) {
    DTOResourceSubscription dtoResourceSubscription = new DTOResourceSubscription();
    dtoResourceSubscription.setDtoResource(modelMapper.map(resource, DTOResource.class));
    List<DTOAvailability> dtoAvailabilities = resource
      .getLines()
      .stream()
      .map(
        resourceAvailability -> {
          DTOAvailability dtoAvailability = new DTOAvailability();
          dtoAvailability.setQuantity(resourceAvailability.getQuantity());
          dtoAvailability.setResourceProvider(modelMapper.map(resourceAvailability.getProvider(), DTOResourceProvider.class));
          return dtoAvailability;
        }
      )
      .collect(Collectors.toList());
    dtoResourceSubscription.setAvailability(dtoAvailabilities);
    return dtoResourceSubscription;
  }

  @Override
  public void subscribeResource(Long userId, Long resourceId) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, userId);
    if (userProfile == null) {
      throw new VirusControlException("No se encontro el usuario con id = [" + userId + "]", VirusControlException.USER_NOT_FOUND);
    }

    Resource resource = entityManager.find(Resource.class, resourceId);
    if (resource == null) {
      throw new VirusControlException("No se encontro el recurso con id = [" + resourceId + "]", VirusControlException.RESOURCE_NOT_FOUND);
    }

    if (!resource.getSubscribers().add(userProfile)) {
      throw new VirusControlException("Ya existe la suscripcion al recurso", VirusControlException.SUBSCRIPTION_ALREADY_EXISTS);
    }

    entityManager.merge(resource);
  }

  @Override
  public void unsubscribeResource(Long userId, Long resourceId) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, userId);
    if (userProfile == null) {
      throw new VirusControlException("No se encontro el usuario con id = [" + userId + "]", VirusControlException.USER_NOT_FOUND);
    }
    Resource resource = entityManager.find(Resource.class, resourceId);
    if (resource == null) {
      throw new VirusControlException("No se encontro el recurso con id = [" + resourceId + "]", VirusControlException.RESOURCE_NOT_FOUND);
    }
    if (!resource.getSubscribers().remove(userProfile)) {
      throw new VirusControlException("No se encontro el recurso con id = [" + resourceId + "]", VirusControlException.RESOURCE_NOT_FOUND);
    }
    entityManager.merge(resource);
  }

  @Override
  public List<DTOResourceAvailability> getAvailability(
    Long diseaseId,
    EnumResourceType resourceType,
    String locationFilter,
    String resourceNameFilter
  )
    throws VirusControlException {
    Disease disease = entityManager.find(Disease.class, diseaseId);
    if (disease == null) {
      throw new VirusControlException("No se encontro una enfermedad con id [" + diseaseId + "]", VirusControlException.DISEASE_NOT_FOUND);
    }
    List<ResourceAvailability> availabilityList = entityManager
      .createNamedQuery("ResourceAvailibility.Filter", ResourceAvailability.class)
      .setParameter("disease", disease)
      .setParameter("resource_type", resourceType)
      .setParameter("provider_name", "%" + resourceNameFilter + "%")
      .setParameter("location_filter", "%" + locationFilter + "%")
      .getResultList();
    List<DTOResourceAvailability> dtoAvailabilityList = new ArrayList<>();
    for (ResourceAvailability resourceAvailability : availabilityList) {
      DTOResourceAvailability availability = modelMapper.map(resourceAvailability, DTOResourceAvailability.class);
      dtoAvailabilityList.add(availability);
    }
    return dtoAvailabilityList;
  }

  @Override
  public void deleteResource(Long id) throws VirusControlException {
    Resource resource = entityManager.find(Resource.class, id);
    if (resource == null) {
      throw new VirusControlException(VirusControlException.ENTITY_NOT_FOUND);
    }
    entityManager.remove(resource);
  }

  @Override
  public DTOResource updateResource(Long id, DTOUpdateResource dtoUpdateResource) throws VirusControlException {
    Resource resource = entityManager.find(Resource.class, id);
    if (resource == null) {
      throw new VirusControlException(VirusControlException.ENTITY_NOT_FOUND);
    }
    resource.setName(dtoUpdateResource.getName());
    resource.setDescription(dtoUpdateResource.getDescription());
    resource.setType(dtoUpdateResource.getResourceType());
    entityManager.persist(resource);
    return modelMapper.map(resource, DTOResource.class);
  }

  @Override
  public List<DTOBasicResource> getResourcesListBasic() {
    List<Resource> resources = entityManager.createNamedQuery("Resource.listBasic", Resource.class).getResultList();
    List<DTOBasicResource> basicResources = new ArrayList<>();
    for (Resource resource : resources) {
      DTOBasicResource basicResource = new DTOBasicResource();
      basicResource.setId(resource.getId());
      basicResource.setName(resource.getName());
      basicResources.add(basicResource);
    }
    return basicResources;
  }
}
