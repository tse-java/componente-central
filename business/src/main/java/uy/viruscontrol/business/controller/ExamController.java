package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.enterprise.event.NotificationOptions;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.CaseCache;
import uy.viruscontrol.business.cache.UserDiseaseCache;
import uy.viruscontrol.business.dto.DTOContagionAlert;
import uy.viruscontrol.business.dto.DTOMedicalCaseStatus;
import uy.viruscontrol.business.dto.DTONotifyExam;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.Exam;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.MedicalCase;
import uy.viruscontrol.business.service.ExamService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.EnumExamStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
@Slf4j
public class ExamController implements ExamService {
  @Resource
  private ManagedExecutorService threadPool;

  @Inject
  private MessageProducer messageProducer;

  @Inject
  private Event<DTOContagionAlert> triggerContagionAlert;

  @Inject
  private Event<DTOMedicalCaseStatus> notifyCase;

  @Inject
  private CaseCache caseCache;

  @Inject
  private UserDiseaseCache userDiaseaseCache;

  @PersistenceContext
  private EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public void newExam(Long doctorId, Long patientId, DTODisease dtoDisease) throws VirusControlException {
    FrontProfile doctor = entityManager.find(FrontProfile.class, doctorId);
    FrontProfile patient = entityManager.find(FrontProfile.class, patientId);
    if (doctor == null || patient == null) {
      throw new VirusControlException("No se encontro el usuario", VirusControlException.ENTITY_NOT_FOUND);
    }
    if (!doctor.getIsDoctor()) {
      throw new VirusControlException("el usuario seleccionado no es medico", VirusControlException.USER_IS_NOT_DOCTOR);
    }

    Disease disease = entityManager.find(Disease.class, dtoDisease.getId());
    if (
      disease == null ||
      disease.getRequestStatus().equals(EnumRequestStatus.REJECTED) ||
      disease.getRequestStatus().equals(EnumRequestStatus.PENDING)
    ) {
      throw new VirusControlException("No se encontro la enfermedad", VirusControlException.ENTITY_NOT_FOUND);
    }

    /*
      Create new Medical Case
     */
    Exam exam = new Exam();
    MedicalCase medicalCase = patient
      .getMedicalCases()
      .stream()
      .filter(aCase -> aCase.getDisease().getName().equalsIgnoreCase(disease.getName()) && aCase.getAsociatedExam() != null)
      .findFirst()
      .orElse(null);
    if (medicalCase == null) {
      caseCache.clear(disease.getId());
      medicalCase = new MedicalCase();
      medicalCase.setPatient(patient);
      medicalCase.setDisease(disease);
      medicalCase.setDepartment(patient.getAddress().getDepartment());
    }

    medicalCase.setStatus(EnumCaseStatus.SUSPECT);
    if (medicalCase.getAsociatedExam() == null) {
      medicalCase.setAsociatedExam(new ArrayList<>());
    }
    medicalCase.getAsociatedExam().add(exam);
    exam.setRequestingDoctor(doctor);
    exam.setPatient(patient);
    exam.setMedicalCase(medicalCase);

    entityManager.persist(exam);
    entityManager.persist(medicalCase);
    entityManager.flush();

    DTOExam dtoExam = modelMapper.map(exam, DTOExam.class);
    messageProducer.requestLabProvider(dtoExam);

    DTOMedicalCaseStatus dtoStatus = new DTOMedicalCaseStatus();
    dtoStatus.setDisease(medicalCase.getDisease().getName());
    dtoStatus.setStatus(medicalCase.getStatus());
    dtoStatus.setPatient(modelMapper.map(exam.getPatient(), DTOFrontProfile.class));
    dtoStatus.setMedic(modelMapper.map(exam.getRequestingDoctor(), DTOFrontProfile.class));
    notifyCase.fireAsync(dtoStatus);
    fireNotification(prepareNotification(exam));
  }

  @Override
  public void updateExam(DTOUpdateExam dtoUpdateExam) throws VirusControlException {
    Exam exam = entityManager.find(Exam.class, dtoUpdateExam.getId());
    if (exam == null) {
      throw new VirusControlException("Exam not found", VirusControlException.EXAM_NOT_FOUND);
    }

    exam.setStatus(EnumExamStatus.DELIVERED);
    exam.setPositive(dtoUpdateExam.getPositive());

    EnumCaseStatus caseStatus = exam.getMedicalCase().getStatus();

    if (caseStatus.equals(EnumCaseStatus.SUSPECT)) {
      if (dtoUpdateExam.getPositive()) {
        exam.getMedicalCase().setStatus(EnumCaseStatus.CONFIRMED);
      } else {
        userDiaseaseCache.removeDisease(exam.getPatient().getId(), exam.getMedicalCase().getDisease().getId());
        exam.getMedicalCase().setStatus(EnumCaseStatus.DISCARDED);
      }
    } else if (caseStatus.equals(EnumCaseStatus.CONFIRMED) && !dtoUpdateExam.getPositive()) {
      userDiaseaseCache.removeDisease(exam.getPatient().getId(), exam.getMedicalCase().getDisease().getId());
      exam.getMedicalCase().setStatus(EnumCaseStatus.CURED);
    }
    caseCache.clear(exam.getMedicalCase().getDisease().getId());
    exam = entityManager.merge(exam);
    notifyUpdate(exam);
  }

  @Override
  public List<DTOExam> getUserExams(Long userId) {
    return entityManager
      .createNamedQuery("Exam.getUserExams", Exam.class)
      .setParameter("userId", userId)
      .getResultStream()
      .map(exam -> modelMapper.map(exam, DTOExam.class))
      .collect(Collectors.toList());
  }

  public DTOExam prepareNotification(Exam exam) {
    /*Send async notification*/
    DTOExam dtoExam = modelMapper.map(exam, DTOExam.class);
    DTODisease dtoDisease = modelMapper.map(exam.getMedicalCase().getDisease(), DTODisease.class);
    DTOFrontProfile dtoFrontProfile = modelMapper.map(exam.getMedicalCase().getPatient(), DTOFrontProfile.class);

    dtoExam.setDisease(dtoDisease);
    dtoExam.getMedicalCase().setPatient(dtoFrontProfile);
    return dtoExam;
  }

  private void notifyUpdate(Exam exam) {
    DTOMedicalCase dtoMedicalCase = new DTOMedicalCase();
    dtoMedicalCase.setPatient(modelMapper.map(exam.getPatient(), DTOFrontProfile.class));
    dtoMedicalCase.setDisease(modelMapper.map(exam.getMedicalCase().getDisease(), DTODisease.class));
    dtoMedicalCase.setStatus(exam.getMedicalCase().getStatus());
    DTOExam dtoExam = prepareNotification(exam);

    /*trigger contagion*/
    if (exam.getPositive()) {
      DTOContagionAlert dtoContagionAlert = new DTOContagionAlert();
      dtoContagionAlert.setDisease(dtoMedicalCase.getDisease());
      dtoContagionAlert.setPatientId(dtoMedicalCase.getPatient().getId());
      triggerContagionAlert.fireAsync(dtoContagionAlert, NotificationOptions.ofExecutor(threadPool));
    }

    {
      //NOTIFY STATUS
      DTOMedicalCaseStatus dtoStatus = new DTOMedicalCaseStatus();
      dtoStatus.setDisease(dtoExam.getDisease().getName());
      dtoStatus.setStatus(dtoExam.getMedicalCase().getStatus());
      dtoStatus.setPatient(modelMapper.map(exam.getPatient(), DTOFrontProfile.class));
      dtoStatus.setMedic(modelMapper.map(exam.getRequestingDoctor(), DTOFrontProfile.class));
      notifyCase.fireAsync(dtoStatus, NotificationOptions.ofExecutor(threadPool));
    }

    //Notify about exam
    fireNotification(dtoExam);
  }

  private void fireNotification(DTOExam dtoExam) {
    DTONotifyExam dtoNotifyExam = new DTONotifyExam();
    dtoNotifyExam.setExam(dtoExam);
    messageProducer.notifyExamStatus(dtoNotifyExam);
  }
}
