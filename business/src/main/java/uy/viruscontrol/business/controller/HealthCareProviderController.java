package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.HealthProviderCache;
import uy.viruscontrol.business.integration.SaludUyIntegrationService;
import uy.viruscontrol.business.model.HealthCareProvider;
import uy.viruscontrol.business.service.HealthCareProviderService;
import uy.viruscontrol.common.dto.DTOHealthProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class HealthCareProviderController implements HealthCareProviderService {
  @PersistenceContext
  EntityManager entityManager;

  @EJB
  HealthProviderCache healthProviderCache;

  @EJB
  SaludUyIntegrationService saludUyService;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public DTOHealthProvider newHealthCareProvider(DTOHealthProvider healthProvider) {
    HealthCareProvider provider = new HealthCareProvider();
    provider.setName(healthProvider.getName());
    provider.setUrl(healthProvider.getUrl());
    entityManager.persist(provider);
    entityManager.flush();
    saludUyService.setHealthProviders(entityManager.createNamedQuery("HealthCareProvider.getIds", Long.class).getResultList());
    DTOHealthProvider dtoHealthProvider = modelMapper.map(provider, DTOHealthProvider.class);
    healthProviderCache.addProvider(dtoHealthProvider);
    return dtoHealthProvider;
  }

  @Override
  public DTOHealthProvider editHealthProvider(Long id, String url, String name) throws VirusControlException {
    HealthCareProvider provider = entityManager.find(HealthCareProvider.class, id);
    if (provider == null) {
      throw new VirusControlException("No se encontro el prestador de salud con id " + id, VirusControlException.ENTITY_NOT_FOUND);
    }
    provider.setUrl(url);
    provider.setName(name);
    entityManager.persist(provider);
    DTOHealthProvider dtoHealthProvider = modelMapper.map(provider, DTOHealthProvider.class);
    saludUyService.setHealthProviders(entityManager.createNamedQuery("HealthCareProvider.getIds", Long.class).getResultList());
    healthProviderCache.addProvider(dtoHealthProvider);
    return dtoHealthProvider;
  }

  @Override
  public DTOHealthProvider getHealthProvider(Long id) throws VirusControlException {
    HealthCareProvider provider = entityManager.find(HealthCareProvider.class, id);
    if (provider == null) {
      throw new VirusControlException("No se encontro el prestador de salud con id " + id, VirusControlException.ENTITY_NOT_FOUND);
    }
    return modelMapper.map(provider, DTOHealthProvider.class);
  }

  @Override
  public List<DTOHealthProvider> getHealthProviders() {
    List<HealthCareProvider> providers = entityManager
      .createNamedQuery("HealthCareProvider.findAll", HealthCareProvider.class)
      .getResultList();
    List<DTOHealthProvider> dtoHealthProviderList = new ArrayList<>();
    for (HealthCareProvider provider : providers) {
      dtoHealthProviderList.add(modelMapper.map(provider, DTOHealthProvider.class));
    }
    return dtoHealthProviderList;
  }
}
