package uy.viruscontrol.business.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.settings.ContagionDistance;
import uy.viruscontrol.business.model.settings.NotificationConfig;
import uy.viruscontrol.business.service.SettingsService;
import uy.viruscontrol.common.dto.DTONotificationConfig;

@Stateless
@Transactional
@Slf4j
public class SettingsController implements SettingsService {
  private static final long SETTING_ID = 1L;

  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public Double getContagionDistance() {
    ContagionDistance contagionDistance = entityManager.find(ContagionDistance.class, SETTING_ID);
    if (contagionDistance == null) {
      log.error("Distancia de contagio no configurada, usando valor por defecto.");
    }
    return contagionDistance == null ? 5D : contagionDistance.getDistance();
  }

  @Override
  public void configureContagionDistance(Double distance) {
    ContagionDistance contagionDistance = entityManager.find(ContagionDistance.class, SETTING_ID);
    if (contagionDistance == null) {
      contagionDistance = new ContagionDistance();
      log.warn("Distancia de contagio no configurada, guardando nueva configuracion.");
    }
    contagionDistance.setDistance(distance);
    contagionDistance.setId(SETTING_ID);
    entityManager.merge(contagionDistance);
  }

  @Override
  public DTONotificationConfig getNotificationConfig() {
    NotificationConfig notificationConfig = entityManager.find(NotificationConfig.class, SETTING_ID);
    if (notificationConfig == null) {
      log.error("Estilo de notificaciones configurada, usando valor por defecto.");
    }
    return notificationConfig == null ? new DTONotificationConfig() : modelMapper.map(notificationConfig, DTONotificationConfig.class);
  }

  @Override
  public void configureNotifications(DTONotificationConfig dtoNotificationConfig) {
    NotificationConfig notificationConfig = entityManager.find(NotificationConfig.class, SETTING_ID);
    if (notificationConfig == null) {
      log.warn("Estilo de notificaciones no configurada, guardando nueva configuracion.");
    }
    notificationConfig = modelMapper.map(dtoNotificationConfig, NotificationConfig.class);
    notificationConfig.setId(SETTING_ID);
    entityManager.merge(notificationConfig);
  }
}
