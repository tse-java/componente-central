package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.InformationSourceType;
import uy.viruscontrol.business.service.InformationSourceTypeService;
import uy.viruscontrol.common.dto.DTOInformationSourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class InformationSourceTypeController implements InformationSourceTypeService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public List<DTOInformationSourceType> getInformationSourceTypes(Boolean active) {
    List<InformationSourceType> sourceTypeList;
    if (active == null) {
      sourceTypeList = entityManager.createNamedQuery("InformationSourceType.findAll", InformationSourceType.class).getResultList();
    } else {
      sourceTypeList =
        entityManager
          .createNamedQuery("InformationSourceType.findByStatus", InformationSourceType.class)
          .setParameter("active", active)
          .getResultList();
    }
    List<DTOInformationSourceType> dtoList = new ArrayList<>();
    for (InformationSourceType sourceType : sourceTypeList) {
      dtoList.add(modelMapper.map(sourceType, DTOInformationSourceType.class));
    }
    return dtoList;
  }

  @Override
  public DTOInformationSourceType getInformationSourceType(Long id) throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, id);
    if (type == null) {
      throw new VirusControlException("No se encontro el tipo", VirusControlException.ENTITY_NOT_FOUND);
    }
    return modelMapper.map(type, DTOInformationSourceType.class);
  }

  @Override
  public void setInformationSourceTypeStatus(Long id, Boolean active) throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, id);
    if (type == null) {
      throw new VirusControlException("No se encontro el tipo", VirusControlException.ENTITY_NOT_FOUND);
    }
    type.setActive(active);
  }
}
