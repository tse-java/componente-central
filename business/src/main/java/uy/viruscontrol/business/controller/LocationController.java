package uy.viruscontrol.business.controller;

import dev.morphia.geo.GeoJson;
import dev.morphia.geo.Point;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import uy.viruscontrol.business.cache.ContagionDistanceCache;
import uy.viruscontrol.business.dao.LocationDao;
import uy.viruscontrol.business.model.LocationReport;
import uy.viruscontrol.business.service.LocationService;
import uy.viruscontrol.common.dto.DTOContagionReport;
import uy.viruscontrol.common.dto.DTOLocationReport;

@Stateless
@Slf4j
public class LocationController implements LocationService {
  @EJB
  private ContagionDistanceCache contagionDistanceCache;

  @EJB
  private LocationDao locationDao;

  @Override
  public void newReport(DTOLocationReport dtoLocationReport) {
    LocationReport locationReport = new LocationReport();
    Point point = GeoJson.point(dtoLocationReport.getLatitude(), dtoLocationReport.getLongitude());

    locationReport.setDate(dtoLocationReport.getDate());
    locationReport.setLocation(point);
    locationReport.setUserId(dtoLocationReport.getUserId());
    locationReport.setDepartment(dtoLocationReport.getDepartment());

    locationDao.add(locationReport);
  }

  @Override
  public List<DTOLocationReport> getLocationsUntilDate(Long userId, Date until) {
    return locationDao
      .getLocationsUntilDate(userId, until)
      .stream()
      .map(
        locationReport -> {
          DTOLocationReport dtoLocationReport = new DTOLocationReport();
          dtoLocationReport.setDate(locationReport.getDate());
          dtoLocationReport.setLatitude(locationReport.getLocation().getLatitude());
          dtoLocationReport.setLongitude(locationReport.getLocation().getLongitude());
          dtoLocationReport.setDepartment(locationReport.getDepartment());
          dtoLocationReport.setUserId(locationReport.getUserId());
          return dtoLocationReport;
        }
      )
      .collect(Collectors.toList());
  }

  @Override
  public List<DTOLocationReport> findContagionByLocation(DTOContagionReport dtoContagionReport) {
    List<DTOLocationReport> result = new ArrayList<>();
    List<Long> ignoreUsers = new ArrayList<>();
    if (!dtoContagionReport.getLocationReport().isEmpty()) {
      ignoreUsers.add(dtoContagionReport.getLocationReport().get(0).getUserId());
      dtoContagionReport
        .getLocationReport()
        .forEach(
          dtoLocationReport -> {
            Double distance = contagionDistanceCache.get();
            Point point = GeoJson.point(dtoLocationReport.getLatitude(), dtoLocationReport.getLongitude());
            Date start = dtoLocationReport.getDate();
            Date end = dtoLocationReport.getDate();
            start = DateUtils.addMinutes(start, -dtoContagionReport.getDisease().getInfectionTime().intValue());
            end = DateUtils.addMinutes(end, +dtoContagionReport.getDisease().getInfectionTime().intValue());
            locationDao
              .findPointsNearLocationAtTime(ignoreUsers, start, end, point, distance)
              .stream()
              .filter(distinctByKey(LocationReport::getUserId))
              .forEach(
                locationReport -> {
                  DTOLocationReport dto = new DTOLocationReport();
                  dto.setUserId(locationReport.getUserId());
                  dto.setDepartment(locationReport.getDepartment());
                  dto.setLongitude(locationReport.getLocation().getLongitude());
                  dto.setLatitude(locationReport.getLocation().getLatitude());
                  dto.setDate(locationReport.getDate());
                  ignoreUsers.add(locationReport.getUserId());
                  result.add(dto);
                }
              );
          }
        );
    }
    return result;
  }

  private <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = ConcurrentHashMap.newKeySet();
    return t -> seen.add(keyExtractor.apply(t));
  }
}
