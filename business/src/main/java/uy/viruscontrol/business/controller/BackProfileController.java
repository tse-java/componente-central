package uy.viruscontrol.business.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.BackProfile;
import uy.viruscontrol.business.service.BackProfileService;
import uy.viruscontrol.common.dto.DTOBackProfile;
import uy.viruscontrol.common.dto.DTONewBackProfile;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
@Transactional
public class BackProfileController implements BackProfileService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public DTOBackProfile newProfile(DTONewBackProfile userProfile) throws VirusControlException {
    if (getProfileByNick(userProfile.getNickname()) != null) {
      throw new VirusControlException("Ya existe un usuario con ese nickname", VirusControlException.USER_ALREADY_EXIST);
    }
    BackProfile user = modelMapper.map(userProfile, BackProfile.class);
    entityManager.persist(user);
    return modelMapper.map(user, DTOBackProfile.class);
  }

  @Override
  public void delete(Long userId) throws VirusControlException {
    BackProfile backProfile = entityManager.find(BackProfile.class, userId);
    if (backProfile == null) {
      throw new VirusControlException("Invalid User or Password", VirusControlException.USER_NOT_FOUND);
    }
    entityManager.remove(backProfile);
  }

  @Override
  public DTOBackProfile doLogin(String nickname, String password) throws VirusControlException {
    BackProfile backProfile = getProfileByNick(nickname);
    if (backProfile == null) {
      throw new VirusControlException("Invalid User or Password", VirusControlException.USER_NOT_FOUND);
    }
    backProfile.checkPassword(password);
    return modelMapper.map(backProfile, DTOBackProfile.class);
  }

  private BackProfile getProfileByNick(String nickname) {
    return entityManager
      .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
      .setParameter("nickname", nickname)
      .getResultStream()
      .findFirst()
      .orElse(null);
  }

  @Override
  public void changePassword(String nickname, String oldPassword, String newPassword) throws VirusControlException {
    BackProfile userProfile = getProfileByNick(nickname);
    if (userProfile == null) {
      throw new VirusControlException("User not found.", VirusControlException.USER_NOT_FOUND);
    } else {
      userProfile.checkPassword(oldPassword);
      userProfile.setPassword(newPassword);
      entityManager.persist(userProfile);
    }
  }

  @Override
  public void updateUser(DTONewBackProfile dtoNewBackProfile) throws VirusControlException {
    BackProfile backProfile = entityManager.find(BackProfile.class, dtoNewBackProfile.getId());
    BackProfile duplicated = entityManager
      .createNamedQuery("BackProfile.findByNickname", BackProfile.class)
      .setParameter("nickname", dtoNewBackProfile.getNickname())
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (backProfile == null) {
      throw new VirusControlException("User not found.", VirusControlException.USER_NOT_FOUND);
    }
    if (duplicated != null && !duplicated.getId().equals(backProfile.getId())) {
      throw new VirusControlException("Ya existe otro usuario con ese nick", 2);
    }
    backProfile.setNickname(dtoNewBackProfile.getNickname());
    backProfile.setFullname(dtoNewBackProfile.getFullName());
    backProfile.setRole(dtoNewBackProfile.getRole());
    if (dtoNewBackProfile.getPassword() != null && !dtoNewBackProfile.getPassword().isEmpty()) {
      backProfile.setPassword(dtoNewBackProfile.getPassword());
    }
    entityManager.merge(backProfile);
  }

  @Override
  public List<DTOBackProfile> listUsers() {
    return entityManager
      .createNamedQuery("BackProfile.list", BackProfile.class)
      .getResultStream()
      .map(backProfile -> modelMapper.map(backProfile, DTOBackProfile.class))
      .collect(Collectors.toList());
  }
}
