package uy.viruscontrol.business.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.LabCache;
import uy.viruscontrol.business.model.ExamProvider;
import uy.viruscontrol.business.service.ExamProviderService;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Slf4j
@Stateless
public class ExamProviderController implements ExamProviderService {
  @EJB
  private LabCache labCache;

  @PersistenceContext
  private EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();
  private static final String PROVIDER_NOT_FOUND = "Proveedor de Examenes no encontrado.";

  @Override
  public List<DTOExamProvider> listLabs() {
    List<ExamProvider> examProviderList = entityManager.createNamedQuery("ExamProvider.list", ExamProvider.class).getResultList();
    return examProviderList.stream().map(examProvider -> modelMapper.map(examProvider, DTOExamProvider.class)).collect(Collectors.toList());
  }

  @Override
  public DTOExamProvider addLab(DTOExamProvider dtoExamProvider) {
    ExamProvider examProvider = new ExamProvider();
    examProvider.setUrl(dtoExamProvider.getUrl());
    examProvider.setName(dtoExamProvider.getName());
    examProvider = entityManager.merge(examProvider);
    labCache.addLab(modelMapper.map(examProvider, DTOExamProvider.class));
    return modelMapper.map(examProvider, DTOExamProvider.class);
  }

  @Override
  public DTOExamProvider updateLab(DTOExamProvider dtoExamProvider) throws VirusControlException {
    ExamProvider examProvider = entityManager.find(ExamProvider.class, dtoExamProvider.getId());
    if (examProvider == null) {
      throw new VirusControlException(PROVIDER_NOT_FOUND, VirusControlException.RESOURCE_NOT_FOUND);
    }
    examProvider.setUrl(dtoExamProvider.getUrl());
    examProvider.setName(dtoExamProvider.getName());
    examProvider.setToken(dtoExamProvider.getToken());
    examProvider.setOk(true);
    examProvider.setMessage("");
    examProvider = entityManager.merge(examProvider);
    labCache.addLab(modelMapper.map(examProvider, DTOExamProvider.class));
    return modelMapper.map(examProvider, DTOExamProvider.class);
  }

  @Override
  public void deleteLab(Long id) throws VirusControlException {
    ExamProvider examProvider = entityManager.find(ExamProvider.class, id);
    if (examProvider == null) {
      throw new VirusControlException(PROVIDER_NOT_FOUND, VirusControlException.RESOURCE_NOT_FOUND);
    }
    Integer examQuantity = entityManager
      .createNamedQuery("ExamProvider.getExamsQuantity", Integer.class)
      .setParameter("id", id)
      .getSingleResult();
    if (!examQuantity.equals(0)) {
      throw new VirusControlException("Tiene Examenes.", VirusControlException.HAS_EXAMS);
    }
    labCache.removeLab(examProvider.getId());
    entityManager.remove(examProvider);
  }

  @Override
  public void updateLabStatus(Long id, Boolean isActive) throws VirusControlException {
    ExamProvider examProvider = entityManager.find(ExamProvider.class, id);
    if (examProvider == null) {
      throw new VirusControlException(PROVIDER_NOT_FOUND, VirusControlException.RESOURCE_NOT_FOUND);
    }
    examProvider.setIsActive(isActive);
    entityManager.persist(examProvider);
  }

  @Override
  public DTOExamProvider setStatus(Long providerId, String message, Boolean isOK) throws VirusControlException {
    ExamProvider examProvider = entityManager.find(ExamProvider.class, providerId);
    if (examProvider == null) {
      throw new VirusControlException("Laboratiorio no encontrado.", VirusControlException.RESOURCE_NOT_FOUND);
    }
    examProvider.setMessage(message);
    examProvider.setOk(isOK);
    labCache.addLab(modelMapper.map(examProvider, DTOExamProvider.class));
    return modelMapper.map(examProvider, DTOExamProvider.class);
  }
}
