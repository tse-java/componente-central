package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.Symptom;
import uy.viruscontrol.business.service.SymptomService;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class SymptomController implements SymptomService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public List<DTOSymptom> getSymptoms() {
    List<DTOSymptom> dtoSymptomList = new ArrayList<>();
    List<Symptom> symptomList = entityManager.createNamedQuery("Symptom.findAll", Symptom.class).getResultList();
    for (Symptom symptom : symptomList) {
      DTOSymptom dtoSymptom = modelMapper.map(symptom, DTOSymptom.class);
      dtoSymptomList.add(dtoSymptom);
    }
    return dtoSymptomList;
  }

  @Override
  public DTOSymptom newSymptom(DTOSymptom dtoSymptom) throws VirusControlException {
    if (
      dtoSymptom.getName() == null ||
      dtoSymptom.getName().equals("") ||
      dtoSymptom.getDescription() == null ||
      dtoSymptom.getDescription().equals("")
    ) {
      throw new VirusControlException("Falta completar campos", VirusControlException.UNCOMPLETED_FIELDS);
    }
    Symptom symptom = modelMapper.map(dtoSymptom, Symptom.class);
    symptom.setId(null);
    entityManager.persist(symptom);
    dtoSymptom = modelMapper.map(symptom, DTOSymptom.class);
    return dtoSymptom;
  }
}
