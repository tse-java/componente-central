package uy.viruscontrol.business.controller;

import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.time.DateUtils;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.integration.DNICIntegrationService;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.Location;
import uy.viruscontrol.business.service.FrontProfileService;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTONotifyStyle;
import uy.viruscontrol.common.dto.DTOUpdateFrontProfile;
import uy.viruscontrol.common.exceptions.VirusControlException;
import uy.viruscontrol.pi.service.DtoCitizen;

@Stateless
public class FrontProfileController implements FrontProfileService {
  //duracion del refresh token en semanas
  private static final int REFRESHTOKEN_EXPIRATION = 2;
  private static final String USER_NOT_FOUND = "User not found.";

  @PersistenceContext
  EntityManager entityManager;

  @EJB
  DNICIntegrationService dnicService;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public String saveRefreshToken(Long userId, String token) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, userId);
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.USER_NOT_FOUND);
    }
    if (
      userProfile.getRefreshToken() != null &&
      userProfile.getRefreshTokenExpire() != null &&
      userProfile.getRefreshTokenExpire().after(new Date())
    ) {
      return userProfile.getRefreshToken();
    }
    userProfile.setRefreshToken(token);
    userProfile.setRefreshTokenExpire(DateUtils.addWeeks(new Date(), REFRESHTOKEN_EXPIRATION));
    entityManager.persist(userProfile);
    return token;
  }

  @Override
  public DTOFrontProfile getProfileByRefreshToken(String token) throws VirusControlException {
    FrontProfile userProfile = entityManager
      .createNamedQuery("FrontProfile.findByRefreshToken", FrontProfile.class)
      .setParameter("refreshToken", token)
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (userProfile == null || userProfile.getRefreshTokenExpire().before(new Date())) {
      throw new VirusControlException("Invalid token", VirusControlException.INVALID_TOKEN);
    }
    return modelMapper.map(userProfile, DTOFrontProfile.class);
  }

  @Override
  public void invalidateRefreshToken(Long userId) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, userId);
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.USER_NOT_FOUND);
    }
    userProfile.setRefreshToken(null);
    entityManager.persist(userProfile);
  }

  @Override
  public DTOFrontProfile getProfileById(Long id) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, id);
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.USER_NOT_FOUND);
    }
    return modelMapper.map(userProfile, DTOFrontProfile.class);
  }

  @Override
  public DTOFrontProfile updateProfile(DTOUpdateFrontProfile updateFrontProfile) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, updateFrontProfile.getId());
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.USER_NOT_FOUND);
    }
    boolean allowUpdateRole = true;
    if (userProfile.getDocumentNumber() == null) {
      FrontProfile userWithDocNumber = entityManager
        .createNamedQuery("FrontProfile.findByDocumentNumber", FrontProfile.class)
        .setParameter("documentNumber", updateFrontProfile.getDocumentNumber())
        .getResultStream()
        .findFirst()
        .orElse(null);
      if (userWithDocNumber != null) {
        if (userWithDocNumber.getExternalId() != null) {
          throw new VirusControlException(
            "Ya existe un usuario con la cedula " + updateFrontProfile.getDocumentNumber(),
            VirusControlException.USER_ALREADY_EXIST
          );
        } else {
          /*
          En caso de que un usuario medico se este logueando por primera vez pero ya tenga consultas medicas
          asignadas.
           */
          userWithDocNumber.setRefreshToken(userProfile.getRefreshToken());
          userWithDocNumber.setIsDoctor(true);
          userWithDocNumber.setEmail(userProfile.getEmail());
          userWithDocNumber.setExternalId(userProfile.getExternalId());
          allowUpdateRole = userProfile.getIsDoctor() == null;
          entityManager.remove(userProfile);
          userProfile = userWithDocNumber;
          entityManager.flush();
        }
      }
    } else if (!userProfile.getDocumentNumber().equals(updateFrontProfile.getDocumentNumber())) {
      throw new VirusControlException(
        "No se puede modificar el numero de cedula de un usuario",
        VirusControlException.FIELD_CANT_BE_UPDATED
      );
    }
    DtoCitizen citizenInfo = dnicService.getCitizenInformation(updateFrontProfile.getDocumentNumber());
    if (citizenInfo == null) {
      throw new VirusControlException("No se pudo conectar a la DNIC", VirusControlException.CANT_CONNECT);
    }
    userProfile.setNationality(citizenInfo.getNationality());
    userProfile.setBirthday(citizenInfo.getBirthDay().getTime());
    userProfile.setFirstname(citizenInfo.getFirstName());
    userProfile.setLastname(citizenInfo.getLastName());
    Location location = modelMapper.map(updateFrontProfile.getAddress(), Location.class);
    location.setId(null);
    entityManager.persist(location);
    userProfile.setAddress(location);
    if (updateFrontProfile.getIsDoctor() != null && allowUpdateRole) {
      userProfile.setIsDoctor(updateFrontProfile.getIsDoctor());
    }
    if (updateFrontProfile.getDocumentNumber() != null) {
      userProfile.setDocumentNumber(updateFrontProfile.getDocumentNumber());
    }
    if (updateFrontProfile.getPhoneNumber() != null) {
      userProfile.setPhoneNumber(updateFrontProfile.getPhoneNumber());
    }
    userProfile.setIsFinished(true);
    entityManager.merge(userProfile);
    return modelMapper.map(userProfile, DTOFrontProfile.class);
  }

  @Override
  public void patchAddress(DTOUpdateFrontProfile updateFrontProfile) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, updateFrontProfile.getId());
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.USER_NOT_FOUND);
    }
    Location location = modelMapper.map(updateFrontProfile.getAddress(), Location.class);
    location.setId(null);
    entityManager.persist(location);
    userProfile.setAddress(location);
    entityManager.persist(userProfile);
  }

  @Override
  public DTOFrontProfile getProfileByExternalId(String externalId) {
    FrontProfile userProfile = entityManager
      .createNamedQuery("FrontProfile.findByExternalId", FrontProfile.class)
      .setParameter("externalId", externalId)
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (userProfile != null) {
      return modelMapper.map(userProfile, DTOFrontProfile.class);
    }
    return null;
  }

  @Override
  public DTOFrontProfile newProfile(DTOFrontProfile dtoFrontProfile) throws VirusControlException {
    FrontProfile frontProfile = entityManager
      .createNamedQuery("FrontProfile.findByExternalId", FrontProfile.class)
      .setParameter("externalId", dtoFrontProfile.getExternalId())
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (frontProfile != null) {
      throw new VirusControlException("User already exists", VirusControlException.USER_ALREADY_EXIST);
    }
    frontProfile = modelMapper.map(dtoFrontProfile, FrontProfile.class);
    frontProfile = entityManager.merge(frontProfile);
    return modelMapper.map(frontProfile, DTOFrontProfile.class);
  }

  @Override
  public boolean hasAnActiveCase(Long userId, Long diseaaseId) {
    FrontProfile frontProfile = entityManager
      .createNamedQuery("FrontProfile.hasActiveCaseForDisease", FrontProfile.class)
      .setParameter("userId", userId)
      .setParameter("diseaseId", diseaaseId)
      .getResultStream()
      .findFirst()
      .orElse(null);
    return frontProfile != null;
  }

  @Override
  public DTOFrontProfile patchNotifications(Long userId, DTONotifyStyle dtoNotifyStyle) throws VirusControlException {
    FrontProfile userProfile = entityManager.find(FrontProfile.class, userId);
    if (userProfile == null) {
      throw new VirusControlException(USER_NOT_FOUND, VirusControlException.ENTITY_NOT_FOUND);
    }
    userProfile.setMailNotify(dtoNotifyStyle.isMailNotify());
    userProfile.setPushNotify(dtoNotifyStyle.isPushNotify());
    userProfile = entityManager.merge(userProfile);
    return modelMapper.map(userProfile, DTOFrontProfile.class);
  }
}
