package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.Resource;
import uy.viruscontrol.business.model.Symptom;
import uy.viruscontrol.business.service.DiseaseService;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOResource;
import uy.viruscontrol.common.dto.DTOUpdateDisease;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class DiseaseController implements DiseaseService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public void requestNewDisease(DTODisease dtoDisease) throws VirusControlException {
    if (
      dtoDisease.getAgentName() == null ||
      dtoDisease.getAgentName().equals("") ||
      dtoDisease.getAgentType() == null ||
      dtoDisease.getInfectionTime() == null ||
      dtoDisease.getName() == null ||
      dtoDisease.getName().equals("") ||
      dtoDisease.getSymptoms() == null ||
      dtoDisease.getSymptoms().isEmpty()
    ) {
      throw new VirusControlException("Faltan completar campos", VirusControlException.UNCOMPLETED_FIELDS);
    }
    Disease disease = modelMapper.map(dtoDisease, Disease.class);
    disease.setId(null);
    Set<Symptom> symptomList = new HashSet<>();
    for (Symptom symptom : disease.getSymptoms()) {
      Symptom persistedSymptom = entityManager.find(Symptom.class, symptom.getId());
      if (persistedSymptom == null) {
        throw new VirusControlException("No se encontro el sintoma", VirusControlException.SYMPTOM_NOT_FOUND);
      }
      symptomList.add(persistedSymptom);
    }
    Set<Resource> resourceSet = new HashSet<>();
    for (DTOResource dtoResource : dtoDisease.getRecommendedResources()) {
      Resource resource = entityManager.find(Resource.class, dtoResource.getId());
      if (resource == null) {
        throw new VirusControlException("No se encontro el recurso", VirusControlException.RESOURCE_NOT_FOUND);
      } else {
        resourceSet.add(resource);
      }
    }
    disease.setSymptoms(symptomList);
    disease.setRecommendedResources(resourceSet);
    disease.setRequestStatus(EnumRequestStatus.PENDING);
    entityManager.persist(disease);
  }

  @Override
  public void approveNewDisease(Long diseaseId) throws VirusControlException {
    Disease disease = entityManager.find(Disease.class, diseaseId);
    if (disease == null) {
      throw new VirusControlException("No se encontro la enfermedad", VirusControlException.DISEASE_NOT_FOUND);
    }
    disease.setRequestStatus(EnumRequestStatus.APPROVED);
    entityManager.persist(disease);
  }

  @Override
  public void rejectNewDisease(Long diseaseId) throws VirusControlException {
    Disease disease = entityManager.find(Disease.class, diseaseId);
    if (disease == null) {
      throw new VirusControlException("No se encontro la enfermedad", VirusControlException.DISEASE_NOT_FOUND);
    }
    disease.setRequestStatus(EnumRequestStatus.REJECTED);
    entityManager.persist(disease);
  }

  @Override
  public List<DTODisease> getDiseasesByStatus(EnumRequestStatus requestStatus) {
    List<Disease> diseases;
    if (requestStatus == null) {
      diseases = entityManager.createNamedQuery("Disease.findAll", Disease.class).getResultList();
    } else {
      diseases =
        entityManager.createNamedQuery("Disease.findByRequestStatus", Disease.class).setParameter("status", requestStatus).getResultList();
    }

    List<DTODisease> dtoDiseases = new ArrayList<>();
    for (Disease disease : diseases) {
      dtoDiseases.add(modelMapper.map(disease, DTODisease.class));
    }
    return dtoDiseases;
  }

  @Override
  public DTODisease getDiseasesById(Long id) throws VirusControlException {
    Disease disease = entityManager.find(Disease.class, id);
    if (disease == null) {
      throw new VirusControlException("Disease not found", VirusControlException.DISEASE_NOT_FOUND);
    }
    return modelMapper.map(disease, DTODisease.class);
  }

  @Override
  public DTODisease updateDisease(Long diseaseId, DTOUpdateDisease dtoUpdateDisease) throws VirusControlException {
    Disease disease = entityManager.find(Disease.class, diseaseId);
    if (disease == null) {
      throw new VirusControlException("Enfermedad no encontrada", VirusControlException.DISEASE_NOT_FOUND);
    }

    List<Resource> newResourceList = new ArrayList<>();
    for (Long resourceId : dtoUpdateDisease.getRecommendedResources()) {
      Resource resource = entityManager.find(Resource.class, resourceId);
      if (resource == null) {
        throw new VirusControlException("Recurso no encontrado " + resourceId.toString(), VirusControlException.RESOURCE_NOT_FOUND);
      }
      newResourceList.add(resource);
    }

    List<Symptom> newSymptomList = new ArrayList<>();
    for (Long symptomId : dtoUpdateDisease.getSymptoms()) {
      Symptom symptom = entityManager.find(Symptom.class, symptomId);
      if (symptom == null) {
        throw new VirusControlException("Sintoma no encontrado " + symptomId.toString(), VirusControlException.SYMPTOM_NOT_FOUND);
      }
      newSymptomList.add(symptom);
    }

    //Disease is the owner of the relation
    disease.getRecommendedResources().clear();
    disease.getRecommendedResources().addAll(newResourceList);

    //Disease is the owner of the relation
    disease.getSymptoms().clear();
    disease.getSymptoms().addAll(newSymptomList);

    disease.setAgentType(dtoUpdateDisease.getAgentType());
    disease.setName(dtoUpdateDisease.getName());
    disease.setRequestStatus(dtoUpdateDisease.getRequestStatus());
    disease.setInfectionTime(dtoUpdateDisease.getInfectionTime());
    disease.setAgentType(dtoUpdateDisease.getAgentType());

    return modelMapper.map(entityManager.merge(disease), DTODisease.class);
  }
}
