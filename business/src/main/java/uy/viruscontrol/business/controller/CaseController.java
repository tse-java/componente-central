package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.CaseCache;
import uy.viruscontrol.business.model.Disease;
import uy.viruscontrol.business.model.FrontProfile;
import uy.viruscontrol.business.model.MedicalCase;
import uy.viruscontrol.business.service.CaseService;
import uy.viruscontrol.common.dto.DTOCasesQuantity;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;
import uy.viruscontrol.common.enumerations.utils.EnumFactories;

@Stateless
public class CaseController implements CaseService {
  @PersistenceContext
  private EntityManager entityManager;

  @Inject
  private CaseCache caseCache;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public List<DTODepartment> getCasesGroupByDepartment(Long diseaseId, List<EnumCaseStatus> caseStatusList) {
    List<DTODepartment> dtoDepartments = new ArrayList<>();
    Stream<Object[]> queryResult;
    if (caseStatusList != null && !caseStatusList.isEmpty()) {
      queryResult =
        entityManager
          .createNamedQuery("Case.countGroupByDepartmentAndStatusFilterByStatus", Object[].class)
          .setParameter("status", caseStatusList)
          .setParameter("diseaseId", diseaseId)
          .getResultStream();
    } else {
      List<DTODepartment> cases = caseCache.getCases(diseaseId);
      if (cases != null) {
        return cases;
      }
      queryResult =
        entityManager
          .createNamedQuery("Case.countGroupByDepartmentAndStatus", Object[].class)
          .setParameter("diseaseId", diseaseId)
          .getResultStream();
    }
    List<Object[]> resultList = queryResult.collect(Collectors.toList());
    for (UruguayanDepartment value : UruguayanDepartment.values()) {
      DTODepartment dtoDepartment = EnumFactories.getDtoDepartment(value);
      dtoDepartment.setCases(new ArrayList<>());
      for (Object[] row : resultList) {
        if (value.equals(row[0])) {
          DTOCasesQuantity casesQuantity = new DTOCasesQuantity();
          casesQuantity.setType((EnumCaseStatus) row[1]);
          casesQuantity.setQuantity((Long) row[2]);
          dtoDepartment.getCases().add(casesQuantity);
        }
      }
      dtoDepartments.add(dtoDepartment);
    }
    if (caseStatusList == null || caseStatusList.isEmpty()) {
      caseCache.addCases(diseaseId, dtoDepartments);
    }
    return dtoDepartments;
  }

  @Override
  public DTOMedicalCase newCase(long userId, long diseaseId, EnumCaseStatus caseStatus) {
    MedicalCase medicalCase = new MedicalCase();
    Disease disease = entityManager.find(Disease.class, diseaseId);
    FrontProfile frontProfile = entityManager.find(FrontProfile.class, userId);
    medicalCase.setDisease(disease);
    medicalCase.setPatient(frontProfile);
    medicalCase.setStatus(caseStatus);
    medicalCase.setDepartment(frontProfile.getAddress().getDepartment());
    caseCache.clear(diseaseId);
    return modelMapper.map(entityManager.merge(medicalCase), DTOMedicalCase.class);
  }
}
