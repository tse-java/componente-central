package uy.viruscontrol.business.controller;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.HealthProviderCache;
import uy.viruscontrol.business.dto.DTONotifyMedicalAppointment;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.business.integration.DNICIntegrationService;
import uy.viruscontrol.business.integration.HealthProviderIntegrationService;
import uy.viruscontrol.business.integration.SaludUyIntegrationService;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.model.*;
import uy.viruscontrol.business.service.MedicalAppointmentService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;
import uy.viruscontrol.cps.service.DtoAppointment;
import uy.viruscontrol.pi.service.DtoCitizen;

@Stateless
@Slf4j
public class MedicalAppointmentController implements MedicalAppointmentService {
  @PersistenceContext
  private EntityManager entityManager;

  @EJB
  private HealthProviderIntegrationService healthProviderIntegrationService;

  @EJB
  private HealthProviderCache healthProviderCache;

  @EJB
  private SaludUyIntegrationService saludUyService;

  @EJB
  private MessageProducer messageProducer;

  @EJB
  private DNICIntegrationService dnicIntegrationService;

  private final ModelMapper modelMapper = new ModelMapper();

  @Override
  public List<DTOMedicalAppointment> getDoctorAppointments(Long doctorId) {
    return entityManager
      .createNamedQuery("MedicalAppointment.listAsDoctor", MedicalAppointment.class)
      .setParameter("doctorId", doctorId)
      .getResultStream()
      .map(this::buildDTOMedicalAppointment)
      .distinct()
      .collect(Collectors.toList());
  }

  @Override
  public List<DTOMedicalAppointment> getPatientAppointments(Long userId) {
    return entityManager
      .createNamedQuery("MedicalAppointment.listAsPatient", MedicalAppointment.class)
      .setParameter("patientId", userId)
      .getResultStream()
      .map(this::buildDTOMedicalAppointment)
      .collect(Collectors.toList());
  }

  private DTOMedicalAppointment buildDTOMedicalAppointment(MedicalAppointment medicalAppointment) {
    DTOMedicalAppointment dtoMedicalAppointment = new DTOMedicalAppointment();
    dtoMedicalAppointment.setId(medicalAppointment.getId());
    dtoMedicalAppointment.setSymptons(Arrays.asList(modelMapper.map(medicalAppointment.getSymptoms(), DTOSymptom[].class)));
    dtoMedicalAppointment.setLocation(modelMapper.map(medicalAppointment.getLocation(), DTOLocation.class));
    dtoMedicalAppointment.setPatient(modelMapper.map(medicalAppointment.getPatient(), DTOFrontProfile.class));
    dtoMedicalAppointment.setDoctor(modelMapper.map(medicalAppointment.getDoctor(), DTOFrontProfile.class));
    dtoMedicalAppointment.setStatus(medicalAppointment.getStatus());
    return dtoMedicalAppointment;
  }

  @Override
  public void newAppointment(DTORequestMedicalAppointment dtoRequestMedicalAppointment) {
    final FrontProfile patient = entityManager.find(FrontProfile.class, dtoRequestMedicalAppointment.getPatientId());
    final Location location = modelMapper.map(dtoRequestMedicalAppointment.getLocation(), Location.class);
    Set<Symptom> symptoms = new HashSet<>();
    for (Long symptomId : dtoRequestMedicalAppointment.getSymptomIds()) {
      Symptom symptom = entityManager.find(Symptom.class, symptomId);
      symptoms.add(symptom);
    }

    MedicalAppointment appointment = new MedicalAppointment();
    appointment.setPatient(patient);
    appointment.setLocation(location);
    appointment.setSymptoms(symptoms);
    appointment.setRequestStatus(EnumRequestStatus.PENDING);
    appointment.setStatus(EnumMedicalAppointmentStatus.PENDING);
    appointment = entityManager.merge(appointment);
    entityManager.flush();
    dtoRequestMedicalAppointment.setId(appointment.getId());
    dtoRequestMedicalAppointment.setDocumentNumber(patient.getDocumentNumber());
    dtoRequestMedicalAppointment.setUseUserHealthProvider(true);
    entityManager.persist(appointment);
    messageProducer.requestMedicalAppointment(dtoRequestMedicalAppointment);
  }

  @Override
  public void findAppointment(DTORequestMedicalAppointment dtoRequestMedicalAppointment) {
    DtoAppointment dtoAppointment;
    if (!dtoRequestMedicalAppointment.isUseUserHealthProvider()) {
      //solicitar otro proveedor de la lista
      DTOHealthProvider healthProvider = healthProviderCache.getRandomProvider();
      dtoAppointment =
        healthProviderIntegrationService.requestAppointment(dtoRequestMedicalAppointment.getDocumentNumber(), healthProvider.getUrl());
    } else {
      //usar el del usuario
      Long healthProviderId = saludUyService.getPatientHealthProvider(dtoRequestMedicalAppointment.getDocumentNumber());
      HealthCareProvider healthCareProvider = entityManager.find(HealthCareProvider.class, healthProviderId);
      dtoRequestMedicalAppointment.setUseUserHealthProvider(false);
      dtoAppointment =
        healthProviderIntegrationService.requestAppointment(dtoRequestMedicalAppointment.getDocumentNumber(), healthCareProvider.getUrl());
    }

    if (dtoAppointment != null) {
      this.assignAppointment(dtoAppointment, dtoRequestMedicalAppointment);
    } else {
      messageProducer.requestMedicalAppointment(dtoRequestMedicalAppointment);
    }
  }

  private void assignAppointment(DtoAppointment dtoAppointment, DTORequestMedicalAppointment dtoRequestMedicalAppointment) {
    MedicalAppointment appointment = entityManager.find(MedicalAppointment.class, dtoRequestMedicalAppointment.getId());
    appointment.setRequestStatus(EnumRequestStatus.APPROVED);
    FrontProfile doctor = entityManager
      .createNamedQuery("FrontProfile.findByDocumentNumber", FrontProfile.class)
      .setParameter("documentNumber", dtoAppointment.getMedic().getDocumentNumber().toString())
      .getResultStream()
      .findFirst()
      .orElse(null);
    if (doctor == null) {
      DtoCitizen citizenInfo = dnicIntegrationService.getCitizenInformation(dtoAppointment.getMedic().getDocumentNumber().toString());
      doctor = new FrontProfile();
      doctor.setFirstname(citizenInfo.getFirstName());
      doctor.setLastname(citizenInfo.getLastName());
      doctor.setBirthday(citizenInfo.getBirthDay().getTime());
      doctor.setNationality(citizenInfo.getNationality());
      doctor.setDocumentNumber(citizenInfo.getDocumentNumber());
      doctor.setIsDoctor(true);
      doctor.setIsFinished(false);
      entityManager.persist(doctor);
    }
    appointment.setDoctor(doctor);
    appointment.setDateTime(dtoAppointment.getDate().getTime());
    DTOMedicalAppointment dtoMedicalAppointment = new DTOMedicalAppointment();

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyy hh:mm");
    String formatedDate = formatter.format(appointment.getDateTime());

    dtoMedicalAppointment.setDateTime(formatedDate);
    dtoMedicalAppointment.setDoctor(modelMapper.map(appointment.getDoctor(), DTOFrontProfile.class));
    dtoMedicalAppointment.setPatient(modelMapper.map(appointment.getPatient(), DTOFrontProfile.class));
    dtoMedicalAppointment.setExternaId(appointment.getExternalId());
    dtoMedicalAppointment.setId(appointment.getId());
    dtoMedicalAppointment.setLocation(modelMapper.map(appointment.getLocation(), DTOLocation.class));

    DTONotifyMedicalAppointment dtoNotifyMedicalAppointment = new DTONotifyMedicalAppointment();

    dtoNotifyMedicalAppointment.setDtoMedicalAppointment(dtoMedicalAppointment);

    messageProducer.notifyMedicAssigment(dtoNotifyMedicalAppointment);
    entityManager.persist(appointment);
  }

  @Override
  public void closeAppointment(Long doctorId, Long appointmentId) throws VirusControlException {
    MedicalAppointment appointment = entityManager.find(MedicalAppointment.class, appointmentId);
    if (appointment == null) {
      throw new VirusControlException("No existe la consulta medica", VirusControlException.ENTITY_NOT_FOUND);
    }
    if (!appointment.getDoctor().getId().equals(doctorId)) {
      throw new VirusControlException("El usuario no es el medico de la consulta", VirusControlException.INVALID_USER);
    }
    if (appointment.getStatus().equals(EnumMedicalAppointmentStatus.DONE)) {
      throw new VirusControlException("La consulta medica ya se encuentra cerrada", VirusControlException.ALREADY_DONE);
    }
    appointment.setStatus(EnumMedicalAppointmentStatus.DONE);
    entityManager.persist(appointment);
  }
}
