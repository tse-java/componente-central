package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.cache.NewsCache;
import uy.viruscontrol.business.init.InformationSourceTypeInitializer;
import uy.viruscontrol.business.integration.RSSIntegrationService;
import uy.viruscontrol.business.integration.TwitterIntegrationService;
import uy.viruscontrol.business.model.*;
import uy.viruscontrol.business.service.NewsService;
import uy.viruscontrol.common.dto.DTONews;
import uy.viruscontrol.common.dto.DTORssNews;
import uy.viruscontrol.common.dto.DTOTwitterNews;
import uy.viruscontrol.common.enumerations.EnumInformationSourceStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Slf4j
@Stateless
public class NewsController implements NewsService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();

  @EJB
  private NewsCache newsCache;

  @EJB
  private RSSIntegrationService rssIntegrationService;

  @EJB
  private TwitterIntegrationService twitterIntegrationService;

  @Override
  public void refreshNews() {
    refreshTwitter();
    refreshRSS();
  }

  void refreshRSS() {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID);
    if (type != null && type.getActive()) {
      List<RSSInformationSource> sourceList = entityManager
        .createNamedQuery("RSSInformationSource.findAll", RSSInformationSource.class)
        .getResultList();
      for (RSSInformationSource source : sourceList) {
        if (source.getStatus().equals(EnumInformationSourceStatus.ACTIVE)) {
          LastRequestStatus status;
          try {
            List<RSSNews> rssNews = rssIntegrationService.getNews(source);
            newsCache.addRSSNews(rssNews);

            status = LastRequestStatus.ok();
          } catch (VirusControlException e) {
            log.error(e.getMessage());

            status = LastRequestStatus.error("ERROR: " + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
          }
          source.setLastRequestStatus(status);
          entityManager.persist(status);
          entityManager.persist(source);
        }
      }
    }
  }

  void refreshTwitter() {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID);
    if (type != null && type.getActive()) {
      List<TwitterInformationSource> sourceList = entityManager
        .createNamedQuery("TwitterInformationSource.findAll", TwitterInformationSource.class)
        .getResultList();
      for (TwitterInformationSource source : sourceList) {
        if (source.getStatus().equals(EnumInformationSourceStatus.ACTIVE)) {
          LastRequestStatus status;
          try {
            List<TwitterNews> twitterNews = twitterIntegrationService.getNews(source);
            newsCache.addTwitterNews(twitterNews);

            status = LastRequestStatus.ok();
          } catch (VirusControlException e) {
            log.error(e.getMessage());

            status = LastRequestStatus.error("ERROR: " + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
          }
          source.setLastRequestStatus(status);
          entityManager.persist(status);
          entityManager.persist(source);
        }
      }
    }
  }

  @Override
  public List<DTONews> getLatestNews() {
    List<DTONews> dtoNews = new ArrayList<>();
    for (News latestNew : newsCache.getLatestNews()) {
      if (latestNew instanceof RSSNews) {
        DTORssNews dtoNewsItem = modelMapper.map(latestNew, DTORssNews.class);
        dtoNewsItem.setType("RSS");
        dtoNewsItem.setSource(((RSSNews) latestNew).getSource().getName());
        dtoNews.add(dtoNewsItem);
      } else {
        DTONews dtoNewsItem = modelMapper.map(latestNew, DTOTwitterNews.class);
        dtoNewsItem.setType("TWITTER");
        dtoNews.add(dtoNewsItem);
      }
    }
    return dtoNews;
  }
}
