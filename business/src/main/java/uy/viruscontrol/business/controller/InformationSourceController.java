package uy.viruscontrol.business.controller;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;
import uy.viruscontrol.business.init.InformationSourceTypeInitializer;
import uy.viruscontrol.business.model.InformationSource;
import uy.viruscontrol.business.model.InformationSourceType;
import uy.viruscontrol.business.model.RSSInformationSource;
import uy.viruscontrol.business.model.TwitterInformationSource;
import uy.viruscontrol.business.service.InformationSourceService;
import uy.viruscontrol.common.dto.DTOInformationSource;
import uy.viruscontrol.common.dto.DTORSSInformationSource;
import uy.viruscontrol.common.dto.DTOTwitterInformationSource;
import uy.viruscontrol.common.enumerations.EnumInformationSourceStatus;
import uy.viruscontrol.common.enumerations.EnumInformationSrcType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
public class InformationSourceController implements InformationSourceService {
  @PersistenceContext
  EntityManager entityManager;

  private final ModelMapper modelMapper = new ModelMapper();
  private static final String SOURCE_NOT_FOUND = "La fuente de informacion no existe";

  @Override
  public List<DTOInformationSource> getInformationSourceList(EnumInformationSrcType enumInformationSrcType) {
    List<InformationSource> sourceList;
    if (enumInformationSrcType.equals(EnumInformationSrcType.RSS)) {
      sourceList =
        entityManager
          .createNamedQuery("InformationSource.getByClass", InformationSource.class)
          .setParameter("class", RSSInformationSource.class)
          .getResultList();
    } else {
      sourceList =
        entityManager
          .createNamedQuery("InformationSource.getByClass", InformationSource.class)
          .setParameter("class", TwitterInformationSource.class)
          .getResultList();
    }
    List<DTOInformationSource> dtoSourceList = new ArrayList<>();
    for (InformationSource source : sourceList) {
      if (source instanceof TwitterInformationSource) {
        dtoSourceList.add(modelMapper.map(source, DTOTwitterInformationSource.class));
      } else {
        dtoSourceList.add(modelMapper.map(source, DTORSSInformationSource.class));
      }
    }
    return dtoSourceList;
  }

  @Override
  public List<DTOInformationSource> getInformationSourceList() {
    List<InformationSource> sourceList = entityManager
      .createNamedQuery("InformationSource.getAll", InformationSource.class)
      .getResultList();
    List<DTOInformationSource> dtoSourceList = new ArrayList<>();
    for (InformationSource source : sourceList) {
      if (source instanceof TwitterInformationSource) {
        dtoSourceList.add(modelMapper.map(source, DTOTwitterInformationSource.class));
      } else {
        dtoSourceList.add(modelMapper.map(source, DTORSSInformationSource.class));
      }
    }
    return dtoSourceList;
  }

  @Override
  public DTORSSInformationSource newRSSFeed(String name, String description, String url, List<String> keyWords)
    throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID);
    if (!type.getActive()) {
      throw new VirusControlException("El tipo RSS no esta activado", VirusControlException.INACTIVE_TYPE);
    }

    List<RSSInformationSource> conflicUrlList = entityManager
      .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
      .setParameter("url", url)
      .getResultList();
    if (!conflicUrlList.isEmpty()) {
      throw new VirusControlException("Ya existe otra fuente de informacion con la misma url", VirusControlException.ENTITY_ALREDY_EXISTS);
    }
    RSSInformationSource newSource = new RSSInformationSource();
    newSource.setStatus(EnumInformationSourceStatus.ACTIVE);
    newSource.setDescription(description);
    newSource.setName(name);
    newSource.setUrl(url);
    newSource.setKeyWords(keyWords);
    entityManager.persist(newSource);
    return modelMapper.map(newSource, DTORSSInformationSource.class);
  }

  @Override
  public DTORSSInformationSource editRSSFeed(DTORSSInformationSource informationSource) throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.RSS_ID);
    if (!type.getActive()) {
      throw new VirusControlException("El tipo RSS no esta activado", VirusControlException.INACTIVE_TYPE);
    }

    RSSInformationSource source = entityManager.find(RSSInformationSource.class, informationSource.getId());
    if (source == null) {
      throw new VirusControlException(SOURCE_NOT_FOUND, VirusControlException.ENTITY_NOT_FOUND);
    }

    List<RSSInformationSource> conflicUrlList = entityManager
      .createNamedQuery("RSSInformationSource.findByUrl", RSSInformationSource.class)
      .setParameter("url", informationSource.getUrl())
      .getResultList();
    for (RSSInformationSource conflictSource : conflicUrlList) {
      if (!conflictSource.getId().equals(informationSource.getId())) {
        throw new VirusControlException(
          "Ya existe otra fuente de informacion con la misma url",
          VirusControlException.ENTITY_ALREDY_EXISTS
        );
      }
    }

    source.setKeyWords(informationSource.getKeyWords());
    source.setUrl(informationSource.getUrl());
    source.setDescription(informationSource.getDescription());
    source.setName(informationSource.getName());
    source.setStatus(informationSource.getStatus());
    entityManager.persist(source);
    return modelMapper.map(source, DTORSSInformationSource.class);
  }

  @Override
  public DTOTwitterInformationSource newTwitterSource(String name, String description, String username) throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID);
    if (!type.getActive()) {
      throw new VirusControlException("El tipo Twitter no esta activado", VirusControlException.INACTIVE_TYPE);
    }
    List<TwitterInformationSource> conflictUsernameList = entityManager
      .createNamedQuery("TwitterInformationSource.findByNickname", TwitterInformationSource.class)
      .setParameter("username", username)
      .getResultList();
    if (!conflictUsernameList.isEmpty()) {
      throw new VirusControlException("Ya existe otra fuente de informacion con el mismo nickname");
    }
    TwitterInformationSource source = new TwitterInformationSource();
    source.setUsername(username);
    source.setDescription(description);
    source.setName(name);
    source.setStatus(EnumInformationSourceStatus.ACTIVE);
    entityManager.persist(source);
    return modelMapper.map(source, DTOTwitterInformationSource.class);
  }

  @Override
  public DTOTwitterInformationSource editTwitterSource(DTOTwitterInformationSource source) throws VirusControlException {
    InformationSourceType type = entityManager.find(InformationSourceType.class, InformationSourceTypeInitializer.TWITTER_ID);
    if (!type.getActive()) {
      throw new VirusControlException("El tipo Twitter no esta activado", VirusControlException.INACTIVE_TYPE);
    }

    TwitterInformationSource persistedSource = entityManager.find(TwitterInformationSource.class, source.getId());
    if (persistedSource == null) {
      throw new VirusControlException(SOURCE_NOT_FOUND, VirusControlException.ENTITY_NOT_FOUND);
    }

    List<TwitterInformationSource> conflictUsernameList = entityManager
      .createNamedQuery("TwitterInformationSource.findByNickname", TwitterInformationSource.class)
      .setParameter("username", source.getUsername())
      .getResultList();
    for (TwitterInformationSource twitterInformationSource : conflictUsernameList) {
      if (!twitterInformationSource.getId().equals(source.getId())) throw new VirusControlException(
        "Ya existe otra fuente de informacion con el mismo nickname"
      );
    }

    persistedSource.setName(source.getName());
    persistedSource.setStatus(source.getStatus());
    persistedSource.setDescription(source.getDescription());
    persistedSource.setUsername(source.getUsername());
    entityManager.persist(persistedSource);
    return modelMapper.map(persistedSource, DTOTwitterInformationSource.class);
  }

  @Override
  public void deleteInformationSource(Long id) throws VirusControlException {
    InformationSource source = entityManager.find(InformationSource.class, id);
    if (source == null) {
      throw new VirusControlException(SOURCE_NOT_FOUND, VirusControlException.ENTITY_NOT_FOUND);
    }
    entityManager.remove(source);
  }
}
