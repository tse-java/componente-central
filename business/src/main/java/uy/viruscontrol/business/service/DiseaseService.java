package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOUpdateDisease;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface DiseaseService {
  /**
   * Solicita una nueva enfermedad, se ignora el identificador del DTO
   *
   * @param disease {@link DTODisease}
   * @throws VirusControlException Si algun sintoma no esta en la base de datos(SYMPTOM_NOT_FOUND) o si falta completar campos (UNCOMPLETED_FIELDS)
   */
  void requestNewDisease(DTODisease disease) throws VirusControlException;

  /**
   * Aprueba una nueva enfermedad
   *
   * @param diseaseId id de la enfermedad
   * @throws VirusControlException - no existe una enfermedad con ese identificador
   */
  void approveNewDisease(Long diseaseId) throws VirusControlException;

  /**
   * rechaza una nueva enfermedad
   *
   * @param diseaseId id de la enfermedad
   * @throws VirusControlException - no existe una enfermedad con ese identificador
   */
  void rejectNewDisease(Long diseaseId) throws VirusControlException;

  /**
   * devuelve el listado de enfermedades
   *
   * @param requestStatus filtro por estado de la solicitud, si es null se ignora
   * @return List of {@Link DTODisease}
   */
  List<DTODisease> getDiseasesByStatus(EnumRequestStatus requestStatus);

  DTODisease getDiseasesById(Long id) throws VirusControlException;

  /**
   * Actualiza una enfermedad con nuevos datos. Las relaciones no especificadas se eliminan.
   * @param diseaseId id de la enfermedad a actualizar
   * @param dtoUpdateDisease DTO con los datos necesarios para actualiar la enfermedad
   * @return DTODisease
   * */
  DTODisease updateDisease(Long diseaseId, DTOUpdateDisease dtoUpdateDisease) throws VirusControlException;
}
