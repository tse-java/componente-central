package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOResourceProvider;
import uy.viruscontrol.common.dto.DTOResourceQuantity;
import uy.viruscontrol.common.dto.DTOWrongId;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface ResourceProviderService {
  /**
   * Crea un nuevo proveedor de recursos con el 'name' e intenta comunicarse con
   * el proveedor a traves de la 'url' para obtener la informacion del local. <br>
   * En caso de no obtener respuesta, se retorna el nuevo recurso sin la informacion
   * del local.
   *
   * @param name nombre del componente periferico
   * @param url url de conexion con el componente periferico
   * @return
   * @throws VirusControlException
   */
  DTOResourceProvider newResourceProvider(String name, String url) throws VirusControlException;

  /**
   * Sincroniza los datos de un proveedor de recursos a traves de la url.
   *
   * @param id identificador del proveedor
   * @return
   * @throws VirusControlException si no existe el id o no se obtiene una respuesta
   */
  DTOResourceProvider syncResourceProvider(Long id) throws VirusControlException;

  /**
   * Actualiza un proveedor de recursos con el 'name' e intenta comunicarse con
   * el proveedor a traves de la 'url' para obtener la informacion del local.
   * Si se deja algun campo en null no se actualiza. <br>
   * En caso de no obtener respuesta, se retorna el recurso sin la informacion
   * del local.
   *
   * @param name nombre del componente periferico
   * @param url url de conexion con el componente periferico
   * @return
   */
  DTOResourceProvider updateResourceProvider(Long id, String name, String url) throws VirusControlException;

  /**
   * Elimina un proveedor de recursos.
   *
   * @param id identificador del proveedor
   * @throws VirusControlException si no existe el id
   */
  void deleteResourceProvider(Long id) throws VirusControlException;

  /**
   *Obtiene el listado de los proveedores de recursos
   *
   * @param nameFilter filtro: parte del nombre del nodo periferico o del nombre comercial
   * @return
   */
  List<DTOResourceProvider> getResourceProviders(String nameFilter);

  /**
   * Obtiene el proveedor de recursos asociado al token,
   * si no existe devuelve null
   *
   * @param token
   * @return
   */
  DTOResourceProvider getResourceProviderByToken(String token);

  /**
   * Carga la disponibilidad de recursos que tiene un proveedor, el nuevo listado de disponibilidad
   * remplazara al antiguo
   *
   * @param providerId identificador de proveedor
   * @param resourceQuantityList listado con identificador de recurso y cantidad
   * @return se devuelve el listado de identificadores de recursos que no existan
   */
  List<DTOWrongId> setResourceProviderAvailability(Long providerId, List<DTOResourceQuantity> resourceQuantityList);

  /**
   * registra el sistema en el proveedor de recursos seleccionado
   * @param providerId
   * @throws VirusControlException si no se encuentra el proveedor o si hay problemas en el registro
   */
  void register(Long providerId) throws VirusControlException;
}
