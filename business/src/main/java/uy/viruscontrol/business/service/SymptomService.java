package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface SymptomService {
  /**
   * devuelve todos los sintomas
   * @return
   */
  List<DTOSymptom> getSymptoms();

  /**
   * crea un nuevo sintoma, se ignora el id del DTO
   * @param dtoSymptom
   * @return
   * @throws VirusControlException si falta completar campos
   */
  DTOSymptom newSymptom(DTOSymptom dtoSymptom) throws VirusControlException;
}
