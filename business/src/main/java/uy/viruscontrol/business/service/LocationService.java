package uy.viruscontrol.business.service;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOContagionReport;
import uy.viruscontrol.common.dto.DTOLocationReport;

@Local
public interface LocationService {
  void newReport(DTOLocationReport dtoLocationReport);
  List<DTOLocationReport> getLocationsUntilDate(Long userId, Date until);
  List<DTOLocationReport> findContagionByLocation(DTOContagionReport contagionReport);
}
