package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTODepartment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

@Local
public interface CaseService {
  /**
   * devuelve la cantidad de casos de una enfermedad, separados por departamento y por estado del caso.
   *
   * @param diseaseId identificador de enfermedad
   * @param caseStatusList listado de estados de casos a filtrar, si esta vacio se ignora
   * @return
   */
  List<DTODepartment> getCasesGroupByDepartment(Long diseaseId, List<EnumCaseStatus> caseStatusList);

  /**
   * Crea un nuevo caso para el usuario.
   *
   * @param userId id del usuario
   * @param diseaseId identificador de enfermedad
   * @param caseStatus Estado del nuevo caso
   * @return
   */
  DTOMedicalCase newCase(long userId, long diseaseId, EnumCaseStatus caseStatus);
}
