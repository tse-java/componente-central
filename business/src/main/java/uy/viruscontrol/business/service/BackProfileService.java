package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOBackProfile;
import uy.viruscontrol.common.dto.DTONewBackProfile;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface BackProfileService {
  /**
   * agrega un nuevo usuario de backoffice al sistema.
   * si ya existe un usuario con el mismo nickname tira excepcion
   * @param userProfile
   * @throws VirusControlException si algo feo pasa
   */
  DTOBackProfile newProfile(DTONewBackProfile userProfile) throws VirusControlException;

  /**
   * Elimina un perfil de un usuario.
   * @param userId id del usuario a eliminar
   * @throws VirusControlException si algo feo pasa
   */
  void delete(Long userId) throws VirusControlException;

  /**
   * Valida los datos de un usuario y retorna un DTO.
   * @param nickname nick del usuario
   * @param password password actual del usuario
   * @throws VirusControlException si el usuario/password es invalido.
   */
  DTOBackProfile doLogin(String nickname, String password) throws VirusControlException;

  /**
   * cambia la contraseña de un usuario de backoffice, si se esta inserando
   * por primera vez la contraseña se debe mandar oldPassword en null
   * @param nickname nick del usuario
   * @param oldPassword password actual del usuario
   * @param newPassword nueva password
   * @throws VirusControlException
   */
  void changePassword(String nickname, String oldPassword, String newPassword) throws VirusControlException;

  void updateUser(DTONewBackProfile dtoNewBackProfile) throws VirusControlException;

  /**
   * lista los fucking profiles
   * @return List of {@Link DTOBackProfile}
   */
  List<DTOBackProfile> listUsers();
}
