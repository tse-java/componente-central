package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface MedicalAppointmentService {
  List<DTOMedicalAppointment> getDoctorAppointments(Long doctorId);

  List<DTOMedicalAppointment> getPatientAppointments(Long userId);

  /**
   * Solicita una consulta medica al proveedor de salud del paciente, si
   * ese proveedor no tiene disponible se intenta en otros proveedores hasta
   * obtener una consulta medica.
   * En caso de no obtener en ningun proveedor se crea la consulta en estado
   * pendiente y de envia nuevamente a la cola de consultas.
   * @param dtoRequestMedicalAppointment appointment data
   */
  void newAppointment(DTORequestMedicalAppointment dtoRequestMedicalAppointment);

  void findAppointment(DTORequestMedicalAppointment dtoRequestMedicalAppointment);

  void closeAppointment(Long doctorId, Long appointmentId) throws VirusControlException;
}
