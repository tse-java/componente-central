package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTONews;

@Local
public interface NewsService {
  /**
   * actualiza las noticias de las fuentes de informacion activas
   */
  void refreshNews();

  /**
   * obtiene las ultimas noticias registradas en el sistema
   * @return
   */
  List<DTONews> getLatestNews();
}
