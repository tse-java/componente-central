package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTODisease;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOUpdateExam;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface ExamService {
  /***
   * Crea un nuevo examen para un paciente
   * @param doctorId id del usuario del medico
   * @param patientId id del usuario del paciente
   * @param dtOExam informacion sobre el examen a realizar
   * @throws VirusControlException Si el paciente o el medico no existe
   */
  void newExam(Long doctorId, Long patientId, DTODisease dtOExam) throws VirusControlException;

  /**
   * Patches an exam by id.
   * @param dtoUpdateExam data to update
   * @throws VirusControlException exam not found
   */
  void updateExam(DTOUpdateExam dtoUpdateExam) throws VirusControlException;

  /**
   * Obtiene ls examenes de un usuario
   * @param userId id del usuario
   * @throws VirusControlException exam not found
   */
  List<DTOExam> getUserExams(Long userId);
}
