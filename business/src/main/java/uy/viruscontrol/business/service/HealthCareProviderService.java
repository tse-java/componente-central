package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOHealthProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface HealthCareProviderService {
  /**
   * Crea un nuevo prestador de salud
   *
   * @param healthProvider
   * @return
   */
  DTOHealthProvider newHealthCareProvider(DTOHealthProvider healthProvider);

  /**
   * Cambia la url de un prestador de salud
   *
   * @param id
   * @param url
   * @return
   */
  DTOHealthProvider editHealthProvider(Long id, String url, String name) throws VirusControlException;

  /**
   * obtiene un prestador de salud por si id
   *
   * @param id
   * @return
   */
  DTOHealthProvider getHealthProvider(Long id) throws VirusControlException;

  /**
   * obtiene todos los prestadores de salud registrados en virusControl
   *
   * @return
   */
  List<DTOHealthProvider> getHealthProviders();
}
