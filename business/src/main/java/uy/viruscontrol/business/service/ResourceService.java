package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumResourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface ResourceService {
  /**
   * Agrega un nuevo recurso
   * @param resource
   */
  void newResource(DTOResource resource);

  /**
   * Lista los recursos
   * @return
   */
  List<DTOResource> listResources();

  /**
   * Lista los recursos y sus susbscriptions
   * @return
   * @param userId
   */
  List<DTOResourceSubscription> listResourcesSubscriptions(Long userId);

  /**
   * subscribe a un recurso
   * @param resourceId
   * @param userId
   * @throws VirusControlException
   */
  void subscribeResource(Long userId, Long resourceId) throws VirusControlException;

  /**
   * desubsccribe de un recurso
   * @param userId
   * @param resourceId
   * @throws VirusControlException
   */
  void unsubscribeResource(Long userId, Long resourceId) throws VirusControlException;

  /**
   * Retorna un listado de recursos asociados a un proveedor y su cantidad disponible.
   * Filtrados por enfer
   * @param diseaseId required - identificador de enfermedad asociada
   * @param resourceType required -  tipo de recurso
   * @param locationFilter filtro: parte del address, town o city de la direccion del proveedor
   * @param resourceNameFilter filtro: parte del nombre de un recurso
   * @return
   * @throws VirusControlException si no existe la enfermedad o el tipo de recurso
   */
  List<DTOResourceAvailability> getAvailability(
    Long diseaseId,
    EnumResourceType resourceType,
    String locationFilter,
    String resourceNameFilter
  )
    throws VirusControlException;

  /**
   * Elimina un recurso, sus subscripciones, etc.
   * @throws VirusControlException si no existe la enfermedad o el tipo de recurso
   */
  void deleteResource(Long id) throws VirusControlException;

  /**
   * Actualiza un recurso (name, description)
   * @throws VirusControlException si no existe la enfermedad o el tipo de recurso
   * @return
   */
  DTOResource updateResource(Long id, DTOUpdateResource dtoUpdateResource) throws VirusControlException;

  List<DTOBasicResource> getResourcesListBasic();
}
