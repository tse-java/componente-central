package uy.viruscontrol.business.service;

import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTONotificationConfig;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface SettingsService {
  Double getContagionDistance();
  void configureContagionDistance(Double dtoNotificationConfig);
  DTONotificationConfig getNotificationConfig();
  void configureNotifications(DTONotificationConfig dtoNotificationConfig);
}
