package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOInformationSource;
import uy.viruscontrol.common.dto.DTORSSInformationSource;
import uy.viruscontrol.common.dto.DTOTwitterInformationSource;
import uy.viruscontrol.common.enumerations.EnumInformationSrcType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface InformationSourceService {
  /**
   * devuelve las fuentes de informacion filtradas por tipo
   * @param enumInformationSrcType
   * @return
   */
  List<DTOInformationSource> getInformationSourceList(EnumInformationSrcType enumInformationSrcType);

  /**
   * devuelve todas las fuentes de informacion
   * @return
   */
  List<DTOInformationSource> getInformationSourceList();

  /**
   * agrega una nueva fuente de informacion de tipo RSS
   * @param description
   * @param name
   * @param url
   * @return
   * @throws VirusControlException Si ya existe una fuente con el mismo url o si el tipo esta deshabilitado
   */
  DTORSSInformationSource newRSSFeed(String name, String description, String url, List<String> keyWords) throws VirusControlException;

  /**
   * modifica una fuente de informacion de tipo RSS
   * @param informationSource
   * @return
   * @throws VirusControlException Si no existe una fuente RSS con esa id, si ya existe otra fuente con el mismo url o si el tipo esta deshabilitado
   */
  DTORSSInformationSource editRSSFeed(DTORSSInformationSource informationSource) throws VirusControlException;

  /**
   * agrega una nueva fuente de informacion de Twitter
   * @param name
   * @param description
   * @param username nombre de usuario de twitter
   * @return
   * @throws VirusControlException si ya existe una fuente con el mismo usuario o si el tipo esta deshabilitado
   */
  DTOTwitterInformationSource newTwitterSource(String name, String description, String username) throws VirusControlException;

  /**
   * modifica una fuente de twitter existente
   * @param source
   * @return
   * @throws VirusControlException Si no existe una fuente de twitter con esa id, si ya existe otra fuente con el mismo usuario o si el tipo esta deshabilitado
   */
  DTOTwitterInformationSource editTwitterSource(DTOTwitterInformationSource source) throws VirusControlException;

  /**
   * elimina la la fuente de noticias, pero no elimina las noticias del cache
   * @param id
   * @throws VirusControlException si no existe la fuente de informacion
   */
  void deleteInformationSource(Long id) throws VirusControlException;
}
