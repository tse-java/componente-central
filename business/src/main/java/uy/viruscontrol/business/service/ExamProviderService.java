package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface ExamProviderService {
  /**
   * Lista los proveedores de examenes
   * @return
   */
  List<DTOExamProvider> listLabs();

  /**
   * Agrega un proveedor de recursos
   * @param examProvider
   * @return
   */
  DTOExamProvider addLab(DTOExamProvider examProvider);

  /**
   * Actualiza un proveedor de examenes. No se puede actualizar el
   * parametro isActive, para esto existe un endpoint específico.
   * @param dtoExamProvider
   * @return
   * @throws VirusControlException
   */
  DTOExamProvider updateLab(DTOExamProvider dtoExamProvider) throws VirusControlException;

  /**
   * Elimina un proveedor de examenes.
   * Solo puede ser eliminado si no tiene examenes asociados
   * @param id
   * @throws VirusControlException
   */
  void deleteLab(Long id) throws VirusControlException;

  /**
   * Actualiza el estado del proveedor de examenes. Cuando esta
   * inactivo no recibe nuevos examenes.
   * @param id
   * @param isActive
   * @throws VirusControlException
   */
  void updateLabStatus(Long id, Boolean isActive) throws VirusControlException;

  DTOExamProvider setStatus(Long providerId, String message, Boolean isOK) throws VirusControlException;
}
