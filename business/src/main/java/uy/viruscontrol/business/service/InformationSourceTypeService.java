package uy.viruscontrol.business.service;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOInformationSourceType;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface InformationSourceTypeService {
  /**
   * devuelve un listado con todos los tipos de fuentes de informacion
   * @param active filtro de estado, si es null se ignora
   * @return
   */
  List<DTOInformationSourceType> getInformationSourceTypes(Boolean active);

  /**
   * devuelve un tipo de fuentes de informacion por su id
   * @param id
   * @return
   * @throws VirusControlException si no existe el tipo
   */
  DTOInformationSourceType getInformationSourceType(Long id) throws VirusControlException;

  /**
   * cambia el estado del tipo de fuentes de informacion
   * @param id
   * @param active
   * @throws VirusControlException si no existe el tipo
   */
  void setInformationSourceTypeStatus(Long id, Boolean active) throws VirusControlException;
}
