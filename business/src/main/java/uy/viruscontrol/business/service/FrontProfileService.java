package uy.viruscontrol.business.service;

import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTONotifyStyle;
import uy.viruscontrol.common.dto.DTOUpdateFrontProfile;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface FrontProfileService {
  /**
   * guarda un nuevo token para el usuario
   *  @param userId
   * @param token
   * @return
   */
  String saveRefreshToken(Long userId, String token) throws VirusControlException;

  /**
   * retorna  el usuario asociado a un refresh token.
   * si el token es invalido tira excepcion
   *
   * @param token
   * @return
   */
  DTOFrontProfile getProfileByRefreshToken(String token) throws VirusControlException;

  /**
   * invalida el token de un usuario.
   * si el usuario es invalido tira excepcion
   *
   * @param userId
   */
  void invalidateRefreshToken(Long userId) throws VirusControlException;

  /**
   * retorna el usuario correspondiente a la id
   *
   * @param id id del usuario
   * @throws VirusControlException
   */
  DTOFrontProfile getProfileById(Long id) throws VirusControlException;

  /**
   * actualiza un usuario
   *
   * @param dtoFrontOfficeUserProfile perfil del usuario
   * @throws VirusControlException si no se encuentra el usuario
   */
  DTOFrontProfile updateProfile(DTOUpdateFrontProfile dtoFrontOfficeUserProfile) throws VirusControlException;

  /**
   * actualiza la direccion de un usuario
   *
   * @param dtoFrontOfficeUserProfile perfil del usuario
   * @throws VirusControlException si no se encuentra el usuario
   */
  void patchAddress(DTOUpdateFrontProfile dtoFrontOfficeUserProfile) throws VirusControlException;

  /**
   * obtiene el perifl de un usuario de front.
   * Si no existe el usuario, se eleva un excepcion
   *
   * @param externalId identificador del usuario en el sistema externo de login
   * @return DTOFrontOfficeUserProfile perfil del usuario
   */
  DTOFrontProfile getProfileByExternalId(String externalId);

  /**
   * agrega un nuevo usuario de frontoffice al sistema.
   * si ya existe un usuario con la misma cedula tira excepcion
   *
   * @param userProfile
   * @throws VirusControlException
   * @return
   */
  DTOFrontProfile newProfile(DTOFrontProfile userProfile) throws VirusControlException;

  /**
   * Informa si un usuario tiene un caso abierto para una enfermedad.
   *
   * @param userId id del usuairo
   * @param diseaaseId id de la enfermedad
   * @return true si encuentra un caso existente para el usuario para dicha enfermedad
   */
  boolean hasAnActiveCase(Long userId, Long diseaaseId);

  DTOFrontProfile patchNotifications(Long userId, DTONotifyStyle dtoNotifyStyle) throws VirusControlException;
}
