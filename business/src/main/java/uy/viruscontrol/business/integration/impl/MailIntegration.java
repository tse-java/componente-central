package uy.viruscontrol.business.integration.impl;

import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import net.sargue.mailgun.Response;
import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.business.integration.MailIntegrationService;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.dto.DTOResourceNotification;

@Stateless
@Slf4j
public class MailIntegration implements MailIntegrationService {
  private final Configuration configuration = new Configuration()
    .domain("mail.viruscontrol.tech")
    .apiKey("ac6a00e5757ac6eafc9cc868e7c9d6c2-0afbfc6c-7f61e53b")
    .from("VirusControl", "notificacion@viruscontrol.tech");

  public void notifyContagion(DTOMedicalCase medicalCase) {
    String email = medicalCase.getPatient().getEmail();
    String disease = medicalCase.getDisease().getName();
    log.info("Notificar mediante CORREO el contagio de: {} por {}", email, disease);
    Response response = Mail
      .using(configuration)
      .to(email)
      .subject("Alerta de contagio.")
      .text("Usted esta siendo notificado por un posible contagio de " + disease)
      .build()
      .send();
    if (!response.isOk()) {
      log.error("Error al Notificar mediante CORREO: {}", response.responseMessage());
    } else {
      log.debug("Notificado mediante correo {} por {}", email, disease);
    }
  }

  @Override
  public void notifyExam(DTOExam exam) {
    String email = exam.getMedicalCase().getPatient().getEmail();
    log.info("Notificar mediante CORREO el exam de: {} paso a estado {}", email, exam.getDisease().getName());
    Response response = Mail
      .using(configuration)
      .to(email)
      .subject("Estado de examen.")
      .text("Usted esta siendo notificado por un cambio de estado en un examen de " + exam.getDisease().getName())
      .build()
      .send();
    if (!response.isOk()) {
      log.error("Error al Notificar mediante CORREO: {}", response.responseMessage());
    } else {
      log.debug("Notificado mediante correo {} por {}", email, exam.getDisease().getName());
    }
  }

  @Override
  public void notifyMedicAssignment(DTOMedicalAppointment medicalAppointment) {
    String email = medicalAppointment.getPatient().getEmail();
    log.info("Notificar mediante CORREO la asignacion medica: {}", email);
    Response response = Mail
      .using(configuration)
      .to(email)
      .subject("Asignacion medica.")
      .text("Fecha para " + medicalAppointment.getDateTime())
      .build()
      .send();
    if (!response.isOk()) {
      log.error("Error al Notificar mediante CORREO: {}", response.responseMessage());
    } else {
      log.debug("Notificado mediante correo {} por {}", email, medicalAppointment.getDateTime());
    }
  }

  @Override
  public void notifyResource(DTOResourceNotification dtoResourceNotification) {
    String email = dtoResourceNotification.getFrontProfile().getEmail();
    log.info("Notificar mediante Mail la disponibilidad de recurso: {}", dtoResourceNotification.getResource().getName());
    Response response = Mail
      .using(configuration)
      .to(email)
      .subject("Estado de recurso.")
      .text("Usted esta siendo notificado por un cambio disponibilidad en " + dtoResourceNotification.getResource().getName())
      .build()
      .send();
    if (!response.isOk()) {
      log.error("Error al Notificar mediante CORREO: {}", response.responseMessage());
    } else {
      log.debug("Notificado mediante correo {} por {}", email, dtoResourceNotification.getResource().getName());
    }
  }

  @Override
  public void notifyCaseStatus(DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus) {
    String email = dtoNotifyMedicalCaseStatus.getEmail();
    log.info("Notificar mediante Mail cambio de estado de caso medico");
    Response response = Mail
      .using(configuration)
      .to(email)
      .subject("Estado de Caso.")
      .text(
        "Usted esta siendo notificado por un cambio en el estado un caso.\nPaciente: " +
        dtoNotifyMedicalCaseStatus.getPatientName() +
        "\nEnfermedad: " +
        dtoNotifyMedicalCaseStatus.getDisease() +
        "\nEstado: " +
        dtoNotifyMedicalCaseStatus.getStatus().getLabel()
      )
      .build()
      .send();
    if (!response.isOk()) {
      log.error("Error al Notificar mediante CORREO: {}", response.responseMessage());
    } else {
      log.debug("Notificado mediante correo {}", email);
    }
  }
}
