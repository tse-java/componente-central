package uy.viruscontrol.business.integration;

import javax.ejb.Local;
import uy.viruscontrol.cps.service.DtoAppointment;

@Local
public interface HealthProviderIntegrationService {
  /**
   * Solicita a un proveedor de salud una consulta medica, en
   * caso de no tener disponible retorna null
   * @param documentNumber cedula de identidad del paciente
   * @param path ruta de acceso al proveedor
   * @return Informacion de la consulta medica
   */
  DtoAppointment requestAppointment(String documentNumber, String path);
}
