package uy.viruscontrol.business.integration;

import javax.ejb.Local;
import uy.viruscontrol.pi.service.DtoCitizen;

@Local
public interface DNICIntegrationService {
  /**
   * Obtiene informacion de un ciudadano a traves de su cedula de identidad,
   * si no se puede conectar al nodo retorna null
   *
   * @param documentNumber cedula de identidad
   * @return Informacion del usuario
   */
  DtoCitizen getCitizenInformation(String documentNumber);
}
