package uy.viruscontrol.business.integration.impl;

import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import uy.viruscontrol.business.integration.LabIntegrationService;
import uy.viruscontrol.business.integration.rest.LabProviderREST;
import uy.viruscontrol.business.service.ExamProviderService;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.dto.DTOUpdateExam;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Slf4j
@Stateless
public class LabIntegration implements LabIntegrationService {
  @Inject
  ExamProviderService examProviderService;

  @Override
  public List<DTOUpdateExam> fetchExams(DTOExamProvider dtoExamProvider) throws VirusControlException {
    try {
      Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(dtoExamProvider.getUrl())
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
      LabProviderREST service = retrofit.create(LabProviderREST.class);
      Response<List<DTOUpdateExam>> dtoExamResponse = service.fetchExams(dtoExamProvider.getToken()).execute();
      if (dtoExamResponse.isSuccessful()) {
        if (!dtoExamProvider.getOk()) {
          examProviderService.setStatus(dtoExamProvider.getId(), "", true);
        }
        return dtoExamResponse.body();
      } else {
        if (dtoExamProvider.getOk()) {
          examProviderService.setStatus(dtoExamProvider.getId(), dtoExamResponse.message(), false);
        }
        log.info("[EXAM] algo salio mal al solicitar resultados {} {}", dtoExamResponse.code(), dtoExamResponse.message());
        throw new VirusControlException("Error al consumir el proveedor de examenes");
      }
    } catch (IllegalArgumentException | IOException e) {
      if (dtoExamProvider.getOk()) {
        examProviderService.setStatus(dtoExamProvider.getId(), e.getMessage(), false);
      }
      log.info("[EXAM] algo salio mal al solicitar resultados {}", e.getMessage());
      throw new VirusControlException("Error al consumir el proveedor de examenes");
    }
  }

  @Override
  public void requestLab(DTOUpdateExam dtoExam, DTOExamProvider dtoExamProvider) throws VirusControlException {
    try {
      Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(dtoExamProvider.getUrl())
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
      LabProviderREST service = retrofit.create(LabProviderREST.class);
      Response<Void> response = service.requestExam(dtoExamProvider.getToken(), dtoExam).execute();
      if (response.isSuccessful()) {
        if (!dtoExamProvider.getOk()) {
          examProviderService.setStatus(dtoExamProvider.getId(), "", true);
        }
        log.info("[EXAM] se pidio el examen :D {}", response.body());
      } else {
        if (dtoExamProvider.getOk()) {
          examProviderService.setStatus(dtoExamProvider.getId(), response.message(), false);
        }
        log.info("[EXAM] algo salio mal {}", response.errorBody());
        throw new VirusControlException("Error al consumir el proveedor de examenes");
      }
    } catch (IllegalArgumentException | IOException e) {
      if (dtoExamProvider.getOk()) {
        examProviderService.setStatus(dtoExamProvider.getId(), e.getMessage(), false);
      }
      log.info("[EXAM] algo salio mal {}", e.getMessage());
      throw new VirusControlException("Error al consumir el proveedor de examenes");
    }
  }
}
