package uy.viruscontrol.business.integration.impl;

import java.util.List;
import javax.ejb.Stateless;
import uy.viruscontrol.business.integration.SaludUyIntegrationService;
import uy.viruscontrol.pi.service.MyService;
import uy.viruscontrol.pi.service.MyServiceImplService;

@Stateless
public class SaludUyIntegration implements SaludUyIntegrationService {

  @Override
  public Long getPatientHealthProvider(String documentNumber) {
    MyServiceImplService m = new MyServiceImplService();
    MyService saludUyPort = m.getMyServiceImplPort();
    return saludUyPort.getPacientHealthProvider(documentNumber);
  }

  @Override
  public void setHealthProviders(List<Long> ids) {
    MyServiceImplService m = new MyServiceImplService();
    MyService saludUyPort = m.getMyServiceImplPort();
    saludUyPort.setHealthProviders(ids);
  }
}
