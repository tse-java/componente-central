package uy.viruscontrol.business.integration.impl;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.InputSource;
import uy.viruscontrol.business.integration.RSSIntegrationService;
import uy.viruscontrol.business.model.RSSInformationSource;
import uy.viruscontrol.business.model.RSSNews;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
@Slf4j
public class RSSIntegration implements RSSIntegrationService {

  @Override
  public List<RSSNews> getNews(RSSInformationSource source) throws VirusControlException {
    SyndFeed feed;
    InputStream is = null;
    List<SyndEntryImpl> entries;
    try {
      URLConnection openConnection = new URL(source.getUrl()).openConnection();
      openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
      is = openConnection.getInputStream();
      if ("gzip".equals(openConnection.getContentEncoding())) {
        is = new GZIPInputStream(is);
      }
      InputSource inputSource = new InputSource(is);
      SyndFeedInput input = new SyndFeedInput();
      feed = input.build(inputSource);

      entries = feed.getEntries();
      log.info("Se obtuvieron " + entries.size() + " noticias RSS de: " + source.getUrl());
    } catch (IOException | FeedException e) {
      throw new VirusControlException(
        "No se pudo obtener RSS Feed.\n URL:" +
        source.getUrl() +
        "\n Mensaje: " +
        e.getMessage() +
        "\n Stacktrace: " +
        Arrays.toString(e.getStackTrace())
      );
    } finally {
      if (is != null) {
        try {
          is.close();
        } catch (IOException e) {
          log.error("************* Error al intentar cerrar conexion *************\n ", e);
        }
      }
    }

    return entries
      .stream()
      .filter(
        syndEntry ->
          source.getKeyWords().isEmpty() ||
          source
            .getKeyWords()
            .stream()
            .anyMatch(
              keyWord ->
                syndEntry.getTitle().toLowerCase().contains(keyWord.toLowerCase()) ||
                (syndEntry.getDescription() != null && syndEntry.getDescription().getValue().toLowerCase().contains(keyWord.toLowerCase()))
            )
      )
      .map(
        syndEntry -> {
          RSSNews newsItem = new RSSNews();
          newsItem.setTitle(syndEntry.getTitle());
          if (syndEntry.getDescription() != null) {
            newsItem.setDescription(syndEntry.getDescription().getValue());
          }
          newsItem.setLink(syndEntry.getLink());
          newsItem.setDate(new Date());
          newsItem.setSource(source);
          return newsItem;
        }
      )
      .collect(Collectors.toList());
  }
}
