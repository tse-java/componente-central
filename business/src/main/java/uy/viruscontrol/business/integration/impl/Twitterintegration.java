package uy.viruscontrol.business.integration.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import uy.viruscontrol.business.integration.TwitterIntegrationService;
import uy.viruscontrol.business.model.TwitterInformationSource;
import uy.viruscontrol.business.model.TwitterNews;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Stateless
@Slf4j
public class Twitterintegration implements TwitterIntegrationService {

  @Override
  public List<TwitterNews> getNews(TwitterInformationSource source) throws VirusControlException {
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb
      .setDebugEnabled(true)
      .setOAuthConsumerKey("fr7RiTVPv2DtXmn0KYngEg9y7")
      .setOAuthConsumerSecret("dUxUKtlIeoMzZ4j94jEVmIzF6NXQQqXT6w99GChGOsN5S4PTa1")
      .setOAuthAccessToken("222116575-M5OsE7b0KX54xd1Bwbz43kS25KwsrWR32MAOjc8n")
      .setOAuthAccessTokenSecret("u4FY7UYWamiPSLs0xsN1tRHgCZ28ZW0BfqwDgBXfzlU5B");
    TwitterFactory tf = new TwitterFactory(cb.build());
    Twitter twitter = tf.getInstance();

    try {
      twitter.verifyCredentials();
    } catch (TwitterException e) {
      log.error("No se pudo verificar las credenciales de Twitter.");
      throw new VirusControlException(
        "No se pudo verificar las credenciales de Twitter. Mensaje: " +
        e.getErrorMessage() +
        "\n Stacktrace: \n" +
        Arrays.toString(e.getStackTrace())
      );
    }

    try {
      List<Status> statuses = twitter.getUserTimeline(source.getUsername());

      log.info("Se obtuvieron " + statuses.size() + " twitts de @" + source.getUsername());

      List<TwitterNews> twitterNewsList = new ArrayList<>();
      for (Status statusItem : statuses) {
        TwitterNews twitterNewsItem = new TwitterNews();
        twitterNewsItem.setDate(statusItem.getCreatedAt());
        twitterNewsItem.setFavoriteCount(statusItem.getFavoriteCount());
        twitterNewsItem.setUserName(statusItem.getUser().getName());
        twitterNewsItem.setNickName(statusItem.getUser().getScreenName());
        twitterNewsItem.setText(statusItem.getText());
        twitterNewsItem.setProfilePhoto(statusItem.getUser().getOriginalProfileImageURL());
        twitterNewsItem.setRetweetCount(statusItem.getRetweetCount());
        twitterNewsItem.setMediaEntities(Arrays.asList(statusItem.getMediaEntities()));
        twitterNewsItem.setLink("https://twitter.com/" + statusItem.getUser().getScreenName() + "/status/" + statusItem.getId());

        twitterNewsList.add(twitterNewsItem);
      }
      return twitterNewsList;
    } catch (TwitterException e) {
      log.error("no se pudo obtener las publicaciones del usuario");
      throw new VirusControlException(
        "No se pudo obtener las publicaciones del usuario. Mensaje: " +
        e.getErrorMessage() +
        "\n Stacktrace: \n" +
        Arrays.toString(e.getStackTrace())
      );
    }
  }
}
