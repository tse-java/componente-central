package uy.viruscontrol.business.integration;

import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.dto.DTOResourceNotification;

public interface MailIntegrationService {
  void notifyContagion(DTOMedicalCase medicalCase);

  void notifyExam(DTOExam exam);

  void notifyMedicAssignment(DTOMedicalAppointment exam);

  void notifyResource(DTOResourceNotification dtoResourceNotification);

  void notifyCaseStatus(DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus);
}
