package uy.viruscontrol.business.integration;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.dto.DTOUpdateExam;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface LabIntegrationService {
  List<DTOUpdateExam> fetchExams(DTOExamProvider dtoExamProvider) throws VirusControlException;
  void requestLab(DTOUpdateExam dtoExam, DTOExamProvider dtoExamProvider) throws VirusControlException;
}
