package uy.viruscontrol.business.integration;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.business.model.RSSInformationSource;
import uy.viruscontrol.business.model.RSSNews;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface RSSIntegrationService {
  /**
   * Devuelve las ultimas noticias de el RSS Feed que se
   * encuentre en la url de la fuente de informacion
   * @param source fuente de informacion RSS
   * @return Listado de noticias rss
   * @throws VirusControlException si no se pueden obtener las noticias
   */
  List<RSSNews> getNews(RSSInformationSource source) throws VirusControlException;
}
