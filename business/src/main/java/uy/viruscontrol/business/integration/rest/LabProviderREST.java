package uy.viruscontrol.business.integration.rest;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import uy.viruscontrol.common.dto.DTOUpdateExam;

public interface LabProviderREST {
  @POST("exams")
  Call<Void> requestExam(@Header("X-Authorization") String token, @Body DTOUpdateExam user);

  @GET("exams")
  Call<List<DTOUpdateExam>> fetchExams(@Header("X-Authorization") String token);
}
