package uy.viruscontrol.business.integration.rest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uy.viruscontrol.business.dto.DTOCentralNode;
import uy.viruscontrol.business.dto.DTOResourceProvider;

public interface ResourceProviderREST {
  @POST("register")
  Call<Void> register(@Body DTOCentralNode dtoCentralNode);

  @GET("provider")
  Call<DTOResourceProvider> getProviderInfo();
}
