package uy.viruscontrol.business.integration;

import javax.ejb.Local;
import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.dto.DTOResourceNotification;

@Local
public interface FirebaseIntegrationService {
  void notifySupervisor(DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus);

  void notifyExam(DTOExam dtoExam);

  void notifyContagion(DTOMedicalCase medicalCase);

  void notifyMedicAssignment(DTOMedicalAppointment medicalCase);

  void notifyResource(DTOResourceNotification dtoResourceNotification);

  void notifyCaseStatusPatient(DTONotifyMedicalCaseStatus dtoMedicalCaseStatus);

  void notifyCaseStatusMedic(DTONotifyMedicalCaseStatus dtoMedicalCaseStatus);
}
