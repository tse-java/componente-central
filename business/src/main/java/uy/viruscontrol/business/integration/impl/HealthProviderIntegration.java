package uy.viruscontrol.business.integration.impl;

import java.net.MalformedURLException;
import java.net.URL;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.integration.HealthProviderIntegrationService;
import uy.viruscontrol.cps.Service;
import uy.viruscontrol.cps.service.DtoAppointment;
import uy.viruscontrol.cps.service.HealthProviderException_Exception;
import uy.viruscontrol.cps.service.HealthProviderService;

@Stateless
@Slf4j
public class HealthProviderIntegration implements HealthProviderIntegrationService {

  @Override
  public DtoAppointment requestAppointment(String documentNumber, String path) {
    try {
      URL url = new URL(path);
      Service service = new Service(url);
      HealthProviderService port = service.getPort();
      return port.requestMedic(documentNumber);
    } catch (MalformedURLException e) {
      log.error("Malformed Url");
      return null;
    } catch (HealthProviderException_Exception e) {
      log.error("Error al invocar al proveedor de salud: {}", e.getMessage());
      return null;
    }
  }
}
