package uy.viruscontrol.business.integration;

import java.util.List;
import javax.ejb.Local;

@Local
public interface SaludUyIntegrationService {
  /**
   * Devuelve el proveedor de Salud para el paciente con cierta cedula
   * consultado al servicio de Salud Uy
   * @param documentNumber documento del paciente
   * @return Id del proveedor de salud
   */
  Long getPatientHealthProvider(String documentNumber);

  /**
   * setea la cantidad de prestadores de salud en saludUy
   * @param ids de prestadores de salud
   */
  void setHealthProviders(List<Long> ids);
}
