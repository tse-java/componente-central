package uy.viruscontrol.business.integration.impl;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import java.util.concurrent.ExecutionException;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.business.integration.FirebaseIntegrationService;
import uy.viruscontrol.common.dto.*;

@Stateless
@Slf4j
public class FirebaseIntegration implements FirebaseIntegrationService {

  @Override
  public void notifyExam(DTOExam dtoExam) {
    try {
      Firestore db = FirestoreClient.getFirestore();

      DTOExamNotification dtoExamNotification = new DTOExamNotification();
      dtoExamNotification.setRead(false);
      dtoExamNotification.setExternalId(dtoExam.getMedicalCase().getPatient().getExternalId());
      dtoExamNotification.setDiseaseName(dtoExam.getDisease().getName());
      dtoExamNotification.setIsPositive(dtoExam.getPositive());
      dtoExamNotification.setStatus(dtoExam.getStatus().name());
      db.collection("examNotifications").add(dtoExamNotification).get();
      if (dtoExam.getMedicalCase().getPatient().isPushNotify()) {
        Notification notfy = Notification
          .builder()
          .setTitle("VirusControl")
          .setBody("Estado de examen.")
          .setImage("https://viruscontrol.fail/icons/exam_result.png")
          .build();
        String topic = dtoExam.getMedicalCase().getPatient().getExternalId();
        Message message = Message
          .builder()
          .setNotification(notfy)
          .putData("type", "exam")
          .putData("status", dtoExam.getStatus().name())
          .putData("diseaseName", dtoExam.getDisease().getName())
          .putData("isPositive", dtoExam.getPositive() ? "true" : "false")
          .setTopic(topic)
          .build();
        FirebaseMessaging.getInstance().send(message);
        log.info("Firebase exam notification PUSH {}", dtoExam.getMedicalCase().getPatient().getFirstname());
      }
      log.info("Notificar mediante Firebase el resultado de examen a {}", dtoExam.getMedicalCase().getPatient().getFirstname());
    } catch (FirebaseMessagingException e) {
      log.error("Firebase error: {}", e.getMessage());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifyContagion(DTOMedicalCase medicalCase) {
    try {
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());

      DTOContagionNotification dtoContagionNotification = new DTOContagionNotification();
      dtoContagionNotification.setRead(false);
      dtoContagionNotification.setExternalId(medicalCase.getPatient().getExternalId());
      dtoContagionNotification.setDiseaseName(medicalCase.getDisease().getName());
      db.collection("contagionNotifications").add(dtoContagionNotification).get();
      if (medicalCase.getPatient().isPushNotify()) {
        String topic = medicalCase.getPatient().getExternalId();
        Notification notfy = Notification
          .builder()
          .setTitle("VirusControl")
          .setBody("Alerta de contagio")
          .setImage("https://viruscontrol.fail/icons/virus_altert.png")
          .build();
        Message message = Message
          .builder()
          .setNotification(notfy)
          .putData("type", "contagion")
          .putData("status", medicalCase.getStatus().name())
          .putData("diseaseName", medicalCase.getDisease().getName())
          .setTopic(topic)
          .build();
        FirebaseMessaging.getInstance().send(message);
        log.info("Firebase contagion notification PUSH {}", medicalCase.getPatient().getFirstname());
      }
      log.info("Notificar mediante Firebase el contagio de: {}", medicalCase.getPatient().getFirstname());
    } catch (FirebaseMessagingException e) {
      log.error("Firebase error: {}", e.getMessage());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifyMedicAssignment(DTOMedicalAppointment medicalAppointment) {
    try {
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
      DTOAppointmentNotification appointmentNotification = new DTOAppointmentNotification();
      appointmentNotification.setRead(false);
      appointmentNotification.setExternalId(medicalAppointment.getPatient().getExternalId());
      appointmentNotification.setDate(medicalAppointment.getDateTime());
      appointmentNotification.setDoctorName(
        medicalAppointment.getDoctor().getFirstname() + " " + medicalAppointment.getDoctor().getLastname()
      );
      db.collection("appointmentNotifications").add(appointmentNotification).get();
      if (medicalAppointment.getPatient().isPushNotify()) {
        String topic = medicalAppointment.getPatient().getExternalId();
        Notification notify = Notification
          .builder()
          .setTitle("VirusControl")
          .setBody("Consulta medica.")
          .setImage("https://viruscontrol.fail/icons/appointment.png")
          .build();
        Message message = Message
          .builder()
          .setNotification(notify)
          .putData("type", "medic")
          .putData("doctor", medicalAppointment.getDoctor().getFirstname() + " " + medicalAppointment.getDoctor().getLastname())
          .putData("date", medicalAppointment.getDateTime())
          .setTopic(topic)
          .build();
        FirebaseMessaging.getInstance().send(message);
        log.info("Firebase medical appointment notification PUSH {}", medicalAppointment.getPatient().getFirstname());
      }
      log.info("Notificar mediante Firebase la asignacion medica: {}", medicalAppointment.getPatient().getFirstname());
    } catch (FirebaseMessagingException e) {
      log.error("Firebase error: {}", e.getMessage());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifyResource(DTOResourceNotification dtoResourceNotification) {
    try {
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
      dtoResourceNotification.setRead(false);
      dtoResourceNotification.setExternalId(dtoResourceNotification.getFrontProfile().getExternalId());
      db.collection("resourceNotifications").add(dtoResourceNotification).get();
      String topic = dtoResourceNotification.getFrontProfile().getExternalId();
      Notification notify = Notification
        .builder()
        .setTitle("VirusControl")
        .setBody("Disponibilidad de recurso.")
        .setImage("https://viruscontrol.fail/icons/resource.png")
        .build();
      if (dtoResourceNotification.getFrontProfile().isPushNotify()) {
        Message message = Message
          .builder()
          .setNotification(notify)
          .putData("type", "resource")
          .putData("resource", dtoResourceNotification.getResource().getName())
          .setTopic(topic)
          .build();
        FirebaseMessaging.getInstance().send(message);
        log.info("Firebase resource availability notification PUSH {}", dtoResourceNotification.getFrontProfile().getFirstname());
      }
      log.info("Notificar mediante Firebase la disponibilidad de recurso: {}", dtoResourceNotification.getResource().getName());
    } catch (FirebaseMessagingException e) {
      log.error("Firebase error: {}", e.getMessage());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifyCaseStatusPatient(DTONotifyMedicalCaseStatus dtoMedicalCaseStatus) {
    try {
      String topic = dtoMedicalCaseStatus.getExternalId();
      Notification notify = Notification
        .builder()
        .setTitle("VirusControl")
        .setBody("Cambio de estado del Caso.")
        .setImage("https://viruscontrol.fail/icons/virus_altert.png")
        .build();
      Message message = Message
        .builder()
        .setNotification(notify)
        .putData("type", "caseStatus")
        .putData("status", dtoMedicalCaseStatus.getStatus().getLabel())
        .putData("disease", dtoMedicalCaseStatus.getDisease())
        .setTopic(topic)
        .build();
      String response = FirebaseMessaging.getInstance().send(message);
      log.info("Firebase status notification sent: {}", response);
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
      db.collection("statusNotifications").add(dtoMedicalCaseStatus).get();
    } catch (InterruptedException | ExecutionException | FirebaseMessagingException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifyCaseStatusMedic(DTONotifyMedicalCaseStatus dtoMedicalCaseStatus) {
    try {
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
      db.collection("statusNotifications").add(dtoMedicalCaseStatus).get();
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }

  @Override
  public void notifySupervisor(DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus) {
    try {
      Firestore db = FirestoreClient.getFirestore(FirebaseApp.getInstance());
      db.collection("statusNotifications").add(dtoNotifyMedicalCaseStatus).get();
    } catch (InterruptedException | ExecutionException e) {
      log.error("Firestore error: {}", e.getMessage());
      Thread.currentThread().interrupt();
    }
  }
}
