package uy.viruscontrol.business.integration;

import javax.ejb.Local;
import uy.viruscontrol.business.model.ResourceProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface ResourceProviderIntegrationService {
  /**
   * Obtiene los datos del local de un proveedor de recursos, si no se encuentra se retorna null
   * @param url url de conexion al nodo
   * @return {@Link ResourceProvider} ResourceProvider
   */
  ResourceProvider sync(String url);

  /**
   * Registra el sistema en el proveedor de recursos, para que pueda recibir la disponibilidad
   *    * de los recursos
   * @param url
   * @param token
   * @throws VirusControlException
   */
  void register(String url, String token) throws VirusControlException;
}
