package uy.viruscontrol.business.integration.impl;

import java.io.IOException;
import java.util.HashSet;
import javax.ejb.Stateless;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import uy.viruscontrol.business.dto.DTOCentralNode;
import uy.viruscontrol.business.dto.DTOResourceProvider;
import uy.viruscontrol.business.integration.ResourceProviderIntegrationService;
import uy.viruscontrol.business.integration.rest.ResourceProviderREST;
import uy.viruscontrol.business.model.OpenScheduleDay;
import uy.viruscontrol.business.model.ResourceProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Slf4j
@Stateless
public class ResourceProviderIntegration implements ResourceProviderIntegrationService {
  private final ModelMapper modelMapper = new ModelMapper();

  private static final String URL = System.getProperty("api-url");

  @Override
  public ResourceProvider sync(String url) {
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl(url + "/") //paso la url de mi dtCreate o de mi dtUpdate
      .addConverterFactory(JacksonConverterFactory.create())
      .build();
    ResourceProviderREST resourceProviderRest = retrofit.create(ResourceProviderREST.class);
    try {
      Response<DTOResourceProvider> response = resourceProviderRest.getProviderInfo().execute();
      if (response.isSuccessful()) {
        ResourceProvider provider = modelMapper.map(response.body(), ResourceProvider.class);
        provider.setOpenSchedule(new HashSet<>());
        for (OpenScheduleDay openScheduleDay : response.body().getOpenSchedule()) {
          provider.getOpenSchedule().add(openScheduleDay);
        }
        return provider;
      }
      log.error(
        "Error al solicitar datos de Proveedor de recursos. Codigo: [{}], Mensaje: [{}], url: [{}]",
        response.code(),
        response.body(),
        url
      );
      return null;
    } catch (IOException e) {
      log.error("SYNC ERROR: {}", e.getMessage());
      return null;
    }
  }

  @Override
  public void register(String url, String token) throws VirusControlException {
    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl(url + "/") //paso la url de mi dtCreate o de mi dtUpdate
      .addConverterFactory(JacksonConverterFactory.create())
      .build();
    ResourceProviderREST resourceProviderRest = retrofit.create(ResourceProviderREST.class);

    DTOCentralNode dtoCentralNode = new DTOCentralNode();
    dtoCentralNode.setGetResourcesUrl(URL + "/resources/basic");
    dtoCentralNode.setSetAvailabilityUrl(URL + "/resources/availability");
    dtoCentralNode.setToken(token);
    try {
      Response<Void> response = resourceProviderRest.register(dtoCentralNode).execute();
      if (!response.isSuccessful()) {
        log.error(
          "Error al registrarse en el Proveedor de recursos. Codigo: [{}], Mensaje: [{}], url: [{}]",
          response.code(),
          response.message(),
          response.raw().request().url()
        );
        throw new VirusControlException("No se pudo registrar en el nodo periferico");
      }
    } catch (IOException e) {
      log.error("SYNC ERROR: {}", e.getMessage());
      throw new VirusControlException("No se pudo registrar en el nodo periferico");
    }
  }
}
