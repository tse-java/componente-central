package uy.viruscontrol.business.integration;

import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.business.model.TwitterInformationSource;
import uy.viruscontrol.business.model.TwitterNews;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Local
public interface TwitterIntegrationService {
  /**
   * Devuelve los ultimos twits del usuario pasado por parametros
   * @param source fuente de informacion de twitter
   * @return listado de noticias de twitter
   * @throws VirusControlException si no se pueden obtener las noticias
   */
  List<TwitterNews> getNews(TwitterInformationSource source) throws VirusControlException;
}
