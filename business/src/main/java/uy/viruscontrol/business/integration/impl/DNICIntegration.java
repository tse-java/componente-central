package uy.viruscontrol.business.integration.impl;

import javax.ejb.Stateless;
import uy.viruscontrol.business.integration.DNICIntegrationService;
import uy.viruscontrol.pi.service.DtoCitizen;
import uy.viruscontrol.pi.service.MyService;
import uy.viruscontrol.pi.service.MyServiceImplService;

@Stateless
public class DNICIntegration implements DNICIntegrationService {

  @Override
  public DtoCitizen getCitizenInformation(String documentNumber) {
    MyServiceImplService m = new MyServiceImplService();
    MyService dnicPort = m.getMyServiceImplPort();
    return dnicPort.getCitizenInformation(documentNumber);
  }
}
