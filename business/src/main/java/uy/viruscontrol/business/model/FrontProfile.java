package uy.viruscontrol.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedQuery(
  name = "FrontProfile.findByRefreshToken",
  query = "select profile from FrontProfile profile left join fetch profile.address where profile.refreshToken=:refreshToken"
)
@NamedQuery(
  name = "FrontProfile.hasActiveCaseForDisease",
  query = "select profile from FrontProfile profile " +
  "join profile.medicalCases medicalCase " +
  "join medicalCase.disease disease " +
  "where profile.id=:userId and disease.id=:diseaseId and (medicalCase.status=3 or medicalCase.status=2)"
)
@NamedQuery(name = "FrontProfile.findByExternalId", query = "select profile from FrontProfile profile where profile.externalId=:externalId")
@NamedQuery(
  name = "FrontProfile.findByDocumentNumber",
  query = "select profile from FrontProfile profile where profile.documentNumber=:documentNumber"
)
@Data
public class FrontProfile extends UserProfile {
  private String externalId;

  @Column(unique = true)
  private String documentNumber;

  private String email;

  private String firstname;

  private String lastname;

  private Date birthday;

  private String nationality;

  private String refreshToken;

  private String phoneNumber;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  private Location address;

  private Date refreshTokenExpire;

  private Boolean isDoctor;

  private Boolean isFinished;

  @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
  private Set<Exam> exams = new HashSet<>();

  @Transient
  private Set<LocationReport> locationReports = new HashSet<>();

  @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
  private Set<MedicalCase> medicalCases = new HashSet<>();

  @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
  private Set<MedicalAppointment> medicalAppointmentAsDoctor = new HashSet<>();

  @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
  private Set<MedicalAppointment> medicalAppointmentAsPatient = new HashSet<>();

  @ManyToMany(mappedBy = "subscribers", fetch = FetchType.LAZY) //owner resource
  private Set<Resource> subscriptions = new HashSet<>();

  @Column(columnDefinition = "boolean default true")
  private boolean pushNotify;

  @Column(columnDefinition = "boolean default true")
  private boolean mailNotify;
}
