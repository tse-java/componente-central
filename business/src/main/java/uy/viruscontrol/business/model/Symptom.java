package uy.viruscontrol.business.model;

import java.util.Objects;
import javax.persistence.*;
import lombok.Data;

@Entity
@NamedQuery(name = "Symptom.findAll", query = "select symptom from Symptom symptom")
@Data
public class Symptom {
  @Id
  @SequenceGenerator(name = "symptom_gen", sequenceName = "symptom_seq", allocationSize = 1)
  @GeneratedValue(generator = "symptom_gen")
  private Long id;

  private String name;

  private String Description;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Symptom symptom = (Symptom) o;
    return Objects.equals(id, symptom.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
