package uy.viruscontrol.business.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RSSNews extends News {
  private String title;

  private String description;

  private String link;

  private RSSInformationSource source;
}
