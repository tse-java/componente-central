package uy.viruscontrol.business.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Entity
@Data
@NamedQuery(
  name = "MedicalAppointment.listAsPatient",
  query = "select appointment from MedicalAppointment appointment " +
  "join fetch appointment.doctor doctor " +
  "join fetch appointment.patient patient " +
  "join fetch appointment.location location " +
  "left join fetch appointment.symptoms symptoms " +
  "left join fetch patient.address " +
  "left join fetch doctor.address " +
  "where patient.id =:patientId " +
  "and appointment.status = uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus.PENDING " +
  "order by appointment.dateTime asc"
)
@NamedQuery(
  name = "MedicalAppointment.listAsDoctor",
  query = "select appointment from MedicalAppointment appointment " +
  "join fetch appointment.doctor doctor " +
  "join fetch appointment.patient patient " +
  "join fetch appointment.location location " +
  "join fetch appointment.symptoms symptoms " +
  "join fetch patient.address " +
  "join fetch doctor.address " +
  "where doctor.id =: doctorId " +
  "and appointment.status = uy.viruscontrol.common.enumerations.EnumMedicalAppointmentStatus.PENDING " +
  "order by appointment.dateTime asc"
)
public class MedicalAppointment {
  @Id
  @SequenceGenerator(name = "appointment_gen", sequenceName = "appointment_seq", allocationSize = 1)
  @GeneratedValue(generator = "appointment_gen")
  private Long id;

  private Long externalId;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  private Location location;

  private EnumMedicalAppointmentStatus status;

  private Date dateTime;

  private EnumRequestStatus requestStatus;

  @ManyToOne(fetch = FetchType.LAZY)
  private HealthCareProvider healthCareProvider;

  @ManyToOne(fetch = FetchType.LAZY)
  private FrontProfile doctor;

  @ManyToOne(fetch = FetchType.LAZY)
  private FrontProfile patient;

  @ManyToMany(fetch = FetchType.LAZY)
  private Set<Symptom> symptoms = new HashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MedicalAppointment that = (MedicalAppointment) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
