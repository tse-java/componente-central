package uy.viruscontrol.business.model;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumExamStatus;

@Entity
@Data
@NamedQuery(name = "Exam.getUserExams", query = "select exam from Exam exam inner join exam.patient patient where patient.id =: userId")
public class Exam {
  @Id
  @SequenceGenerator(name = "exam_gen", sequenceName = "exam_seq", allocationSize = 1)
  @GeneratedValue(generator = "exam_gen")
  private Long id;

  private Long externalId;

  private Date date = new Date();

  private EnumExamStatus status = EnumExamStatus.REQUESTED;

  private Boolean positive = false;

  @ManyToOne
  private FrontProfile requestingDoctor;

  @ManyToOne
  private FrontProfile patient;

  @ManyToOne
  private ExamProvider examProvider;

  @ManyToOne
  private MedicalCase medicalCase;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Exam exam = (Exam) o;
    return Objects.equals(id, exam.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
