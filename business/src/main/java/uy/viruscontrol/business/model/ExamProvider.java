package uy.viruscontrol.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NamedQuery(name = "ExamProvider.list", query = "select examP from ExamProvider examP")
@NamedQuery(name = "ExamProvider.getExamsQuantity", query = "select count(e) from Exam e join e.examProvider ep where ep.id = :id")
public class ExamProvider extends PeripheralNode {
  private String token;

  private Boolean isActive;

  @Column(columnDefinition = "boolean default true")
  private boolean ok = true;

  @Column(columnDefinition = "varchar(255) default ''")
  private String message = "";
}
