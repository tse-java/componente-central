package uy.viruscontrol.business.model.settings;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Data
@Entity
public class NotificationConfig {
  @Id
  @SequenceGenerator(name = "config_gen", sequenceName = "config_seq", allocationSize = 1)
  @GeneratedValue(generator = "config_gen")
  private Long id;

  private boolean positiveMedic;
  private boolean negativeMedic;
  private boolean suspiciousMedic;
  private boolean recoveredMedic;

  private boolean positiveSupervisor;
  private boolean negativeSupervisor;
  private boolean suspiciousSupervisor;
  private boolean recoveredSupervisor;

  private boolean positivePatient;
  private boolean negativePatient;
  private boolean suspiciousPatient;
  private boolean recoveredPatient;
}
