package uy.viruscontrol.business.model;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import twitter4j.MediaEntity;

@EqualsAndHashCode(callSuper = true)
@Data
public class TwitterNews extends News {
  private String userName;

  private String nickName;

  private String text;

  private String link;

  private String profilePhoto;

  private int favoriteCount;

  private int retweetCount;

  private List<MediaEntity> mediaEntities;
}
