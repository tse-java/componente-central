package uy.viruscontrol.business.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;
import uy.viruscontrol.common.enumerations.Day;

@Entity
@Data
public class OpenScheduleDay {
  @Id
  @SequenceGenerator(name = "schedule_gen", sequenceName = "schedule_seq", allocationSize = 1)
  @GeneratedValue(generator = "schedule_gen")
  private Long id;

  private Day dayOfWeek;

  private String openingHour;

  private String closingHour;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenScheduleDay that = (OpenScheduleDay) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dayOfWeek);
  }
}
