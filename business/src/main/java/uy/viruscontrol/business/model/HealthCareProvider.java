package uy.viruscontrol.business.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedQuery(name = "HealthCareProvider.findAll", query = "select h from HealthCareProvider as h")
@NamedQuery(name = "HealthCareProvider.getIds", query = "select h.id from HealthCareProvider as h")
@Data
public class HealthCareProvider extends PeripheralNode {
  @OneToMany(mappedBy = "healthCareProvider", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  private Set<MedicalAppointment> medicalConsultations = new HashSet<>();
}
