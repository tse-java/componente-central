package uy.viruscontrol.business.model;

import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.*;
import lombok.Data;

@Entity
@NamedQuery(
  name = "ResourceAvailibility.Filter",
  query = "select ra " +
  "from ResourceAvailability ra " +
  "join ra.resource.diseases d " +
  "where d = :disease and ra.resource.type = :resource_type " +
  "and ra.provider.name like :provider_name and " +
  "(ra.provider.address.street like :location_filter or " +
  "ra.provider.address.city like :location_filter or " +
  "ra.provider.address.town like :location_filter)"
)
@Data
public class ResourceAvailability {
  @Id
  @SequenceGenerator(name = "resourceline_gen", sequenceName = "resourceline_seq", allocationSize = 1)
  @GeneratedValue(generator = "resourceline_gen")
  private Long id;

  @ManyToOne
  private Resource resource;

  @ManyToOne
  private ResourceProvider provider;

  private BigDecimal quantity;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ResourceAvailability that = (ResourceAvailability) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
