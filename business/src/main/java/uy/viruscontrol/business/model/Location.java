package uy.viruscontrol.business.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@Entity
@Data
public class Location {
  @Id
  @SequenceGenerator(name = "location_gen", sequenceName = "location_seq", allocationSize = 1)
  @GeneratedValue(generator = "location_gen")
  private Long id;

  private String coordinates;

  private String street;

  private String town;

  private String city;

  private UruguayanDepartment department;
}
