package uy.viruscontrol.business.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumResourceType;

@Entity
@NamedQuery(name = "Resource.listBasic", query = "select resource from Resource resource ")
@NamedQuery(
  name = "Resource.list",
  query = "select resource from Resource resource " +
  "left join fetch resource.lines lines " +
  "left join fetch lines.provider provider " +
  "left join fetch provider.address " +
  "left join fetch provider.openSchedule "
)
@NamedQuery(
  name = "Resource.notSubscribed",
  query = "select resource from Resource resource " +
  "left join fetch resource.lines lines " +
  "left join fetch lines.provider provider " +
  "left join fetch provider.address " +
  "left join fetch provider.openSchedule " +
  "where resource.id not in " +
  "(select r.id from Resource r inner join r.subscribers subscribers where subscribers.id =: userId)"
)
@NamedQuery(
  name = "Resource.subscribed",
  query = "select resource from Resource resource inner join resource.subscribers subscribers " +
  "left join fetch resource.lines lines " +
  "left join fetch lines.provider provider " +
  "left join fetch provider.address " +
  "left join fetch provider.openSchedule " +
  "where subscribers.id =: userId"
)
@Data
public class Resource {
  @Id
  @SequenceGenerator(name = "resource_gen", sequenceName = "resource_seq", allocationSize = 1)
  @GeneratedValue(generator = "resource_gen")
  private Long id;

  private String name;

  private String description;

  private EnumResourceType type;

  @ManyToMany(mappedBy = "recommendedResources", fetch = FetchType.LAZY) //owner disease
  private Set<Disease> diseases = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY)
  private Set<ResourceAvailability> lines = new HashSet<>();

  @ManyToMany(fetch = FetchType.LAZY)
  private Set<FrontProfile> subscribers = new HashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Resource resource = (Resource) o;
    return id.equals(resource.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
