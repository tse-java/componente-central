package uy.viruscontrol.business.model;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Field;
import dev.morphia.annotations.Index;
import dev.morphia.annotations.Indexes;
import dev.morphia.geo.Point;
import dev.morphia.utils.IndexType;
import java.util.Date;
import java.util.Objects;
import lombok.Data;

@Data
@Entity
@Indexes(
  {
    @Index(fields = @Field(value = "location", type = IndexType.GEO2DSPHERE)),
    @Index(fields = @Field(value = "date", type = IndexType.DESC)),
    @Index(fields = @Field(value = "userId", type = IndexType.DESC))
  }
)
public class LocationReport extends BaseEntity {
  private Date date;

  private Point location;

  private String department;

  private Long userId;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    LocationReport report = (LocationReport) o;
    return Objects.equals(id, report.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
