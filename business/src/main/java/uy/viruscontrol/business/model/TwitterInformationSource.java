package uy.viruscontrol.business.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("tw")
@Data
@NamedQuery(
  name = "TwitterInformationSource.findByNickname",
  query = "SELECT s FROM TwitterInformationSource AS s WHERE s.username = : username"
)
@NamedQuery(name = "TwitterInformationSource.findAll", query = "SELECT s FROM TwitterInformationSource AS s ")
public class TwitterInformationSource extends InformationSource {
  private String username;
}
