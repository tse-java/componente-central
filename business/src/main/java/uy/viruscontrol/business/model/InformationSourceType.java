package uy.viruscontrol.business.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import lombok.Data;

@Entity
@Data
@NamedQuery(name = "InformationSourceType.findByStatus", query = "select i from InformationSourceType as i where i.active = :active")
@NamedQuery(name = "InformationSourceType.findAll", query = "select i from InformationSourceType as i")
public class InformationSourceType {
  @Id
  private Long id;

  private String name;

  private String description;

  private Boolean active;
}
