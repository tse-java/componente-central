package uy.viruscontrol.business.model;

import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;

@Entity
@NamedQuery(
  name = "Case.countGroupByDepartmentAndStatusFilterByStatus",
  query = "select c.department, c.status, count(c) as quantity  " +
  "from MedicalCase c " +
  "where c.status in (:status) and c.disease.id = :diseaseId " +
  "group by c.department, c.status " +
  "order by c.department ASC "
)
@NamedQuery(
  name = "Case.countGroupByDepartmentAndStatus",
  query = "select c.department, c.status, count(c) as quantity  " +
  "from MedicalCase c " +
  "where c.disease.id = :diseaseId " +
  "group by c.department, c.status " +
  "order by c.department ASC "
)
@Data
public class MedicalCase {
  @Id
  @SequenceGenerator(name = "case_gen", sequenceName = "case_seq", allocationSize = 1)
  @GeneratedValue(generator = "case_gen", strategy = GenerationType.SEQUENCE)
  private Long id;

  private EnumCaseStatus status;

  private UruguayanDepartment department;

  @OneToMany(mappedBy = "medicalCase")
  private List<Exam> asociatedExam;

  @ManyToOne
  private Disease disease;

  @ManyToOne
  private FrontProfile patient;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MedicalCase that = (MedicalCase) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
