package uy.viruscontrol.business.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Data
@Entity
public class LastRequestStatus implements Serializable {
  public static final int STATUS_OK = 1;
  public static final int STATUS_ERROR = 2;

  @Id
  @SequenceGenerator(name = "status_gen", sequenceName = "status_seq", allocationSize = 1)
  @GeneratedValue(generator = "status_gen")
  private Long id;

  private Date date;

  private int status;

  private String description;

  public static LastRequestStatus error(String description) {
    LastRequestStatus requestStatus = new LastRequestStatus();
    requestStatus.setDescription(description);
    requestStatus.setDate(new Date());
    requestStatus.setStatus(STATUS_ERROR);
    return requestStatus;
  }

  public static LastRequestStatus ok(String description) {
    LastRequestStatus requestStatus = new LastRequestStatus();
    requestStatus.setDescription(description);
    requestStatus.setDate(new Date());
    requestStatus.setStatus(STATUS_OK);
    return requestStatus;
  }

  public static LastRequestStatus ok() {
    LastRequestStatus requestStatus = new LastRequestStatus();
    requestStatus.setDescription("OK");
    requestStatus.setDate(new Date());
    requestStatus.setStatus(STATUS_OK);
    return requestStatus;
  }
}
