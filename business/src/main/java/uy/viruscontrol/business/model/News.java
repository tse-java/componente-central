package uy.viruscontrol.business.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public abstract class News implements Comparable<News>, Serializable {
  private Date date;

  @Override
  public int compareTo(@NotNull News news) {
    return getDate().compareTo(news.getDate());
  }
}
