package uy.viruscontrol.business.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedQuery(name = "ResourceProvider.findAll", query = "select rp from ResourceProvider rp")
@NamedQuery(
  name = "ResourceProvider.findByName",
  query = "select rp from ResourceProvider rp " + "where rp.name like :nameFilter or rp.commercialName like :nameFilter"
)
@NamedQuery(name = "ResourceProvider.getByToken", query = "select rp from ResourceProvider as rp where rp.token = :token")
@Data
public class ResourceProvider extends PeripheralNode {
  @OneToOne(cascade = CascadeType.ALL)
  private Location address;

  private String commercialName;

  private String token;

  @OneToOne(cascade = CascadeType.ALL)
  private LastRequestStatus lastRequestStatus;

  @OneToMany(cascade = CascadeType.ALL)
  @OrderBy("dayOfWeek asc")
  private Set<OpenScheduleDay> openSchedule = new HashSet<>();

  @OneToMany(mappedBy = "provider", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Set<ResourceAvailability> resources = new HashSet<>();
}
