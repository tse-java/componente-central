package uy.viruscontrol.business.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("rss")
@Data
@NamedQuery(name = "RSSInformationSource.findByUrl", query = "SELECT i FROM RSSInformationSource AS i WHERE i.url = :url")
@NamedQuery(name = "RSSInformationSource.findAll", query = "SELECT i FROM RSSInformationSource AS i join fetch i.keyWords")
public class RSSInformationSource extends InformationSource {
  private String url;

  /**
   * Listado de palabras clave, se utilizaran para filtrar
   * las noticias del Feed. Si la noticia no tiene al menos
   * una de ellas se ignora.
   */
  @ElementCollection
  private List<String> keyWords = new ArrayList<>();
}
