package uy.viruscontrol.business.model;

import at.favre.lib.crypto.bcrypt.BCrypt;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;
import uy.viruscontrol.common.exceptions.VirusControlException;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedQuery(name = "BackProfile.list", query = "select profile from BackProfile profile")
@NamedQuery(name = "BackProfile.findByNickname", query = "select profile from BackProfile profile where profile.nickname=:nickname")
@Data
public class BackProfile extends UserProfile {
  private String nickname;

  private String fullname;

  private String password;

  private EnumBackofficeUserRole role;

  public void setPassword(String password) {
    this.password = BCrypt.withDefaults().hashToString(12, password.toCharArray());
  }

  public void checkPassword(String password) throws VirusControlException {
    BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), this.password);
    if (!result.verified) {
      throw new VirusControlException("Invalid password.", VirusControlException.INVALID_PASSWORD);
    }
  }
}
