package uy.viruscontrol.business.model;

import javax.persistence.*;
import lombok.Data;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
public class UserProfile {
  @Id
  @SequenceGenerator(name = "userprofile_gen", sequenceName = "userprofile_seq", allocationSize = 1)
  @GeneratedValue(generator = "userprofile_gen")
  private Long id;
}
