package uy.viruscontrol.business.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumInfectiousAgentType;
import uy.viruscontrol.common.enumerations.EnumRequestStatus;

@Entity
@NamedQuery(name = "Disease.findAll", query = "select d from Disease d")
@NamedQuery(name = "Disease.findByRequestStatus", query = "select d from Disease d where d.requestStatus = :status")
@Data
public class Disease {
  @Id
  @SequenceGenerator(name = "disease_gen", sequenceName = "disease_seq", allocationSize = 1)
  @GeneratedValue(generator = "disease_gen")
  private Long id;

  private String name;

  private String agentName;

  private EnumInfectiousAgentType agentType;

  private EnumRequestStatus requestStatus;

  /**
   * tiempo de contagio, en segundos
   */
  private Long infectionTime;

  @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
  private Set<Resource> recommendedResources = new HashSet<>();

  @ManyToMany(fetch = FetchType.LAZY)
  private Set<Symptom> symptoms = new HashSet<>();

  @OneToMany(mappedBy = "disease", fetch = FetchType.LAZY)
  private Set<MedicalCase> medicalCases = new HashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Disease disease = (Disease) o;
    return Objects.equals(id, disease.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
