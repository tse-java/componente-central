package uy.viruscontrol.business.model;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumInformationSourceStatus;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Data
@NamedQuery(name = "InformationSource.getAll", query = "SELECT i FROM InformationSource i")
@NamedQuery(name = "InformationSource.getByClass", query = "select i from InformationSource i where TYPE(i) in :class")
public abstract class InformationSource implements Serializable {
  @Id
  @SequenceGenerator(name = "source_gen", sequenceName = "source_seq", allocationSize = 1)
  @GeneratedValue(generator = "source_gen")
  private Long id;

  private String name;

  private String description;

  private EnumInformationSourceStatus status;

  @OneToOne(cascade = CascadeType.ALL)
  private LastRequestStatus lastRequestStatus;
}
