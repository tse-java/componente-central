package uy.viruscontrol.business.model;

import javax.persistence.*;
import lombok.Data;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class PeripheralNode {
  @Id
  @SequenceGenerator(name = "node_gen", sequenceName = "node_seq", allocationSize = 1)
  @GeneratedValue(generator = "node_gen")
  private Long id;

  private String name;

  private String url;
}
