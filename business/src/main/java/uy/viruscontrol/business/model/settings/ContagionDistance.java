package uy.viruscontrol.business.model.settings;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Data
@Entity
public class ContagionDistance {
  @Id
  @SequenceGenerator(name = "contagiondistance_gen", sequenceName = "contagiondistance_seq", allocationSize = 1)
  @GeneratedValue(generator = "contagiondistance_gen")
  private Long id;

  private Double distance;
}
