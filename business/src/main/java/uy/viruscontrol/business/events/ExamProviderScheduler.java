package uy.viruscontrol.business.events;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.cache.LabCache;
import uy.viruscontrol.business.integration.LabIntegrationService;
import uy.viruscontrol.business.service.ExamService;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Singleton
@Slf4j
public class ExamProviderScheduler {
  @EJB
  private ExamService examService;

  @EJB
  private LabIntegrationService labIntegration;

  @EJB
  private LabCache labCache;

  @Schedule(hour = "*", minute = "*/1")
  public void examProviderSync() {
    List<DTOExamProvider> examProviders = labCache.getExamProviders();
    examProviders.forEach(this::processExams);
  }

  private void processExams(DTOExamProvider examProvider) {
    try {
      labIntegration
        .fetchExams(examProvider)
        .forEach(
          dtoUpdateExam -> {
            try {
              log.info("[LAB] Procesando resultado: {}", dtoUpdateExam);
              examService.updateExam(dtoUpdateExam);
            } catch (VirusControlException virusControlException) {
              log.error("[LAB] error: {}", virusControlException.getMessage());
            }
          }
        );
    } catch (VirusControlException virusControlException) {
      log.error("[LAB] error: {}", virusControlException.getMessage());
    }
  }
}
