package uy.viruscontrol.business.events;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import uy.viruscontrol.business.cache.NotificationConfigCache;
import uy.viruscontrol.business.dto.DTOMedicalCaseStatus;
import uy.viruscontrol.business.dto.DTONotifyMedicalCaseStatus;
import uy.viruscontrol.business.integration.FirebaseIntegrationService;
import uy.viruscontrol.business.integration.MailIntegrationService;
import uy.viruscontrol.business.service.BackProfileService;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.EnumBackofficeUserRole;

@Stateless
public class MedicalCaseEvent {
  @EJB
  BackProfileService backProfileService;

  @EJB
  NotificationConfigCache cache;

  @Inject
  MailIntegrationService mailIntegrationService;

  @Inject
  FirebaseIntegrationService firebaseIntegrationService;

  public void notifySupervisorAboutMedicalCase(@NotNull @ObservesAsync DTOMedicalCaseStatus dtoMedicalCase) {
    DTONotificationConfig config = cache.get();
    switch (dtoMedicalCase.getStatus()) {
      case CURED:
        {
          if (config.isRecoveredSupervisor()) {
            notifySupervisors(dtoMedicalCase);
          }
          if (config.isRecoveredPatient()) {
            notifyPatient(dtoMedicalCase);
          }
          if (config.isRecoveredMedic()) {
            notifyMedic(dtoMedicalCase);
          }
          break;
        }
      case CONFIRMED:
        {
          if (config.isPositiveSupervisor()) {
            notifySupervisors(dtoMedicalCase);
          }
          if (config.isPositivePatient()) {
            notifyPatient(dtoMedicalCase);
          }
          if (config.isPositiveMedic()) {
            notifyMedic(dtoMedicalCase);
          }
          break;
        }
      case DISCARDED:
        {
          if (config.isNegativeSupervisor()) {
            notifySupervisors(dtoMedicalCase);
          }

          if (config.isNegativePatient()) {
            notifyPatient(dtoMedicalCase);
          }
          if (config.isNegativeMedic()) {
            notifyMedic(dtoMedicalCase);
          }
          break;
        }
      case SUSPECT:
        {
          if (config.isSuspiciousPatient()) {
            notifyPatient(dtoMedicalCase);
          }
          if (config.isSuspiciousSupervisor()) {
            notifySupervisors(dtoMedicalCase);
          }
          break;
        }
      default:
        {}
    }
  }

  private void notifyPatient(@NotNull DTOMedicalCaseStatus caseStatus) {
    DTOFrontProfile patient = caseStatus.getPatient();
    DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus = new DTONotifyMedicalCaseStatus();
    dtoNotifyMedicalCaseStatus.setEmail(caseStatus.getPatient().getEmail());
    dtoNotifyMedicalCaseStatus.setExternalId(caseStatus.getPatient().getExternalId());
    dtoNotifyMedicalCaseStatus.setStatus(caseStatus.getStatus());
    dtoNotifyMedicalCaseStatus.setDisease(caseStatus.getDisease());
    dtoNotifyMedicalCaseStatus.setPatientName(patient.getFirstname() + " " + patient.getLastname());
    if (caseStatus.getPatient().isPushNotify()) {
      mailIntegrationService.notifyCaseStatus(dtoNotifyMedicalCaseStatus);
    }
    if (caseStatus.getPatient().isMailNotify()) {
      firebaseIntegrationService.notifyCaseStatusPatient(dtoNotifyMedicalCaseStatus);
    }
  }

  private void notifyMedic(@NotNull DTOMedicalCaseStatus caseStatus) {
    DTOFrontProfile patient = caseStatus.getPatient();
    DTOFrontProfile medic = caseStatus.getMedic();
    DTONotifyMedicalCaseStatus dtoNotifyMedicalCaseStatus = new DTONotifyMedicalCaseStatus();
    dtoNotifyMedicalCaseStatus.setEmail(medic.getEmail());
    dtoNotifyMedicalCaseStatus.setExternalId(medic.getExternalId());
    dtoNotifyMedicalCaseStatus.setStatus(caseStatus.getStatus());
    dtoNotifyMedicalCaseStatus.setDisease(caseStatus.getDisease());
    dtoNotifyMedicalCaseStatus.setPatientName(patient.getFirstname() + " " + patient.getLastname());

    if (caseStatus.getPatient().isPushNotify()) {
      mailIntegrationService.notifyCaseStatus(dtoNotifyMedicalCaseStatus);
    }
    if (caseStatus.getPatient().isMailNotify()) {
      firebaseIntegrationService.notifyCaseStatusMedic(dtoNotifyMedicalCaseStatus);
    }
  }

  private void notifySupervisors(DTOMedicalCaseStatus dtoMedicalCase) {
    backProfileService
      .listUsers()
      .stream()
      .filter(dtoBackProfile -> dtoBackProfile.getRole().equals(EnumBackofficeUserRole.SUPERVISOR))
      .map(
        dtoBackProfile -> {
          DTONotifyMedicalCaseStatus dtoMedicalCaseStatus = new DTONotifyMedicalCaseStatus();
          dtoMedicalCaseStatus.setDisease(dtoMedicalCase.getDisease());
          dtoMedicalCaseStatus.setStatus(dtoMedicalCase.getStatus());
          dtoMedicalCaseStatus.setExternalId(dtoBackProfile.getNickname());
          return dtoMedicalCaseStatus;
        }
      )
      .forEach(dtoMedicalCaseStatus -> firebaseIntegrationService.notifySupervisor(dtoMedicalCaseStatus));
  }
}
