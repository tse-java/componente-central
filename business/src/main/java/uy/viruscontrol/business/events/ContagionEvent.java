package uy.viruscontrol.business.events;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.ObservesAsync;
import org.apache.commons.lang3.time.DateUtils;
import uy.viruscontrol.business.dto.DTOContagionAlert;
import uy.viruscontrol.business.jms.MessageProducer;
import uy.viruscontrol.business.service.LocationService;
import uy.viruscontrol.common.dto.DTOContagionReport;
import uy.viruscontrol.common.dto.DTOLocationReport;

@Stateless
public class ContagionEvent {
  @EJB
  LocationService locationService;

  @EJB
  MessageProducer messageProducer;

  public void verifyContagious(@ObservesAsync DTOContagionAlert contagionAlert) {
    Long userId = contagionAlert.getPatientId();
    Date date = DateUtils.addDays(new Date(), (int) -contagionAlert.getDisease().getInfectionTime());
    List<DTOLocationReport> locationsUntilDate = locationService.getLocationsUntilDate(userId, date);
    DTOContagionReport contagionReport = new DTOContagionReport();
    contagionReport.setDisease(contagionAlert.getDisease());
    contagionReport.setLocationReport(new ArrayList<>());
    contagionReport.getLocationReport().addAll(locationsUntilDate);
    messageProducer.reportContagion(contagionReport);
  }
}
