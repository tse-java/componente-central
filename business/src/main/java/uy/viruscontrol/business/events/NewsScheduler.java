package uy.viruscontrol.business.events;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.service.NewsService;

@Singleton
@Slf4j
public class NewsScheduler {
  @EJB
  private NewsService newsService;

  @Schedule(hour = "*", minute = "*/15")
  private void newsSync() {
    log.info("===============se actualizan las noticias==================");
    newsService.refreshNews();
  }
}
