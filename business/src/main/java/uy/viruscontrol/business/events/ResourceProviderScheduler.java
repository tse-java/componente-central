package uy.viruscontrol.business.events;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.service.ResourceProviderService;
import uy.viruscontrol.common.dto.DTOResourceProvider;
import uy.viruscontrol.common.exceptions.VirusControlException;

@Singleton
@Slf4j
public class ResourceProviderScheduler {
  @EJB
  private ResourceProviderService providerService;

  @Schedule(hour = "*/2")
  public void resourceProviderSync() {
    log.info("Sincronizando proveedores de recursos");
    List<DTOResourceProvider> resourceProviderList = providerService.getResourceProviders(null);
    for (DTOResourceProvider resourceProvider : resourceProviderList) {
      try {
        providerService.syncResourceProvider(resourceProvider.getId());
      } catch (VirusControlException e) {
        log.warn("No se pudo sincronizar el recurso [{}]. Mensaje: {}", resourceProvider.getId(), e.getMessage());
      }
    }
  }
}
