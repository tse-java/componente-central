package uy.viruscontrol.business.init;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uy.viruscontrol.business.model.InformationSourceType;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup
public class InformationSourceTypeInitializer {
  public static final Long RSS_ID = 1L;
  public static final Long TWITTER_ID = 2L;

  public static final Boolean START_ACTIVE = true;

  @PersistenceContext
  EntityManager entityManager;

  @PostConstruct
  public void init() {
    InformationSourceType type = entityManager.find(InformationSourceType.class, RSS_ID);
    if (type == null) {
      InformationSourceType sourceType = new InformationSourceType();
      sourceType.setId(RSS_ID);
      sourceType.setName("RSS Feed");
      sourceType.setDescription(
        "Consumidor de RSS Feed ingresando una url. Con filtro de palabras clave (OR), si esta vacio no se filtra "
      );
      sourceType.setActive(START_ACTIVE);
      entityManager.persist(sourceType);
    }
    type = entityManager.find(InformationSourceType.class, TWITTER_ID);

    if (type == null) {
      InformationSourceType sourceType = new InformationSourceType();
      sourceType.setId(TWITTER_ID);
      sourceType.setName("Twitter");
      sourceType.setDescription("Consumidor de twits por nombre de Usuario");
      sourceType.setActive(START_ACTIVE);
      entityManager.persist(sourceType);
    }
  }
}
