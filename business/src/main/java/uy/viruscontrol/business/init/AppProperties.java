package uy.viruscontrol.business.init;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum AppProperties {
  INSTANCE;

  private final Properties properties;

  AppProperties() {
    properties = new Properties();
    try {
      properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
    } catch (IOException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
    }
  }

  public String getMongoHost() {
    return properties.getProperty("mongo.host");
  }

  public int getMongoPort() {
    return Integer.parseInt(properties.getProperty("mongo.port"));
  }

  public String getMongoUser() {
    return properties.getProperty("mongo.user");
  }

  public String getMongoPassword() {
    return properties.getProperty("mongo.password");
  }

  public String getRedisUri() {
    return properties.getProperty("redis.uri");
  }
}
