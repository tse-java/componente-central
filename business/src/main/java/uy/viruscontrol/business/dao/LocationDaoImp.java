package uy.viruscontrol.business.dao;

import dev.morphia.geo.Point;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import org.bson.types.ObjectId;
import uy.viruscontrol.business.model.LocationReport;

@Stateless
public class LocationDaoImp implements LocationDao {
  private final MongoDatastore mongoConnection = MongoDatastore.getInstance();

  @Override
  public LocationReport findById(ObjectId id) {
    return mongoConnection.getDatastore().createQuery(LocationReport.class).field("_id").equal(id).first();
  }

  @Override
  public List<LocationReport> findAll() {
    return mongoConnection.getDatastore().createQuery(LocationReport.class).find().toList();
  }

  @Override
  public void add(LocationReport locationReport) {
    mongoConnection.getDatastore().save(locationReport);
  }

  @Override
  public List<LocationReport> getLocationsUntilDate(Long userId, Date date) {
    return mongoConnection
      .getDatastore()
      .createQuery(LocationReport.class)
      .field("userId")
      .equal(userId)
      .field("date")
      .greaterThan(date)
      .find()
      .toList();
  }

  public List<LocationReport> findPointsNearLocationAtTime(List<Long> ignoreUsers, Date start, Date end, Point point, double distance) {
    return mongoConnection
      .getDatastore()
      .find(LocationReport.class)
      .field("location")
      .near(point, distance, 0D)
      .field("date")
      .greaterThan(start)
      .field("date")
      .lessThan(end)
      .field("userId")
      .hasNoneOf(ignoreUsers)
      .find()
      .toList();
  }
}
