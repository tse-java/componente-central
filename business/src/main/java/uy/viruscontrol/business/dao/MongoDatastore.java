package uy.viruscontrol.business.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import uy.viruscontrol.business.init.AppProperties;

public class MongoDatastore {
  private final Datastore datastore;
  private static MongoDatastore instance;

  private MongoDatastore() {
    Morphia morphia = new Morphia();
    morphia.mapPackage("uy.viruscontrol.business.model", true);
    MongoCredential credentials = MongoCredential.createCredential(
      AppProperties.INSTANCE.getMongoUser(),
      "admin",
      AppProperties.INSTANCE.getMongoPassword().toCharArray()
    );
    datastore =
      morphia.createDatastore(
        new MongoClient(
          new ServerAddress(AppProperties.INSTANCE.getMongoHost(), AppProperties.INSTANCE.getMongoPort()),
          credentials,
          MongoClientOptions.builder().build()
        ),
        "viruscontrol"
      );
    datastore.ensureIndexes();
  }

  public static MongoDatastore getInstance() {
    if (instance == null) {
      instance = new MongoDatastore();
    }
    return instance;
  }

  public Datastore getDatastore() {
    return datastore;
  }
}
