package uy.viruscontrol.business.dao;

import java.util.List;
import org.bson.types.ObjectId;

public interface CRUD<T> {
  T findById(ObjectId id);
  List<T> findAll();
  void add(T t);
}
