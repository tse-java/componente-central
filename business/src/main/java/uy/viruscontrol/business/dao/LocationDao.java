package uy.viruscontrol.business.dao;

import dev.morphia.geo.Point;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import uy.viruscontrol.business.model.LocationReport;

@Local
public interface LocationDao extends CRUD<LocationReport> {
  List<LocationReport> getLocationsUntilDate(Long userId, Date date);
  List<LocationReport> findPointsNearLocationAtTime(List<Long> ignoreUsers, Date start, Date end, Point point, double distance);
}
