package uy.viruscontrol.business.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOMedicalCase;

@Data
public class DTONotifyContagion implements Serializable {
  private DTOMedicalCase medicalCase;
}
