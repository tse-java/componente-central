package uy.viruscontrol.business.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.dto.DTODisease;

@Data
public class DTOContagionAlert implements Serializable {
  private Long patientId;
  private DTODisease disease;
}
