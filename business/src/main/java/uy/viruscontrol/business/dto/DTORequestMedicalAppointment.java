package uy.viruscontrol.business.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOLocation;

/**
 * Clase utilizada para transmitir datos en el JMS
 */

@Data
public class DTORequestMedicalAppointment implements Serializable {
  private Long id;

  private String documentNumber;

  private Long patientId;

  private List<Long> symptomIds;

  private DTOLocation location;

  private boolean useUserHealthProvider;
}
