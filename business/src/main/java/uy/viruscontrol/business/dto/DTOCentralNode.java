package uy.viruscontrol.business.dto;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class DTOCentralNode {
  private int id;

  private String setAvailabilityUrl;

  private String getResourcesUrl;

  private String token;

  private LocalDateTime registeredAt;
}
