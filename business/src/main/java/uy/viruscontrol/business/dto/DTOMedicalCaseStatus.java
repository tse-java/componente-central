package uy.viruscontrol.business.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

@Data
public class DTOMedicalCaseStatus implements Serializable {
  DTOFrontProfile patient;
  DTOFrontProfile medic;
  EnumCaseStatus status;
  String disease;
}
