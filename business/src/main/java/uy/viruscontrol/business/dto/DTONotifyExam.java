package uy.viruscontrol.business.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOExam;

@Data
public class DTONotifyExam implements Serializable {
  private DTOExam exam;
}
