package uy.viruscontrol.business.dto;

import java.io.Serializable;
import lombok.Data;
import uy.viruscontrol.common.dto.DTOMedicalAppointment;

@Data
public class DTONotifyMedicalAppointment implements Serializable {
  private DTOMedicalAppointment dtoMedicalAppointment;
}
