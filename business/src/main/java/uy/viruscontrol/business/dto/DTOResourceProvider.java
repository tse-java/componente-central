package uy.viruscontrol.business.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import uy.viruscontrol.business.model.*;

@Data
public class DTOResourceProvider {
  private Location address;

  private String commercialName;

  private List<OpenScheduleDay> openSchedule = new ArrayList<>();
}
