package uy.viruscontrol.business.dto;

import java.io.Serializable;
import java.util.Objects;
import lombok.Data;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

@Data
public class DTONotifyMedicalCaseStatus implements Serializable {
  boolean read = false;
  String id;
  String externalId;
  String email;
  String patientName;
  EnumCaseStatus status;
  String disease;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DTONotifyMedicalCaseStatus that = (DTONotifyMedicalCaseStatus) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
