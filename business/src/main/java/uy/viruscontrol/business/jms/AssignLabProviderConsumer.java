package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.cache.LabCache;
import uy.viruscontrol.business.integration.LabIntegrationService;
import uy.viruscontrol.business.service.ExamService;
import uy.viruscontrol.common.dto.DTOExam;
import uy.viruscontrol.common.dto.DTOExamProvider;
import uy.viruscontrol.common.dto.DTOUpdateExam;
import uy.viruscontrol.common.exceptions.VirusControlException;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_assign_lab_provider"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class AssignLabProviderConsumer implements MessageListener {
  @Inject
  private ExamService examService;

  @Inject
  private MessageProducer messageProducer;

  @EJB
  private LabIntegrationService labIntegration;

  @EJB
  private LabCache labCache;

  private DTOExam dtoExam = null;

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      dtoExam = (DTOExam) objectMessage.getObject();
      DTOExamProvider examProvider = labCache.getRandomLab();
      if (examProvider != null) {
        DTOUpdateExam dtoUpdateExam = new DTOUpdateExam();
        dtoUpdateExam.setId(dtoExam.getId());
        labIntegration.requestLab(dtoUpdateExam, examProvider);
      } else {
        log.error("[JMS] No hay provedores de examenes?");
        messageProducer.requestLabProvider(dtoExam);
      }
    } catch (NullPointerException e) {
      log.error("[JMS] error: {}", e.getMessage());
    } catch (JMSException e) {
      messageProducer.requestLabProvider(dtoExam);
      log.error("[JMS] error: {}", e.getMessage());
    } catch (VirusControlException virusControlException) {
      messageProducer.requestLabProvider(dtoExam);
      log.error("[JMS] Re queue exma.  error: {}", virusControlException.getMessage());
    }
  }
}
