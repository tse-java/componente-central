package uy.viruscontrol.business.jms;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.jms.*;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.dto.DTONotifyContagion;
import uy.viruscontrol.business.dto.DTONotifyExam;
import uy.viruscontrol.business.dto.DTONotifyMedicalAppointment;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.common.dto.*;

@Singleton
@Slf4j
public class MessageProducer {
  JMSContext jmsContext;
  JMSProducer jmsProducer;

  @Resource
  private ConnectionFactory connectionFactory;

  @Resource(lookup = "java:/queue/queue_assign_lab_provider")
  private Queue assingLabProviderQueue;

  @Resource(lookup = "java:/queue/queue_report_contagion")
  private Queue contagionQueue;

  @Resource(lookup = "java:/queue/queue_new_chat_message")
  private Queue messageQueue;

  @Resource(lookup = "java:/queue/queue_report_location")
  private Queue locationQueue;

  @Resource(lookup = "java:/queue/queue_notify_resource_availability")
  private Queue notifyResourceAvailability;

  @Resource(lookup = "java:/queue/queue_notify_contagion")
  private Queue notifyContagion;

  @Resource(lookup = "java:/queue/queue_notify_exam_status")
  private Queue notifyExamStatus;

  @Resource(lookup = "java:/queue/queue_notify_medic_assignment")
  private Queue notifyMedicAssigment;

  @Resource(lookup = "java:/queue/queue_new_medical_appointment")
  private Queue medicalAppointmentQueue;

  @PostConstruct
  public void start() {
    jmsContext = connectionFactory.createContext();
    jmsProducer = jmsContext.createProducer();
  }

  public void notifyExamStatus(DTONotifyExam dtoNotifyExam) {
    jmsProducer.send(notifyExamStatus, dtoNotifyExam);
  }

  public void notifyContagion(DTONotifyContagion dtoNotifyContagion) {
    jmsProducer.send(notifyContagion, dtoNotifyContagion);
  }

  public void notifyResourceAvailability(DTOResourceNotification dtoResourceNotification) {
    jmsProducer.send(notifyResourceAvailability, dtoResourceNotification);
  }

  public void notifyMedicAssigment(DTONotifyMedicalAppointment dtoNotifyMedicalAppointment) {
    jmsProducer.send(notifyMedicAssigment, dtoNotifyMedicalAppointment);
  }

  public void reportLocation(DTOLocationReport dtoLocationReport) {
    jmsProducer.send(locationQueue, dtoLocationReport);
  }

  public void sendMessage(DTOMessage message) {
    jmsProducer.send(messageQueue, message);
  }

  public void reportContagion(DTOContagionReport contagionReport) {
    jmsProducer.send(contagionQueue, contagionReport);
  }

  public void requestLabProvider(DTOExam dtoExam) {
    jmsProducer.send(assingLabProviderQueue, dtoExam);
  }

  public void requestMedicalAppointment(DTORequestMedicalAppointment medicalAppointment) {
    try {
      ObjectMessage objectMessage = jmsContext.createObjectMessage();
      objectMessage.setObject(medicalAppointment);
      objectMessage.setLongProperty("AMQ_SCHEDULED_DELAY", 5000);
      jmsProducer.send(medicalAppointmentQueue, objectMessage);
    } catch (JMSException e) {
      log.error("Error al crear la peticion medica: {}", e.getMessage());
    }
  }
}
