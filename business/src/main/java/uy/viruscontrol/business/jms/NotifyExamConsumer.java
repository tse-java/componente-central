package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.dto.DTONotifyExam;
import uy.viruscontrol.business.integration.FirebaseIntegrationService;
import uy.viruscontrol.business.integration.MailIntegrationService;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_notify_exam_status"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class NotifyExamConsumer implements MessageListener {
  @Inject
  private FirebaseIntegrationService firebaseIntegrationService;

  @Inject
  private MailIntegrationService mailIntegration;

  public NotifyExamConsumer() {}

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      DTONotifyExam dtoNotifyExam = (DTONotifyExam) objectMessage.getObject();
      if (dtoNotifyExam.getExam().getMedicalCase().getPatient().isMailNotify()) {
        mailIntegration.notifyExam(dtoNotifyExam.getExam());
      }
      firebaseIntegrationService.notifyExam(dtoNotifyExam.getExam());
    } catch (JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    }
  }
}
