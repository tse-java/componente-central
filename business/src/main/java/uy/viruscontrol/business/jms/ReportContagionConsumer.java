package uy.viruscontrol.business.jms;

import java.util.List;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.cache.UserDiseaseCache;
import uy.viruscontrol.business.dto.DTOMedicalCaseStatus;
import uy.viruscontrol.business.dto.DTONotifyContagion;
import uy.viruscontrol.business.service.CaseService;
import uy.viruscontrol.business.service.FrontProfileService;
import uy.viruscontrol.business.service.LocationService;
import uy.viruscontrol.common.dto.DTOContagionReport;
import uy.viruscontrol.common.dto.DTOLocationReport;
import uy.viruscontrol.common.dto.DTOMedicalCase;
import uy.viruscontrol.common.enumerations.EnumCaseStatus;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "10"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_report_contagion"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class ReportContagionConsumer implements MessageListener {
  @EJB
  private FrontProfileService frontProfileService;

  @EJB
  private LocationService locationService;

  @EJB
  private CaseService caseService;

  @Inject
  private UserDiseaseCache userDiaseaseCache;

  @Inject
  private MessageProducer messageProducer;

  @Inject
  private Event<DTOMedicalCaseStatus> notifyCase;

  public ReportContagionConsumer() {}

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      DTOContagionReport dtoContagionReport = (DTOContagionReport) objectMessage.getObject();
      log.info("[JMS] Contagion recived: {}", dtoContagionReport.getDisease());
      List<DTOLocationReport> locationReports = locationService.findContagionByLocation(dtoContagionReport);
      for (DTOLocationReport locationReport : locationReports) {
        long userId = locationReport.getUserId();
        long diseaseId = dtoContagionReport.getDisease().getId();
        if (!userDiaseaseCache.userHasCase(userId, diseaseId) && !frontProfileService.hasAnActiveCase(userId, diseaseId)) {
          DTOMedicalCase dtoMedicalCase = caseService.newCase(userId, diseaseId, EnumCaseStatus.SUSPECT);
          DTONotifyContagion notifyContagion = new DTONotifyContagion();
          notifyContagion.setMedicalCase(dtoMedicalCase);
          messageProducer.notifyContagion(notifyContagion);
          userDiaseaseCache.storeDisease(userId, diseaseId);
          {
            DTOMedicalCaseStatus dto = new DTOMedicalCaseStatus();
            dto.setPatient(dtoMedicalCase.getPatient());
            dto.setDisease(dtoMedicalCase.getDisease().getName());
            dto.setStatus(dtoMedicalCase.getStatus());
            dto.setStatus(EnumCaseStatus.SUSPECT);
            notifyCase.fireAsync(dto);
          }
        }
      }
    } catch (JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    }
  }
}
