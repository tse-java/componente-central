package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.dto.DTORequestMedicalAppointment;
import uy.viruscontrol.business.service.MedicalAppointmentService;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_new_medical_appointment"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class MedicalAppointmentConsumer implements MessageListener {
  @EJB
  private MedicalAppointmentService appointmentService;

  public MedicalAppointmentConsumer() {}

  @Override
  public void onMessage(Message message) {
    DTORequestMedicalAppointment dtoRequestMedicalAppointment;
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      dtoRequestMedicalAppointment = (DTORequestMedicalAppointment) objectMessage.getObject();
      appointmentService.findAppointment(dtoRequestMedicalAppointment);
    } catch (JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    }
  }
}
