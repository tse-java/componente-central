package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.dto.DTONotifyMedicalAppointment;
import uy.viruscontrol.business.integration.FirebaseIntegrationService;
import uy.viruscontrol.business.integration.MailIntegrationService;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_notify_medic_assignment"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class NotifyMedicalAppointmentConsumer implements MessageListener {
  @Inject
  private FirebaseIntegrationService firebaseIntegrationService;

  @Inject
  private MailIntegrationService mailIntegration;

  public NotifyMedicalAppointmentConsumer() {}

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      DTONotifyMedicalAppointment dtoNotifyMedicalAppointment = (DTONotifyMedicalAppointment) objectMessage.getObject();
      if (dtoNotifyMedicalAppointment.getDtoMedicalAppointment().getPatient().isMailNotify()) {
        mailIntegration.notifyMedicAssignment(dtoNotifyMedicalAppointment.getDtoMedicalAppointment());
      }
      firebaseIntegrationService.notifyMedicAssignment(dtoNotifyMedicalAppointment.getDtoMedicalAppointment());
    } catch (JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    }
  }
}
