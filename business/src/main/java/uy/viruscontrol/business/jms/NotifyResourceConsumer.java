package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.integration.FirebaseIntegrationService;
import uy.viruscontrol.business.integration.MailIntegrationService;
import uy.viruscontrol.common.dto.*;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_notify_resource_availability"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class NotifyResourceConsumer implements MessageListener {
  @Inject
  private MessageProducer messageProducer;

  @Inject
  MailIntegrationService mailIntegrationService;

  @Inject
  FirebaseIntegrationService firebaseIntegrationService;

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      DTOResourceNotification dtoResourceNotification = (DTOResourceNotification) objectMessage.getObject();
      if (dtoResourceNotification.getFrontProfile().isMailNotify()) {
        mailIntegrationService.notifyResource(dtoResourceNotification);
      }
      if (dtoResourceNotification.getFrontProfile().isPushNotify()) {
        firebaseIntegrationService.notifyResource(dtoResourceNotification);
      }
    } catch (NullPointerException | JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    }
  }
}
