package uy.viruscontrol.business.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import lombok.extern.slf4j.Slf4j;
import uy.viruscontrol.business.service.LocationService;
import uy.viruscontrol.common.dto.DTOLocationReport;

@MessageDriven(
  activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/queue/queue_report_location"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
  }
)
@Slf4j
public class ReportLocationConsumer implements MessageListener {
  @EJB
  private LocationService locationService;

  public ReportLocationConsumer() {}

  @Override
  public void onMessage(Message message) {
    try {
      ObjectMessage objectMessage = (ObjectMessage) message;
      log.debug("%%%%%%%%%%%%%%%%%%%%%%%%%");
      log.debug("[JMS] Location recived: {}", objectMessage);
      DTOLocationReport dtoLocationReport = (DTOLocationReport) objectMessage.getObject();
      log.debug("[JMS] Message dto pre persist: {}", dtoLocationReport);
      locationService.newReport(dtoLocationReport);
    } catch (JMSException e) {
      log.error("[JMS] error: {}", e.getMessage());
    } finally {
      log.debug("##########################");
    }
  }
}
