--
-- Data for Name: backprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (21, 'Walter White', 'wwhite', '$2a$12$edkdHiBQZk9dQdL1OCXZtONEcrvfc7vhvbkRvktpSs6nV9CAD1nHq', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (22, 'Gustavo Fernandez', 'mueller', '$2a$12$XrpbiShSliWffslRM.cj2OJDGpKkKjup1Yss3XIbw0.lRauEuGKLa', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (23, 'Jose Dominguez', 'kling', '$2a$12$B5CsNcrUwhS3X4J0r96jze8Ze.WgCYvDtsOIndrk.2h8Wa6XlYMWW', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (24, 'Martin Ferreira', 'becker', '$2a$12$9pxgh/.Ed3VdvNdiGvt0GOwHj79b.1Oqa7gYOBZYLAIQLDzxCa1MK', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (25, 'Thiago Vasquez', 'pouros', '$2a$12$gzlJdD3HFzihgvIfESRHGeRWhgqfYlBJrRZMluz37ekeC90a.2oMi', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (26, 'Pablo Gutierrez', 'gleichner', '$2a$12$VyAAEx0gf7mLCMoxWTFCs.H9x9udkgg0n7IpaERfRwCv2aSYfLWte', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (27, 'Ramiro Arias', 'barton', '$2a$12$nkWEkcHzZCljv8XobxySvOcrTn92oNOYRIq.crRxOt0tvvtHBRC42', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (28, 'Walter Rodriguez', 'thiel', '$2a$12$Et0iyXtsF1Lj8p8GrO61O.IU4xY9SeqwQjrQlqAhzqYaIgP7E1hKe', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (29, 'Lucia Weber', 'weber', '$2a$12$KLD8pW8WatYDnAu7R.uYu.dUmtAPeQS2GqvuPTEbM8SVK3Hz48ble', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (30, 'Julieta Ortiz', 'ortiz', '$2a$12$jEjFn/uTbNbuG/RD8w9ElOzNgTaRzu/54sBFxdVqKlVKaB7FwzkiW', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (31, 'Enrique Herrera', 'sauer', '$2a$12$abqH3xRNCwYUve6UDUvgg.MJYozfMtwq8Lg0HoA1RJ1z0Y73bTg2S', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (32, 'RAUL MONTEIRO BUSQUETS', 'blanda', '$2a$12$zppI57igfB05hk6hZvg5zuTk2zHlzCjJOgQzhKqfxjIWCEBwFrH9m', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (33, 'VERONICA SANTANDER', 'wuckert', '$2a$12$HZ7VP1oE19yYaoZD5oJEVegAdQ47rn3.hlVRoqF2kPvTJGG5u6T4O', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (34, 'IRENE CARBONELL ', 'waters', '$2a$12$bWKsR0/dnG6vvNOh53dmlu3ltyzDhn7rD/CWNDBFw7YUWEhbphGim', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (35, 'TOMAS SABATE', 'blanda', '$2a$12$d/ncJudr5gdjRncuMxS8Z.umMbMkXRYgMySLeotesY4./l5fBqVhO', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (36, 'FRANCISCO FERRO ', 'd''amore', '$2a$12$7hPrczEbJXA6xtXaHfGszu01VltCcLIjJRqACEKHKeAWRVtGFirqe', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (37, 'Isabel Montilla', 'schneider', '$2a$12$0Yns.DQ2koQ1vUmu2Uy5XutntXvEzNy8J.9k1QtMQ5lGh/pT.4iem', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (38, 'Daniel Hernandez', 'upton', '$2a$12$fKkHAocFifHYOi099M10duz/DJsxuhYXO697TGGEjWqT.tjQ56012', 0);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (39, 'Alex Lucas', 'donnelly', '$2a$12$2TB905sDff5CvJ7zbiBwu.mOUgqgYVzKtkxv5mG3s5.7hoZfkqaJ6', 1);
INSERT INTO public.backprofile (id, fullname, nickname, password, role) VALUES (40, 'Elisabet Paredes', 'corkery', '$2a$12$0wzG06vIWEEIZJwy8GWgne.ccNq0WBTH.rX/P0NRFywI/qicroHFK', 0);


--
-- Data for Name: disease; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.disease (id, agentname, agenttype, infectiontime, name, requeststatus) VALUES (1, 'COVID-19', 0, 5, 'Coronavirus', 1);
INSERT INTO public.disease (id, agentname, agenttype, infectiontime, name, requeststatus) VALUES (2, 'H1N1', 6, 5, 'Gripe H1N1', 0);
INSERT INTO public.disease (id, agentname, agenttype, infectiontime, name, requeststatus) VALUES (3, 'DEN1/DEN2/DEN3/DEN4', 6, 5, 'Dengue', 0);
INSERT INTO public.disease (id, agentname, agenttype, infectiontime, name, requeststatus) VALUES (4, 'Germen', 4, 5, 'Tuberculosis', 0);


--
-- Data for Name: resource; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.resource (id, description, name, type) VALUES (1, 'Pañuelos descartables', 'Pañuelos', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (2, 'Mascarilla N95', 'Mascarillas', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (3, 'Kill it before it lays eggs', 'Lanzallamas', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (4, 'Alcohol en gel', 'Alcohol en gel', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (5, 'Alcohol Isopropilico', 'Alcohol Isopropilico', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (6, 'Antibiotico de amplio espectro', 'Amoxidal', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (7, 'Baja la fiebre', 'Paracetamol', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (8, 'Termometro', 'Termometro', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (9, 'Guantes de latex', 'Guantes de latex', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (10, 'Calmante', 'Ibuprofeno', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (11, 'Relajante muscular', 'Diclofenac', 1);
INSERT INTO public.resource (id, description, name, type) VALUES (12, 'Para superficies', 'Desinfectante', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (13, 'Termometro laser', 'Termometro laser', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (14, 'Mascara de Acrilico', 'Mascara de Acrilico', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (15, 'Jabon en barra', 'Jabon en barra', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (16, 'Lentes de Proteccion', 'Lentes de Proteccion', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (17, 'Alfombra desinfectante', 'Alfombra desinfectante', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (18, 'Jabon Liquido', 'Jabon Liquido', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (19, 'Toallas desinfectantes', 'Toallas desinfectantes', 0);
INSERT INTO public.resource (id, description, name, type) VALUES (20, 'Tanque de oxigeno', 'Tanque de oxigeno', 1);


--
-- Data for Name: disease_resource; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (1, 17);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (1, 3);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (1, 7);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (1, 13);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (1, 16);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (2, 18);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (2, 19);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (2, 4);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (2, 12);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (3, 2);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (3, 19);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (3, 11);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (4, 1);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (4, 3);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (4, 4);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (4, 11);
INSERT INTO public.disease_resource (diseases_id, recommendedresources_id) VALUES (4, 12);


--
-- Data for Name: symptom; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.symptom (id, description, name) VALUES (1, 'Espectoraciones', 'Tos');
INSERT INTO public.symptom (id, description, name) VALUES (2, 'Dolor de Garganta', 'Dolor de Garganta');
INSERT INTO public.symptom (id, description, name) VALUES (3, 'Temperatura mayor a 37,5 grados', 'Fiebre');
INSERT INTO public.symptom (id, description, name) VALUES (4, 'Dificultad para respirar', 'Dificultad para respirar');
INSERT INTO public.symptom (id, description, name) VALUES (5, 'Dolor de Muscular', 'Dolor de Muscular');


--
-- Data for Name: disease_symptom; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (1, 1);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (1, 3);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (2, 2);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (2, 4);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (3, 2);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (3, 3);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (4, 2);
INSERT INTO public.disease_symptom (disease_id, symptoms_id) VALUES (4, 3);


--
-- Data for Name: peripheralnode; Type: TABLE DATA; Schema: public; Owner: postgres
--


INSERT INTO public.peripheralnode (id, name, url) VALUES (1, 'Laboratiorio 1', 'https://lab-provider-1-viruscontrol2.apps.ca-central-1.starter.openshift-online.com/api/');
INSERT INTO public.peripheralnode (id, name, url) VALUES (2, 'Mutualista 1', 'https://componente-periferico-salud-1-viruscontrol2020.apps.ca-central-1.starter.openshift-online.com/healthProvider?wsdl');
INSERT INTO public.peripheralnode (id, name, url) VALUES (3, 'Mutualista 2', 'https://componente-periferico-salud-2-viruscontrol2020.apps.ca-central-1.starter.openshift-online.com/healthProvider?wsdl');
INSERT INTO public.peripheralnode (id, name, url) VALUES (4, 'Farmacia 1', 'https://resource-provider-1-viruscontrol.apps.ca-central-1.starter.openshift-online.com/api');
INSERT INTO public.peripheralnode (id, name, url) VALUES (5, 'Farmacia 2', 'https://resource-provider-2-viruscontrol.apps.ca-central-1.starter.openshift-online.com/api');


--
-- Data for Name: examprovider; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.examprovider (token, id, isactive) VALUES ('276ZJV6Glyf5ia0m8N0-NnvJRAdRh7Te', 1, true);


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (2, 'Nueva Helvecia', 'ODRhfGEfX', 4, '18 de Julio 1086', 'Nueva Helvecia');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (3, 'Santa Lucia', 'NBaxYoMSiMNxLXFLxIok', 2, 'Av. Artigas 349', 'Santa Lucia');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (4, 'Paso de los Toros', 'WtKhlfZzDZ', 18, 'Calle Principal 123', 'Paso de los Toros');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (9, 'Tranqueras', 'wTiYlONPbytccZevAHtGXfY', 13, 'Calle Falsa s/n', 'Tranqueras');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (1, 'Canelones', 'FBaYZvbJSRsxeDp', 2, 'Jose Enrique Rodo 451', 'Canelones');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (5, 'Salto', 'cwOMAnizMjcHvTdAqEeTahiGze', 15, 'Lavalleja 390', 'Salto');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (6, 'Quebracho', 'sJpgFePnOyns', 11, 'Calle Principal s/n', 'Quebracho');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (7, 'La Paloma', 'JWekLYCilwsmSglpMXbLsYY', 14, 'Av. del Faro s/n', 'La Paloma');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (8, 'Montevideo', 'XdgmWNbwLUXUeMm', 10, 'Magallanes 1582', 'Centro');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (10, 'Montevideo', NULL, 9, 'Av. Millan 2515', 'Reducto');
INSERT INTO public.location (id, city, coordinates, department, street, town) VALUES (11, 'Canelones', NULL, 2, 'Calle falsa 123', 'Santa Lucia');


--
-- Data for Name: frontprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (1, '2024-06-18 18:37:27.997', 'yedUsFwdkelQbxeTeQOvaScfqIOOmaa', 'jacob.hansen@example.com', 'eOMtThyhVNLWUZNRcBaQKxI', 'Lyndon', true, false, 'Friesen', 'JxkyvRnL', 'VLhpfQGTMDYpsBZxvfBoeygjb', 'RYtGKbgicZaHCBRQDSx', '2029-10-26 12:23:07.734', 1);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (2, '2015-01-25 07:36:02.024', 'Oxm', 'ashanti.mclaughlin@example.com', 'sPB', 'Helen', true, false, 'Frami', 'QIn', 'AAAryjCRhLTuhnTodUewZQqaZErU', 'yIvpRgmgQsYEKk', '2022-03-06 06:29:23.046', 2);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (3, '2026-02-12 20:50:56.683', 'OV', 'burl.smitham@example.com', 'gEYzhlSTPkcSUyTSbBkU', 'Myrtis', false, true, 'Ryan', 'dwprLNbgNvZUMpqAXLJbjE', 'Wru', 'TiQaUNWftniOAotnzWvkQt', '2027-07-05 21:14:41.723', 3);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (4, '2024-04-27 17:20:27.778', 'O', 'fernando.wolf@example.com', 'M', 'Becky', true, false, 'Hackett', 'yybxVLsNlAeLWVhnIULZAyLBms', 'jzlglRKAeamYUmWJtnJZLqwakeYcea', 'LcdeWTdXPlQgjMVXbpRYzBT', '2011-06-10 07:33:51.482', 4);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (5, '2025-12-04 16:16:11.437', 'ExoLZAnxWro', 'hung.rosenbaum@example.com', 'yLUuEMaNwExpndMYYptSXZv', 'Brenton', true, true, 'Schmeler', 'zQeAydAJIwkGuKWiVZLRcElhreuxvw', 'etgDUdmb', 'YVeSelxWdEtYtJHV', '2021-07-29 12:13:55.818', 5);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (6, '2028-02-29 20:29:29.388', 'MedmIqFUtdxTB', 'leandro.gaylord@example.com', 'aNVLssRUe', 'Gale', true, true, 'Kozey', 'RRVabC', 'IQrWtYnHcO', 'DwmrfUyJVSbdBFIyjTfpDItaXB', '2012-12-23 19:51:10.334', 6);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (7, '2029-07-03 23:31:39.414', 'BXjwiQjQKBhPbNYa', 'trent.schumm@example.com', 'FkG', 'Jesus', true, false, 'Runte', 'oDLDPR', 'ImfitPVWqHXHNeZIUAYI', 'JuhuUTlSCYdTVoSsJGyClHNnCWZGwZ', '2011-04-25 03:21:41.98', 5);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (8, '2011-05-23 18:56:40.839', 'ZfevGGIyInuRGUCg', 'caitlin.morar@example.com', 'qPFJOkHIpWuqUBpPiSGKutNzPuBDe', 'Spencer', false, true, 'Bahringer', 'GcGUaENkutTuUTC', 'VaNvgPNPNdoDlMXoAwSuSKKPcTH', 'nBJbTUuAgHABZTPBC', '2011-10-25 22:46:58.405', 6);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (9, '2018-06-28 16:55:52.486', 'OeNujiqMhRMxZqYNVITQFLfat', 'robbi.wintheiser@example.com', 'PSxwMDkpVdpPPmMkvQzMPMzmv', 'Hung', true, false, 'O''Keefe', 'DljaRmBHVP', 'TqAeeGGOsayeZaiid', 'K', '2027-06-28 10:06:44.229', 6);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (10, '2021-07-03 02:47:04.409', 'TRibibvfrV', 'will.casper@example.com', 'oMDBOXQZoTV', 'Vernia', true, false, 'Lehner', 'UEZ', 'MFOntGwQ', 'vXnsKKeWRvXUOo', '2017-12-06 09:13:39.644', 7);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (11, '2015-03-03 00:05:16.597', 'AFCgCUKZOnRWjfYXGfQa', 'sima.waelchi@example.com', 'TFWc', 'Aron', true, true, 'Schmidt', 'eulM', 'DqpvBvXwUntedYgKyfsYslKCiAqRV', 'ShbjkpWHaCgIH', '2013-01-21 03:40:22.869', 7);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (12, '2015-03-18 09:50:09.017', 'blYrsQFiCOxvKWEjrAsPDzKxhf', 'nery.dickens@example.com', 'rNdKIneNxIvWnrOaoRCo', 'Kendall', false, false, 'Hoppe', 'mjLgOGYauEgmMmxBzA', 'KeaQbsnmRMhHfErVCEkcH', 'LR', '2019-03-27 13:32:26.474', 8);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (13, '2018-10-13 14:35:22.912', 'EMePviztmdfqpvNrRqHVbFAphbnBMTI', 'richard.braun@example.com', 'yaVUnOnIbTeygCJPTWqGbAIxv', 'Latesha', true, false, 'Howe', 'ROL', 'Pomfs', 'ytPfLdSDzTAVPwbGXOu', '2014-03-25 06:48:43.141', 8);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (14, '2022-04-14 19:17:13.363', 'KgZIFWDnwypaEupdRXDoXqDqJYSu', 'cary.kling@example.com', 'QpVRFpRXlWkaZgdLUFlWyKvdrHf', 'Michel', false, false, 'Hirthe', 'EuOxsDtMWutbmjqfypdBQigZk', 'T', 'VfaVtB', '2010-09-02 08:52:28.272', 7);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (15, '2023-10-12 08:57:24.753', 'WXBjxEYAukF', 'alva.cummings@example.com', 'vGOosmixlhAkVJqloTKfzouiPpa', 'Eliseo', false, false, 'Schamberger', 'KNbgNP', 'OcAqemov', 'kMqmUbi', '2023-09-07 10:20:24.686', 6);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (16, '2011-07-04 17:28:13.788', 'vMfRBFBQTCAXujDhGGHwUpKLx', 'tianna.ernser@example.com', 'LsBVdlMNlSsOFvnyYG', 'Marlon', false, false, 'Heathcote', 'nEOxJtAzCNYJESqYyrownyle', 'rsiAhLzZQryGvbioU', 'tAnEMeXnRAVNUSkRy', '2028-12-08 09:55:14.444', 9);

INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, mailnotify, nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (17,'2004-06-13 21:18:59.728','12312312','chacrariverau@gmail.com','116984182089642924171','Susana',false,true,'Leuschke',true,'Uruguay','098079806',true,'524f418c-41f6-45f6-8c9d-6ff16c8daa93','2020-06-25 19:06:41.834',10) ;
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (18, '2023-07-13 01:51:49.404', 'Esta es tu CI: 3', 'mathiaszunino@gmail.com', '116052536051863409762', 'Mathias', true, true, 'Zunino', '35456356', 'pEVnNlLNEgy', 'lBwZlNVKwsmgT', '2022-07-30 05:58:06.238', 5);
INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (20, '2013-10-13 14:09:27.355', 'Esta es tu CI: 2', 'ramiroalves999@gmail.com', '104278475872083579949', 'Ramiro', true, true, 'Alves', 'XeD', '4564345', 'ZLbuxf', '2022-07-15 03:51:17.748', 11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (21,'2017-06-20 23:02:33.716','51823522','mateosayaspereira@gmail.com','110302053278996441593','Timika',false,true,'Streich',true,'Peru','1039392929',true,'4ffaf01b-5d41-4146-8aae-af30700c6b22','2020-06-25 22:33:47.121',11);

INSERT INTO public.frontprofile (id, birthday, documentnumber, email, externalid, firstname, isdoctor, isfinished, lastname, nationality, phonenumber, refreshtoken, refreshtokenexpire, address_id) VALUES (19, '2021-06-30 04:42:07.457', 'Esta es tu CI: 1', 'alguien845@gmail.com', '105059637064936111147', 'Mariano', true, true, 'Zunino', '49746164', '098914497', 'XAEUPNrvhXOvldCUDmTQTFQYhtRqbtZ', '2022-10-05 02:34:59.097', 10);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (22,'1977-06-02 14:03:55.597','49746165','vc.paciente@gmail.com','117165910937015961027','Madaline',false,true,'Lubowitz',true,'Uruguay','43328766',true,'858487fa-79b1-4faf-80f5-f2275b83a078','2020-07-03 17:35:57.904',1);

INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (50,'2017-06-20 23:02:33.716','00000050','cuenta-50@viruscontrol.tech','00000050','Usuario-00000050',false,true,'Streich',true,'Peru','1039392929',true,'token-00000050','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (51,'2017-06-20 23:02:33.716','00000051','cuenta-51@viruscontrol.tech','00000051','Usuario-00000051',false,true,'Streich',true,'Peru','1039392929',true,'token-00000051','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (52,'2017-06-20 23:02:33.716','00000052','cuenta-52@viruscontrol.tech','00000052','Usuario-00000052',false,true,'Streich',true,'Peru','1039392929',true,'token-00000052','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (53,'2017-06-20 23:02:33.716','00000053','cuenta-53@viruscontrol.tech','00000053','Usuario-00000053',false,true,'Streich',true,'Peru','1039392929',true,'token-00000053','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (54,'2017-06-20 23:02:33.716','00000054','cuenta-54@viruscontrol.tech','00000054','Usuario-00000054',false,true,'Streich',true,'Peru','1039392929',true,'token-00000054','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (55,'2017-06-20 23:02:33.716','00000055','cuenta-55@viruscontrol.tech','00000055','Usuario-00000055',false,true,'Streich',true,'Peru','1039392929',true,'token-00000055','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (56,'2017-06-20 23:02:33.716','00000056','cuenta-56@viruscontrol.tech','00000056','Usuario-00000056',false,true,'Streich',true,'Peru','1039392929',true,'token-00000056','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (57,'2017-06-20 23:02:33.716','00000057','cuenta-57@viruscontrol.tech','00000057','Usuario-00000057',false,true,'Streich',true,'Peru','1039392929',true,'token-00000057','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (58,'2017-06-20 23:02:33.716','00000058','cuenta-58@viruscontrol.tech','00000058','Usuario-00000058',false,true,'Streich',true,'Peru','1039392929',true,'token-00000058','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (59,'2017-06-20 23:02:33.716','00000059','cuenta-59@viruscontrol.tech','00000059','Usuario-00000059',false,true,'Streich',true,'Peru','1039392929',true,'token-00000059','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (60,'2017-06-20 23:02:33.716','00000060','cuenta-60@viruscontrol.tech','00000060','Usuario-00000060',false,true,'Streich',true,'Peru','1039392929',true,'token-00000060','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (61,'2017-06-20 23:02:33.716','00000061','cuenta-61@viruscontrol.tech','00000061','Usuario-00000061',false,true,'Streich',true,'Peru','1039392929',true,'token-00000061','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (62,'2017-06-20 23:02:33.716','00000062','cuenta-62@viruscontrol.tech','00000062','Usuario-00000062',false,true,'Streich',true,'Peru','1039392929',true,'token-00000062','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (63,'2017-06-20 23:02:33.716','00000063','cuenta-63@viruscontrol.tech','00000063','Usuario-00000063',false,true,'Streich',true,'Peru','1039392929',true,'token-00000063','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (64,'2017-06-20 23:02:33.716','00000064','cuenta-64@viruscontrol.tech','00000064','Usuario-00000064',false,true,'Streich',true,'Peru','1039392929',true,'token-00000064','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (65,'2017-06-20 23:02:33.716','00000065','cuenta-65@viruscontrol.tech','00000065','Usuario-00000065',false,true,'Streich',true,'Peru','1039392929',true,'token-00000065','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (66,'2017-06-20 23:02:33.716','00000066','cuenta-66@viruscontrol.tech','00000066','Usuario-00000066',false,true,'Streich',true,'Peru','1039392929',true,'token-00000066','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (67,'2017-06-20 23:02:33.716','00000067','cuenta-67@viruscontrol.tech','00000067','Usuario-00000067',false,true,'Streich',true,'Peru','1039392929',true,'token-00000067','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (68,'2017-06-20 23:02:33.716','00000068','cuenta-68@viruscontrol.tech','00000068','Usuario-00000068',false,true,'Streich',true,'Peru','1039392929',true,'token-00000068','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (69,'2017-06-20 23:02:33.716','00000069','cuenta-69@viruscontrol.tech','00000069','Usuario-00000069',false,true,'Streich',true,'Peru','1039392929',true,'token-00000069','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (70,'2017-06-20 23:02:33.716','00000070','cuenta-70@viruscontrol.tech','00000070','Usuario-00000070',false,true,'Streich',true,'Peru','1039392929',true,'token-00000070','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (71,'2017-06-20 23:02:33.716','00000071','cuenta-71@viruscontrol.tech','00000071','Usuario-00000071',false,true,'Streich',true,'Peru','1039392929',true,'token-00000071','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (72,'2017-06-20 23:02:33.716','00000072','cuenta-72@viruscontrol.tech','00000072','Usuario-00000072',false,true,'Streich',true,'Peru','1039392929',true,'token-00000072','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (73,'2017-06-20 23:02:33.716','00000073','cuenta-73@viruscontrol.tech','00000073','Usuario-00000073',false,true,'Streich',true,'Peru','1039392929',true,'token-00000073','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (74,'2017-06-20 23:02:33.716','00000074','cuenta-74@viruscontrol.tech','00000074','Usuario-00000074',false,true,'Streich',true,'Peru','1039392929',true,'token-00000074','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (75,'2017-06-20 23:02:33.716','00000075','cuenta-75@viruscontrol.tech','00000075','Usuario-00000075',false,true,'Streich',true,'Peru','1039392929',true,'token-00000075','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (76,'2017-06-20 23:02:33.716','00000076','cuenta-76@viruscontrol.tech','00000076','Usuario-00000076',false,true,'Streich',true,'Peru','1039392929',true,'token-00000076','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (77,'2017-06-20 23:02:33.716','00000077','cuenta-77@viruscontrol.tech','00000077','Usuario-00000077',false,true,'Streich',true,'Peru','1039392929',true,'token-00000077','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (78,'2017-06-20 23:02:33.716','00000078','cuenta-78@viruscontrol.tech','00000078','Usuario-00000078',false,true,'Streich',true,'Peru','1039392929',true,'token-00000078','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (79,'2017-06-20 23:02:33.716','00000079','cuenta-79@viruscontrol.tech','00000079','Usuario-00000079',false,true,'Streich',true,'Peru','1039392929',true,'token-00000079','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (80,'2017-06-20 23:02:33.716','00000080','cuenta-80@viruscontrol.tech','00000080','Usuario-00000080',false,true,'Streich',true,'Peru','1039392929',true,'token-00000080','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (81,'2017-06-20 23:02:33.716','00000081','cuenta-81@viruscontrol.tech','00000081','Usuario-00000081',false,true,'Streich',true,'Peru','1039392929',true,'token-00000081','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (82,'2017-06-20 23:02:33.716','00000082','cuenta-82@viruscontrol.tech','00000082','Usuario-00000082',false,true,'Streich',true,'Peru','1039392929',true,'token-00000082','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (83,'2017-06-20 23:02:33.716','00000083','cuenta-83@viruscontrol.tech','00000083','Usuario-00000083',false,true,'Streich',true,'Peru','1039392929',true,'token-00000083','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (84,'2017-06-20 23:02:33.716','00000084','cuenta-84@viruscontrol.tech','00000084','Usuario-00000084',false,true,'Streich',true,'Peru','1039392929',true,'token-00000084','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (85,'2017-06-20 23:02:33.716','00000085','cuenta-85@viruscontrol.tech','00000085','Usuario-00000085',false,true,'Streich',true,'Peru','1039392929',true,'token-00000085','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (86,'2017-06-20 23:02:33.716','00000086','cuenta-86@viruscontrol.tech','00000086','Usuario-00000086',false,true,'Streich',true,'Peru','1039392929',true,'token-00000086','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (87,'2017-06-20 23:02:33.716','00000087','cuenta-87@viruscontrol.tech','00000087','Usuario-00000087',false,true,'Streich',true,'Peru','1039392929',true,'token-00000087','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (88,'2017-06-20 23:02:33.716','00000088','cuenta-88@viruscontrol.tech','00000088','Usuario-00000088',false,true,'Streich',true,'Peru','1039392929',true,'token-00000088','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (89,'2017-06-20 23:02:33.716','00000089','cuenta-89@viruscontrol.tech','00000089','Usuario-00000089',false,true,'Streich',true,'Peru','1039392929',true,'token-00000089','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (90,'2017-06-20 23:02:33.716','00000090','cuenta-90@viruscontrol.tech','00000090','Usuario-00000090',false,true,'Streich',true,'Peru','1039392929',true,'token-00000090','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (91,'2017-06-20 23:02:33.716','00000091','cuenta-91@viruscontrol.tech','00000091','Usuario-00000091',false,true,'Streich',true,'Peru','1039392929',true,'token-00000091','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (92,'2017-06-20 23:02:33.716','00000092','cuenta-92@viruscontrol.tech','00000092','Usuario-00000092',false,true,'Streich',true,'Peru','1039392929',true,'token-00000092','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (93,'2017-06-20 23:02:33.716','00000093','cuenta-93@viruscontrol.tech','00000093','Usuario-00000093',false,true,'Streich',true,'Peru','1039392929',true,'token-00000093','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (94,'2017-06-20 23:02:33.716','00000094','cuenta-94@viruscontrol.tech','00000094','Usuario-00000094',false,true,'Streich',true,'Peru','1039392929',true,'token-00000094','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (95,'2017-06-20 23:02:33.716','00000095','cuenta-95@viruscontrol.tech','00000095','Usuario-00000095',false,true,'Streich',true,'Peru','1039392929',true,'token-00000095','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (96,'2017-06-20 23:02:33.716','00000096','cuenta-96@viruscontrol.tech','00000096','Usuario-00000096',false,true,'Streich',true,'Peru','1039392929',true,'token-00000096','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (97,'2017-06-20 23:02:33.716','00000097','cuenta-97@viruscontrol.tech','00000097','Usuario-00000097',false,true,'Streich',true,'Peru','1039392929',true,'token-00000097','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (98,'2017-06-20 23:02:33.716','00000098','cuenta-98@viruscontrol.tech','00000098','Usuario-00000098',false,true,'Streich',true,'Peru','1039392929',true,'token-00000098','2021-06-25 22:33:47.121',11);
INSERT INTO public.frontprofile (id,birthday,documentnumber,email,externalid,firstname,isdoctor,isfinished,lastname,mailnotify,nationality,phonenumber,pushnotify,refreshtoken,refreshtokenexpire,address_id) VALUES (99,'2017-06-20 23:02:33.716','00000099','cuenta-99@viruscontrol.tech','00000099','Usuario-00000099',false,true,'Streich',true,'Peru','1039392929',true,'token-00000099','2021-06-25 22:33:47.121',11);
--
-- Data for Name: medicalcase; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.medicalcase (id,department,status,disease_id,patient_id) VALUES (1,2,0,1,20);
INSERT INTO public.medicalcase (id,department,status,disease_id,patient_id) VALUES (2,1,1,1,1);
INSERT INTO public.medicalcase (id,department,status,disease_id,patient_id) VALUES (3,2,2,1,2);
INSERT INTO public.medicalcase (id,department,status,disease_id,patient_id) VALUES (4,3,3,1,3);

insert into public.medicalcase (id, department, status, disease_id, patient_id) values (5, 8, 1, 1, 5);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (6, 15, 1, 1, 6);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (7, 6, 3, 1, 7);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (8, 2, 2, 1, 8);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (9, 15, 2, 1, 9);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (10, 7, 1, 1, 10);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (11, 1, 2, 1, 11);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (12, 5, 1, 1, 12);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (13, 9, 2, 1, 13);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (14, 9, 2, 1, 14);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (15, 3, 1, 1, 15);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (16, 5, 1, 1, 16);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (17, 14, 2, 1, 17);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (18, 0, 2, 1, 18);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (19, 3, 1, 1, 19);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (20, 18, 1, 1, 50);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (21, 12, 3, 1, 51);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (22, 8, 1, 1, 52);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (23, 11, 1, 1, 53);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (24, 0, 2, 1, 54);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (25, 10, 1, 1, 55);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (26, 3, 1, 1, 56);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (27, 10, 1, 1, 57);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (28, 4, 2, 1, 58);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (29, 15, 1, 1, 59);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (30, 8, 1, 1, 60);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (31, 12, 3, 1, 61);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (32, 1, 3, 1, 62);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (33, 0, 1, 1, 63);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (34, 13, 1, 1, 64);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (35, 5, 3, 1, 65);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (36, 4, 3, 1, 66);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (37, 6, 3, 1, 67);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (38, 14, 0, 1, 68);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (39, 17, 2, 1, 69);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (40, 13, 0, 1, 70);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (41, 14, 1, 1, 71);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (42, 18, 1, 1, 72);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (43, 9, 3, 1, 73);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (44, 14, 0, 1, 74);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (45, 1, 3, 1, 75);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (46, 5, 1, 1, 76);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (47, 8, 2, 1, 77);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (48, 0, 3, 1, 78);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (49, 1, 0, 1, 79);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (50, 11, 1, 1, 80);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (51, 8, 3, 1, 81);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (52, 0, 0, 1, 82);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (53, 10, 1, 1, 83);
insert into public.medicalcase (id, department, status, disease_id, patient_id) values (54, 3, 2, 1, 84);


--
-- Data for Name: exam; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.exam (id,"date",externalid,positive,status,examprovider_id,medicalcase_id,patient_id,requestingdoctor_id) VALUES (1,'2020-06-02 00:00:00.000',NULL,true,2,1,18,18,19);
INSERT INTO public.exam (id,"date",externalid,positive,status,examprovider_id,medicalcase_id,patient_id,requestingdoctor_id) VALUES (2,'2020-06-02 00:00:00.000',NULL,false,1,1,52,82,19);
INSERT INTO public.exam (id,"date",externalid,positive,status,examprovider_id,medicalcase_id,patient_id,requestingdoctor_id) VALUES (3,'2020-06-02 00:00:00.000',NULL,false,1,1,53,83,19);
INSERT INTO public.exam (id,"date",externalid,positive,status,examprovider_id,medicalcase_id,patient_id,requestingdoctor_id) VALUES (4,'2020-06-02 00:00:00.000',NULL,false,1,1,49,79,19);
-- INSERT INTO public.exam (id,"date",externalid,positive,status,examprovider_id,medicalcase_id,patient_id,requestingdoctor_id) VALUES (5,'2020-06-02 00:00:00.000',NULL,false,1,1,18,18,19);

--
-- Data for Name: healthcareprovider; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.healthcareprovider (id) VALUES (2);
INSERT INTO public.healthcareprovider (id) VALUES (3);


--
-- Data for Name: lastrequeststatus; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: informationsource; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.informationsource (type, id, description, name, status, username, url, lastrequeststatus_id) VALUES ('rss', 1, 'El Observador', 'El Observador - seccion Salud', 2, NULL, 'https://www.elobservador.com.uy/rss/elobservador/salud.xml', NULL);
INSERT INTO public.informationsource (type, id, description, name, status, username, url, lastrequeststatus_id) VALUES ('rss', 2, 'La diaria', 'La Diaria', 2, NULL, 'https://ladiaria.com.uy/feeds/articulos/', NULL);
INSERT INTO public.informationsource (type, id, description, name, status, username, url, lastrequeststatus_id) VALUES ('tw', 3, 'Sistema Nacional de Emergencias', 'Sinae', 2, 'sinae_oficial', NULL, NULL);

INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'covid');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'coronavirus');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'pandemia');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'confinamiento');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'cuarentena');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'virus');

INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'covid');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'coronavirus');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'pandemia');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'confinamiento');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'cuarentena');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (2, 'virus');


--
-- Data for Name: informationsourcetype; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.informationsourcetype (id, active, description, name) VALUES (1, true, 'Consumidor de RSS Feed ingresando una url. Con filtro de palabras clave (OR), si esta vacio no se filtra ', 'RSS Feed');
INSERT INTO public.informationsourcetype (id, active, description, name) VALUES (2, true, 'Consumidor de twits por nombre de Usuario', 'Twitter');


--
-- Data for Name: locationreport; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: medicalappointment; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.medicalappointment (id,datetime,externalid,requeststatus,status,doctor_id,healthcareprovider_id,location_id,patient_id) VALUES (1,'2021-05-28 14:41:29.102',NULL,1,0,19,2,7,20) ;
INSERT INTO public.medicalappointment (id,datetime,externalid,requeststatus,status,doctor_id,healthcareprovider_id,location_id,patient_id) VALUES (2,'2021-05-29 14:41:29.102',NULL,1,0,19,2,7,8) ;
INSERT INTO public.medicalappointment (id,datetime,externalid,requeststatus,status,doctor_id,healthcareprovider_id,location_id,patient_id) VALUES (3,'2021-05-29 14:41:29.102',NULL,1,0,19,2,7,22) ;
INSERT INTO public.medicalappointment (id,datetime,externalid,requeststatus,status,doctor_id,healthcareprovider_id,location_id,patient_id) VALUES (4,'2021-05-28 14:41:29.102',NULL,1,0,19,2,7,21) ;



--
-- Data for Name: medicalappointment_symptom; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (1,1);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (2,1);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (2,2);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (2,3);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (2,4);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (3,1);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (3,2);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (3,3);
INSERT INTO public.medicalappointment_symptom (medicalappointment_id,symptoms_id) VALUES (3,4);

--
-- Data for Name: resource_frontprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (1, 11);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (2, 18);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (3, 14);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (3, 13);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (4, 1);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (4, 17);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (7, 14);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (11, 16);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (11, 11);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (12, 7);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (12, 4);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (13, 4);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (16, 11);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (17, 18);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (17, 13);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (18, 1);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (18, 19);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (19, 16);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (19, 6);
INSERT INTO public.resource_frontprofile (subscriptions_id, subscribers_id) VALUES (19, 9);


--
-- Data for Name: resourceprovider; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.resourceprovider (commercialname,"token",id,address_id,lastrequeststatus_id) VALUES ('Farmacia de ejemplo','61c9efa2-62ba-4734-bc8e-ea64cb077b0f',4,2,NULL);
INSERT INTO public.resourceprovider (commercialname,"token",id,address_id,lastrequeststatus_id) VALUES ('Farmacia de ejemplo','57183438-69c2-4132-833c-b1eae5c47804',5,3,NULL);


--
-- Data for Name: openscheduleday; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (1,'22:00',0,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (2,'22:00',1,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (3,'22:00',2,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (4,'22:00',3,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (5,'22:00',4,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (6,'13:00',5,'10:00')
-- INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (7,'22:00',6,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (7,'18:00',0,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (8,'18:00',1,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (9,'18:00',2,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (10,'18:00',3,'10:00')
INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (11,'18:00',4,'10:00')
-- INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (13,'22:00',5,'10:00')
-- INSERT INTO public.openscheduleday (id,closinghour,dayofweek,openinghour) VALUES (14,'22:00',6,'10:00')



--
-- Data for Name: resourceprovider_openscheduleday; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,1);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,2);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,3);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,4);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,5);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,6);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (4,7);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,8);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,9);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,10);
INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,11);
-- INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,12);
-- INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,13);
-- INSERT INTO public.resourceprovider_openscheduleday (resourceprovider_id,openschedule_id) VALUES (5,14);



--
-- Data for Name: rssinformationsource_keywords; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'Covid');
INSERT INTO public.rssinformationsource_keywords (rssinformationsource_id, keywords) VALUES (1, 'coronavirus');


INSERT INTO public.notificationconfig (id,negativemedic,negativepatient,negativesupervisor,positivemedic,positivepatient,positivesupervisor,recoveredmedic,recoveredpatient,recoveredsupervisor,suspiciousmedic,suspiciouspatient,suspicioussupervisor) VALUES (1,true,true,true,true,true,true,true,true,true,true,true,true);
INSERT INTO public.contagiondistance (id,distance) VALUES (1,20);

SELECT pg_catalog.setval('public.config_seq', 999, false);



--
-- Data for Name: userprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
--    ACTUALIZAR LAS SEQ
--

--
-- Name: appointment_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_seq', 999, false);


--
-- Name: case_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.case_seq', 999, false);


--
-- Name: disease_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.disease_seq', 999, true);


--
-- Name: exam_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.exam_seq', 999, false);


--
-- Name: location_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.location_seq', 999, true);


--
-- Name: node_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.node_seq', 999, true);


--
-- Name: notification_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_seq', 999, false);


--
-- Name: resource_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.resource_seq', 999, true);


--
-- Name: resourceline_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.resourceline_seq', 999, true);


--
-- Name: schedule_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.schedule_seq', 999, true);


--
-- Name: source_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.source_seq', 999, true);


--
-- Name: sourcetype_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sourcetype_seq', 999, false);


--
-- Name: status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_seq', 999, true);


--
-- Name: symptom_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.symptom_seq', 999, true);


--
-- Name: userprofile_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.userprofile_seq', 999, true);
