Para que la persisencia funcione se debe:
    1. Crear una base de datos con el nombre "viruscontrol"
    2. Copiar a la carpeta {Wildfly_dir}/standalone/deployments los archivos "postgresql-42.2.5.jar" y "viruscontrol-ds.xml"
    3. Abrir el archivo "viruscontrol-ds.xml" y modificar a la contraseña de su usuario postgres
